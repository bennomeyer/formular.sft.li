<?
session_start();
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
if ((!$_SESSION['login_ok']) || (($_SESSION['film_id'] == "") && ($_SESSION['m'] == "u"))) {
	header("Location: login.php");
	exit;
}


$Filmtitel = "";
$zz_Synthese_Genre = "";
$Produktionsjahr = "";
$Dauer_Minuten = "";
$zz_Synthese_Bildformat = "";
$zz_Synthese_Buch = "";
$zz_Synthese_Darsteller = "";
$zz_Synthese_FarbeOderSW = "";
$zz_Synthese_Filmformat = "";
$zz_Synthese_Genre = "";
$zz_Synthese_Kamera = "";
$zz_Synthese_Montage = "";
$zz_Synthese_Musik = "";
$zz_Synthese_Premierentyp = "";
$zz_Synthese_Produktion = "";
$zz_Synthese_Produktionsland = "";
$zz_Synthese_Regie = "";
$zz_Synthese_Release = "";
$zz_Synthese_Sprache = "";
$zz_Synthese_Synopsis = "";
$zz_Synthese_Ton = "";
$zz_Synthese_Tonformat = "";
$zz_Synthese_Untertitel = "";
$zz_Synthese_Verleih = "";
$zz_Synthese_Weltrechte = "";
$zz_Synthese_Weltvertrieb = "";
$isan = "";
$zz_Anmeldung_Anmelderinfos = "";
$step = (isset($_POST['step_14'])) ? $_POST['step_14'] : "";
$direction = (isset($_POST['direction'])) ? $_POST['direction'] : "";
$target = (isset($_POST['target'])) ? $_POST['target'] : "";

require_once ('includes/db.inc.php');

// get Data out of DB
$q = FX_open_layout("cgi_Zusammenfassung", "999");
$q->AddDBParam('_kp__id', $_SESSION['film_id']);
$q->AddDBParam('_kf__Anmeldeuser_Id', $_SESSION['user_id']); 
$DBData = $q->FMFind();  
foreach ($DBData['data'] as $key => $value) {
	$Filmtitel = $value['Filmtitel'][0];
	$zz_Synthese_Genre = $value['zz_Synthese_Genre_'.$_SESSION['sprache']][0];
	$Produktionsjahr = $value['Produktionsjahr'][0];
	$Dauer_Minuten = $value['Dauer_Minuten'][0];
	$zz_Synthese_Bildformat = $value['zz_Synthese_Bildformat'][0];
	$zz_Synthese_Buch = $value['zz_Synthese_Buch'][0];
	$zz_Synthese_Darsteller = $value['zz_Synthese_Darsteller'][0];
	$zz_Synthese_FarbeOderSW = $value['zz_Synthese_FarbeOderSW_'.$_SESSION['sprache']][0];
	$zz_Synthese_Filmformat = $value['zz_Synthese_Filmformat'][0];
	$zz_Synthese_Genre = $value['zz_Synthese_Genre_'.$_SESSION['sprache']][0];
	$zz_Synthese_Kamera = $value['zz_Synthese_Kamera'][0];
	$zz_Synthese_Montage = $value['zz_Synthese_Montage'][0];
	$zz_Synthese_Musik = $value['zz_Synthese_Musik'][0];
	$zz_Synthese_Premierentyp = $value['zz_Synthese_Premierentyp_'.$_SESSION['sprache']][0];
	$zz_Synthese_Produktion = $value['zz_Synthese_Produktion'][0];
	$zz_Synthese_Produktionsland = $value['zz_Synthese_Produktionsland_'.$_SESSION['sprache']][0];
	$zz_Synthese_Regie = $value['zz_Synthese_Regie'][0];
	$zz_Synthese_Release = $value['zz_Synthese_Release_'.$_SESSION['sprache']][0];
	$zz_Synthese_Sprache = $value['zz_Synthese_Sprache_'.$_SESSION['sprache']][0];
	$zz_Synthese_Synopsis = $value['zz_Synthese_Synopsis'][0];
	$zz_Synthese_Ton = $value['zz_Synthese_Ton'][0];
	$zz_Synthese_Tonformat = $value['zz_Synthese_Tonformat'][0];
	$zz_Synthese_Untertitel = $value['zz_Synthese_Untertitel_'.$_SESSION['sprache']][0];
	$zz_Synthese_Verleih = $value['zz_Synthese_Verleih'][0];
	$zz_Synthese_Weltrechte = $value['zz_Synthese_Weltrechte'][0];
	$zz_Synthese_Weltvertrieb = $value['zz_Synthese_Weltvertrieb'][0];
	$isan = $value['ISAN'][0];
	$zz_Anmeldung_Anmelderinfos = $value['zz_Anmeldung_Anmelderinfos'][0];
}


if (($direction == "back") && ($step == "2")) {
	if ($target != "") {
		header("Location: /$target");
	} else {
		//redirect zur vorherigen Seite
		header("Location: /step08_13.php");
	}
	exit;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Online Registration</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <link href="/css/style2008.css" rel="stylesheet" type="text/css" title="KFT" />
 
 <script type="text/JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
function jumpto(target,direction) {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = direction;
	document.getElementById('target').value = target;
	document.form1.submit();
}
//-->
 </script>
 <script language="javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>
</head>
<body onload="MM_preloadImages('images/loader.gif')">

<div id="container">
  <div id="top">
    <? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
  </div>
	<br clear="all" />
<div id="navi"><? $seite = "14"; include($_SERVER['DOCUMENT_ROOT'].'/includes/navigation.inc.php'); ?><div id="loader" style="display:none"><img src="/images/loader.gif" style="margin-top: 8px; margin-left:45px" width="32" height="32" /></div></div>
  <div id="leftSide">
<fieldset>
<p class="legend"><?=$_SESSION['Leg_8']?> 14 / 14: <?=$_SESSION['Leg_133']?></p>

	<p><?=$_SESSION['Leg_113']?></p>
	<p><?=$_SESSION['Leg_119']?></p>
	<p><?=nl2br($_SESSION['Leg_135'])?></p>

		<?
		/*
		$pdf_de = ($_SESSION['sprache'] == "de") ? "http://www.solothurnerfilmtage.ch/download/23/page/1950_agb_sft43_de.pdf" : "";
		$pdf_en = ($_SESSION['sprache'] == "en") ? "http://www.solothurnerfilmtage.ch/download/23/page/1952_agb_sft43_en.pdf" : "";
		$pdf_fr = ($_SESSION['sprache'] == "fr") ? "http://www.solothurnerfilmtage.ch/download/23/page/1951_agb_sft43_fr.pdf" : "";
		$pdf_it = ($_SESSION['sprache'] == "it") ? "http://www.solothurnerfilmtage.ch/download/23/page/1951_agb_sft43_fr.pdf" : "";
		*/
		$pdf_de = ($_SESSION['sprache'] == "de") ? "http://www.solothurnerfilmtage.ch/download/23/page/1896_dl_1896_forum_reglement_43_08.pdf" : "";
		$pdf_en = ($_SESSION['sprache'] == "en") ? "http://www.solothurnerfilmtage.ch/download/23/page/1896_dl_1896_forum_reglement_43_08.pdf" : "";
		$pdf_fr = ($_SESSION['sprache'] == "fr") ? "http://www.solothurnerfilmtage.ch/download/23/page/1896_dl_1896_forum_reglement_43_08.pdf" : "";
		$pdf_it = ($_SESSION['sprache'] == "it") ? "http://www.solothurnerfilmtage.ch/download/23/page/1896_dl_1896_forum_reglement_43_08.pdf" : "";
		?>
		<p><?=$_SESSION['Leg_120']?><br />&nbsp;<br /></p>

	<div class="zusammenfassung_links"><?=$_SESSION['Leg_247']?></div>
	<div  class="div_blankbox" style="background:none;"><input type="button" name="print2" id="print2" value="<?=$_SESSION['Leg_116']?>" onclick="MM_openBrWindow('print_filminfo.php?f=<?=$_SESSION['film_id']?>','Drucken','resizable=yes,width=550,height=650')"/></div>
	<br clear="all" />
	
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_1']?></div>
	<div class="div_blankbox"><?=$Filmtitel?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_2']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Genre?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_3']?></div>
	<div class="div_blankbox"><?=$Produktionsjahr?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_33']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Produktionsland?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_4']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Sprache?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_5']?></div>
	<div class="div_blankbox"><?=$Dauer_Minuten?> <?=$_SESSION['Leg_6']?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_7']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_FarbeOderSW?></div>
	<br clear="all" />
	<hr size="2" noshade="noshade" style="color:#000000"/>
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_9']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Regie?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_262']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Buch?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_263']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Kamera?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_264']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Montage?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_265']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Ton?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_266']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Musik?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_267']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Darsteller?></div>
	<br clear="all" />
	<hr size="2" noshade="noshade" style="color:#000000"/>
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_10']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Produktion?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_11']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Weltrechte?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_252']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Weltvertrieb?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_12']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Verleih?></div>
	<br clear="all" />
	<hr size="2" noshade="noshade" style="color:#000000"/>
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_15']?></div>
	<div class="div_blankbox"><?=nl2br($zz_Synthese_Synopsis)?></div>
	<br clear="all" />
	<hr size="2" noshade="noshade" style="color:#000000"/>
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_68']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Filmformat?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_69']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Bildformat?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_70']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Tonformat?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_67']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Untertitel?></div>
	<br clear="all" />
	<hr size="2" noshade="noshade" style="color:#000000"/>
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_269']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Premierentyp?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_268']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Release?></div>
	<br clear="all" />
	<hr size="2" noshade="noshade" style="color:#000000"/>
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_28']?></div>
	<div class="div_blankbox">
	<?
	if (file_exists($_SERVER['DOCUMENT_ROOT']."/bilder/a_small/".$_SESSION['film_id'].".jpg")) echo '<img src="/bilder/a_small/'.$_SESSION['film_id'].'.jpg" style="margin-right:15px">';
	if (file_exists($_SERVER['DOCUMENT_ROOT']."/bilder/b_small/".$_SESSION['film_id'].".jpg")) echo '<img src="/bilder/b_small/'.$_SESSION['film_id'].'.jpg" style="margin-right:15px">';
	if (file_exists($_SERVER['DOCUMENT_ROOT']."/bilder/c_small/".$_SESSION['film_id'].".jpg")) echo '<img src="/bilder/c_small/'.$_SESSION['film_id'].'.jpg">';
	?></div>
	<br clear="all" />
	<hr size="2" noshade="noshade" style="color:#000000"/>
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_296']?></div>
	<div class="div_blankbox"><?=$isan?></div>
	<br clear="all" />
	<hr size="2" noshade="noshade" style="color:#000000"/>

	<div class="zusammenfassung_links"><?=$_SESSION['Leg_299']?></div>
	<div class="div_blankbox"><?=nl2br($zz_Anmeldung_Anmelderinfos)?></div>
	<br clear="all" />
	<hr size="2" noshade="noshade" style="color:#000000"/>

	<div style="width: 400px; float:left"><p><?=$_SESSION['Leg_117']?></p></div>
	<div style="float:left; vertical-align:bottom;"><p>&nbsp;<br /><input type="button" name="print2" id="print2" value="<?=$_SESSION['Leg_116']?>" onclick="MM_openBrWindow('print_filminfo.php?f=<?=$_SESSION['film_id']?>','Drucken','resizable=yes,width=550,height=650')"/></p></div>
	<br clear="all" />
		<table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td><p><?=$_SESSION['Leg_35']?></p></td>
            <td>_________________________</td>
          </tr>
          <tr>
            <td><p><?=$_SESSION['Leg_39']?></p></td>
            <td>_________________________</td>
          </tr>
          <tr>
            <td><p><?=$_SESSION['Leg_118']?></p></td>
            <td>_________________________</td>
          </tr>
        </table><br />&nbsp;<br />
		<input type="button" value="<?=$_SESSION['Leg_99']?>" onclick="parent.location.href='/films_overview.php'" /><br />&nbsp;<br />
	<br clear="all" />
<div class="clear"></div>
</fieldset>

  </div>
	<br clear="all" />
  <div class="clear"></div>
</div>

<form action="<?=$_SERVER['PHP_SELF']?>" method="post" name="form1" enctype="multipart/form-data">
<input type="hidden" name="step_14" value="2" />
<input type="hidden" name="direction" id="direction" value="" />
<input type="hidden" name="target" id="target" value="" />
</form>

<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>
</body>
</html>
