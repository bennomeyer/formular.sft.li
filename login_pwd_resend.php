<?
session_start();
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
$sprache = (isset($_GET['sprache'])) ? $_GET['sprache'] : "de";
$s = (isset($_POST['s'])) ? $_POST['s'] : "1";
$u = (isset($_POST['u'])) ? $_POST['u'] : "";
$foundrec = "";
$error = FALSE;

require_once ('includes/db.inc.php');

if ($s == "2") {
	$find_user =& $fm->newFindCommand('cgi_Anmeldeuser'); 
	$find_user->addFindCriterion('Mail', "==\"".$u."\""); 
	$result_user = $find_user->execute(); 
	if (!FileMaker::isError($result_user)) {
		$records = $result_user->getRecords(); 
		$foundrec = $result_user->getFoundSetCount();
		$record = $records[0];
		$vorname = $record->getField('Vorname');
		$name = $record->getField('Name');
		$email = $record->getField('Mail');
		$pwd = $record->getField('Passwort');
		
		require($_SERVER['DOCUMENT_ROOT'] . "/classes/phpmailer/class.phpmailer.php");
		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->SMTPDebug= false;
		$mail->Host= 'smtp.solothurnerfilmtage.ch';
		$mail->SMTPAuth = true;
		$mail->Port = 587;
		# $mail->SMTPSecure = 'ssl';
		$mail->Username = 'no-reply@solothurnerfilmtage.ch';
		$mail->Password = 'gramsci50';
		$mail->From     = "no-reply@solothurnerfilmtage.ch";
		$mail->FromName = "Solothurner Filmtage";
		$mail->Subject	= "Account-data Solothurn";
		$body = $_SESSION['Leg_231']." $vorname $name

".$_SESSION['Leg_232']."

".$_SESSION['Leg_222'].": $email
".$_SESSION['Leg_223'].": $pwd

".$_SESSION['Leg_233'];
		$mail->SetLanguage("en", $_SERVER['DOCUMENT_ROOT']."/classes/phpmailer/language/"); 
		$mail->Body    = utf8_decode($body);
		$mail->AddAddress($email);
		if(!$mail->Send()) echo "Ihre Anmeldung konnte nicht bearbeitet werden.<br>";
		$mail->ClearAddresses();
		$mail->ClearAttachments();
		$mail->SmtpClose(); 		
		
		
	} else {
		$error = TRUE;
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Online Registration</title>
<link href="/css/style2008.css" rel="stylesheet" type="text/css" title="KFT" />
</head>
<body>
<div id="container">
  <div id="top">
    <? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
  </div>
	<br clear="all" />
<div id="navi">
	<? if ($_SESSION['sprache'] == "de") { ?>
	<a href="http://www.solothurnerfilmtage.ch/filmanmeldung" target="_top"><?=$_SESSION['Leg_285']?></a>
	<? } ?>
	<? if ($_SESSION['sprache'] == "fr") { ?>
	<a href="http://www.journeesdesoleure.ch/inscription" target="_top"><?=$_SESSION['Leg_285']?></a>
	<? } ?>
	<? if ($_SESSION['sprache'] == "en") { ?>
	<a href="http://www.solothurnerfilmtage.ch/filmentry" target="_top"><?=$_SESSION['Leg_285']?></a>
	<? } ?>
	<? if ($_SESSION['sprache'] == "it") { ?>
	<a href="http://www.journeesdesoleure.ch/inscription" target="_top"><?=$_SESSION['Leg_285']?></a>
	<? } ?>
</div>
  <div id="leftSide">
<fieldset>

<? if (($s == "1") && (!$error)) { ?>

<legend><?=$_SESSION['Leg_224']?></legend>
<p><?=$_SESSION['Leg_234']?></p>

<form action="login_pwd_resend.php" method="post">
	<input type="hidden" name="s" value="2" />
	<label><?=$_SESSION['Leg_44']?></label>
	<div class="div_blankbox"><input type="text" name="u" style="width: 110px" class="textbox" /></div> <br clear="all" />
	<label>&nbsp;</label>
	<div class="div_blankbox" style="background:none">
	<input type="submit" value="<?=$_SESSION['Leg_235']?>"  class="link_button"/><br />
	</div>		
<br clear="all" />
</form>

<? } 
if (($s == "2") && (!$error)) { ?>
<legend><?=$_SESSION['Leg_224']?></legend>
<p><?=$_SESSION['Leg_236']?><br /><br />
<a href="login.php" class="link_button"><?=$_SESSION['Leg_237']?></a></p>

<? } 
if (($s == "2") && ($error)) { ?>
<legend><?=$_SESSION['Leg_224']?></legend>
<p><?=$_SESSION['Leg_238']?><br /><br />
<a href="login_register.php" class="link_button"><?=$_SESSION['Leg_226']?></a></p>

<? } ?>

<div class="clear"></div>
</fieldset>
  </div>
  <div class="clear"></div>
</div>
</body>
</html>
