<?
session_start();
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
if ((!$_SESSION['login_ok']) || (($_SESSION['film_id'] == "") && ($_SESSION['m'] == "u"))) {
	header("Location: /login.php");
	exit;
}

require_once (__DIR__.'/../includes/db.inc.php');
$q = FX_open_layout("cgi_h_02__filme", "1"); 

$step = (isset($_POST['step_11'])) ? $_POST['step_11'] : "";
$direction = (isset($_POST['direction'])) ? $_POST['direction'] : "";
$target = (isset($_POST['target'])) ? $_POST['target'] : "";
$Premiere = (isset($_POST['Premiere'])) ? $_POST['Premiere'] : "";
$PremiereMonat = (isset($_POST['PremiereMonat'])) ? $_POST['PremiereMonat'] : "";
$PremiereMonatID = (isset($_POST['PremiereMonat'])) ? explode("|",$_POST['PremiereMonat']) : "";
$PremiereJahr = (isset($_POST['PremiereJahr'])) ? $_POST['PremiereJahr'] : "";
$PremiereLand = (isset($_POST['PremiereLand'])) ? $_POST['PremiereLand'] : "";

$F_Jahr = (!empty($_POST['F_Jahr'])) ? $_POST['F_Jahr'] : array();
$F_Land = (!empty($_POST['F_Land'])) ? $_POST['F_Land'] : array();
$F_Txt = (!empty($_POST['F_Txt'])) ? $_POST['F_Txt'] : array();
$P_Jahr = (!empty($_POST['P_Jahr'])) ? $_POST['P_Jahr'] : array();
$P_Ort = (!empty($_POST['P_Ort'])) ? $_POST['P_Ort'] : array();
$P_Land = (!empty($_POST['P_Land'])) ? $_POST['P_Land'] : array();
$P_Txt = (!empty($_POST['P_Txt'])) ? $_POST['P_Txt'] : array();


// UPDATE init - alte Daten aus DB ziehen
//__________________________________________________

if (($_SESSION['m'] == "u") && ($step != "2")) {
	
	// Filmdaten holen
	$find =& $fm->newFindCommand('cgi_h_02__filme'); 
	$find->addFindCriterion('_kp__id', $_SESSION['film_id']); 
	$find->addFindCriterion('_kf__Anmeldeuser_Id', $_SESSION['user_id']); 
	$result = $find->execute(); 
	if (!FileMaker::isError($result)) {
		$records = $result->getRecords(); 
		$record = $records[0];
		$Premiere = $record->getField('_kf__Premierentyp');
		$PremiereMonat = $record->getField('_kf__Erstauffuehrung_Monat');	
		$PremiereJahr = $record->getField('_kf__Erstauffuehrung_Jahr');	
		$PremiereLand = $record->getField('_kf__Erstauffuehrung_Land');
		
		// Infos aus Kreuztabellen holen
		$relatedSet = $record->getRelatedSet('zz_Festivals');
		if (!FileMaker::isError($relatedSet)) {
			foreach ($relatedSet as $relatedRow) {
				$F_Jahr[] = $relatedRow->getField('zz_Festivals::Jahr');
				$F_Land[] = $relatedRow->getField('zz_Festivals::_kf__ISO_Land_Id').'|'.$relatedRow->getField('zz_Festival_Laenderliste_web::Land_'.$_SESSION['sprache']);
				$F_Txt[] = htmlspecialchars($relatedRow->getField('zz_Festivals::Festival'));
			}
		}
		$relatedSet = $record->getRelatedSet('zz_Preise');
		if (!FileMaker::isError($relatedSet)) {
			foreach ($relatedSet as $relatedRow) {
				$P_Jahr[] = $relatedRow->getField('zz_Preise::Jahr');
				$P_Ort[] = htmlspecialchars($relatedRow->getField('zz_Preise::Ort'));
				$P_Txt[] = htmlspecialchars($relatedRow->getField('zz_Preise::Preis'));
				$P_Land[] = $relatedRow->getField('zz_Preise::_kf__ISO_Land_Id').'|'.$relatedRow->getField('zz_Preise_Laenderliste_web::Land_'.$_SESSION['sprache']);
			}
		}
	}
}


// ERROR-Handling
//__________________________________________________
$error = "";
if ($step == "2") {
	if (empty($_POST['Premiere'])) $error .= $_SESSION['Leg_199'];
	if (($Premiere != "3") && (($PremiereMonat == "") || ($PremiereJahr == "") || ($PremiereLand == ""))) $error .= $_SESSION['Leg_275'];
	
} 

if (($step == "2") && ($error != "")) {
	// Update Seite2Flag mit 0
	$q->AddDBParam('-recid', $_SESSION['record_id']); 
	$q->AddDBParam('zz_Anmeldung_Seite11_Flag', "0");
	$DBData = $q->FMEdit(); 	
} elseif (($step == "2") && ($error == "")) {
	// Update Seite2Flag mit 1
	$q->AddDBParam('-recid', $_SESSION['record_id']); 
	$q->AddDBParam('zz_Anmeldung_Seite11_Flag', "1");
	$DBData = $q->FMEdit(); 	
}


// UPDATE final - Daten aktualisieren
//__________________________________________________

if ((($_SESSION['m'] == "u") && ($step == "2") && ($error == "")) || (($_SESSION['m'] == "u") && ($step == "2") && ($direction == "back"))) {
	$q = FX_open_layout("cgi_h_02__filme", "1"); 
	$q->AddDBParam('-recid', $_SESSION['record_id']); 
	$q->AddDBParam('_kf__Premierentyp', $Premiere);
	$q->AddDBParam('_kf__Erstauffuehrung_Monat', $PremiereMonatID[0]);
	$q->AddDBParam('_kf__Erstauffuehrung_Jahr', $PremiereJahr);
	$q->AddDBParam('_kf__Erstauffuehrung_Land', $PremiereLand);
	$DBData = $q->FMEdit(); 
	
	// Alle Infos aus den Kreuztabellen löschen
	if ($_SESSION['film_id'] != "") {
		$find =& $fm->newFindCommand('cgi_k_30__filme_festivals'); 
		$find->addFindCriterion('_kf__Film_Id', $_SESSION['film_id']); 
		$result = $find->execute(); 
		if (!FileMaker::isError($result)) {
			$records = $result->getRecords(); 
			foreach ($records as $record) {
				$rec_ID = $record->getRecordId();
				$rec = $fm->getRecordById('cgi_k_30__filme_festivals', $rec_ID); 
				$rec->delete();
			}
		}
		$find =& $fm->newFindCommand('cgi_k_32__filme_preise'); 
		$find->addFindCriterion('_kf__Film_Id', $_SESSION['film_id']); 
		$result = $find->execute(); 
		if (!FileMaker::isError($result)) {
			$records = $result->getRecords(); 
			foreach ($records as $record) {
				$rec_ID = $record->getRecordId();
				$rec = $fm->getRecordById('cgi_k_32__filme_preise', $rec_ID); 
				$rec->delete();
			}
		}
	}
	if (isset($F_Jahr[0])) {
		$i = 0;
		foreach ($F_Jahr as $F_Jahr_zahl) { 
			$f_land_code = explode("|", $F_Land[$i]);
			
			// Festivaleintrag
			$q = FX_open_layout("cgi_h_31__festivals", "1"); 
			$q->AddDBParam('Jahr', $F_Jahr[$i]);
			$q->AddDBParam('_kf__ISO_Land_Id', $f_land_code[0]);	
			$q->AddDBParam('Festival', htmlspecialchars_decode($F_Txt[$i]));	
			$DBData = $q->FMNew();
			foreach ($DBData['data'] as $key => $value) {
				$festival_id = $value['_kp__id'][0];
			}
			
			// Kreuztabelleneintrag
			$q = FX_open_layout( "cgi_k_30__filme_festivals", "1"); 
			$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);	
			$q->AddDBParam('_kf__Festival_Id', $festival_id);	
			$q->AddDBParam('_kf__ISO_Land_Id', $f_land_code[0]);	
			$DBData = $q->FMNew();
			$i++;		
		}
	}				
	// Sprache-Film Kreuztabelle
	if (isset($P_Jahr[0])) {
		$i = 0;
		foreach ($P_Jahr as $P_Jahr_zahl) { 
			$p_land_code = explode("|", $P_Land[$i]);
			
			// Preiseintrag
			$q = FX_open_layout("cgi_h_33__preise", "1"); 
			$q->AddDBParam('Jahr', $P_Jahr[$i]);
			$q->AddDBParam('_kf__ISO_Land_Id', $p_land_code[0]);	
			$q->AddDBParam('Preis', htmlspecialchars_decode($P_Txt[$i]));	
			$q->AddDBParam('Ort', htmlspecialchars_decode($P_Ort[$i]));	
			$DBData = $q->FMNew();
			foreach ($DBData['data'] as $key => $value) {
				$preis_id = $value['_kp__id'][0];
			}
			
			// Kreuztabelleneintrag
			$q = FX_open_layout( "cgi_k_32__filme_preise", "1"); 
			$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);	
			$q->AddDBParam('_kf__Preis_Id', $preis_id);	
			$q->AddDBParam('_kf__ISO_Land_Id', $p_land_code[0]);	
			$DBData = $q->FMNew();
			$i++;		
		}
	}		
}



// DB-Actions für Neueintragung
//__________________________________________________
if (($error == "") && ($step == "2") && ($_SESSION['m'] != "u")) {
	
	if (isset($F_Jahr[0])) {
		$i = 0;
		foreach ($F_Jahr as $F_Jahr_zahl) { 
			$f_land_code = explode("|", $F_Land[$i]);
			
			// Festivaleintrag
			$q = FX_open_layout("cgi_h_31__festivals", "1"); 
			$q->AddDBParam('Jahr', $F_Jahr[$i]);
			$q->AddDBParam('_kf__ISO_Land_Id', $f_land_code[0]);	
			$q->AddDBParam('Festival', $F_Txt[$i]);	
			$DBData = $q->FMNew();
			foreach ($DBData['data'] as $key => $value) {
				$festival_id = $value['_kp__id'][0];
			}
			
			// Kreuztabelleneintrag
			$q = FX_open_layout("cgi_k_30__filme_festivals", "1"); 
			$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);	
			$q->AddDBParam('_kf__Festival_Id', $festival_id);	
			$q->AddDBParam('_kf__ISO_Land_Id', $f_land_code[0]);	
			$DBData = $q->FMNew();
			$i++;		
		}
	}	
	if (isset($P_Jahr[0])) {
		$i = 0;
		foreach ($P_Jahr as $P_Jahr_zahl) { 
			$p_land_code = explode("|", $P_Land[$i]);
			
			// Preiseintrag
			$q = FX_open_layout("cgi_h_33__preise", "1"); 
			$q->AddDBParam('Jahr', $P_Jahr[$i]);
			$q->AddDBParam('_kf__ISO_Land_Id', $p_land_code[0]);	
			$q->AddDBParam('Preis', $P_Txt[$i]);	
			$q->AddDBParam('Ort', $P_Ort[$i]);	
			$DBData = $q->FMNew();
			foreach ($DBData['data'] as $key => $value) {
				$preis_id = $value['_kp__id'][0];
			}
			
			// Kreuztabelleneintrag
			$q = FX_open_layout( "cgi_k_32__filme_preise", "1"); 
			$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);	
			$q->AddDBParam('_kf__Preis_Id', $preis_id);	
			$q->AddDBParam('_kf__ISO_Land_Id', $p_land_code[0]);	
			$DBData = $q->FMNew();
			$i++;		
		}
		
	}	
	if ($Premiere != "") {
		$q = FX_open_layout("cgi_h_02__filme", "1"); 
		$q->AddDBParam('-recid', $_SESSION['record_id']); 
		$q->AddDBParam('_kf__Premierentyp', $Premiere);
		$q->AddDBParam('_kf__Erstauffuehrung_Monat', $PremiereMonatID[0]);
		$q->AddDBParam('_kf__Erstauffuehrung_Jahr', $PremiereJahr);
		$q->AddDBParam('_kf__Erstauffuehrung_Land', $PremiereLand);
		$DBData = $q->FMEdit(); 		
	}
}


if (($direction == "next") && ($step == "2") && ($error == "")) {
	if ($target != "") {
		header("Location: /$target");
	} else {
		//redirect zur nächsten Seite
		header("Location: /sas/step08_12.php");
	}
	exit;
}
if (($direction == "back") && ($step == "2")) {
	if ($target != "") {
		header("Location: /$target");
	} else {
		//redirect zur vorherigen Seite
		header("Location: /sas/step08_10.php");
	}
	exit;
}
include($_SERVER['DOCUMENT_ROOT']. "/includes/x_get_value_list10.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Online Registration</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <link href="/css/style2008.css" rel="stylesheet" type="text/css" title="KFT" />
 
<script type="text/javascript" src="/includes/scripts.js"></script>
<script type="text/javascript" language="javascript">
<!--
	var ol_width = 300;
	var ol_fgcolor='#d9e2ec';
	var ol_bgcolor='#006699';
//-->
</script>
<link rel="stylesheet" type="text/css" href="/includes/yui/container/assets/container.css" />
<script type="text/javascript" src="/includes/yui/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript" src="/includes/yui/animation/animation-min.js"></script>
<script type="text/javascript" src="/includes/yui/dragdrop/dragdrop-min.js"></script>
<script type="text/javascript" src="/includes/yui/connection/connection.js"></script>
<script type="text/javascript" src="/includes/yui/container/container.js"></script>
<script language="javascript">

function htmlspecialchars(str,typ) {
  if(typeof str=="undefined") str="";
  if(typeof typ!="number") typ=2;
  typ=Math.max(0,Math.min(3,parseInt(typ)));
  var from=new Array(/&/g,/</g,/>/g);
  var to=new Array("&amp;","&lt;","&gt;");
  if(typ==1 || typ==3) {from.push(/'/g); to.push("&#039;");}
  if(typ==2 || typ==3) {from.push(/"/g); to.push("&quot;");}
  for(var i in from) str=str.replace(from[i],to[i]);
  return str;
}

// ###### Festivals #######

var f_jahr = new Array;
var f_land = new Array;
var f_txt = new Array;

function addFestival() {
	if 	(
		(document.getElementById('f_jahr_select').value != "") &&
		(document.getElementById('f_land_select').value != "") &&
		(document.getElementById('f_txt').value != "")
		) {
		
		f_jahr.push(document.getElementById('f_jahr_select').value);
		document.getElementById('f_jahr_select').value = "";
		f_land.push(document.getElementById('f_land_select').value);
		document.getElementById('f_land_select').value = "";
		f_txt.push(document.getElementById('f_txt').value);
		document.getElementById('f_txt').value = "";
		showFestival();		
	} else {
		miniDialog = new YAHOO.widget.SimpleDialog("mini_dlg", { visible:false, width: "25em", effect:[
{effect:YAHOO.widget.ContainerEffect.SLIDE,duration:0.25}, {effect:YAHOO.widget.ContainerEffect.FADE,duration:0.25}
], fixedcenter : true, modal:true, draggable:false });
		miniDialog.setHeader("<?=$_SESSION['Leg_91']?>");
		miniDialog.setBody("<?=$_SESSION['Leg_95']?>");
		miniDialog.cfg.queueProperty("icon",YAHOO.widget.SimpleDialog.ICON_WARN);
		miniDialog.cfg.queueProperty("buttons", [ 
			{ text: "<?=$_SESSION['Leg_93']?>", handler:handlerHide, isDefault:true  }
		]);
		miniDialog.render(document.body);
		miniDialog.show();
	}
}
function delFestival(i) {
	f_jahr.splice(i,1);
	f_land.splice(i,1);
	f_txt.splice(i,1);
	showFestival();	
}
function showFestival() {
	var output = "";
	for (var i = 0; i < f_jahr.length; ++i) {
		var land = f_land[i].split("|");
 		output += "<div id=\"list_values\"><input type=\"hidden\" name=\"F_Jahr[]\" value=\"" + f_jahr[i] + "\">\n";
 		output += "<input type=\"hidden\" name=\"F_Land[]\" value=\"" + f_land[i] + "\">\n";
		output += "<input type=\"hidden\" name=\"F_Txt[]\" value=\"" + htmlspecialchars(f_txt[i]) + "\">\n";		
		output += f_jahr[i] + "<br>" + land[1] + "<br />" + f_txt[i] + "<br />&nbsp;<br />\n";
 		output += "</div><div id=\"list_delicons\"><img src=\"/images/delete.png\" onClick=\"delFestival(" + i + ")\" align=\"absmiddle\" title=\"<?=$_SESSION['Leg_88']?>\"></div><br clear \"all\" />\n";
	}
	document.getElementById('Festivals').innerHTML = "<p>"+ output +"</p>";
	document.getElementById('Festivals').style.display = "block";
}


// ###### Preise #######

var p_jahr = new Array;
var p_land = new Array;
var p_ort = new Array;
var p_txt = new Array;

function addPreis() {
	if 	(
		(document.getElementById('p_jahr_select').value != "") &&
		(document.getElementById('p_land_select').value != "") &&
		(document.getElementById('p_ort').value != "") &&
		(document.getElementById('p_txt').value != "")
		) {
		
		p_jahr.push(document.getElementById('p_jahr_select').value);
		document.getElementById('p_jahr_select').value = "";
		p_land.push(document.getElementById('p_land_select').value);
		document.getElementById('p_land_select').value = "";
		p_ort.push(document.getElementById('p_ort').value);
		document.getElementById('p_ort').value = "";
		p_txt.push(document.getElementById('p_txt').value);
		document.getElementById('p_txt').value = "";
		showPreis();		
	} else {
		miniDialog = new YAHOO.widget.SimpleDialog("mini_dlg", { visible:false, width: "25em", effect:[
{effect:YAHOO.widget.ContainerEffect.SLIDE,duration:0.25}, {effect:YAHOO.widget.ContainerEffect.FADE,duration:0.25}
], fixedcenter : true, modal:true, draggable:false });
		miniDialog.setHeader("<?=$_SESSION['Leg_91']?>");
		miniDialog.setBody("<?=$_SESSION['Leg_95']?>");
		miniDialog.cfg.queueProperty("icon",YAHOO.widget.SimpleDialog.ICON_WARN);
		miniDialog.cfg.queueProperty("buttons", [ 
			{ text: "<?=$_SESSION['Leg_93']?>", handler:handlerHide, isDefault:true  }
		]);
		miniDialog.render(document.body);
		miniDialog.show();
	}
}
function delPreis(i) {
	p_jahr.splice(i,1);
	p_land.splice(i,1);
	p_txt.splice(i,1);
	p_ort.splice(i,1);
	showPreis();	
}
function showPreis() {
	var output = "";
	for (var i = 0; i < p_jahr.length; ++i) {
		var land2 = p_land[i].split("|");
 		output += "<div id=\"list_values\"><input type=\"hidden\" name=\"P_Jahr[]\" value=\"" + p_jahr[i] + "\">\n";
 		output += "<input type=\"hidden\" name=\"P_Land[]\" value=\"" + p_land[i] + "\">\n";
 		output += "<input type=\"hidden\" name=\"P_Ort[]\" value=\"" + htmlspecialchars(p_ort[i]) + "\">\n";
		output += "<input type=\"hidden\" name=\"P_Txt[]\" value=\"" + htmlspecialchars(p_txt[i]) + "\">\n";		
		output += p_jahr[i] + " | " + p_ort[i] + "<br />" + land2[1] + "<br />" + p_txt[i] + "<br />&nbsp;<br />\n";
 		output += "</div><div id=\"list_delicons\"><img src=\"/images/delete.png\" onClick=\"delPreis(" + i + ")\" align=\"absmiddle\" title=\"<?=$_SESSION['Leg_88']?>\"></div><br clear \"all\" />\n";
	}
	document.getElementById('Preise').innerHTML = "<p>"+ output +"</p>";
	document.getElementById('Preise').style.display = "block";
}


function goBack() {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv_sas']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = "back";
	document.form1.submit();
}
function goNext() {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv_sas']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = "next";
	document.form1.submit();
}
function jumpto(target,direction) {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv_sas']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = direction;
	document.getElementById('target').value = target;
	document.form1.submit();
}
</script> 
<script language="javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>
</head>
<body onload="MM_preloadImages('/images/loader.gif')">


<div id="container">
  <div id="top">
    <? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
  </div>
	<br clear="all" />
<div id="navi"><? $seite = "11"; include($_SERVER['DOCUMENT_ROOT'].'/includes/navigation_sas.inc.php'); ?><div id="loader" style="display:none"><img src="/images/loader.gif" style="margin-top: 8px; margin-left:45px" width="32" height="32" /></div></div>
  <div id="leftSide">
<fieldset>
<p class="legend"><?=$_SESSION['Leg_8']?> 11 / 13: <?=$_SESSION['Leg_130']?></p>
	<? if (($error != "") && ($step == "2")) { ?>	
	<p style="border:1px solid #990000; background-color:#FFDCD6; padding:5px; width: 545px"><?=$error; ?></p>
	<? } ?>
	<? echo ($_SESSION['Leg_110'] != "") ? '<p class="pagenote">'.$_SESSION['Leg_110'].'</p>' : ""; ?>
<form action="<?=$_SERVER['PHP_SELF']?>" method="post" name="form1">
<input type="hidden" value="2" name="step_11" />
<input type="hidden" name="direction" id="direction" value="" />
<input type="hidden" name="target" id="target" value="" />

	
	<label for="name"><?=$_SESSION['Leg_159']?>*</label>
	<div class="div_blankbox">
		<p><?=$_SESSION['Leg_115']?></p>
		<div style="float:left">
			<select class="post" name="Premiere" id="Premiere" style="border: 1px solid #666666; font-size:11px;">
			<option value="">-</option>
			<?=$output_Premiere?>
			</select>
			<p><?=$_SESSION['Leg_259']?></p>
			<div style="float:left;width: 60px"><?=$_SESSION['Leg_258']?></div>
			<div style="float:left;width: 220px">
			<select class="post" name="PremiereMonat" id="PremiereMonat" style="border: 1px solid #666666; font-size:11px;">
			<option value="">-</option>
			<?=$output_PremiereMonat?>
			</select></div>
			<br clear="all" />
			<div style="float:left;width: 60px"><?=$_SESSION['Leg_56']?></div>
			<div style="float:left;width: 220px">
			<select class="post" name="PremiereJahr" id="PremiereJahr" style="border: 1px solid #666666; font-size:11px;">
			<option value="">-</option>
			<?=$output_PremiereJahr?>
			</select>
			</div>
			<br clear="all" />
			<div style="float:left;width: 60px"><?=$_SESSION['Leg_40']?></div>
			<div style="float:left;width: 220px">
			<select class="post" name="PremiereLand" id="PremiereLand" style="border: 1px solid #666666; font-size:11px;">
			<option value="">-</option>
			<?=$output_PremiereLand?>
			</select></div>
			<br clear="all" />
		</div>
	</div>
	<br clear="all" />
	<label for="name"><?=$_SESSION['Leg_20']?></label>
	<div class="div_blankbox">
		<p><?=$_SESSION['Leg_21']?></p>
		<div id="select_div" style="float:left; width: 210px;">
			<?=$_SESSION['Leg_56']?><br />
			<select class="post" name="f_jahr_select" id="f_jahr_select" style="border: 1px solid #666666; font-size:11px;">
			<option value="">-</option>
			<?=$output_Jahr?>
			</select><br />
			<?=$_SESSION['Leg_40']?><br />
			<select class="post" name="f_land_select" id="f_land_select" style="border: 1px solid #666666; font-size:11px;">
			<option value="">-</option>
			<?=$output_Land?>
			</select><br />
			<?=$_SESSION['Leg_55']?><br />
			<input type="text" name="f_txt" id="f_txt" class="textbox_medium2" /> <img src="/images/arrow.png" alt="<?=$_SESSION['Leg_78']?>" align="absmiddle" onclick="addFestival()" title="<?=$_SESSION['Leg_78']?>" /><br />
		</div>
		<div id="Festivals" class="list_div_high"> </div>
		<br clear="all" />
	</div>
	<br clear="all" />
	<label for="name"><?=$_SESSION['Leg_22']?></label>
	<div class="div_blankbox">
		<p><?=$_SESSION['Leg_23']?></p>
		<div style="float:left"><?=$_SESSION['Leg_56']?><br />
			<select class="post" name="p_jahr_select" id="p_jahr_select" style="border: 1px solid #666666; font-size:11px;">
			<option value="">-</option>
			<?=$output_Jahr?>
			</select><br />
			<?=$_SESSION['Leg_40']?><br />
			<select class="post" name="p_land_select" id="p_land_select" style="border: 1px solid #666666; font-size:11px;">
			<option value="">-</option>
			<?=$output_Land?>
			</select>
			<br />
			<?=$_SESSION['Leg_57']?><br />
			<input type="text" name="p_txt" id="p_txt" class="textbox_medium2" /><br />
			<?=$_SESSION['Leg_39']?><br />
			<input type="text" name="p_txt" id="p_ort" class="textbox_medium2" /> <img src="/images/arrow.png" alt="<?=$_SESSION['Leg_78']?>" align="absmiddle" onclick="addPreis()" title="<?=$_SESSION['Leg_78']?>" /><br />
		</div>
		<div id="Preise" class="list_div_high"></div>
	</div>
	<br clear="all" />
	<div class="prevBtn"><input type="button" id="back" value="<?=$_SESSION['Leg_31']?>" onclick="goBack('')" /></div>
	<div class="nxtBtn"><input type="button" id="next" value="<?=$_SESSION['Leg_32']?>" onclick="goNext('')" /></div>
	<br clear="all" />
</form>
<div class="clear"></div>
</fieldset>

  </div>
	<br clear="all" />
  <div class="clear"></div>
</div>


<? if ((($step == "2") && isset($F_Jahr[0])) || (($_SESSION['m'] == "u") && isset($F_Jahr[0]))) {
echo '<script language="javascript">'."\n";
echo 'f_jahr.push("'.implode("\",\"", $F_Jahr).'");'."\n";
echo 'f_land.push("'.implode("\",\"", $F_Land).'");'."\n";
echo 'f_txt.push("'.implode("\",\"", $F_Txt).'");'."\n";
echo 'showFestival();'."\n";
echo '</script>'."\n";
}
?>
<? if ((($step == "2") && isset($P_Jahr[0])) || (($_SESSION['m'] == "u") && isset($P_Jahr[0]))) {
echo '<script language="javascript">'."\n";
echo 'p_jahr.push("'.implode("\",\"", $P_Jahr).'");'."\n";
echo 'p_ort.push("'.implode("\",\"", $P_Ort).'");'."\n";
echo 'p_land.push("'.implode("\",\"", $P_Land).'");'."\n";
echo 'p_txt.push("'.implode("\",\"", $P_Txt).'");'."\n";
echo 'showPreis();'."\n";
echo '</script>'."\n";
}
?>
<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>
</body>
</html>
