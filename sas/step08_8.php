<?
session_start();
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
if ((!$_SESSION['login_ok']) || (($_SESSION['film_id'] == "") && ($_SESSION['m'] == "u"))) {
	header("Location: login.php");
	exit;
}

require_once (__DIR__.'/../includes/db.inc.php');
$q = FX_open_layout("cgi_h_02__filme", "1"); 


$step = (isset($_POST['step_8'])) ? $_POST['step_8'] : "";
$direction = (isset($_POST['direction'])) ? $_POST['direction'] : "";
$target = (isset($_POST['target'])) ? $_POST['target'] : "";
$B_Bandname = (!empty($_POST['b_bandname'])) ? $_POST['b_bandname'] : "";
$B_Bandmitglieder = (!empty($_POST['b_bandmitglieder'])) ? $_POST['b_bandmitglieder'] : "";
$B_Musikstil = (!empty($_POST['b_musikstil'])) ? $_POST['b_musikstil'] : "";
$B_Musikstil_Sonst = (!empty($_POST['b_musikstil_sonst'])) ? $_POST['b_musikstil_sonst'] : "";
$B_Diskographie = (!empty($_POST['b_diskographie'])) ? $_POST['b_diskographie'] : "";


// UPDATE init - alte Daten aus DB ziehen
//__________________________________________________

if (($_SESSION['m'] == "u") && ($step != "2")) {
	
	// Filmdaten holen
	$find =& $fm->newFindCommand('cgi_h_02__filme'); 
	$find->addFindCriterion('_kp__id', $_SESSION['film_id']); 
	$find->addFindCriterion('_kf__Anmeldeuser_Id', $_SESSION['user_id']); 
	$result = $find->execute(); 
	if (!FileMaker::isError($result)) {
		$records = $result->getRecords(); 
		$record = $records[0];
		$_SESSION['record_id'] = $record->getField('_record__id');
		$B_Bandname = $record->getField('Band_Name');
		$B_Bandmitglieder = $record->getField('Band_Mitglieder');
		$B_Musikstil = $record->getField('_kf__Band_musikstilID');
		$B_Musikstil_Sonst = $record->getField('Band_Musikstil');
		$B_Diskographie = $record->getField('Band_Diskographie');
	}
}

// ERROR-Handling
//__________________________________________________
$error = "";
if (($B_Bandname == "") || ($B_Bandmitglieder == "") || ($B_Musikstil == "") || ($B_Diskographie == "") || (($B_Musikstil == "14")&&($B_Musikstil_Sonst == ""))) {
	$error .= $_SESSION['Leg_141'];
}

if (($step == "2") && ($error != "")) {
	// Update Seite2Flag mit 0
	$q->AddDBParam('-recid', $_SESSION['record_id']); 
	$q->AddDBParam('zz_Anmeldung_Seite08_Flag', "0");
	$DBData = $q->FMEdit(); 	
} elseif (($step == "2") && ($error == "")) {
	// Update Seite2Flag mit 1
	$q->AddDBParam('-recid', $_SESSION['record_id']); 
	$q->AddDBParam('zz_Anmeldung_Seite08_Flag', "1");
	$DBData = $q->FMEdit(); 	
}


// UPDATE final & Neueintrag - Daten aktualisieren
//__________________________________________________
if ((($error == "") && ($step == "2") && ($direction == "next")) || (($step == "2") && ($direction == "back"))) {

	$q = FX_open_layout("cgi_h_02__filme", "1");
	$q->AddDBParam('-recid', $_SESSION['record_id']); 
	$q->AddDBParam('Band_Name', $B_Bandname);
	$q->AddDBParam('Band_Mitglieder', $B_Bandmitglieder);
	$q->AddDBParam('_kf__Band_MusikstilID', $B_Musikstil);
	$q->AddDBParam('Band_Musikstil', $B_Musikstil_Sonst);
	$q->AddDBParam('Band_Diskographie', $B_Diskographie);
	$DBData = $q->FMEdit();	
}


if (($direction == "next") && ($step == "2") && ($error == "")) {
	if ($target != "") {
		header("Location: /$target");
	} else {
		//redirect zur nächsten Seite
		header("Location: /sas/step08_9.php");
	}
	exit;
}
if (($direction == "back") && ($step == "2")) {
	if ($target != "") {
		header("Location: /$target");
	} else {
		//redirect zur vorherigen Seite
		header("Location: /sas/step08_7.php");
	}
	exit;
}

include($_SERVER['DOCUMENT_ROOT']. "/includes/x_get_value_list_sas8.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Online Registration</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <link href="/css/style2008.css" rel="stylesheet" type="text/css" title="KFT" />
<script language="javascript">
function goBack() {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv_sas']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = "back";
	document.form1.submit();
}
function goNext() {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv_sas']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = "next";
	document.form1.submit();
}
function jumpto(target,direction) {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv_sas']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = direction;
	document.getElementById('target').value = target;
	document.form1.submit();
}
</script>
<script language="javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>
</head>
<body onload="MM_preloadImages('/images/loader.gif')">


<div id="container">
  <div id="top">
    <? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
  </div>
	<br clear="all" />
<div id="navi"><? $seite = "8"; include($_SERVER['DOCUMENT_ROOT'].'/includes/navigation_sas.inc.php'); ?><div id="loader" style="display:none"><img src="/images/loader.gif" style="margin-top: 8px; margin-left:45px" width="32" height="32" /></div></div>
  <div id="leftSide">
<fieldset>
<legend><?=$_SESSION['Leg_8']?> 8 / 13: <?=$_SESSION['Leg_140']?></legend>
	<? echo ($_SESSION['Leg_141'] != "") ? '<p class="pagenote">'.$_SESSION['Leg_141'].'</p>' : ""; ?>
	<? if (($step == "2") && ($error != "")) { ?>	
	<p style="border:1px solid #990000; background-color:#FFDCD6; padding:5px; width: 545px"><?=$error; ?></p>
	<? } ?>
<form action="<?=$_SERVER['PHP_SELF']?>" method="post" name="form1">
<input type="hidden" value="2" name="step_8" />
<input type="hidden" name="direction" id="direction" value="" />
<input type="hidden" name="target" id="target" value="" />
	<!-- p><?=$_SESSION['Leg_73']?></p -->

	<label for="name"><?=$_SESSION['Leg_139']?></label>
	<div class="div_blankbox">
			<?=$_SESSION['Leg_62']?><br />
			<input type="text" name="b_bandname" id="b_bandname" class="textbox" value="<?=$B_Bandname?>" /><br />&nbsp;<br />
			<?=$_SESSION['Leg_63']?><br />
			<textarea name="b_bandmitglieder" id="b_bandmitglieder" class="textbox"><?=$B_Bandmitglieder?></textarea><br />&nbsp;<br />
			<?=$_SESSION['Leg_64']?><br />
			<select name="b_musikstil" id="b_musikstil">
			<option value="">-</option>
			<?=$output_Musikstil?>
			</select><br />&nbsp;<br />
			<?=$_SESSION['Leg_146']?><br />
			<input type="text" name="b_musikstil_sonst" id="b_musikstil_sonst" class="textbox" value="<?=$B_Musikstil_Sonst?>"/><br />&nbsp;<br />
			<?=$_SESSION['Leg_65']?><br />
			<textarea name="b_diskographie" id="b_diskographie" class="textbox"><?=$B_Diskographie?></textarea>
		<br clear="all" />
	</div>
	<br clear="all" />

	<br clear="all" />
	<div class="prevBtn"><input type="button" id="back" value="<?=$_SESSION['Leg_31']?>" onclick="goBack('')" /></div>
	<div class="nxtBtn"><input type="button" id="next" value="<?=$_SESSION['Leg_32']?>" onclick="goNext('')" /></div>
	<br clear="all" />
</form>
<div class="clear"></div>
</fieldset>

  </div>
	<br clear="all" />
  <div class="clear"></div>
</div>


<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>

</body>
</html>
