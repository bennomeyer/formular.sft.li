<?
session_start();
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
if ((!$_SESSION['login_ok']) || (($_SESSION['film_id'] == "") && ($_SESSION['m'] == "u"))) {
	header("Location: /login.php");
	exit;
}

require_once (__DIR__.'/../includes/db.inc.php');
$q = FX_open_layout("cgi_h_02__filme", "1"); 

$step = (isset($_POST['step4'])) ? $_POST['step4'] : "";
$direction = (isset($_POST['direction'])) ? $_POST['direction'] : "";
$target = (isset($_POST['target'])) ? $_POST['target'] : "";


/*
$hasEntry = (isset($_POST['hasEntry'])) ? $_POST['hasEntry'] : "";

$error = "";
if ($step == "2") {
	$error .= ($hasEntry == "") ? "Bitte geben Sie mindestens eine Firma an.<br />" : "";
} 

if (($step == "2") && ($error == "")) {

	// redirect zur nächsten Seite
	header("Location: /step4.php");
	exit;
}
*/
if ($step == "2") {
	// Update Seite2Flag mit 1
	$q->AddDBParam('-recid', $_SESSION['record_id']); 
	$q->AddDBParam('zz_Anmeldung_Seite04_Flag', "1");
	$DBData = $q->FMEdit(); 	
}

if (($direction == "next") && ($step == "2") && ($error == "")) {
	if ($target != "") {
		header("Location: /$target");
	} else {
		//redirect zur nächsten Seite
		header("Location: /sas/step08_5.php");
	}
	exit;
}
if (($direction == "back") && ($step == "2")) {
	if ($target != "") {
		header("Location: /$target");
	} else {
		//redirect zur vorherigen Seite
		header("Location: /sas/step08_3.php");
	}
	exit;
}

include($_SERVER['DOCUMENT_ROOT']. "/includes/x_get_value_list2.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Online Registration</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <link href="/css/style2008.css" rel="stylesheet" type="text/css" title="KFT" />
<script type="text/javascript" src="/includes/scripts.js"></script>
<link rel="stylesheet" type="text/css" href="/includes/yui/container/assets/container.css" />
<script type="text/javascript" src="/includes/yui/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript" src="/includes/yui/animation/animation-min.js"></script>
<script type="text/javascript" src="/includes/yui/dragdrop/dragdrop-min.js"></script>
<script type="text/javascript" src="/includes/yui/connection/connection.js"></script>
<script type="text/javascript" src="/includes/yui/container/container.js"></script>
<script language="javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>
</head>
<body onload="MM_preloadImages('/images/loader.gif')">

<script type="text/javascript" src="/includes/safari_hack.js"></script>
<script type="text/javascript" language="javascript">
<? include($_SERVER['DOCUMENT_ROOT'].'/includes_js/inits.js.php'); ?>	
function init() {
	<? include($_SERVER['DOCUMENT_ROOT'].'/includes_js/callbacks.js.php'); ?>
<? include($_SERVER['DOCUMENT_ROOT'].'/includes_js/newFirma_dialog.js.php'); ?>	
<? include($_SERVER['DOCUMENT_ROOT'].'/includes_js/addFirma_dialog.js.php'); ?>	
}
<? include($_SERVER['DOCUMENT_ROOT'].'/includes_js/firmen_handler.js.php'); ?>

function goBack() {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv_sas']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = "back";
	document.form1.submit();
}
function goNext() {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv_sas']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = "next";
	document.form1.submit();
}
function jumpto(target,direction) {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv_sas']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = direction;
	document.getElementById('target').value = target;
	document.form1.submit();
}
</script>
<div id="container">
  <div id="top">
    <? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
	<? /* if ($error != "") { ?>	
	<p style="border:1px solid #990000; background-color:#FFDCD6; padding:5px; width: 450px"><?=$error; ?></p>
	<? }  */?>
  </div>
	<br clear="all" />
<div id="navi"><? $seite = "4"; include($_SERVER['DOCUMENT_ROOT'].'/includes/navigation_sas.inc.php'); ?><div id="loader" style="display:none"><img src="/images/loader.gif" style="margin-top: 8px; margin-left:45px" width="32" height="32" /></div></div>
  <div id="leftSide">
<fieldset>
<p class="legend"><?=$_SESSION['Leg_8']?> 4 / 13: <?=$_SESSION['Leg_124']?></p>
	<? echo ($_SESSION['Leg_104'] != "") ? '<p class="pagenote">'.$_SESSION['Leg_104'].'</p>' : ""; ?>
<form action="<?=$_SERVER['PHP_SELF']?>" method="post" name="form1">
<input type="hidden" value="2" name="step4" />
<input type="hidden" name="direction" id="direction" value="" />
<input type="hidden" name="target" id="target" value="" />
	<label for="name"><?=$_SESSION['Leg_11']?></label>
	<div class="div_blankbox">

		<input type="button" class="liteoption" value="<?=$_SESSION['Leg_144']?>" onclick="javascript:newFirma('2');" />
		&nbsp;<br />&nbsp;<br />
		<div id="firmen_list">
		<?
		$q = FX_open_layout("cgi_k_45__filme_firmen", "999");
$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);
$q->AddDBParam('_kf__Firmentyp_Id', "2");	
$DBData = $q->FMFind();  

$span = "";
foreach ($DBData['data'] as $key => $value) {
	$span .= $value['h_44__firmen::Firmenname'][0].' '.$value['h_44__firmen::PLZ'][0].' '.$value['h_44__firmen::Ort'][0].' <img src="/images/delete.png" border="0" style="cursor:pointer" onClick="deleteFirma(\'2\',\''.$value['_kp__record_id'][0].'\')" align="absmiddle"><br /><input type="hidden" name="hasEntry" value="1" />';
}
	echo $span;
		?>
		</div>
		
		
	</div>
	<br clear="all" />
	<div class="prevBtn"><input type="button" id="back" value="<?=$_SESSION['Leg_31']?>" onclick="goBack('')" /></div>
	<div class="nxtBtn"><input type="button" id="next" value="<?=$_SESSION['Leg_32']?>" onclick="goNext('')" /></div>
	<br clear="all" />
</form>

<div class="clear"></div>
</fieldset>

  </div>
	<br clear="all" />
  <div class="clear"></div>
</div>

<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>

<div id="div_dlg_newfirma">
	<div class="hd">Firma suchen</div>
	<div class="bd">
		<div id="span_newfirma_step1">
		 <form id="form_search_newfirma" action="includes/ajax_search_firma.php" method="post"> 
			<div style="float:left; font-size:11px;">
				Suche nach<br />
				Firma<br />
				<input type="text" class="post" name="s_firma" id="s_firma" value="" size="18" style="border: 1px solid #666666; font-size:11px;" />
			</div>
			<div style="float:left; font-size:11px;">
				&nbsp;<br />
				Ort<br />
				<input type="text" class="post" name="s_ort" id="s_ort" value="" size="18" style="border: 1px solid #666666; font-size:11px;" />
				<input type="hidden" name="type" value="2" />
			</div>
			<div style="float:left; font-size:11px;">
				&nbsp;<br />
				&nbsp;<br />
				<input type="button" class="mainoption" value="<?=$_SESSION['Leg_100']?>" onclick="newfirma_search();" />
			</div>
			 <br clear="all" /> 
			
		 </form><br />
		<span id="span_searchresult_newfirma"></span>
		</div>
		<span id="span_newfirma_step2"></span>
	</div>
</div>

<div id="div_dlg_addfirma">
	<div class="hd">Firma hinzufügen</div>
	<div class="bd">
		<p><?=$_SESSION['Leg_257']?></p>
		<div id="span_addfirma">
		 <form id="form_add_newfirma" action="includes/ajax_add_newfirma.php" method="post" enctype="multipart/form-data"> 
			<div style="float:left; width: 250px;">
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_53']?>*</div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="firma" id="firma" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_36']?>*</div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="strasse1" id="strasse1" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_37']?></div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="strasse2" id="strasse2" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_38']?>*</div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="plz" id="plz" value="" size="6" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_39']?>*</div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="ort" id="ort" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_40']?>*</div>
				<div style="float:left; font-size:11px;">
    <select class="post" name="land" id="land" style="border: 1px solid #666666; font-size:11px;">
	<option value="">-</option>
	<?=$output_Land?>
	</select></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_41']?></div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="fon" id="fon" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_43']?></div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="fax" id="fax" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_44']?></div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="email" id="email" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_45']?></div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="www" id="www" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
			</div>
			<br clear="all" />
			<input type="hidden" name="type" value="2" />
		 </form><br />
		<span id="span_searchresult_newfirma"></span>
		</div>
		<span id="span_newfirma_step2"></span>
	</div>
</div>

<script language="javascript" type="text/javascript">
<!--

init();

//-->
</script>
</body>
</html>
