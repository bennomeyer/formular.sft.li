<?
session_start();
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
if ($_SESSION['film_id'] == "") $_SESSION['film_id'] = (isset($_REQUEST['f'])) ? $_REQUEST['f'] : "";
if ($_SESSION['m'] == "") $_SESSION['m'] = (isset($_REQUEST['m'])) ? $_REQUEST['m'] : "";

if ((!$_SESSION['login_ok']) || (($_SESSION['film_id'] == "") && ($_SESSION['m'] == "u"))) {
	header("Location: /login.php");
	exit;
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/classes/FX/FX.php"); 

if ($_SESSION['maxminutes'] == "") {
	$typ = (isset($_GET['typ'])) ? $_GET['typ'] : "";
	$_SESSION['maxminutes'] = ($typ == "1")? "59" : "300";
}
$step = (isset($_POST['step'])) ? $_POST['step'] : "";
$Filmtitel = (isset($_POST['Filmtitel'])) ? $_POST['Filmtitel'] : "";
$Genre = (isset($_POST['Genre'])) ? $_POST['Genre'] : "";
$Produktionsjahr = (isset($_POST['Produktionsjahr'])) ? $_POST['Produktionsjahr'] : "";
$Produktionsland = (!empty($_POST['Produktionsland'])) ? $_POST['Produktionsland'] : array();
$target = (isset($_POST['target'])) ? $_POST['target'] : "";
$direction = (isset($_POST['direction'])) ? $_POST['direction'] : "";

if(in_array('Originalsprache',array_keys($_POST))) {
	$Originalsprache = ((count($_POST['Originalsprache']))>0) ? $_POST['Originalsprache'] : array();
} else {
	$Originalsprache = array();
}
$Dauer = (isset($_POST['Dauer'])) ? $_POST['Dauer'] : "";
$Farbe = (isset($_POST['Farbe'])) ? $_POST['Farbe'] : "";
require_once(__DIR__.'/../includes/db.inc.php'); 

// UPDATE init - alte Daten aus DB ziehen
//__________________________________________________

if (($_SESSION['m'] == "u") && ($step != "2")) {
	
	// Filmdaten holen
	$find =& $fm->newFindCommand('cgi_h_02__filme'); 
	$find->addFindCriterion('_kp__id', $_SESSION['film_id']); 
	$find->addFindCriterion('_kf__Anmeldeuser_Id', $_SESSION['user_id']); 
	$result = $find->execute(); 
	if (!FileMaker::isError($result)) {
		$records = $result->getRecords(); 
		$record = $records[0];
		$_SESSION['record_id'] = $record->getField('_record__id');
		$Filmtitel = $record->getField('Filmtitel');
		$Produktionsjahr = $record->getField('Produktionsjahr');	
		$Dauer = $record->getField('Dauer_Minuten');	
		$Farbe = $record->getField('_kf__farbe_oder_sw');
		$Genre = $record->getField('_kf__Genre');
		
		// Infos aus Kreuztabellen holen
		$relatedSet = $record->getRelatedSet('zz_Produktionslaender');
		if (!FileMaker::isError($relatedSet)) {
			foreach ($relatedSet as $relatedRow) {
				$Produktionsland[] = $relatedRow->getField('zz_Produktionslaender::_kf__ISO_Laender_ID').'|'.$relatedRow->getField('zz_Produktionslaender_Laenderliste::Land_'.$_SESSION['sprache']);
			}
		}
		$relatedSet = $record->getRelatedSet('zz_Originalsprachen');
		if (!FileMaker::isError($relatedSet)) {
			foreach ($relatedSet as $relatedRow) {
				$Originalsprache[] = $relatedRow->getField('zz_Originalsprachen::_kf__ISO_Sprachen_Id').'|'.$relatedRow->getField('zz_Originalsprachen_Sprachliste::Sprache_'.$_SESSION['sprache']);
			}
		}
	} else {
		header("Location: films_overview.php");
		exit;
	}
	
	


}

// ERROR-Handling
//__________________________________________________
$error = "";
if ($step == "2") {
	$error .= ($Filmtitel == "") ? $_SESSION['Leg_76']."<br />" : "";
	$error .= ((!isset($Produktionsland[0])) || (!isset($Originalsprache[0]))) ? $_SESSION['Leg_75']."<br />" : "";
	$error .= (($Dauer >20) || (!(preg_match("/^[0-9]{1,3}$/",$Dauer)))) ? $_SESSION['Leg_147']."<br />" : "";
	$error .= ($Farbe == "") ? $_SESSION['Leg_74']."<br />" : "";
} 

// UPDATE final - Daten aktualisieren
//__________________________________________________

if ((($_SESSION['m'] == "u") && ($step == "2") && ($error == "")) || (($step == "2") && ($direction == "back")&& ($_SESSION['m'] == "u"))) {
	$q = FX_open_layout( "cgi_h_02__filme", "1"); 
	
	$q->AddDBParam('-recid', $_SESSION['record_id']); 
	$q->AddDBParam('Filmtitel', $Filmtitel);
	$q->AddDBParam('Produktionsjahr', $Produktionsjahr);	
	$q->AddDBParam('Dauer_Minuten', $Dauer);	
	$q->AddDBParam('_kf__farbe_oder_sw', $Farbe);
	$q->AddDBParam('_kf__Genre', $Genre);
	if ($error == "") $q->AddDBParam('zz_Anmeldung_Seite01_Flag', "1");
	if ($error != "") $q->AddDBParam('zz_Anmeldung_Seite01_Flag', "0");
	$DBData = $q->FMEdit(); 
	
	// Alle Infos aus den Kreuztabellen löschen

		if ($_SESSION['film_id'] != "") {
		$find =& $fm->newFindCommand('cgi_k_07__filme_ISO_laender'); 
		$find->addFindCriterion('_kf__Film_Id', $_SESSION['film_id']); 
		$result = $find->execute(); 
		if (!FileMaker::isError($result)) {
			$records = $result->getRecords(); 
			foreach ($records as $record) {
				$rec_ID = $record->getRecordId();
				$rec = $fm->getRecordById('cgi_k_07__filme_ISO_laender', $rec_ID); 
				$rec->delete();
			}
		}
		$find =& $fm->newFindCommand('cgi_k_09__filme_ISO_sprachen'); 
		$find->addFindCriterion('_kf__Film_Id', $_SESSION['film_id']); 
		$result = $find->execute(); 
		if (!FileMaker::isError($result)) {
			$records = $result->getRecords(); 
			foreach ($records as $record) {
				$rec_ID = $record->getRecordId();
				$rec = $fm->getRecordById('cgi_k_09__filme_ISO_sprachen', $rec_ID); 
				$rec->delete();
			}
		}
	}
	if (isset($Produktionsland[0])) {
		$i = 1;
		foreach ($Produktionsland as $land_id) { 
			$land = explode("|", $land_id);
			$land_code = $land[0];
			$q = FX_open_layout("cgi_k_07__filme_ISO_laender", "1"); 
			$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);	
			$q->AddDBParam('_kf__ISO_Laender_ID', $land_code);	
			$q->AddDBParam('Hierarchienummer', $i);	
			$DBData = $q->FMNew(); 
			$i++;		
		}
	}				
	// Sprache-Film Kreuztabelle
	if (isset($Originalsprache[0])) {
		$i = 1;
		foreach ($Originalsprache as $sprache_id) { 
			$sprache = explode("|", $sprache_id);
			$sprache_code = $sprache[0];
			$q = FX_open_layout("cgi_k_09__filme_ISO_sprachen", "1"); 
			$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);	
			$q->AddDBParam('_kf__ISO_Sprachen_Id', $sprache_code);	
			$q->AddDBParam('Hierarchienummer', $i);	
			$DBData = $q->FMNew(); 
			$i++;		
		}
	}		
	
}


// DB-Actions für Neueintragung
//__________________________________________________
if ((($error == "") && ($step == "2") && ($_SESSION['m'] != "u")) || (($step == "2") && ($direction == "back")&& ($_SESSION['m'] != "u"))) {
	$_SESSION['m'] = "u";
	//Film wird neu eingetragen
	$q = FX_open_layout("cgi_h_02__filme", "1"); 
	$q->AddDBParam('Filmtitel', $Filmtitel);
	$q->AddDBParam('Produktionsjahr', $Produktionsjahr);	
	$q->AddDBParam('Dauer_Minuten', $Dauer);	
	$q->AddDBParam('_kf__farbe_oder_sw', $Farbe);
	$q->AddDBParam('_kf__Genre', $Genre);
	$q->AddDBParam('_kf__Anmeldeuser_Id', $_SESSION['user_id']);
	$q->AddDBParam('Sektion', 'BSVC');
	$q->AddDBParam('zz_Anmeldung_Seite01_Flag', '1');
	$DBData = $q->FMNew(); 		

	foreach ($DBData['data'] as $key => $value) {
		$film_id = $value['_kp__id'][0];
		$_SESSION['film_id'] = $film_id;
		$_SESSION['record_id'] = $value['_record__id'][0];
	}
	if (isset($film_id)) {
	
		
		// Land-Film Kreuztabelle
		if (isset($Produktionsland[0])) {
			$i = 1;
			foreach ($Produktionsland as $land_id) { 
				$land = explode("|", $land_id);
				$land_code = $land[0];
				$q = FX_open_layout("cgi_k_07__filme_ISO_laender", "1"); 
				$q->AddDBParam('_kf__Film_Id', $film_id);	
				$q->AddDBParam('_kf__ISO_Laender_ID', $land_code);	
				$q->AddDBParam('Hierarchienummer', $i);	
				$DBData = $q->FMNew(); 
				$i++;		
			}
		}				
		// Sprache-Film Kreuztabelle
		if (isset($Originalsprache[0])) {
			$i = 1;
			foreach ($Originalsprache as $sprache_id) { 
				$sprache = explode("|", $sprache_id);
				$sprache_code = $sprache[0];
				$q = FX_open_layout("cgi_k_09__filme_ISO_sprachen", "1"); 
				$q->AddDBParam('_kf__Film_Id', $film_id);	
				$q->AddDBParam('_kf__ISO_Sprachen_Id', $sprache_code);	
				$q->AddDBParam('Hierarchienummer', $i);	
				$DBData = $q->FMNew(); 
				$i++;		
			}
		}		
	}		
}



if (($direction == "back") && ($step == "2")) {
	if ($target != "") {
		header("Location: /$target");
	} else {
		//redirect zur nächsten Seite
		header("Location: /films_overview.php");
	}
	exit;
}

if (($direction == "next") && ($step == "2") && ($error == "")) {
	if ($target != "") {
		header("Location: /$target");
	} else {
		//redirect zur nächsten Seite
		header("Location: /sas/step08_2.php");
	}
	exit;
}

include($_SERVER['DOCUMENT_ROOT']. "/includes/x_get_value_list1.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Online Registration</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <link href="/css/style2008.css" rel="stylesheet" type="text/css" title="KFT" />
<script language="javascript">
var laender = new Array;
function addLand() {
	if (document.getElementById('Produktionsland_select').value != "") {
		laender.push(document.getElementById('Produktionsland_select').value);
		document.getElementById('Produktionsland_select').value = "";
		showLaender();		
	}
}
function delLand(i) {
	laender.splice(i,1);	
	showLaender();	
}
function showLaender() {
	var output = "";
	for (var i = 0; i < laender.length; ++i) {
		var land = laender[i].split("|");
		var land_code = land[0];
		var land_txt = land[1];
 		output += "<div id=\"list_values\"><input type=\"hidden\" name=\"Produktionsland[]\" value=\"" + laender[i] + "\">" + land_txt + "</div><div id=\"list_delicons\"><img src=\"/images/delete.png\" onClick=\"delLand(" + i + ")\" align=\"absmiddle\" title=\"<?=$_SESSION['Leg_88']?>\"></div><br clear \"all\" />\n";
	}
	document.getElementById('Laender').innerHTML = output;
	document.getElementById('Laender').style.display = "block";
}
var sprachen = new Array;
function addSprache() {
	if (document.getElementById('Originalsprache_select').value != "") {
		sprachen.push(document.getElementById('Originalsprache_select').value);
		document.getElementById('Originalsprache_select').value = "";
		showSprachen();		
	}
}
function delSprache(i) {
	sprachen.splice(i,1);	
	showSprachen();	
}
function showSprachen() {
	var output_sprachen = "";
	for (var i = 0; i < sprachen.length; ++i) {
		var sprache = sprachen[i].split("|");
		var sprache_code = sprache[0];
		var sprache_txt = sprache[1];
 		output_sprachen += "<div id=\"list_values\"><input type=\"hidden\" name=\"Originalsprache[]\" value=\"" + sprachen[i] + "\">" + sprache_txt + "</div><div id=\"list_delicons\"><img src=\"/images/delete.png\" onClick=\"delSprache(" + i + ")\" align=\"absmiddle\" title=\"<?=$_SESSION['Leg_88']?>\"></div><br clear \"all\" />\n";
	}
	document.getElementById('Sprachen').innerHTML = output_sprachen;
	document.getElementById('Sprachen').style.display = "block";
}
function goBack() {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv_sas']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = "back";
	document.form1.submit();
}
function goNext() {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv_sas']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = "next";
	document.form1.submit();
}
function jumpto(target,direction) {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv_sas']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = direction;
	document.getElementById('target').value = target;
	document.form1.submit();
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; 
    for(i=0; i<a.length; i++) {
      if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}
    }
  }
}
//-->
</script>
</head>
<body onload="MM_preloadImages('/images/loader.gif');document.getElementById('Filmtitel').focus();">


<div id="container">
  <div id="top">
    <? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
  </div>
	<br clear="all" />
<div id="navi"><? $seite = "1"; include($_SERVER['DOCUMENT_ROOT'].'/includes/navigation_sas.inc.php'); ?><div id="loader" style="display:none"><img src="/images/loader.gif" style="margin-top: 8px; margin-left:45px" width="32" height="32" /></div></div>
<div id="leftSide">
<fieldset>
<p class="legend"><?=$_SESSION['Leg_8']?> 1 / 13: <?=$_SESSION['Leg_121']?></p>
	<? if ($error != "") { ?>	
	<p style="border:1px solid #990000; background-color:#FFDCD6; padding:5px; width: 545px"><?=$error; ?></p>
	<? } ?>
<? echo ($_SESSION['Leg_101'] != "") ? '<p class="pagenote">'.$_SESSION['Leg_101'].'</p>' : ""; ?>
<form action="<?=$_SERVER['PHP_SELF']?>" method="post" name="form1">
<input type="hidden" value="2" name="step" />
<input type="hidden" name="m" value="<?=$_SESSION['m']?>" />
<input type="hidden" name="direction" id="direction" value="" />
<input type="hidden" name="target" id="target" value="" />
	<label for="name"><?=$_SESSION['Leg_1']?></label>
    <div class="div_blankbox">
		<input name="Filmtitel" type="text" class="textbox" id="Filmtitel" value="<?=htmlspecialchars($Filmtitel)?>" />
		<input type="hidden" name="Genre" value="5" />
	</div>
	<br clear="all" />
	
	<label for="Produktionsjahr"><?=$_SESSION['Leg_3']?></label>
	<div class="div_blankbox">
		<select name="Produktionsjahr">
		<option value="">-</option>
		<?=$output_Jahr?>
		</select>
	</div>
	<br clear="all" />
	
	<label for="Produktionsland"><?=$_SESSION['Leg_33']?></label>
	<div class="div_blankbox">
		<div id="select_div" style="float:left; width: 210px;">
			<select name="Produktionsland_select" id="Produktionsland_select">
			<option value="">-</option>
			<?=$output_Land?>
			</select>
			<img src="/images/arrow.png" alt="<?=$_SESSION['Leg_78']?>" align="middle" onclick="addLand()" title="<?=$_SESSION['Leg_78']?>" /><br />
			<?=$_SESSION['Leg_136']?>
		</div>
		<div id="Laender" class="list_div"> </div>
		<br clear="all" />
	</div>
	<br clear="all" />
	
	<label for="Originalsprache"><?=$_SESSION['Leg_4']?></label>
	<div class="div_blankbox">
		<div id="select_div" style="float:left; width: 210px;">
			<select name="Originalsprache_select" id="Originalsprache_select">
			<option value="">-</option>
			<?=$output_Sprache?>
			</select>
			<img src="/images/arrow.png" alt="<?=$_SESSION['Leg_78']?>" align="middle" onclick="addSprache()" title="<?=$_SESSION['Leg_78']?>" /><br />
			<?=$_SESSION['Leg_137']?>
		</div>
		<div id="Sprachen" class="list_div"> </div>
		<br clear="all" />
	</div>
	<br clear="all" />
	
	<label for="Dauer"><?=$_SESSION['Leg_5']?></label>
    <div class="div_blankbox">
		<input name="Dauer" type="text" class="textbox_short" id="Dauer" value="<?=$Dauer?>" /> <?=$_SESSION['Leg_6']?> (1-20)
	</div>
	<br clear="all" />
	
	<label for="Farbe"><?=$_SESSION['Leg_7']?></label>
    <div class="div_blankbox">
		<?=$output_Farbe?>
	</div>
	<br clear="all" />
	
	<div class="button_div">
		<input type="button" id="next" value="<?=$_SESSION['Leg_32']?>" onclick="goNext('')" />
	</div>
</form>
<div class="clear"></div>
</fieldset>

  </div>
  <br clear="all" />
  <div class="clear"></div>
</div>


<? if ((($step == "2") && isset($Produktionsland[0])) || (($_SESSION['m'] == "u") && isset($Produktionsland[0]))) {
echo '<script language="javascript">'."\n";
echo 'laender.push("'.implode("\",\"", $Produktionsland).'");'."\n";
echo 'showLaender();'."\n";
echo '</script>'."\n";
}
?>
<? if ((($step == "2") && isset($Originalsprache[0])) || (($_SESSION['m'] == "u") && isset($Originalsprache[0]))) {
echo '<script language="javascript">'."\n";
echo 'sprachen.push("'.implode("\",\"", $Originalsprache).'");'."\n";
echo 'showSprachen();'."\n";
echo '</script>'."\n";
}
?>
<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>

</body>
</html>
