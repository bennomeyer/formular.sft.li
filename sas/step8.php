<?
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
session_start();

if (!isset($_SESSION['Leg_1'])) {
	
	// redirect zur�ck zur Startseite wg. abgelaufener Session
	header("Location: /index.php?error=Session%20abgelaufen");
	exit;
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/classes/FX/FX.php"); 

$step = (isset($_POST['step_8'])) ? $_POST['step_8'] : "";

$B_Bandname = (!empty($_POST['b_bandname'])) ? $_POST['b_bandname'] : "";
$B_Bandmitglieder = (!empty($_POST['b_bandmitglieder'])) ? $_POST['b_bandmitglieder'] : "";
$B_Musikstil = (!empty($_POST['b_musikstil'])) ? $_POST['b_musikstil'] : "";
$B_Musikstil_Sonst = (!empty($_POST['b_musikstil_sonst'])) ? $_POST['b_musikstil_sonst'] : "";
$B_Diskographie = (!empty($_POST['b_diskographie'])) ? $_POST['b_diskographie'] : "";


$error = "";
if ($step == "2") {
	if ((empty($_POST['b_bandname'])) || (empty($_POST['b_bandmitglieder'])) || (empty($_POST['b_musikstil'])) || (empty($_POST['b_diskographie'])) ) $error .= $_SESSION['Leg_141'];
} 



if (($error == "") && ($step == "2")) {
		
			
			// Bandeintrag
			$q = new FX('host8.kon5.net', '80', 'FMPro7');
			$q->SetDBData("Filmtage-Solothurn", "cgi_h_43__bands", "1"); 
			$q->SetDBUserPass ("Admin", "xs4kon5");
			$q->AddDBParam('Bandname', utf8_encode($B_Bandname));
			$q->AddDBParam('Bandmitglieder', utf8_encode($B_Bandmitglieder));	
			$q->AddDBParam('_kf__musikstilID', $B_Musikstil);	
			$q->AddDBParam('Musikstil', utf8_encode($B_Musikstil_Sonst));	
			$q->AddDBParam('Diskographie', utf8_encode($B_Diskographie));	
			$DBData = $q->FMNew();
			foreach ($DBData['data'] as $key => $value) {
				$band_id = $value['_kp__id'][0];
			}
			
			// Kreuztabelleneintrag
			$q = new FX('host8.kon5.net', '80', 'FMPro7');
			$q->SetDBData("Filmtage-Solothurn", "cgi_k_42__filme_bands", "1"); 
			$q->SetDBUserPass ("Admin", "xs4kon5");
			$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);	
			$q->AddDBParam('_kf__Band_Id', $band_id);		
			$DBData = $q->FMNew();

		
	// redirect zur n�chsten Seite
	header("Location: /sas/step9.php");
	exit;
}

include($_SERVER['DOCUMENT_ROOT']. "/includes/x_get_value_list_sas8.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Online Registration</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 <link href="/css/style.css" rel="stylesheet" type="text/css" title="KFT" />
 
<script type="text/javascript" src="/includes/scripts.js"></script>
<link rel="stylesheet" type="text/css" href="/includes/yui/container/assets/container.css">
<script type="text/javascript" src="/includes/yui/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript" src="/includes/yui/animation/animation-min.js"></script>
<script type="text/javascript" src="/includes/yui/dragdrop/dragdrop-min.js"></script>
<script type="text/javascript" src="/includes/yui/connection/connection.js"></script>
<script type="text/javascript" src="/includes/yui/container/container.js"></script>
</head>
<body>


<div id="container">
  <div id="top">
    <? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
	<? if (($error != "") && ($step == "2")) { ?>	
	<p style="border:1px solid #990000; background-color:#FFDCD6; padding:5px; width: 450px"><?=$error; ?></p>
	<? } ?>
  </div>
	<br clear="all" />
  <div id="leftSide">
<fieldset>
<legend><?=$_SESSION['Leg_8']?> 8 / 10: <?=$_SESSION['Leg_140']?></legend>
	<? echo ($_SESSION['Leg_141'] != "") ? '<p class="pagenote">'.$_SESSION['Leg_141'].'</p>' : ""; ?>
<form action="<?=$_SERVER['PHP_SELF']?>" method="post" name="form1">
<input type="hidden" value="2" name="step_8">

	
	<label for="name"><?=$_SESSION['Leg_139']?></label>
	<div class="div_blankbox">
			<?=$_SESSION['Leg_62']?><br />
			<input type="text" name="b_bandname" id="b_bandname" class="textbox" value="<?=$B_Bandname?>" /><br />&nbsp;<br />
			<?=$_SESSION['Leg_63']?><br />
			<textarea name="b_bandmitglieder" id="b_bandmitglieder" class="textbox"><?=$B_Bandmitglieder?></textarea><br />&nbsp;<br />
			<?=$_SESSION['Leg_64']?><br />
			<select name="b_musikstil" id="b_musikstil">
			<option value="">-</option>
			<?=$output_Musikstil?>
			</select><br />&nbsp;<br />
			<?=$_SESSION['Leg_146']?><br />
			<input type="text" name="b_musikstil_sonst" id="b_musikstil_sonst" class="textbox" value="<?=$B_Musikstil_Sonst?>"/><br />&nbsp;<br />
			<?=$_SESSION['Leg_65']?><br />
			<textarea name="b_diskographie" id="b_diskographie" class="textbox"><?=$B_Diskographie?></textarea>
		<br clear="all" />
	</div>
	<br clear="all" />
	<div class="button_div">
	<input name="submit" type="submit" id="submit" value="<?=$_SESSION['Leg_32']?>">
	</div>
</form>
<div class="clear"></div>
</fieldset>

  </div>
  <div class="clear"></div>
</div>


<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>
</body>
</html>
