<?
session_start();
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
if ((!$_SESSION['login_ok']) || (($_SESSION['film_id'] == "") && ($_SESSION['m'] == "u"))) {
	header("Location: login.php");
	exit;
}

$film_id = (isset($_GET['f']))? $_GET['f'] : "";
$Filmtitel = "";
$zz_Synthese_Genre = "";
$Produktionsjahr = "";
$Dauer_Minuten = "";
$zz_Synthese_Bildformat = "";
$zz_Synthese_Buch = "";
$zz_Synthese_Darsteller = "";
$zz_Synthese_FarbeOderSW = "";
$zz_Synthese_Filmformat = "";
$zz_Synthese_Genre = "";
$zz_Synthese_Kamera = "";
$zz_Synthese_Montage = "";
$zz_Synthese_Musik = "";
$zz_Synthese_Premierentyp = "";
$zz_Synthese_Produktion = "";
$zz_Synthese_Produktionsland = "";
$zz_Synthese_Regie = "";
$zz_Synthese_Release = "";
$zz_Synthese_Sprache = "";
$zz_Synthese_Synopsis = "";
$zz_Synthese_Ton = "";
$zz_Synthese_Tonformat = "";
$zz_Synthese_Untertitel = "";
$zz_Synthese_Verleih = "";
$zz_Synthese_Weltrechte = "";
$zz_Synthese_Weltvertrieb = "";
$Band_Name = "";
$zz_Synthese_Label = "";
$complete = FALSE; 
$isan = "";
$zz_Anmeldung_Anmelderinfos = "";


require_once (__DIR__.'/../includes/db.inc.php');
$find =& $fm->newFindCommand('cgi_h_02__filme'); 
$find->addFindCriterion('_kp__id', $film_id); 
$find->addFindCriterion('_kf__Anmeldeuser_Id', $_SESSION['user_id']); 
$find->addFindCriterion('zz_Anmeldung_Vollstaendigkeitscheck', "1"); 
$result = $find->execute(); 
if (!FileMaker::isError($result)) {
	$complete = TRUE; 
}

// get Data out of DB
$q = FX_open_layout("cgi_Zusammenfassung", "999");

$q->AddDBParam('_kp__id', $film_id);
$q->AddDBParam('_kf__Anmeldeuser_Id', $_SESSION['user_id']); 
$DBData = $q->FMFind();  
foreach ($DBData['data'] as $key => $value) {
	$Filmtitel = $value['Filmtitel'][0];
	$zz_Synthese_Genre = $value['zz_Synthese_Genre_'.$_SESSION['sprache']][0];
	$Produktionsjahr = $value['Produktionsjahr'][0];
	$Dauer_Minuten = $value['Dauer_Minuten'][0];
	$zz_Synthese_Bildformat = $value['zz_Synthese_Bildformat'][0];
	$zz_Synthese_Buch = $value['zz_Synthese_Buch'][0];
	$zz_Synthese_Darsteller = $value['zz_Synthese_Darsteller'][0];
	$zz_Synthese_FarbeOderSW = $value['zz_Synthese_FarbeOderSW_'.$_SESSION['sprache']][0];
	$zz_Synthese_Filmformat = $value['zz_Synthese_Filmformat'][0];
	$zz_Synthese_Genre = $value['zz_Synthese_Genre_'.$_SESSION['sprache']][0];
	$zz_Synthese_Kamera = $value['zz_Synthese_Kamera'][0];
	$zz_Synthese_Montage = $value['zz_Synthese_Montage'][0];
	$zz_Synthese_Musik = $value['zz_Synthese_Musik'][0];
	$zz_Synthese_Premierentyp = $value['zz_Synthese_Premierentyp_'.$_SESSION['sprache']][0];
	$zz_Synthese_Produktion = $value['zz_Synthese_Produktion'][0];
	$zz_Synthese_Produktionsland = $value['zz_Synthese_Produktionsland_'.$_SESSION['sprache']][0];
	$zz_Synthese_Regie = $value['zz_Synthese_Regie'][0];
	$zz_Synthese_Release = $value['zz_Synthese_Release_'.$_SESSION['sprache']][0];
	$zz_Synthese_Sprache = $value['zz_Synthese_Sprache_'.$_SESSION['sprache']][0];
	$zz_Synthese_Synopsis = $value['zz_Synthese_Synopsis'][0];
	$zz_Synthese_Ton = $value['zz_Synthese_Ton'][0];
	$zz_Synthese_Tonformat = $value['zz_Synthese_Tonformat'][0];
	$zz_Synthese_Untertitel = $value['zz_Synthese_Untertitel_'.$_SESSION['sprache']][0];
	$zz_Synthese_Verleih = $value['zz_Synthese_Verleih'][0];
	$zz_Synthese_Weltrechte = $value['zz_Synthese_Weltrechte'][0];
	$zz_Synthese_Weltvertrieb = $value['zz_Synthese_Weltvertrieb'][0];
	$Band_Name = $value['Band_Name'][0];
	$zz_Synthese_Label = $value['zz_Synthese_Label'][0];
	$isan = $value['ISAN'][0];
	$zz_Anmeldung_Anmelderinfos = $value['zz_Anmeldung_Anmelderinfos'][0];
}




?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Online Registration</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link href="/css/style2008.css" rel="stylesheet" type="text/css" title="KFT" />
<style type="text/css">
body {
	margin-left: 20px;
	background-image: none;
	<? if (!$complete) echo 'background-image:url(/images/unvollstaendig.jpg);' ?>
	<? if (!$complete) echo 'background-repeat:repeat-y;' ?>
}
#container {
	background:none;
	width:570px;
}
</style>
<script type="text/JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
</script>
</head>
<body>


<div id="container">
  <div id="top">
  </div>
	<br clear="all" />
  <div id="leftSide">
<fieldset>
<? if ($complete) { ?>
<p><?=nl2br($_SESSION['Leg_326'])?></p>
<p><?=nl2br($_SESSION['Leg_160'])?></p>
<? } else { ?>
<p><?=$_SESSION['Leg_280']?></p>
<? } ?>

	<div class="zusammenfassung_links" style="background:none;">&nbsp;</div>
	<div  class="div_blankbox" style="background:none;"><input type="button" name="print2" id="print2" value="<?=$_SESSION['Leg_116']?>" onclick="MM_openBrWindow('print_filminfo.php?f=<?=$film_id?>','Drucken','resizable=yes,width=550,height=650')"/></div>
	<br clear="all" />
	<br clear="all" />

	<div class="zusammenfassung_links"><?=$_SESSION['Leg_1']?></div>
	<div class="div_blankbox"><?=$Filmtitel?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_2']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Genre?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_3']?></div>
	<div class="div_blankbox"><?=$Produktionsjahr?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_33']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Produktionsland?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_4']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Sprache?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_5']?></div>
	<div class="div_blankbox"><?=$Dauer_Minuten?> <?=$_SESSION['Leg_6']?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_7']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_FarbeOderSW?></div>
	<br clear="all" />
	<hr size="2" noshade="noshade" style="color:#000000"/>
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_9']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Regie?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_262']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Buch?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_263']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Kamera?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_264']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Montage?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_62']?></div>
	<div class="div_blankbox"><?=$Band_Name?></div>
	<br clear="all" />
	<hr size="2" noshade="noshade" style="color:#000000"/>
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_10']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Produktion?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_11']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Weltrechte?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_252']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Weltvertrieb?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_12']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Verleih?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_284']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Label?></div>
	<br clear="all" />
	<hr size="2" noshade="noshade" style="color:#000000"/>
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_68']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Filmformat?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_69']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Bildformat?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_70']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Tonformat?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_67']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Untertitel?></div>
	<br clear="all" />
	<hr size="2" noshade="noshade" style="color:#000000"/>
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_269']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Premierentyp?></div>
	<br clear="all" />
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_268']?></div>
	<div class="div_blankbox"><?=$zz_Synthese_Release?></div>
	<br clear="all" />
	<hr size="2" noshade="noshade" style="color:#000000"/>
	<div class="zusammenfassung_links"><?=$_SESSION['Leg_28']?></div>
	<div class="div_blankbox">
	<?
	if (file_exists($_SERVER['DOCUMENT_ROOT']."/bilder/a_small/".$film_id.".jpg")) echo '<img src="/bilder/a_small/'.$film_id.'.jpg" style="margin-right:15px">';
	if (file_exists($_SERVER['DOCUMENT_ROOT']."/bilder/b_small/".$film_id.".jpg")) echo '<img src="/bilder/b_small/'.$film_id.'.jpg" style="margin-right:15px">';
	if (file_exists($_SERVER['DOCUMENT_ROOT']."/bilder/c_small/".$film_id.".jpg")) echo '<img src="/bilder/c_small/'.$film_id.'.jpg">';
	?></div>
	<br clear="all" />
	<hr size="2" noshade="noshade" style="color:#000000"/>

	<div class="zusammenfassung_links"><?=$_SESSION['Leg_299']?></div>
	<div class="div_blankbox"><?=nl2br($zz_Anmeldung_Anmelderinfos)?></div>
	<br clear="all" />
	<hr size="2" noshade="noshade" style="color:#000000"/>

	<div style="width: 400px; float:left"><p><?=$_SESSION['Leg_117']?></p></div>
	<div style="float:left; vertical-align:bottom;"><p>&nbsp;<br /><input type="button" name="print2" id="print2" value="<?=$_SESSION['Leg_116']?>" onclick="MM_openBrWindow('print_filminfo.php?f=<?=$film_id?>','Drucken','resizable=yes,width=550,height=650')"/></p></div>
	<br clear="all" />
		<table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td><p><?=$_SESSION['Leg_35']?></p></td>
            <td>_________________________</td>
          </tr>
          <tr>
            <td><p><?=$_SESSION['Leg_39']?></p></td>
            <td>_________________________</td>
          </tr>
          <tr>
            <td><p><?=$_SESSION['Leg_118']?></p></td>
            <td>_________________________</td>
          </tr>
        </table><br />&nbsp;<br />
		<?
		$pdf_de = ($_SESSION['sprache'] == "de") ? "http://www.solothurnerfilmtage.ch/download/23/page/1950_agb_sft43_de.pdf" : "";
		$pdf_en = ($_SESSION['sprache'] == "en") ? "http://www.solothurnerfilmtage.ch/download/23/page/1952_agb_sft43_en.pdf" : "";
		$pdf_fr = ($_SESSION['sprache'] == "fr") ? "http://www.solothurnerfilmtage.ch/download/23/page/1951_agb_sft43_fr.pdf" : "";
		$pdf_it = ($_SESSION['sprache'] == "it") ? "http://www.solothurnerfilmtage.ch/download/23/page/1951_agb_sft43_fr.pdf" : "";
		?>
		<p><a href="<?=${'pdf_'.$_SESSION['sprache']}?>" target="_blank"><?=$_SESSION['Leg_120']?></a><br />&nbsp;<br /></p>
		<input type="button" value="<?=$_SESSION['Leg_99']?>" onclick="self.close();"><br />&nbsp;<br />
	<br clear="all" />
<div class="clear"></div>
</fieldset>

  </div>
  <div class="clear"></div>
</div>


<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>
</body>
</html>
