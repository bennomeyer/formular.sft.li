<?
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
session_start();
if (!isset($_SESSION['Leg_1'])) {
	
	// redirect zur�ck zur Startseite wg. abgelaufener Session
	header("Location: /index.php?error=Session%20abgelaufen");
	exit;
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/classes/FX/FX.php"); 

$step = (isset($_POST['step_8'])) ? $_POST['step_8'] : "";

$Filmformat = (!empty($_POST['Filmformat'])) ? $_POST['Filmformat'] : array();
$Bildformat = (!empty($_POST['Bildformat'])) ? $_POST['Bildformat'] : array();
$Tonformat = (!empty($_POST['Tonformat'])) ? $_POST['Tonformat'] : array();

$error = "";
if ($step == "2") {
	if ( (empty($_POST['Filmformat'])) || (empty($_POST['Bildformat'])) || (empty($_POST['Tonformat'])) ) $error .= $_SESSION['Leg_92'];
} 



if (($error == "") && ($step == "2")) {

	if (isset($Filmformat[0])) {
		$i = 0;
		foreach ($Filmformat as $filmformat_id) { 
			$filmformat = explode("|", $Filmformat[$i]);
			$bildformat = explode("|", $Bildformat[$i]);
			$tonformat = explode("|", $Tonformat[$i]);
	
			$q = new FX('host8.kon5.net', '80', 'FMPro7');
			$q->SetDBData("Filmtage-Solothurn", "cgi_k_20__filme_vorfuehrkopie", "1"); 
			$q->SetDBUserPass ("Admin", "xs4kon5");
			$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);	
			$q->AddDBParam('_kf__Filmformat_Id', $filmformat[0]);	
			$q->AddDBParam('_kf__Bildformat_Id', $bildformat[0]);	
			$q->AddDBParam('_kf__Tonformat_Id', $tonformat[0]);	
			$DBData = $q->FMNew(); 
			$i++;		
		}
	}	
	// redirect zur n�chsten Seite
	header("Location: /sas/step8.php");
	exit;
}

$show_value_lists_4 = "sas";
include($_SERVER['DOCUMENT_ROOT']. "/includes/x_get_value_list8.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Online Registration</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 <link href="/css/style.css" rel="stylesheet" type="text/css" title="KFT" />
 
<script type="text/javascript" src="/includes/scripts.js"></script>
<link rel="stylesheet" type="text/css" href="/includes/yui/container/assets/container.css">
<script type="text/javascript" src="/includes/yui/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript" src="/includes/yui/animation/animation-min.js"></script>
<script type="text/javascript" src="/includes/yui/dragdrop/dragdrop-min.js"></script>
<script type="text/javascript" src="/includes/yui/connection/connection.js"></script>
<script type="text/javascript" src="/includes/yui/container/container.js"></script>
<script language="javascript">
var sprachen = new Array;
var untertitels = new Array;
var filmformate = new Array;
var bildformate = new Array;
var tonformate = new Array;

function addEntry() {
	if 	(
		(document.getElementById('Filmformat_select').value != "") &&
		(document.getElementById('Bildformat_select').value != "") &&
		(document.getElementById('Tonformat_select').value != "") 
		) {
		filmformate.push(document.getElementById('Filmformat_select').value);
		document.getElementById('Filmformat_select').value = "";
		bildformate.push(document.getElementById('Bildformat_select').value);
		document.getElementById('Bildformat_select').value = "";
		tonformate.push(document.getElementById('Tonformat_select').value);
		document.getElementById('Tonformat_select').value = "";
		showEntries();		
	} else {
		miniDialog = new YAHOO.widget.SimpleDialog("mini_dlg", { visible:false, width: "25em", effect:[
{effect:YAHOO.widget.ContainerEffect.SLIDE,duration:0.25}, {effect:YAHOO.widget.ContainerEffect.FADE,duration:0.25}
], fixedcenter : true, modal:true, draggable:false });
		miniDialog.setHeader("<?=$_SESSION['Leg_91']?>");
		miniDialog.setBody("<?=$_SESSION['Leg_92']?>");
		miniDialog.cfg.queueProperty("icon",YAHOO.widget.SimpleDialog.ICON_WARN);
		miniDialog.cfg.queueProperty("buttons", [ 
			{ text: "<?=$_SESSION['Leg_93']?>", handler:handlerHide, isDefault:true  }
		]);
		miniDialog.render(document.body);
		miniDialog.show();
	}
}
function delEntry(i) {
	filmformate.splice(i,1);
	bildformate.splice(i,1);
	tonformate.splice(i,1);	
	showEntries();	
}
function showEntries() {
	var output = "";
	for (var i = 0; i < filmformate.length; ++i) {
		var filmformat = filmformate[i].split("|");
		var bildformat = bildformate[i].split("|");
		var tonformat = tonformate[i].split("|");
 		output += "<input type=\"hidden\" name=\"Filmformat[]\" value=\"" + filmformate[i] + "\"><?=$_SESSION['Leg_68']?>: " + filmformat[1] + "<br />\n";
 		output += "<input type=\"hidden\" name=\"Bildformat[]\" value=\"" + bildformate[i] + "\"><?=$_SESSION['Leg_69']?>: " + bildformat[1] + "<br />\n";
 		output += "<input type=\"hidden\" name=\"Tonformat[]\" value=\"" + tonformate[i] + "\"><?=$_SESSION['Leg_70']?>: " + tonformat[1] + "<br />\n";
 		output += "<img src=\"/images/delete.png\" onClick=\"delEntry(" + i + ")\" align=\"absmiddle\"> <a href=\"javascript:void(0);\" onClick=\"delEntry(" + i + ")\"><?=$_SESSION['Leg_88']?></a><br />\n";
 		output += "<hr style=\"width: 330px; height: 2px\" noshade=\"noshade\" color=\"#FFFFFF\"><br />\n";
	}
	document.getElementById('Vorfuehrkopien').innerHTML = "<p>"+ output +"</p>";
	document.getElementById('Vorfuehrkopien').style.display = "block";
}

function checkEntry() {
	if 	(
		(document.getElementById('Filmformat_select').value == "") &&
		(document.getElementById('Bildformat_select').value == "") &&
		(document.getElementById('Tonformat_select').value == "")
		) { 
			return true; 
		} else {
			return false;
		}
	
}
</script> 
</head>
<body>


<div id="container">
  <div id="top">
    <? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
	<? if (($error != "") && ($step == "2")) { ?>	
	<p style="border:1px solid #990000; background-color:#FFDCD6; padding:5px; width: 450px"><?=$error; ?></p>
	<? } ?>
  </div>
	<br clear="all" />
  <div id="leftSide">
<fieldset>
<legend><?=$_SESSION['Leg_8']?> 7 / 10: <?=$_SESSION['Leg_128']?></legend>
	<? echo ($_SESSION['Leg_108'] != "") ? '<p class="pagenote">'.$_SESSION['Leg_108'].'</p>' : ""; ?>
<form action="<?=$_SERVER['PHP_SELF']?>" method="post" name="form1" onsubmit="return checkEntry()">
<input type="hidden" value="2" name="step_8">

	<!-- p><?=$_SESSION['Leg_74']?></p -->
	<label for="name"><?=$_SESSION['Leg_14']?></label>
	<div class="div_blankbox">
		<br clear="all" />
		<div style="float:left; width:90px; padding-right: 10px;"><?=$_SESSION['Leg_68']?></div>
		<div style="float:left;">
			<select name="Filmformat_select" id="Filmformat_select">
			<option value="">-</option>
			<?=$output_Filmformat?>
			</select>
		</div>
		<br clear="all" />
		<div style="float:left; width:90px; padding-right: 10px;"><?=$_SESSION['Leg_69']?></div>
		<div style="float:left;">
			<select name="Bildformat_select" id="Bildformat_select">
			<option value="">-</option>
			<?=$output_Bildformat?>
			</select>
		</div>
		<br clear="all" />
		<div style="float:left; width:90px; padding-right: 10px;"><?=$_SESSION['Leg_70']?></div>
		<div style="float:left;">
			<select name="Tonformat_select" id="Tonformat_select">
			<option value="">-</option>
			<?=$output_Tonformat?>
			</select>
		</div>
		<br clear="all" />
	<img src="/images/add.png" alt="+" width="16" height="16" align="absmiddle" onClick="addEntry()"> <a href="#" onClick="addEntry()"><?=$_SESSION['Leg_78']?></a>
	</div>
	<br clear="all">
	<label for="Vorfuehrkopien"><?=$_SESSION['Leg_94']?></label>	
	<div class="div_blankbox">
		<div id="Vorfuehrkopien"></div>
	</div>
	<div class="button_div">
	<input name="submit" type="submit" id="submit" value="<?=$_SESSION['Leg_32']?>">
	</div>
</form>
<div class="clear"></div>
</fieldset>

  </div>
  <div class="clear"></div>
</div>


<? if (($step == "2") && isset($Produktionsland[0])) {
echo '<script language="javascript">'."\n";
echo 'laender.push("'.implode("\",\"", $Produktionsland).'");'."\n";
echo 'showLaender();'."\n";
echo '</script>'."\n";
}
?>
<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>
</body>
</html>
