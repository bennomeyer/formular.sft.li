<?
session_start();
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
if ((!$_SESSION['login_ok']) || (($_SESSION['film_id'] == "") && ($_SESSION['m'] == "u"))) {
	header("Location: /login.php");
	exit;
}


require_once (__DIR__.'/../includes/db.inc.php');
$q = FX_open_layout("cgi_h_02__filme", "1"); 

$step = (isset($_POST['step9'])) ? $_POST['step9'] : "";
$direction = (isset($_POST['direction'])) ? $_POST['direction'] : "";
$target = (isset($_POST['target'])) ? $_POST['target'] : "";

if(in_array('BuGanz',array_keys($_POST))) {
	$BuGanz = ((count($_POST['BuGanz']))>0) ? $_POST['BuGanz'] : array();
} else {
	$BuGanz = array();
}
if(in_array('BuVorname',array_keys($_POST))) {
	$BuVorname = ((count($_POST['BuVorname']))>0) ? $_POST['BuVorname'] : array();
} else {
	$BuVorname = array();
}
if(in_array('BuName',array_keys($_POST))) {
	$BuName = ((count($_POST['BuName']))>0) ? $_POST['BuName'] : array();
} else {
	$BuName = array();
}
if(in_array('KaGanz',array_keys($_POST))) {
	$KaGanz = ((count($_POST['KaGanz']))>0) ? $_POST['KaGanz'] : array();
} else {
	$KaGanz = array();
}
if(in_array('KaVorname',array_keys($_POST))) {
	$KaVorname = ((count($_POST['KaVorname']))>0) ? $_POST['KaVorname'] : array();
} else {
	$KaVorname = array();
}
if(in_array('KaName',array_keys($_POST))) {
	$KaName = ((count($_POST['KaName']))>0) ? $_POST['KaName'] : array();
} else {
	$KaName = array();
}
if(in_array('MoGanz',array_keys($_POST))) {
	$MoGanz = ((count($_POST['MoGanz']))>0) ? $_POST['MoGanz'] : array();
} else {
	$MoGanz = array();
}
if(in_array('MoVorname',array_keys($_POST))) {
	$MoVorname = ((count($_POST['MoVorname']))>0) ? $_POST['MoVorname'] : array();
} else {
	$MoVorname = array();
}
if(in_array('MoName',array_keys($_POST))) {
	$MoName = ((count($_POST['MoName']))>0) ? $_POST['MoName'] : array();
} else {
	$MoName = array();
}


// UPDATE init - alte Daten aus DB ziehen
//__________________________________________________

if (($_SESSION['m'] == "u") && ($step != "2")) {
	
	
	$find =& $fm->newFindCommand('cgi_k_Stabmitglieder'); 
	$find->addFindCriterion('_kf__Film_Id', $_SESSION['film_id']); 
	$result = $find->execute(); 
	if (!FileMaker::isError($result)) {
		$records = $result->getRecords(); 
		foreach ($records as $record) {
			// Stabdaten "Buch" holen
			if ($record->getField('_kf__Stabbezeichnung_Id') == "2") {		
				$BuGanz[] = htmlspecialchars($record->getField('Vorname').'||'.$record->getField('Name'));
				$BuVorname[] = htmlspecialchars($record->getField('Vorname'));
				$BuName[] = htmlspecialchars($record->getField('Name'));
			}
			// Stabdaten "Kamera" holen
			if ($record->getField('_kf__Stabbezeichnung_Id') == "3") {		
				$KaGanz[] = htmlspecialchars($record->getField('Vorname').'||'.$record->getField('Name'));
				$KaVorname[] = htmlspecialchars($record->getField('Vorname'));
				$KaName[] = htmlspecialchars($record->getField('Name'));
			}
			// Stabdaten "Montage" holen
			if ($record->getField('_kf__Stabbezeichnung_Id') == "4") {		
				$MoGanz[] = htmlspecialchars($record->getField('Vorname').'||'.$record->getField('Name'));
				$MoVorname[] = htmlspecialchars($record->getField('Vorname'));
				$MoName[] = htmlspecialchars($record->getField('Name'));
			}
		}	
	}
	
}


// ERROR-Handling
//__________________________________________________
$error = "";
if ($step == "2") {
	if ((!isset($BuVorname[0])) || (!isset($BuName[0])) || (!isset($KaVorname[0])) || (!isset($KaName[0])) || (!isset($MoVorname[0])) || (!isset($MoName[0])) ) {
		$error .= $_SESSION['Leg_106']."<br />";
	}
} 

if (($step == "2") && ($error != "")) {
	// Update Seite2Flag mit 0
	$q->AddDBParam('-recid', $_SESSION['record_id']); 
	$q->AddDBParam('zz_Anmeldung_Seite09_Flag', "0");
	$DBData = $q->FMEdit(); 	
} elseif (($step == "2") && ($error == "")) {
	// Update Seite2Flag mit 1
	$q->AddDBParam('-recid', $_SESSION['record_id']); 
	$q->AddDBParam('zz_Anmeldung_Seite09_Flag', "1");
	$DBData = $q->FMEdit(); 	
}

// UPDATE final - Daten aktualisieren
//__________________________________________________

if  ((($_SESSION['m'] == "u") && ($step == "2") && ($error == "")) ||
	(($_SESSION['m'] == "u") && ($step == "2") && ($direction == "back"))) {
	// Alle Infos aus Stabbezeichnungs-Tabelle löschen;
	if ($_SESSION['film_id'] != "") {
		$find =& $fm->newFindCommand('cgi_k_Stabmitglieder'); 
		$find->addFindCriterion('_kf__Film_Id', $_SESSION['film_id']); 
		$find->addFindCriterion('_kf__Stabbezeichnung_Id', '2...4'); 
		$result = $find->execute(); 
		if (!FileMaker::isError($result)) {
			$records = $result->getRecords(); 
			foreach ($records as $record) {
				$rec_ID = $record->getRecordId();
				$rec = $fm->getRecordById('cgi_k_Stabmitglieder', $rec_ID); 
				$rec->delete();
			}
		}
	}			
}



// DB-Actions für Neueintragung
//__________________________________________________
if ((($error == "") && ($step == "2") && ($direction == "next")) || (($step == "2") && ($direction == "back"))) {

	// Datenbank-Eintrag der einzelnen Stabmitglieder
		
	$q = FX_open_layout("cgi_k_Stabmitglieder", "1"); 
	for ($i = 0 ; $i < count($BuName) ; $i++) {
		$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);
		$q->AddDBParam('_kf__Stabbezeichnung_Id', "2");	
		$q->AddDBParam('Vorname', htmlspecialchars_decode($BuVorname[$i]));	
		$q->AddDBParam('Name', htmlspecialchars_decode($BuName[$i]));
		$DBData = $q->FMNew(); 		
	}
	for ($i = 0 ; $i < count($KaName) ; $i++) {
		$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);
		$q->AddDBParam('_kf__Stabbezeichnung_Id', "3");	
		$q->AddDBParam('Vorname', htmlspecialchars_decode($KaVorname[$i]));	
		$q->AddDBParam('Name', htmlspecialchars_decode($KaName[$i]));
		$DBData = $q->FMNew(); 		
	}
	for ($i = 0 ; $i < count($MoName) ; $i++) {
		$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);
		$q->AddDBParam('_kf__Stabbezeichnung_Id', "4");	
		$q->AddDBParam('Vorname', htmlspecialchars_decode($MoVorname[$i]));	
		$q->AddDBParam('Name', htmlspecialchars_decode($MoName[$i]));
		$DBData = $q->FMNew(); 		
	}
}


if (($direction == "next") && ($step == "2") && ($error == "")) {
	if ($target != "") {
		header("Location: /$target");
	} else {
		//redirect zur nächsten Seite
		header("Location: /sas/step08_10.php");
	}
	exit;
}
if (($direction == "back") && ($step == "2")) {
	if ($target != "") {
		header("Location: /$target");
	} else {
		//redirect zur vorherigen Seite
		header("Location: /sas/step08_8.php");
	}
	exit;
}

include($_SERVER['DOCUMENT_ROOT']. "/includes/x_get_value_list2.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Online Registration</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <link href="/css/style2008.css" rel="stylesheet" type="text/css" title="KFT" />
<script type="text/javascript" src="/includes/scripts.js"></script>
<link rel="stylesheet" type="text/css" href="/includes/yui/container/assets/container.css" />
<script type="text/javascript" src="/includes/yui/fileupload/yahoo-dom-event.js"></script>
<script type="text/javascript" src="/includes/yui/animation/animation-min.js"></script>
<script type="text/javascript" src="/includes/yui/dragdrop/dragdrop-min.js"></script>
<script type="text/javascript" src="/includes/yui/fileupload/connection.js"></script>
<script type="text/javascript" src="/includes/yui/fileupload/container.js"></script>
<script type="text/javascript" src="/includes/yui/fileupload/Ext.js"></script>
<script type="text/javascript" src="/includes/yui/fileupload/DomQuery.js"></script>
<script language="javascript">

function htmlspecialchars(str,typ) {
  if(typeof str=="undefined") str="";
  if(typeof typ!="number") typ=2;
  typ=Math.max(0,Math.min(3,parseInt(typ)));
  var from=new Array(/&/g,/</g,/>/g);
  var to=new Array("&amp;","&lt;","&gt;");
  if(typ==1 || typ==3) {from.push(/'/g); to.push("&#039;");}
  if(typ==2 || typ==3) {from.push(/"/g); to.push("&quot;");}
  for(var i in from) str=str.replace(from[i],to[i]);
  return str;
}
var bu = new Array;
function addBu() {
	if ((document.getElementById('BuVor').value != "") && (document.getElementById('BuNach').value != "")) {
		bu.push(document.getElementById('BuVor').value+'||' +document.getElementById('BuNach').value);
		document.getElementById('BuVor').value = "";
		document.getElementById('BuNach').value = "";
		showBu();		
	}
}
function delBu(i) { bu.splice(i,1);	showBu(); }
function showBu() {
	var output = "";
	for (var i = 0; i < bu.length; ++i) {
		var teil = bu[i].split("||");
 		output += "<div id=\"list_values\"><input type=\"hidden\" name=\"BuGanz[]\" value=\"" + htmlspecialchars(bu[i]) + "\"><input type=\"hidden\" name=\"BuVorname[]\" value=\"" + htmlspecialchars(teil[0]) + "\"><input type=\"hidden\" name=\"BuName[]\" value=\"" + htmlspecialchars(teil[1]) + "\">" + teil[0] + " " + teil[1] + "</div><div id=\"list_delicons\"><img src=\"/images/delete.png\" onClick=\"delBu(" + i + ")\" align=\"absmiddle\" title=\"<?=$_SESSION['Leg_88']?>\"></div><br clear \"all\" />\n";
	}
	document.getElementById('Bu_div').innerHTML = output;
	document.getElementById('Bu_div').style.display = "block";
}


var ka = new Array;
function addKa() {
	if ((document.getElementById('KaVor').value != "") && (document.getElementById('KaNach').value != "")) {
		ka.push(document.getElementById('KaVor').value+'||' +document.getElementById('KaNach').value);
		document.getElementById('KaVor').value = "";
		document.getElementById('KaNach').value = "";
		showKa();		
	}
}
function delKa(i) { ka.splice(i,1);	showKa(); }
function showKa() {
	var output = "";
	for (var i = 0; i < ka.length; ++i) {
		var teil = ka[i].split("||");
 		output += "<div id=\"list_values\"><input type=\"hidden\" name=\"KaGanz[]\" value=\"" + htmlspecialchars(ka[i]) + "\"><input type=\"hidden\" name=\"KaVorname[]\" value=\"" + htmlspecialchars(teil[0]) + "\"><input type=\"hidden\" name=\"KaName[]\" value=\"" + htmlspecialchars(teil[1]) + "\">" + teil[0] + " " + teil[1] + "</div><div id=\"list_delicons\"><img src=\"/images/delete.png\" onClick=\"delKa(" + i + ")\" align=\"absmiddle\" title=\"<?=$_SESSION['Leg_88']?>\"></div><br clear \"all\" />\n";
	}
	document.getElementById('Ka_div').innerHTML = output;
	document.getElementById('Ka_div').style.display = "block";
}


var mo = new Array;
function addMo() {
	if ((document.getElementById('MoVor').value != "") && (document.getElementById('MoNach').value != "")) {
		mo.push(document.getElementById('MoVor').value+'||' +document.getElementById('MoNach').value);
		document.getElementById('MoVor').value = "";
		document.getElementById('MoNach').value = "";
		showMo();		
	}
}
function delMo(i) { mo.splice(i,1);	showMo(); }
function showMo() {
	var output = "";
	for (var i = 0; i < mo.length; ++i) {
		var teil = mo[i].split("||");
 		output += "<div id=\"list_values\"><input type=\"hidden\" name=\"MoGanz[]\" value=\"" + htmlspecialchars(mo[i]) + "\"><input type=\"hidden\" name=\"MoVorname[]\" value=\"" + htmlspecialchars(teil[0]) + "\"><input type=\"hidden\" name=\"MoName[]\" value=\"" + htmlspecialchars(teil[1]) + "\">" + teil[0] + " " + teil[1] + "</div><div id=\"list_delicons\"><img src=\"/images/delete.png\" onClick=\"delMo(" + i + ")\" align=\"absmiddle\" title=\"<?=$_SESSION['Leg_88']?>\"></div><br clear \"all\" />\n";
	}
	document.getElementById('Mo_div').innerHTML = output;
	document.getElementById('Mo_div').style.display = "block";
}
</script>	
<script language="javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>
</head>
<body onload="MM_preloadImages('/images/loader.gif')">

<script type="text/javascript" src="/includes/safari_hack.js"></script>
<script type="text/javascript" language="javascript">
<? include($_SERVER['DOCUMENT_ROOT'].'/includes_js/inits.js.php'); ?>	
function init() {
	<? include($_SERVER['DOCUMENT_ROOT'].'/includes_js/callbacks.js.php'); ?>
<? include($_SERVER['DOCUMENT_ROOT'].'/includes_js/newPerson_dialog.js.php'); ?>	
<? include($_SERVER['DOCUMENT_ROOT'].'/includes_js/addStab_dialog.js.php'); ?>	
}
<? include($_SERVER['DOCUMENT_ROOT'].'/includes_js/person_handler.js.php'); ?>	
<? include($_SERVER['DOCUMENT_ROOT'].'/includes_js/stab_handler.js.php'); ?>
function goBack() {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv_sas']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = "back";
	document.form1.submit();
}
function goNext() {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv_sas']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = "next";
	document.form1.submit();
}
function jumpto(target,direction) {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv_sas']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = direction;
	document.getElementById('target').value = target;
	document.form1.submit();
}
</script>
<div id="container">
  <div id="top">
    <? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
  </div>
	<br clear="all" />
<div id="navi"><? $seite = "9"; include($_SERVER['DOCUMENT_ROOT'].'/includes/navigation_sas.inc.php'); ?><div id="loader" style="display:none"><img src="/images/loader.gif" style="margin-top: 8px; margin-left:45px" width="32" height="32" /></div></div>
  <div id="leftSide">
<fieldset>
<p class="legend"><?=$_SESSION['Leg_8']?> 9 / 13: <?=$_SESSION['Leg_126']?></p>

	<? if ($error != "") { ?>	
	<p style="border:1px solid #990000; background-color:#FFDCD6; padding:5px; width: 545px"><?=$error; ?></p>
	<? } ?>
	
	<? echo ($_SESSION['Leg_106'] != "") ? '<p class="pagenote">'.$_SESSION['Leg_106'].'</p>' : ""; ?>
<form action="step08_9.php" method="post" name="form1">
<input type="hidden" value="2" name="step9" />
<input type="hidden" name="direction" id="direction" value="" />
<input type="hidden" name="target" id="target" value="" />
	<label for="Bu"><?=$_SESSION['Leg_262']?>*</label>
	<div class="div_blankbox">
		<div id="select_div" style="float:left; width: 95px;">
			<?=$_SESSION['Leg_34']?><br />
			<input type="text" name="BuVor" id="BuVor" value="" style="width: 90px; font-size:11px" /> 
		</div>
		<div id="select_div" style="float:left; width: 115px;">
			<?=$_SESSION['Leg_35']?><br />
			<input type="text" name="BuNach" id="BuNach" value="" style="width: 90px; font-size:11px" />
			<img src="/images/arrow.png" alt="<?=$_SESSION['Leg_78']?>" align="absmiddle" onclick="addBu()" title="<?=$_SESSION['Leg_78']?>" /><br />
		</div>
		<div id="Bu_div" class="list_div"> </div>
		<br clear="all" />
	</div>
	<br clear="all" />
	
	
	<label for="Ka"><?=$_SESSION['Leg_263']?>*</label>
	<div class="div_blankbox">
		<div id="select_div" style="float:left; width: 95px;">
			<?=$_SESSION['Leg_34']?><br />
			<input type="text" name="KaVor" id="KaVor" value="" style="width: 90px; font-size:11px" /> 
		</div>
		<div id="select_div" style="float:left; width: 115px;">
			<?=$_SESSION['Leg_35']?><br />
			<input type="text" name="KaNach" id="KaNach" value="" style="width: 90px; font-size:11px" />
			<img src="/images/arrow.png" alt="<?=$_SESSION['Leg_78']?>" align="absmiddle" onclick="addKa()" title="<?=$_SESSION['Leg_78']?>" /><br />
		</div>
		<div id="Ka_div" class="list_div"> </div>
		<br clear="all" />
	</div>
	<br clear="all" />
	
	<label for="Mo"><?=$_SESSION['Leg_264']?>*</label>
	<div class="div_blankbox">
		<div id="select_div" style="float:left; width: 95px;">
			<?=$_SESSION['Leg_34']?><br />
			<input type="text" name="MoVor" id="MoVor" value="" style="width: 90px; font-size:11px" /> 
		</div>
		<div id="select_div" style="float:left; width: 115px;">
			<?=$_SESSION['Leg_35']?><br />
			<input type="text" name="MoNach" id="MoNach" value="" style="width: 90px; font-size:11px" />
			<img src="/images/arrow.png" alt="<?=$_SESSION['Leg_78']?>" align="absmiddle" onclick="addMo()" title="<?=$_SESSION['Leg_78']?>" /><br />
		</div>
		<div id="Mo_div" class="list_div"> </div>
		<br clear="all" />
	</div>
	
	<br clear="all" />
	<div class="prevBtn"><input type="button" id="back" value="<?=$_SESSION['Leg_31']?>" onclick="goBack('')" /></div>
	<div class="nxtBtn"><input type="button" id="next" value="<?=$_SESSION['Leg_32']?>" onclick="goNext('')" /></div>
	<br clear="all" />
</form>

<div class="clear"></div>
</fieldset>

  </div>
	<br clear="all" />
  <div class="clear"></div>
</div>

<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>


<? 

if ((($step == "2") && isset($BuGanz[0])) || (($_SESSION['m'] == "u") && isset($BuGanz[0]))) {
echo '<script language="javascript">'."\n";
echo 'bu.push("'.implode("\",\"", $BuGanz).'");'."\n";
echo 'showBu();'."\n";
echo '</script>'."\n";
}
if ((($step == "2") && isset($KaGanz[0])) || (($_SESSION['m'] == "u") && isset($KaGanz[0]))) {
echo '<script language="javascript">'."\n";
echo 'ka.push("'.implode("\",\"", $KaGanz).'");'."\n";
echo 'showKa();'."\n";
echo '</script>'."\n";
}
if ((($step == "2") && isset($MoGanz[0])) || (($_SESSION['m'] == "u") && isset($MoGanz[0]))) {
echo '<script language="javascript">'."\n";
echo 'mo.push("'.implode("\",\"", $MoGanz).'");'."\n";
echo 'showMo();'."\n";
echo '</script>'."\n";
}
?>
</body>
</html>
