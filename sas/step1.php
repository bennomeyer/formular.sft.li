<?
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
session_start();
if (!isset($_SESSION['Leg_1'])) {
	$location = "/index.php?error=Session%20abgelaufen&typ=".$_SESSION['typ'];
	// redirect zur�ck zur Startseite wg. abgelaufener Session
	header("Location: $location");
	exit;
}

if (!empty($_SESSION['step1'])) {
	// redirect zur�ck zur nachfolgendenn Seite (Back.Button aushebeln)
	header("Location: step2.php");
	exit;
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/classes/FX/FX.php"); 

$step = (isset($_POST['step'])) ? $_POST['step'] : "";
$Filmtitel = (isset($_POST['Filmtitel'])) ? $_POST['Filmtitel'] : "";
if(in_array('Genre',array_keys($_POST))) {
	$Genre = ((count($_POST['Genre']))>0) ? $_POST['Genre'] : array();
} else {
	$Genre = array();
}
$Produktionsjahr = (isset($_POST['Produktionsjahr'])) ? $_POST['Produktionsjahr'] : "";
$Produktionsland = (!empty($_POST['Produktionsland'])) ? $_POST['Produktionsland'] : array();

if(in_array('Originalsprache',array_keys($_POST))) {
	$Originalsprache = ((count($_POST['Originalsprache']))>0) ? $_POST['Originalsprache'] : array();
} else {
	$Originalsprache = array();
}
$Dauer = (isset($_POST['Dauer'])) ? $_POST['Dauer'] : "";
$Farbe = (isset($_POST['Farbe'])) ? $_POST['Farbe'] : "";

// ERROR-Handling
//__________________________________________________
$error = "";
if ($step == "2") {
	$error .= ($Filmtitel == "") ? $_SESSION['Leg_76']."<br />" : "";
	$error .= ((!isset($Produktionsland[0])) || (!isset($Originalsprache[0]))) ? $_SESSION['Leg_75']."<br />" : "";
	$error .= (($Dauer < 1) || ($Dauer > 20) || (!(preg_match("/^[0-9]{1,3}$/",$Dauer)))) ? $_SESSION['Leg_147']."<br />" : "";
	$error .= ($Farbe == "") ? $_SESSION['Leg_74']."<br />" : "";
} 


// DB-Actions
//__________________________________________________
if (($error == "") && ($step == "2")) {
	//Film wird neu eingetragen
	$q = new FX('host8.kon5.net', '80', 'FMPro7');
	$q->SetDBData("Filmtage-Solothurn", "cgi_h_02__filme", "1"); 
	$q->SetDBUserPass ("Admin", "xs4kon5");
	$q->AddDBParam('Filmtitel', utf8_encode($Filmtitel));
	$q->AddDBParam('Produktionsjahr', utf8_encode($Produktionsjahr));	
	$q->AddDBParam('Dauer_Minuten', utf8_encode($Dauer));	
	$q->AddDBParam('_kf__farbe_oder_sw', utf8_encode($Farbe));
	$DBData = $q->FMNew(); 		

	foreach ($DBData['data'] as $key => $value) {
		$film_id = $value['_kp__id'][0];
		$_SESSION['film_id'] = $film_id;
		$_SESSION['record_id'] = $value['_record__id'][0];
	}
	if (isset($film_id)) {
	
		// Genre-Film Kreuztabelle
		$q = new FX('host8.kon5.net', '80', 'FMPro7');
		$q->SetDBData("Filmtage-Solothurn", "cgi_k_11__filme_genres", "1"); 
		$q->SetDBUserPass ("Admin", "xs4kon5");
		$q->AddDBParam('_kf__Film_Id', $film_id);	
		$q->AddDBParam('_kf__Genre_Id', '5');	
		$DBData = $q->FMNew(); 		
		
		// Land-Film Kreuztabelle
		if (isset($Produktionsland[0])) {
			$i = 1;
			foreach ($Produktionsland as $land_id) { 
				$land = explode("|", $land_id);
				$land_code = $land[0];
				$q = new FX('host8.kon5.net', '80', 'FMPro7');
				$q->SetDBData("Filmtage-Solothurn", "cgi_k_07__filme_ISO_laender", "1"); 
				$q->SetDBUserPass ("Admin", "xs4kon5");
				$q->AddDBParam('_kf__Film_Id', $film_id);	
				$q->AddDBParam('_kf__ISO_Laender_ID', $land_code);	
				$q->AddDBParam('Hierarchienummer', $i);	
				$DBData = $q->FMNew(); 
				$i++;		
			}
		}				
		// Sprache-Film Kreuztabelle
		if (isset($Originalsprache[0])) {
			$i = 1;
			foreach ($Originalsprache as $sprache_id) { 
				$sprache = explode("|", $sprache_id);
				$sprache_code = $sprache[0];
				$q = new FX('host8.kon5.net', '80', 'FMPro7');
				$q->SetDBData("Filmtage-Solothurn", "cgi_k_09__filme_ISO_sprachen", "1"); 
				$q->SetDBUserPass ("Admin", "xs4kon5");
				$q->AddDBParam('_kf__Film_Id', $film_id);	
				$q->AddDBParam('_kf__ISO_Sprachen_Id', $sprache_code);	
				$q->AddDBParam('Hierarchienummer', $i);	
				$DBData = $q->FMNew(); 
				$i++;		
			}
		}				
	}	
	$_SESSION['step1'] = "OK";
	// redirect zur n�chsten Seite
	header("Location: /sas/step2.php");
	exit;
}

include($_SERVER['DOCUMENT_ROOT']. "/includes/x_get_value_list1.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Online Registration</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="expires" content="01.01.1970" />
 <link href="/css/style.css" rel="stylesheet" type="text/css" title="KFT" />
<script language="javascript">
var laender = new Array;
function addLand() {
	if (document.getElementById('Produktionsland_select').value != "") {
		laender.push(document.getElementById('Produktionsland_select').value);
		document.getElementById('Produktionsland_select').value = "";
		showLaender();		
	}
}
function delLand(i) {
	laender.splice(i,1);	
	showLaender();	
}
function showLaender() {
	var output = "";
	for (var i = 0; i < laender.length; ++i) {
		var land = laender[i].split("|");
		var land_code = land[0];
		var land_txt = land[1];
 		output += "<input type=\"hidden\" name=\"Produktionsland[]\" value=\"" + laender[i] + "\">" + land_txt + "<img src=\"/images/delete.png\" onClick=\"delLand(" + i + ")\" align=\"absmiddle\" title=\"<?=$_SESSION['Leg_88']?>\"><br />\n";
	}
	document.getElementById('Laender').innerHTML = "<p>"+ output +"</p>";
	document.getElementById('Laender').style.display = "block";
}
var sprachen = new Array;
function addSprache() {
	if (document.getElementById('Originalsprache_select').value != "") {
		sprachen.push(document.getElementById('Originalsprache_select').value);
		document.getElementById('Originalsprache_select').value = "";
		showSprachen();		
	}
}
function delSprache(i) {
	sprachen.splice(i,1);	
	showSprachen();	
}
function showSprachen() {
	var output_sprachen = "";
	for (var i = 0; i < sprachen.length; ++i) {
		var sprache = sprachen[i].split("|");
		var sprache_code = sprache[0];
		var sprache_txt = sprache[1];
 		output_sprachen += "<input type=\"hidden\" name=\"Originalsprache[]\" value=\"" + sprachen[i] + "\">" + sprache_txt + "<img src=\"/images/delete.png\" onClick=\"delSprache(" + i + ")\" align=\"absmiddle\" title=\"<?=$_SESSION['Leg_88']?>\"><br />\n";
	}
	document.getElementById('Sprachen').innerHTML = "<p>"+ output_sprachen +"</p>";
	document.getElementById('Sprachen').style.display = "block";
}
</script> 
</head>
<body>


<div id="container">
  <div id="top">
    <? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
	<? if ($error != "") { ?>	
	<p style="border:1px solid #990000; background-color:#FFDCD6; padding:5px; width: 450px"><?=$error; ?></p>
	<? } ?>
  </div>
	<br clear="all" />
  <div id="leftSide">
<fieldset>
<legend><?=$_SESSION['Leg_8']?> 1 / 10: <?=$_SESSION['Leg_121']?></legend>
<? echo ($_SESSION['Leg_101'] != "") ? '<p class="pagenote">'.$_SESSION['Leg_101'].'</p>' : ""; ?>
<form action="<?=$_SERVER['PHP_SELF']?>" method="post" name="form1">
<input type="hidden" value="2" name="step">
	<label for="name"><?=$_SESSION['Leg_1']?></label>
    <div class="div_texbox">
    <input name="Filmtitel" type="text" class="textbox" id="Filmtitel" value="<?=$Filmtitel?>" />
	</div>
	<br clear="all">
	<label for="Produktionsjahr"><?=$_SESSION['Leg_3']?></label>
	<div class="div_blankbox">
    <select name="Produktionsjahr">
	<option value="">-</option>
	<?=$output_Jahr?>
	</select>
	</div>
	<br clear="all">
	<label for="Produktionsland"><?=$_SESSION['Leg_33']?></label>
	<div class="div_blankbox">
    <select name="Produktionsland_select" id="Produktionsland_select">
	<option value="">-</option>
	<?=$output_Land?>
	</select>
	<img src="/images/add.png" alt="<?=$_SESSION['Leg_78']?>" width="16" height="16" align="absmiddle" onClick="addLand()" title="<?=$_SESSION['Leg_78']?>"><br />
	<?=$_SESSION['Leg_136']?>
	<div id="Laender"></div>
	</div>
	<br clear="all">
	<label for="Originalsprache"><?=$_SESSION['Leg_4']?></label>
	<div class="div_blankbox">
    <select name="Originalsprache_select" id="Originalsprache_select">
	<option value="">-</option>
	<?=$output_Sprache?>
	</select>
	<img src="/images/add.png" alt="<?=$_SESSION['Leg_78']?>" width="16" height="16" align="absmiddle" onClick="addSprache()" title="<?=$_SESSION['Leg_78']?>"><br />
	<?=$_SESSION['Leg_137']?>
	<div id="Sprachen"></div>
	</div>
	<br clear="all">
	<label for="Dauer"><?=$_SESSION['Leg_5']?></label>
    <div class="div_texbox">
    <input name="Dauer" type="text" class="textbox_short" id="Dauer" value="<?=$Dauer?>" /> 
    <?=$_SESSION['Leg_6']?> (1-20)
	</div>
	<br clear="all">
	<label for="Farbe"><?=$_SESSION['Leg_7']?></label>
    <div class="div_blankbox">
    <?=$output_Farbe?>
	</div>
	<br clear="all">
	<div class="button_div">
	<input name="submit" type="submit" id="submit" value="<?=$_SESSION['Leg_32']?>">
	</div>
</form>
<div class="clear"></div>
</fieldset>

  </div>
  <div class="clear"></div>
</div>


<? if (($step == "2") && isset($Produktionsland[0])) {
echo '<script language="javascript">'."\n";
echo 'laender.push("'.implode("\",\"", $Produktionsland).'");'."\n";
echo 'showLaender();'."\n";
echo '</script>'."\n";
}
?>
<? if (($step == "2") && isset($Originalsprache[0])) {
echo '<script language="javascript">'."\n";
echo 'sprachen.push("'.implode("\",\"", $Originalsprache).'");'."\n";
echo 'showSprachen();'."\n";
echo '</script>'."\n";
}
?>
<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>

</body>
</html>
