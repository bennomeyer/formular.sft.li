<?
session_start();
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
if ((!$_SESSION['login_ok']) || (($_SESSION['film_id'] == "") && ($_SESSION['m'] == "u"))) {
	header("Location: login.php");
	exit;
}

require_once ('includes/db.inc.php');
$q = FX_open_layout( "cgi_h_02__filme", "1"); 

$step = (isset($_POST['step_12'])) ? $_POST['step_12'] : "";
$direction = (isset($_POST['direction'])) ? $_POST['direction'] : "";
$target = (isset($_POST['target'])) ? $_POST['target'] : "";

$Budget = (isset($_POST['Budget'])) ? $_POST['Budget'] : "";
$Anteil = (isset($_POST['Anteil'])) ? $_POST['Anteil'] : "";
$Finanzkategorie = (!empty($_POST['Finanzkategorie'])) ? $_POST['Finanzkategorie'] : array();
$Betrag = (!empty($_POST['Betrag'])) ? $_POST['Betrag'] : array();


// UPDATE init - alte Daten aus DB ziehen
//__________________________________________________

if (($_SESSION['m'] == "u") && ($step != "2")) {
	
	// Filmdaten holen
	$find =& $fm->newFindCommand('cgi_h_02__filme'); 
	$find->addFindCriterion('_kp__id', $_SESSION['film_id']); 
	$find->addFindCriterion('_kf__Anmeldeuser_Id', $_SESSION['user_id']); 
	$result = $find->execute(); 
	if (!FileMaker::isError($result)) {
		$records = $result->getRecords(); 
		$record = $records[0];
		$Budget = $record->getField('Budget');
		$Anteil = $record->getField('CH_Finanzierungsanteil_Prozent');
		
		// Infos aus Kreuztabellen holen
		$relatedSet = $record->getRelatedSet('zz_Finanzierung');
		if (!FileMaker::isError($relatedSet)) {
			foreach ($relatedSet as $relatedRow) {
				$Finanzkategorie[] = $relatedRow->getField('zz_Finanzierung::_kf__finanzierungskategorie_Id').'|'.$relatedRow->getField('zz_Finanzierungskategorien::Kategorie_'.$_SESSION['sprache']);
				$Betrag[] = $relatedRow->getField('zz_Finanzierung::Betrag');
			}
		}
	}
}


// ERROR-Handling
//__________________________________________________
$error = "";
if ($step == "2") {
	//if ((empty($_POST['Sprache'])) || (empty($_POST['Filmformat'])) || (empty($_POST['Bildformat'])) || (empty($_POST['Tonformat'])) ) $error .= $_SESSION['Leg_92'];
} 

if (($step == "2") && ($error != "")) {
	// Update Seite2Flag mit 0
	$q->AddDBParam('-recid', $_SESSION['record_id']); 
	$q->AddDBParam('zz_Anmeldung_Seite12_Flag', "0");
	$DBData = $q->FMEdit(); 	
} elseif (($step == "2") && ($error == "")) {
	// Update Seite2Flag mit 1
	$q->AddDBParam('-recid', $_SESSION['record_id']); 
	$q->AddDBParam('zz_Anmeldung_Seite12_Flag', "1");
	$DBData = $q->FMEdit(); 	
}



// UPDATE final - Daten aktualisieren
//__________________________________________________

if ((($_SESSION['m'] == "u") && ($step == "2") && ($error == "")) || (($_SESSION['m'] == "u") && ($step == "2") && ($direction == "back"))) {
	
	// Alle Infos aus den Kreuztabellen löschen
	if ($_SESSION['film_id'] != "") {
		$find =& $fm->newFindCommand('cgi_k_27__filme_finanzierung'); 
		$find->addFindCriterion('_kf__Film_Id', $_SESSION['film_id']); 
		$result = $find->execute(); 
		if (!FileMaker::isError($result)) {
			$records = $result->getRecords(); 
			foreach ($records as $record) {
				$rec_ID = $record->getRecordId();
				$rec = $fm->getRecordById('cgi_k_27__filme_finanzierung', $rec_ID); 
				$rec->delete();
			}
		}
	}
}



// DB-Actions für Neueintragung & Update
//__________________________________________________
if (($error == "") && ($step == "2")) {
	
	//Update im Film-Eintrag
	$q = FX_open_layout( "cgi_h_02__filme", "1"); 
	$q->AddDBParam('-recid', $_SESSION['record_id']); 
	$q->AddDBParam('Budget', $Budget);
	$q->AddDBParam('CH_Finanzierungsanteil_Prozent', $Anteil);
	$DBData = $q->FMEdit(); 		
	
	
	//Eintrag der Kat/Betrag-Kreuz...
	if (isset($Finanzkategorie[0])) {
		$i = 0;
		foreach ($Finanzkategorie as $finanzkategorie_id) { 
			$finanzkategorie = explode("|", $Finanzkategorie[$i]);
	
			$q = FX_open_layout( "cgi_k_27__filme_finanzierung", "1"); 
			$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);	
			$q->AddDBParam('_kf__finanzierungskategorie_Id', $finanzkategorie[0]);	
			$q->AddDBParam('Betrag', $Betrag[$i]);	
			$DBData = $q->FMNew();
			$i++;		
		}
	}	
}

if (($direction == "next") && ($step == "2") && ($error == "")) {
	if ($target != "") {
		header("Location: /$target");
	} else {
		//redirect zur nächsten Seite
		header("Location: /step08_13.php");
	}
	exit;
}
if (($direction == "back") && ($step == "2")) {
	if ($target != "") {
		header("Location: /$target");
	} else {
		//redirect zur vorherigen Seite
		header("Location: /step08_11.php");
	}
	exit;
}

include($_SERVER['DOCUMENT_ROOT']. "/includes/x_get_value_list9.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Online Registration</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <link href="/css/style2008.css" rel="stylesheet" type="text/css" title="KFT" />
 
<script type="text/javascript" src="includes/scripts.js"></script>
<script type="text/javascript" language="javascript">
<!--
	var ol_width = 300;
	var ol_fgcolor='#d9e2ec';
	var ol_bgcolor='#006699';
//-->
</script>
<link rel="stylesheet" type="text/css" href="includes/yui/container/assets/container.css" />
<script type="text/javascript" src="includes/yui/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript" src="includes/yui/animation/animation-min.js"></script>
<script type="text/javascript" src="includes/yui/dragdrop/dragdrop-min.js"></script>
<script type="text/javascript" src="includes/yui/connection/connection.js"></script>
<script type="text/javascript" src="includes/yui/container/container.js"></script>
<script language="javascript">
var finanzkategorien = new Array;
var betraege = new Array;

function addEntry() {
	if 	(
		(document.getElementById('Finanzkategorie_select').value != "") &&
		(document.getElementById('Betrag_select').value != "")
		) {
		finanzkategorien.push(document.getElementById('Finanzkategorie_select').value);
		document.getElementById('Finanzkategorie_select').value = "";
		betraege.push(document.getElementById('Betrag_select').value);
		document.getElementById('Betrag_select').value = "";
		showEntries();		
	} else {
		miniDialog = new YAHOO.widget.SimpleDialog("mini_dlg", { visible:false, width: "25em", effect:[
{effect:YAHOO.widget.ContainerEffect.SLIDE,duration:0.25}, {effect:YAHOO.widget.ContainerEffect.FADE,duration:0.25}
], fixedcenter : true, modal:true, draggable:false });
		miniDialog.setHeader("<?=$_SESSION['Leg_91']?>");
		miniDialog.setBody("<?=$_SESSION['Leg_95']?>");
		miniDialog.cfg.queueProperty("icon",YAHOO.widget.SimpleDialog.ICON_WARN);
		miniDialog.cfg.queueProperty("buttons", [ 
			{ text: "<?=$_SESSION['Leg_93']?>", handler:handlerHide, isDefault:true  }
		]);
		miniDialog.render(document.body);
		miniDialog.show();
	}
}
function delEntry(i) {
	finanzkategorien.splice(i,1);
	betraege.splice(i,1);
	showEntries();	
}
function showEntries() {
	var output = "";
	for (var i = 0; i < finanzkategorien.length; ++i) {
		var finanzkategorie = finanzkategorien[i].split("|");
 		output += "<div id=\"list_values\"><input type=\"hidden\" name=\"Finanzkategorie[]\" value=\"" + finanzkategorien[i] + "\">" + finanzkategorie[1] + "<br />\n";
 		output += "<input type=\"hidden\" name=\"Betrag[]\" value=\"" + betraege[i] + "\">" + betraege[i] + " CHF<br />&nbsp;<br>\n";
 		output += "</div><div id=\"list_delicons\"><img src=\"images/delete.png\" onClick=\"delEntry(" + i + ")\" align=\"absmiddle\" title=\"<?=$_SESSION['Leg_88']?>\"></div><br clear \"all\" />\n";
	}
	document.getElementById('Finanzkategorien').innerHTML = "<p>"+ output +"</p>";
	document.getElementById('Finanzkategorien').style.display = "block";
}
function checkEntry() {
	if 	(
		(document.getElementById('Finanzkategorie_select').value == "") &&
		(document.getElementById('Betrag_select').value == "") 
		) { 
			return true; 
		} else {
			return false;
		}
	
}

function goBack() {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = "back";
	document.form1.submit();
}
function goNext() {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = "next";
	document.form1.submit();
}
function jumpto(target,direction) {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = direction;
	document.getElementById('target').value = target;
	document.form1.submit();
}
</script> 
<script language="javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>
</head>
<body onload="MM_preloadImages('images/loader.gif')">


<div id="container">
  <div id="top">
    <? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
  </div>
	<br clear="all" />
<div id="navi"><? $seite = "12"; include($_SERVER['DOCUMENT_ROOT'].'/includes/navigation.inc.php'); ?><div id="loader" style="display:none"><img src="/images/loader.gif" style="margin-top: 8px; margin-left:45px" width="32" height="32" /></div></div>
  <div id="leftSide">
<fieldset>
<p class="legend"><?=$_SESSION['Leg_8']?> 12 / 14: <?=$_SESSION['Leg_129']?></p>
	<? if (($error != "") && ($step == "2")) { ?>	
	<p style="border:1px solid #990000; background-color:#FFDCD6; padding:5px; width: 545px"><?=$error; ?></p>
	<? } ?>
	<? echo ($_SESSION['Leg_109'] != "") ? '<p class="pagenote">'.$_SESSION['Leg_109'].'</p>' : ""; ?>
<form action="<?=$_SERVER['PHP_SELF']?>" method="post" name="form1">
<input type="hidden" value="2" name="step_12" />
<input type="hidden" name="direction" id="direction" value="" />
<input type="hidden" name="target" id="target" value="" />

	<!-- p><?=$_SESSION['Leg_74']?></p -->
	<label for="name"><?=$_SESSION['Leg_16']?></label>
	<div class="div_blankbox">
		<?=$_SESSION['Leg_17']?> <input type="text" name="Budget" id="Budget" class="textbox_medium" value="<?=$Budget?>" /><br />&nbsp;<br />
		<?=$_SESSION['Leg_18']?> <input type="text" name="Anteil" class="textbox_short" id="Anteil"  value="<?=$Anteil?>" />
	</div>
	<br clear="all" />
	<label for="Finanzkategorien"><?=$_SESSION['Leg_19']?></label>	
	<div class="div_blankbox">
		
		<div id="select_div" style="float:left; width: 210px;">
			<?=$_SESSION['Leg_96']?><br />
			<select name="Finanzkategorie_select" id="Finanzkategorie_select">
			<option value="">-</option>
			<?=$output_Finanzkategorie?>
			</select><br />
			<?=$_SESSION['Leg_97']?><br />
			<input type="text" name="Betrag_select" class="textbox_medium" id="Betrag_select" /> <img src="images/arrow.png" alt="<?=$_SESSION['Leg_78']?>" align="absmiddle" onclick="addEntry()" title="<?=$_SESSION['Leg_78']?>" /><br />
		</div>
		<div id="Finanzkategorien" class="list_div_high"></div>
		<br clear="all" />
	</div>
	<div class="prevBtn"><input type="button" id="back" value="<?=$_SESSION['Leg_31']?>" onclick="goBack('')" /></div>
	<div class="nxtBtn"><input type="button" id="next" value="<?=$_SESSION['Leg_32']?>" onclick="goNext('')" /></div>
	<br clear="all" />
</form>
<div class="clear"></div>
</fieldset>

  </div>
	<br clear="all" />
  <div class="clear"></div>
</div>


<? if ((($step == "2") && isset($Finanzkategorie[0])) || (($_SESSION['m'] == "u") && isset($Finanzkategorie[0]))) {
echo '<script language="javascript">'."\n";
echo 'finanzkategorien.push("'.implode("\",\"", $Finanzkategorie).'");'."\n";
echo 'betraege.push("'.implode("\",\"", $Betrag).'");'."\n";
echo 'showEntries();'."\n";
echo '</script>'."\n";
}
?>
<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>
</body>
</html>
