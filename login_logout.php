<?
# preserve language during logout
$link = 'login.php';
if (isset($_SESSION['sprache'])) $link .= '?sprache='.$_SESSION['sprache'];
session_start();
session_destroy();
header('Location: '.$link);
?>