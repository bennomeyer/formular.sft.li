<?
session_start();
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
if ((!$_SESSION['login_ok']) || (($_SESSION['film_id'] == "") && ($_SESSION['m'] == "u"))) {
	header("Location: login.php");
	exit;
}

require_once ('includes/db.inc.php');
$q = FX_open_layout( "cgi_h_02__filme", "1"); 

$step = (isset($_POST['step_11'])) ? $_POST['step_11'] : "";
$direction = (isset($_POST['direction'])) ? $_POST['direction'] : "";
$target = (isset($_POST['target'])) ? $_POST['target'] : "";
$Premiere = "";
$PremiereMonat = "";
$PremiereLand = "";
$FS_Datum = (!empty($_POST['FS_Datum'])) ? $_POST['FS_Datum'] : array();
$FS_Land = (!empty($_POST['FS_Land'])) ? $_POST['FS_Land'] : array();
$FS_Txt = (!empty($_POST['FS_Txt'])) ? $_POST['FS_Txt'] : array();
$VV_Land = (!empty($_POST['VV_Land'])) ? $_POST['VV_Land'] : array();
$VV_Txt = (!empty($_POST['VV_Txt'])) ? $_POST['VV_Txt'] : array();
$isan = (isset($_POST['isan'])) ? $_POST['isan'] : "";


// UPDATE init - alte Daten aus DB ziehen
//__________________________________________________

if (($_SESSION['m'] == "u") && ($step != "2")) {


	
	// Filmdaten holen
	$find =& $fm->newFindCommand('cgi_h_02__filme'); 
	$find->addFindCriterion('_kp__id', $_SESSION['film_id']); 
	$find->addFindCriterion('_kf__Anmeldeuser_Id', $_SESSION['user_id']); 
	$result = $find->execute(); 
	if (!FileMaker::isError($result)) {
		$records = $result->getRecords(); 
		$record = $records[0];
		$Premiere = $record->getField('_kf__Premierentyp');
		$PremiereMonat = $record->getField('_kf__Erstauffuehrung_Monat');	
		$PremiereJahr = $record->getField('_kf__Erstauffuehrung_Jahr');	
		$PremiereLand = $record->getField('_kf__Erstauffuehrung_Land');
		$isan = $record->getField('ISAN');
		
		// Infos aus Kreuztabellen holen
		$relatedSet = $record->getRelatedSet('zz_Fernsehsender');
		if (!FileMaker::isError($relatedSet)) {
			foreach ($relatedSet as $relatedRow) {
				$FS_Datum[] = $relatedRow->getField('zz_Fernsehsender::Datum');
				$FS_Land[] = $relatedRow->getField('zz_Fernsehsender::_kf__ISO_Land_Id').'|'.$relatedRow->getField('zz_Fernsehsender_Laenderliste_web::Land_'.$_SESSION['sprache']);
				$FS_Txt[] = htmlspecialchars($relatedRow->getField('zz_Fernsehsender::Station'));
			}
		}
		$relatedSet = $record->getRelatedSet('zz_Verleiher');
		if (!FileMaker::isError($relatedSet)) {
			foreach ($relatedSet as $relatedRow) {
				$VV_Txt[] = htmlspecialchars($relatedRow->getField('zz_Verleiher::Verleiher'));
				$VV_Land[] = $relatedRow->getField('zz_Verleiher::_kf__ISO_Land_Id').'|'.$relatedRow->getField('zz_Verleiher_Laenderliste_web::Land_'.$_SESSION['sprache']);
			}
		}
	}
}


// ERROR-Handling
//__________________________________________________
$error = "";
if ($step == "2") {
	// if (empty($_POST['Premiere'])) $error .= $_SESSION['Leg_199'];
} 

if (($step == "2") && ($error != "")) {
	// Update Seite2Flag mit 0
	$q->AddDBParam('-recid', $_SESSION['record_id']); 
	$q->AddDBParam('zz_Anmeldung_Seite11_Flag', "0");
	$DBData = $q->FMEdit(); 	
} elseif (($step == "2") && ($error == "")) {
	// Update Seite2Flag mit 1
	$q->AddDBParam('-recid', $_SESSION['record_id']); 
	$q->AddDBParam('zz_Anmeldung_Seite11_Flag', "1");
	$DBData = $q->FMEdit(); 	
}


// UPDATE final - Daten aktualisieren
//__________________________________________________

if ((($_SESSION['m'] == "u") && ($step == "2") && ($error == "")) || (($_SESSION['m'] == "u") && ($step == "2") && ($direction == "back"))) {


	if ($isan != "") {
	
		//Update im Film-Eintrag
		$q = FX_open_layout( "cgi_h_02__filme", "1"); 
		$q->AddDBParam('-recid', $_SESSION['record_id']); 
		$q->AddDBParam('ISAN', $isan);
		$DBData = $q->FMEdit(); 	

	}
	
	// Alle Infos aus den Kreuztabellen löschen
	if ($_SESSION['film_id'] != "") {
		$find =& $fm->newFindCommand('cgi_k_36__filme_fernsehen'); 
		$find->addFindCriterion('_kf__Film_Id', $_SESSION['film_id']); 
		$result = $find->execute(); 
		if (!FileMaker::isError($result)) {
			$records = $result->getRecords(); 
			foreach ($records as $record) {
				$rec_ID = $record->getRecordId();
				$rec = $fm->getRecordById('cgi_k_36__filme_fernsehen', $rec_ID); 
				$rec->delete();
			}
		}
		$find =& $fm->newFindCommand('cgi_k_34__filme_verkauf'); 
		$find->addFindCriterion('_kf__Film_Id', $_SESSION['film_id']); 
		$result = $find->execute(); 
		if (!FileMaker::isError($result)) {
			$records = $result->getRecords(); 
			foreach ($records as $record) {
				$rec_ID = $record->getRecordId();
				$rec = $fm->getRecordById('cgi_k_34__filme_verkauf', $rec_ID); 
				$rec->delete();
			}
		}
	}
}



// DB-Actions für Neueintragung & Update
//__________________________________________________
if (($error == "") && ($step == "2")) {

	if ($isan != "") {
	
		//Update im Film-Eintrag
		$q = FX_open_layout("cgi_h_02__filme", "1"); 
		$q->AddDBParam('-recid', $_SESSION['record_id']); 
		$q->AddDBParam('ISAN', $isan);
		$DBData = $q->FMEdit(); 	

	}		
	
	if (isset($FS_Datum[0])) {
		$i = 0;
		foreach ($FS_Datum as $fs_datum_zahl) { 
			$fs_land_code = explode("|", $FS_Land[$i]);
			
			$q = FX_open_layout( "cgi_h_37__fernsehen", "1"); 
			$q->AddDBParam('Datum', $FS_Datum[$i]);
			$q->AddDBParam('_kf__ISO_Land_Id', $fs_land_code[0]);	
			$q->AddDBParam('Station', htmlspecialchars_decode($FS_Txt[$i]));	
			$DBData = $q->FMNew();
			foreach ($DBData['data'] as $key => $value) {
				$fs_id = $value['_kp__id'][0];
			}
			
			$q = FX_open_layout( "cgi_k_36__filme_fernsehen", "1"); 
			$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);	
			$q->AddDBParam('_kf__Fernsehen_Id', $fs_id);	
			$q->AddDBParam('_kf__ISO_Land_Id', $fs_land_code[0]);	
			$DBData = $q->FMNew();
			$i++;		
		}
	}	
	if (isset($VV_Land[0])) {
		$i = 0;

		foreach ($VV_Land as $vv_land_zahl) { 
			$vv_land_code = explode("|", $VV_Land[$i]);
			
			// Verkeufseintrag
			$q = FX_open_layout("cgi_h_35__verkauf", "1"); 
			$q->AddDBParam('_kf__ISO_Land_Id', $vv_land_code[0]);	
			$q->AddDBParam('Verleiher', htmlspecialchars_decode($VV_Txt[$i]));
			$DBData = $q->FMNew();
			foreach ($DBData['data'] as $key => $value) {
				$vv_id = $value['_kp__id'][0];
			}

			// Kreuztabelleneintrag
			$q = FX_open_layout( "cgi_k_34__filme_verkauf", "1"); 
			$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);	
			$q->AddDBParam('_kf__Verkauf_Id', $vv_id);		
			$DBData = $q->FMNew();
			$i++;		
		}
	}	
}


if (($direction == "next") && ($step == "2") && ($error == "")) {
	if ($target != "") {
		header("Location: /$target");
	} else {
		//redirect zur nächsten Seite
		header("Location: /step08_12.php");
	}
	exit;
}
if (($direction == "back") && ($step == "2")) {
	if ($target != "") {
		header("Location: /$target");
	} else {
		//redirect zur vorherigen Seite
		header("Location: /step08_10.php");
	}
	exit;
}
include($_SERVER['DOCUMENT_ROOT']. "/includes/x_get_value_list10.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Online Registration</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <link href="/css/style2008.css" rel="stylesheet" type="text/css" title="KFT" />
 
<script type="text/javascript" src="includes/scripts.js"></script>
<link rel="stylesheet" type="text/css" href="includes/yui/container/assets/container.css" />
<script type="text/javascript" src="includes/yui/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript" src="includes/yui/animation/animation-min.js"></script>
<script type="text/javascript" src="includes/yui/dragdrop/dragdrop-min.js"></script>
<script type="text/javascript" src="includes/yui/connection/connection.js"></script>
<script type="text/javascript" src="includes/yui/container/container.js"></script>
<script language="javascript">


function htmlspecialchars(str,typ) {
  if(typeof str=="undefined") str="";
  if(typeof typ!="number") typ=2;
  typ=Math.max(0,Math.min(3,parseInt(typ)));
  var from=new Array(/&/g,/</g,/>/g);
  var to=new Array("&amp;","&lt;","&gt;");
  if(typ==1 || typ==3) {from.push(/'/g); to.push("&#039;");}
  if(typ==2 || typ==3) {from.push(/"/g); to.push("&quot;");}
  for(var i in from) str=str.replace(from[i],to[i]);
  return str;
}

// ###### Fersehsender #######

var fs_datum = new Array;
var fs_land = new Array;
var fs_txt = new Array;

function addFS() {
	if 	(
		(document.getElementById('fs_datum').value != "") &&
		(document.getElementById('fs_land_select').value != "") &&
		(document.getElementById('fs_txt').value != "")
		) {
		
		fs_datum.push(document.getElementById('fs_datum').value);
		document.getElementById('fs_datum').value = "";
		fs_land.push(document.getElementById('fs_land_select').value);
		document.getElementById('fs_land_select').value = "";
		fs_txt.push(document.getElementById('fs_txt').value);
		document.getElementById('fs_txt').value = "";
		showFS();		
	} else {
		miniDialog = new YAHOO.widget.SimpleDialog("mini_dlg", { visible:false, width: "25em", effect:[
{effect:YAHOO.widget.ContainerEffect.SLIDE,duration:0.25}, {effect:YAHOO.widget.ContainerEffect.FADE,duration:0.25}
], fixedcenter : true, modal:true, draggable:false });
		miniDialog.setHeader("<?=$_SESSION['Leg_91']?>");
		miniDialog.setBody("<?=$_SESSION['Leg_95']?>");
		miniDialog.cfg.queueProperty("icon",YAHOO.widget.SimpleDialog.ICON_WARN);
		miniDialog.cfg.queueProperty("buttons", [ 
			{ text: "<?=$_SESSION['Leg_93']?>", handler:handlerHide, isDefault:true  }
		]);
		miniDialog.render(document.body);
		miniDialog.show();
	}
}
function delFS(i) {
	fs_datum.splice(i,1);
	fs_land.splice(i,1);
	fs_txt.splice(i,1);
	showFS();	
}
function showFS() {
	var output = "";
	for (var i = 0; i < fs_datum.length; ++i) {
		var land = fs_land[i].split("|");
 		output += "<div id=\"list_values\"><input type=\"hidden\" name=\"FS_Datum[]\" value=\"" + fs_datum[i] + "\">\n";
 		output += "<input type=\"hidden\" name=\"FS_Land[]\" value=\"" + fs_land[i] + "\">\n";
		output += "<input type=\"hidden\" name=\"FS_Txt[]\" value=\"" + htmlspecialchars(fs_txt[i]) + "\">\n";		
		output += fs_datum[i] + "<br />" + land[1] + "<br />" + fs_txt[i] +"<br />&nbsp;<br />";
 		output += "</div><div id=\"list_delicons\"><img src=\"images/delete.png\" onClick=\"delFS(" + i + ")\" align=\"absmiddle\" title=\"<?=$_SESSION['Leg_88']?>\"></div><br clear \"all\" />\n";
	}
	document.getElementById('FS').innerHTML = "<p>"+ output +"</p>";
	document.getElementById('FS').style.display = "block";
}

function show_help() {
	miniDialog = new YAHOO.widget.SimpleDialog("mini_dlg", { visible:false, width: "25em", effect:[
{effect:YAHOO.widget.ContainerEffect.SLIDE,duration:0.25}, {effect:YAHOO.widget.ContainerEffect.FADE,duration:0.25}
], fixedcenter : true, modal:true, draggable:false });
	miniDialog.setHeader("<?=$_SESSION['Leg_296']?>");
	miniDialog.setBody("<?=$_SESSION['Leg_298']?>");
	miniDialog.cfg.queueProperty("icon",YAHOO.widget.SimpleDialog.ICON_WARN);
	miniDialog.cfg.queueProperty("buttons", [ 
		{ text: "<?=$_SESSION['Leg_93']?>", handler:handlerHide, isDefault:true  }
	]);
	miniDialog.render(document.body);
	miniDialog.show();
}
// ###### Verleih Verkauf #######

var vv_land = new Array;
var vv_txt = new Array;

function addVV() {
	if 	(
		(document.getElementById('vv_land_select').value != "") &&
		(document.getElementById('vv_txt').value != "")
		) {
		
		vv_land.push(document.getElementById('vv_land_select').value);
		document.getElementById('vv_land_select').value = "";
		vv_txt.push(document.getElementById('vv_txt').value);
		document.getElementById('vv_txt').value = "";
		showVV();		
	} else {
		miniDialog = new YAHOO.widget.SimpleDialog("mini_dlg", { visible:false, width: "25em", effect:[
{effect:YAHOO.widget.ContainerEffect.SLIDE,duration:0.25}, {effect:YAHOO.widget.ContainerEffect.FADE,duration:0.25}
], fixedcenter : true, modal:true, draggable:false });
		miniDialog.setHeader("<?=$_SESSION['Leg_91']?>");
		miniDialog.setBody("<?=$_SESSION['Leg_95']?>");
		miniDialog.cfg.queueProperty("icon",YAHOO.widget.SimpleDialog.ICON_WARN);
		miniDialog.cfg.queueProperty("buttons", [ 
			{ text: "<?=$_SESSION['Leg_93']?>", handler:handlerHide, isDefault:true  }
		]);
		miniDialog.render(document.body);
		miniDialog.show();
	}
}
function delVV(i) {
	vv_land.splice(i,1);
	vv_txt.splice(i,1);
	showVV();	
}
function showVV() {
	var output = "";
	for (var i = 0; i < vv_land.length; ++i) {
		var land2 = vv_land[i].split("|");
 		output += "<div id=\"list_values\"><input type=\"hidden\" name=\"VV_Land[]\" value=\"" + vv_land[i] + "\">\n";
		output += "<input type=\"hidden\" name=\"VV_Txt[]\" value=\"" + htmlspecialchars(vv_txt[i]) + "\">\n";		
		output += land2[1] + "<br />" +  vv_txt[i] +"<br />&nbsp;<br />";
 		output += "</div><div id=\"list_delicons\"><img src=\"images/delete.png\" onClick=\"delVV(" + i + ")\" align=\"absmiddle\" title=\"<?=$_SESSION['Leg_88']?>\"></div><br clear \"all\" />\n";
	}
	document.getElementById('VV').innerHTML = "<p>"+ output +"</p>";
	document.getElementById('VV').style.display = "block";
}


function goBack() {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = "back";
	document.form1.submit();
}
function goNext() {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = "next";
	document.form1.submit();
}
function jumpto(target,direction) {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = direction;
	document.getElementById('target').value = target;
	document.form1.submit();
}
</script> 
<script language="javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>
</head>
<body onload="MM_preloadImages('images/loader.gif')">


<div id="container">
  <div id="top">
    <? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
  </div>
	<br clear="all" />
<div id="navi"><? $seite = "11"; include($_SERVER['DOCUMENT_ROOT'].'/includes/navigation.inc.php'); ?><div id="loader" style="display:none"><img src="/images/loader.gif" style="margin-top: 8px; margin-left:45px" width="32" height="32" /></div></div>
  <div id="leftSide">
<fieldset>
<p class="legend"><?=$_SESSION['Leg_8']?> 11 / 14: <?=$_SESSION['Leg_131']?></p>
	<? if (($error != "") && ($step == "2")) { ?>	
	<p style="border:1px solid #990000; background-color:#FFDCD6; padding:5px; width: 545px"><?=$error; ?></p>
	<? } ?>
	<? echo ($_SESSION['Leg_111'] != "") ? '<p class="pagenote">'.$_SESSION['Leg_111'].'</p>' : ""; ?>
<form action="<?=$_SERVER['PHP_SELF']?>" method="post" name="form1">
<input type="hidden" value="2" name="step_11" />
<input type="hidden" name="direction" id="direction" value="" />
<input type="hidden" name="target" id="target" value="" />

	
	<label for="name"><?=$_SESSION['Leg_24']?></label>
	<div class="div_blankbox">
		<p><?=$_SESSION['Leg_25']?></p>
		<div id="select_div" style="float:left; width: 210px;">
			<?=$_SESSION['Leg_61']?><br />
			<input type="text" name="fs_datum" id="fs_datum" class="textbox_short" style="width:80px !important; width:70px;" /><br />
			<?=$_SESSION['Leg_60']?><br />
			<input type="text" name="fs_txt" id="fs_txt" class="textbox_medium2" /><br />
			<?=$_SESSION['Leg_40']?><br />
			<select name="fs_land_select" id="fs_land_select">
			<option value="">-</option>
			<?=$output_Land?>
			</select> <img src="images/arrow.png" alt="<?=$_SESSION['Leg_78']?>" align="absmiddle" onclick="addFS()" title="<?=$_SESSION['Leg_78']?>" /><br />
		</div>
		<div id="FS" class="list_div_high"> </div>
		<br clear="all" />
	</div>
	<br clear="all" />
	<label for="name"><?=$_SESSION['Leg_26']?></label>
	<div class="div_blankbox">
		<p><?=$_SESSION['Leg_27']?></p>
		<div id="select_div" style="float:left; width: 210px;">
			<?=$_SESSION['Leg_59']?><br />
			<input type="text" name="vv_txt" id="vv_txt" class="textbox_medium2" /><br />
			<?=$_SESSION['Leg_40']?><br />
			<select name="vv_land_select" id="vv_land_select">
			<option value="">-</option>
			<?=$output_Land?>
			</select> <img src="images/arrow.png" alt="<?=$_SESSION['Leg_78']?>" align="absmiddle" onclick="addVV()" title="<?=$_SESSION['Leg_78']?>" /><br />
		</div>
		<div id="VV" class="list_div_high"></div>
		<br clear="all" />
	</div>
	<br clear="all" />
	<label for="name"><?=$_SESSION['Leg_296']?></label>
	<div class="div_blankbox">
		<input type="text" class="textbox_flex" name="isan" id="isan" value="<?=$isan?>" size="24" style="border: 1px solid #666666; font-size:11px; width: 220px" /> 
		<input type="button" id="help" value="<?=$_SESSION['Leg_297']?>" onclick="show_help()" /></div>
	<br clear="all" />
	<div class="prevBtn"><input type="button" id="back" value="<?=$_SESSION['Leg_31']?>" onclick="goBack('')" /></div>
	<div class="nxtBtn"><input type="button" id="next" value="<?=$_SESSION['Leg_32']?>" onclick="goNext('')" /></div>
	<br clear="all" />
</form>
<div class="clear"></div>
</fieldset>

  </div>
	<br clear="all" />
  <div class="clear"></div>
</div>


<? if ((($step == "2") && isset($FS_Datum[0])) || (($_SESSION['m'] == "u") && isset($FS_Datum[0]))) {
echo '<script language="javascript">'."\n";
echo 'fs_datum.push("'.implode("\",\"", $FS_Datum).'");'."\n";
echo 'fs_land.push("'.implode("\",\"", $FS_Land).'");'."\n";
echo 'fs_txt.push("'.implode("\",\"", $FS_Txt).'");'."\n";
echo 'showFS();'."\n";
echo '</script>'."\n";
}
?>
<? if ((($step == "2") && isset($VV_Land[0])) || (($_SESSION['m'] == "u") && isset($VV_Land[0]))) {
echo '<script language="javascript">'."\n";
echo 'vv_land.push("'.implode("\",\"", $VV_Land).'");'."\n";
echo 'vv_txt.push("'.implode("\",\"", $VV_Txt).'");'."\n";
echo 'showVV();'."\n";
echo '</script>'."\n";
}
?>
<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>
</body>
</html>
