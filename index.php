<?
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
$step = (isset($_GET['step'])) ? $_GET['step'] : "";
$sprache = (isset($_GET['sprache'])) ? $_GET['sprache'] : "de";
$typ = (isset($_GET['typ'])) ? $_GET['typ'] : ".";
//echo $step.'-'.$sprache.'-'.$typ;
session_start();
session_destroy();
session_start();
$error = (isset($_GET['error'])) ? $_GET['error'] : "";

require_once ('includes/db.inc.php');

if ($step == "2") {
	read_labels($sprache);	

	$_SESSION['sprache'] = $sprache;
	$_SESSION['Regisseure'] = "";
	$_SESSION['typ'] = $typ;
	
	// redirect zur nächsten Seite
	header("Location: $typ/step1.php");
	exit;
}
//print_r($_SESSION);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Online Registration</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <link href="/css/style.css" rel="stylesheet" type="text/css" title="Solothurn" />
</head>
<body>
<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>

	<? if ($error != "") { ?>	
	<p style="border:1px solid #990000; background-color:#FFDCD6; padding:5px; width: 450px"><?=$error; ?></p>
	<? } ?>

Sound &amp; Stories  
<div style="float:left; width: 80px; text-align:center"><a href="index.php?sprache=de&step=2&typ=sas"><img src="images/de.png" alt="Deutsch" width="50" height="30" border="0" style="margin-bottom:3px;" /><br />
Deutsch</a></div>
<div style="float:left; width: 80px; text-align:center"><a href="index.php?sprache=en&step=2&typ=sas"><img src="images/en.png" alt="English" width="50" height="25" border="0" style="margin-bottom:8px;" /><br />
English</a></div>
<div style="float:left; width: 80px; text-align:center"><a href="index.php?sprache=fr&step=2&typ=sas"><img src="images/fr.png" alt="Francais" width="50" height="33" border="0" /><br />
Francais</a></div>
<div style="float:left; width: 80px; text-align:center"><a href="index.php?sprache=it&step=2&typ=sas"><img src="images/it.png" alt="Italiano" width="50" height="33" border="0" /><br />
Italiano</a></div>
<br clear="all" />
&nbsp;<br />

Forum Schweiz  
<div style="float:left; width: 80px; text-align:center"><a href="index.php?sprache=de&step=2"><img src="images/de.png" alt="Deutsch" width="50" height="30" border="0" style="margin-bottom:3px;" /><br />
Deutsch</a></div>
<div style="float:left; width: 80px; text-align:center"><a href="index.php?sprache=en&step=2"><img src="images/en.png" alt="English" width="50" height="25" border="0" style="margin-bottom:8px;" /><br />
English</a></div>
<div style="float:left; width: 80px; text-align:center"><a href="index.php?sprache=fr&step=2"><img src="images/fr.png" alt="Francais" width="50" height="33" border="0" /><br />
Francais</a></div>
<div style="float:left; width: 80px; text-align:center"><a href="index.php?sprache=it&step=2"><img src="images/it.png" alt="Italiano" width="50" height="33" border="0" /><br />
Italiano</a></div>
<br clear="all" />
<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>
</body>
</html>
