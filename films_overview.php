<?
session_start();
if (!$_SESSION['login_ok']) {
	header("Location: login.php");
	exit;
}
$_SESSION['film_id'] = "";
$_SESSION['m'] = "";
$_SESSION['maxminutes'] = "";

$_SESSION['navi_passiv'] = str_replace("'","*","<p>".$_SESSION['Leg_8']." 1: ".$_SESSION['Leg_121']."</p><p>".$_SESSION['Leg_8']." 2: ".$_SESSION['Leg_122']."</p><p>".$_SESSION['Leg_8']." 3: ".$_SESSION['Leg_123']."</p><p>".$_SESSION['Leg_8']." 4: ".$_SESSION['Leg_124']."</p><p>".$_SESSION['Leg_8']." 5: ".$_SESSION['Leg_252']."</p><p>".$_SESSION['Leg_8']." 6: ".$_SESSION['Leg_125']."</p><p>".$_SESSION['Leg_8']." 7: ".$_SESSION['Leg_126']."</p><p>".$_SESSION['Leg_8']." 8: ".$_SESSION['Leg_127']."</p><p>".$_SESSION['Leg_8']." 9: ".$_SESSION['Leg_128']."</p><p>".$_SESSION['Leg_8']." 10: ".$_SESSION['Leg_130']."</p><p>".$_SESSION['Leg_8']." 11: ".$_SESSION['Leg_131']."</p><p>".$_SESSION['Leg_8']." 12: ".$_SESSION['Leg_129']."</p><p>".$_SESSION['Leg_8']." 13: ".$_SESSION['Leg_132']."</p><p>".$_SESSION['Leg_8']." 14: ".$_SESSION['Leg_133']."</p><div id=\"loader\" style=\"display:none\"><img src=\"/images/loader.gif\" style=\"margin-top:8px;margin-left:45px\" width=\"32\" height=\"32\"></div>");

$_SESSION['navi_passiv_sas'] = str_replace("'","*","<p>".$_SESSION['Leg_8']." 1: ".$_SESSION['Leg_121']."</p><p>".$_SESSION['Leg_8']." 2: ".$_SESSION['Leg_122']."</p><p>".$_SESSION['Leg_8']." 3: ".$_SESSION['Leg_123']."</p><p>".$_SESSION['Leg_8']." 4: ".$_SESSION['Leg_124']."</p><p>".$_SESSION['Leg_8']." 5: ".$_SESSION['Leg_252']."</p><p>".$_SESSION['Leg_8']." 6: ".$_SESSION['Leg_125']."</p><p>".$_SESSION['Leg_8']." 7: ".$_SESSION['Leg_284']."</p><p>".$_SESSION['Leg_8']." 8: ".$_SESSION['Leg_140']."</p><p>".$_SESSION['Leg_8']." 9: ".$_SESSION['Leg_126']."</p><p>".$_SESSION['Leg_8']." 10: ".$_SESSION['Leg_128']."</p><p>".$_SESSION['Leg_8']." 11: ".$_SESSION['Leg_130']."</p><p>".$_SESSION['Leg_8']." 12: ".$_SESSION['Leg_132']."</p><p>".$_SESSION['Leg_8']." 13: ".$_SESSION['Leg_133']."</p><div id=\"loader\" style=\"display:none\"><img src=\"/images/loader.gif\" style=\"margin-top:8px;margin-left:45px\" width=\"32\" height=\"32\"></div>");
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");

require_once ('includes/db.inc.php');
include_once('classes/bbcode/stringparser_bbcode.class.php');
include_once('includes/bbcode_functions.php');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Online Registration</title>
<link href="css/style2008.css" rel="stylesheet" type="text/css" title="KFT" />
<style type="text/css">
th {
 background-color:#EEEEEE;
 color:#666666;
 font-size:10px;
 font-family:Arial, Helvetica, sans-serif;
}
td {
 font-size:10px;
 font-family:Arial, Helvetica, sans-serif;
}
	
</style>
 <script type="text/JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
 </script>
</head>
<body>
<div id="container">
  <div id="top">
    <? include('includes/ly_header.inc.php'); ?>
  </div>
	<br clear="all" />
<div id="navi" style="text-align:center;"><a href="login_logout.php" class="link_button_grey"> Log-Out </a></div>
  <div id="leftSide">
<fieldset>

<legend><?=$_SESSION['Leg_240']?></legend>
<p>&nbsp;</p>
<p><strong><?=$_SESSION['Leg_222']?></strong> <?=$_SESSION['email']?> </p>

<p><?=$bbcode->parse($_SESSION['Leg_241'])?></p>
<?php
$find =& $fm->newFindAllCommand('cgi_Formularsperrungen');
$result = $find->execute(); 
if (!FileMaker::isError($result)) {
	$records = $result->getRecords(); 
	$record = $records[0];
	$ab60 = $record->getField('Anmeldesperrung_Forum_Ab60');
	$unter60 = $record->getField('Anmeldesperrung_Forum_Unter60');
	$sas = $record->getField('Anmeldesperrung_SandS');
}
?>
<? if ($unter60 != "1") { ?><p class="link_button" style="width: 500px; text-align:center"><a href="step08_1.php?typ=1" style="color:#333333; font-weight:bold; text-decoration:none"><?=$_SESSION['Leg_242']?></a></p><? } ?>
<? if ($ab60 != "1") { ?><p class="link_button" style="width: 500px; text-align:center"><a href="step08_1.php?typ=2" style="color:#333333; font-weight:bold; text-decoration:none"><?=$_SESSION['Leg_243']?></a></p><? } ?>
<? if ($sas != "1") { ?><p class="link_button" style="width: 500px; text-align:center"><a href="sas/step08_1.php" style="color:#333333; font-weight:bold; text-decoration:none"><?=$_SESSION['Leg_244']?></a></p><? } ?>

<p><strong><?=$_SESSION['Leg_245']?></strong></p>
<?
$find_user =& $fm->newFindCommand('cgi_Anmeldeuser'); 
$find_user->addFindCriterion('Mail', "==\"".$_SESSION['email']."\""); 
$result_user = $find_user->execute(); 
if (!FileMaker::isError($result_user)) {
	$foundrec = $result_user->getFoundSetCount();
	$records = $result_user->getRecords(); 
	$record = $records[0];
	$relatedSet = $record->getRelatedSet('zz_FILME');
	//print_r ($relatedSet);
	if (!FileMaker::isError($relatedSet)) {
		echo '<table with="350" cellpadding="2" cellspacing="2" border="0">'."\n";
		echo '<tr><th>'.$_SESSION['Leg_1'].'</th><th>'.$_SESSION['Leg_9'].'</th><th>'.$_SESSION['Leg_248'].'</th><th>min</th><th>Typ</th><th></th><th></th></tr>'."\n";
			foreach ($relatedSet as $relatedRow) {
				$typ = ($relatedRow->getField('zz_FILME::Dauer_Minuten') < 60) ? "1" : "2";
				echo '<tr><td>'.$relatedRow->getField('zz_FILME::Filmtitel').'</td><td>'.$relatedRow->getField('zz_FILME::zz_Synthese_Regie').'</td><td align="center">'.$relatedRow->getField('w__Anmeldestatus::Status_'.$_SESSION['sprache']).'</td><td>'.$relatedRow->getField('zz_FILME::Dauer_Minuten').'</td><td>'.$relatedRow->getField('zz_FILME::Sektion').'</td><td>';
				$linkprefix = (in_array($relatedRow->getField('zz_FILME::Sektion'),array("SaS",'BSVC'))) ? "/sas/" : "";
				if ($relatedRow->getField('zz_FILME::_kf__Anmeldestatus') != "3") echo '<a href="'.$linkprefix.'step08_1.php?m=u&f='.$relatedRow->getField('zz_FILME::_kp__id').'&typ='.$typ.'" class="link_button">'.$_SESSION['Leg_246'].'</a>';
			
				echo '</td><td><a href="javascript:void(0);" class="link_button" onclick="MM_openBrWindow(\''.$linkprefix.'popup_filminfo.php?f='.$relatedRow->getField('zz_FILME::_kp__id').'\',\'Info\',\'resizable=yes,width=620,height=650,scrollbars=yes\')">'.$_SESSION['Leg_247'].'</a></td></tr>'."\n";
			}	
		echo '</table>'."\n";
	} else {
		echo '<p>'.$_SESSION['Leg_277'].'</p>';	
	}
	$_SESSION['email'] = $record->getField('Mail');
	$_SESSION['login_ok'] = TRUE;
} else {
	$error = TRUE;
	echo '<p>'.$_SESSION['Leg_277'].'</p>';
}
?> 
<div class="clear"></div>
</fieldset>

  </div>
  <br clear="all" />
  <div class="clear"></div>
</div>
</body>
</html>
