<?
session_start();
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
if ((!$_SESSION['login_ok']) || (($_SESSION['film_id'] == "") && ($_SESSION['m'] == "u"))) {
	//  header("Location: login.php");
	// exit;
}

require_once 'includes/read_layout_list.php'; // includes db.inc.php
require_once 'includes/render_film_copy.php';


$step = nvl($_POST['step_9']);
$direction = nvl($_POST['direction']);
$target = nvl($_POST['target']);

$Untertitel = nvl($_POST['Untertitel']);
$Filmformat =  nvl($_POST['Filmformat']);
$Bildformat =  nvl($_POST['Bildformat']);
$Tonformat =  nvl($_POST['Tonformat']);


// alle Datenänderungen erfolgen über AJAX

if  ($step == "2") {
	// Update Seite2Flag mit 1
	$q = FX_open_layout( "cgi_h_02__filme", "1");
	$q->AddDBParam('-recid', $_SESSION['record_id']);
	$q->AddDBParam('zz_Anmeldung_Seite09_Flag', "1");
	$DBData = $q->FMEdit();
}


if (($direction == "next") && ($step == "2") && ($error == "")) {
	if ($target != "") {
		header("Location: /$target");
	} else {
		//redirect zur nächsten Seite
		header("Location: /step08_10.php");
	}
	exit;
}
if (($direction == "back") && ($step == "2")) {
	if ($target != "") {
		header("Location: /$target");
	} else {
		//redirect zur vorherigen Seite
		header("Location: /step08_8.php");
	}
	exit;
}

// Filmdaten und Vorführkopien holen
$values=array(
		'_kp__id'=> 'id',
		'_kf__Filmformat_Id'=>'Filmformat',
		'_kf__Bildformat_Id'=>'Bildformat',
		'_kf__Tonformat_Id'=>'Tonformat',
		// 'Synchronfassung_Flag'=>'Synchronfassung_Flag',
		// '_kf__Synchronsprache_iso'=>'Synchronsprache_iso',
		'_kf__Untertiteltyp_Id'=>'Untertitel_Typ',
		'_kf__Untertitelsprachen_Id'=>'Untertitel');

$find =& $fm->newFindCommand('cgi_k_20__filme_vorfuehrkopie');
$find->addFindCriterion('_kf__Film_id', $_SESSION['film_id']);
// echo "Film-id: ".$_SESSION['film_id']."<br>\n";
$result = $find->execute();
$copies = array();
if (!FileMaker::isError($result)) {
	$records = $result->getRecords();
	$copies = array();
	foreach ($records as $record) {
		$copies[]=get_film_copy_from_record($record);
	}

} else {
	// echo $result->getMessage();
}


$show_value_lists_4 = "forumch";
include 'includes/x_get_value_list8.inc.php';
$Untertitel_Typen = read_layout_list('untertitel_typen');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Online Registration</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="/css/style2008.css" rel="stylesheet" type="text/css"
	title="KFT" />
<link href="css/step9.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="includes_js/jquery.min.js"></script>
<script type="text/javascript" language="javascript">
var navi_passive= <?= json_encode($_SESSION['navi_passiv'])  ?>;
var bildformate= <?= json_encode(get_format_list('bildformate')); ?>;
var tonformate= <?= json_encode(get_format_list('tonformate')) ?>;
</script>
<script type="text/javascript" src="includes_js/step9.js"></script>
<script language="javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>
</head>
<body onload="MM_preloadImages('images/loader.gif')">
<!-- <?php // var_dump($_SESSION) ?> -->

	<div id="container">
		<div id="top">
		<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
		</div>
		<br clear="all" />
		<div id="navi">
		<? $seite = "9"; include($_SERVER['DOCUMENT_ROOT'].'/includes/navigation.inc.php'); ?>
			<div id="loader" style="display: none">
				<img src="/images/loader.gif"
					style="margin-top: 8px; margin-left: 45px" width="32" height="32" />
			</div>
		</div>
		<div id="leftSide">
			<fieldset>
				<p class="legend">
				<?=$_SESSION['Leg_8']?>
					9 / 14:
					<?=$_SESSION['Leg_128']?>
				</p>
				<p id="errors"
					style="display: none; border: 1px solid #990000; background-color: #FFDCD6; padding: 5px; width: 545px"></p>
					<? echo ($_SESSION['Leg_108'] != "") ? '<p class="pagenote">'.$_SESSION['Leg_108'].'</p>' : ""; ?>
				<form id="copy_form" action="#">
				<label for="name"><?=$_SESSION['Leg_14']?> </label>
				<div class="div_blankbox" style="min-height: 265px;">
					<label for="Filmformat"><?=$_SESSION['Leg_68']?> </label>
					<div style="float: left;">
						<select name="Filmformat" id="Filmformat">
							<option value="">-</option>
							<?=$output_Filmformat?>
						</select>
					</div>
					<br clear="all" /> <label for="Bildformat"><?=$_SESSION['Leg_69']?>
					</label>
					<div style="float: left;">
						<select name="Bildformat" id="Bildformat">
							<option value="">-</option>
						</select>
					</div>
					<br clear="all" /> <label for="Tonformat"><?=$_SESSION['Leg_70']?>
					</label>
					<div style="float: left;">
						<select name="Tonformat" id="Tonformat">
							<option value="">-</option>
						</select>
					</div>
					<br clear="all" />
					<?php // --------------- Synchronfassung bei !DCP ------------------------ ?>
					<div id="sync_lang">
						<label for="Synchronfassung_Flag"><?= $_SESSION['Leg_318'] ?> </label>
						<input type="checkbox" name="Synchronfassung_Flag" id="Synchronfassung_Flag" value="1" />
						<div class="sync_lang">
						<?= $_SESSION['Leg_323']?>
							<select name="Synchronsprache_iso">
								<option value="">-</option>
								<?= $output_Sprache_2 ?>
							</select>
						</div>
						<br clear="all" />
					</div>
					<?php // --------------- Audio-Sprachen bei DCP (und Auswahl der UT-Art) ------------------------ ?>
					<div class="audio">
						<br clear="all" /> <label for="Untertitel_Typ"><?=$_SESSION['Leg_67']?>
						</label>
						<div class="radios">
						<?php $val_column="Untertiteltyp_".$_SESSION['sprache'];
						foreach ($Untertitel_Typen as $ut) { ?>
							<input type="radio" name="Untertitel_Typ" value="<?= $ut['__kp_id'] ?>" id="utt<?= $ut['__kp_id'] ?>" /> 
							<label class="radio" for="utt<?= $ut['__kp_id'] ?>"><?= $ut[$val_column] ?></label><br />
						<?php }?>
						</div>
					</div>
					<?php // --------------- Untertitel-Sprache (imprint, open caption) ------------------------ ?>
					<div id="uts">
						<label for="Untertitel"><?= $_SESSION['Leg_322'] ?> </label> 
						<select	name="Untertitel">
							<option value="">-</option>
							<?= render_value_list('untertitel', $Untertitel) ?>
						</select> <br clear="all" />
					</div>
					<?php // --------------- Untertitel-Sprache (Dateien, clodes caption) ------------------------ ?>
					<div id="ut_sprachen">
						<div >
							<label for="cc_select"><span id="uts_dcp"><?= $_SESSION['Leg_322'] ?></span><span id="uts_other"><?= $_SESSION['Leg_324'] ?></span> </label>
							<div class="multi_select">
							 <select name="cc_select" id="cc_select">
								<option value="">-</option>
								<?= $output_Sprache_2 ?>
							</select> 
							<img src="images/arrow.png" alt="<?=$_SESSION['Leg_78']?>" id="btn_add_ut" class="btn"	title="<?=$_SESSION['Leg_78']?>" />
							<br />
								<?=$_SESSION['Leg_137']?>
						</div>
						<div id="cc_list" class="list_div"></div>
						</div>
					</div>
				</div>
				</form>
				<div class="clear"></div>
				<div id='save_wrapper'>
					<div id='btn_save'>
						<img src="images/arrow.png" alt="" id="btn_add_copy" class="btn" title="<?=$_SESSION['Leg_325']?>" />
							<?=$_SESSION['Leg_325']?>
					</div>
					<div id="copy_loader" style="display:none"><img width="19" height="19" src="images/loader.gif"></img></div>
				</div>

				<?php // -------------------- Anzeige der bestehenden Filmkopien ---------------- ?>
				<label for="copies"><?=$_SESSION['Leg_317']?> </label>
				<div id="saved_copies">
				<?php foreach ($copies as $copy) { ?>
					<?= render_film_copy($copy) ?>
				<?php } ?>
				</div>


				<br clear="all" />
				<form action="<?=$_SERVER['PHP_SELF']?>" method="post" name="form1">
					<input type="hidden" value="2" name="step_9" /> <input
						type="hidden" name="direction" id="direction" value="" /> <input
						type="hidden" name="target" id="target" value="" />
					<div class="prevBtn">
						<input type="button" id="back" value="<?=$_SESSION['Leg_31']?>"
							onclick="goBack('')" />
					</div>
					<div class="nxtBtn">
						<input type="button" id="next" value="<?=$_SESSION['Leg_32']?>"
							onclick="goNext('')" />
					</div>
					<br clear="all" />
				</form>
				<div class="clear"> </div>
			</fieldset>

		</div>
		<br clear="all" />
		<div class="clear"> </div>
	</div>


	<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>
</body>
</html>
