<?
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
session_start();
if (!isset($_SESSION['Leg_1'])) {
	
	// redirect zur�ck zur Startseite wg. abgelaufener Session
	header("Location: /index.php?error=Session%20abgelaufen");
	exit;
}
require_once($_SERVER['DOCUMENT_ROOT'] . "/classes/FX/FX.php"); 

$step = (isset($_POST['step2'])) ? $_POST['step2'] : "";
$hasEntry = (isset($_POST['hasEntry1'])) ? $_POST['hasEntry1'] : "";
$_SESSION['tmp_image_name'] = time().rand(1000, 9999);

$error = "";
if ($step == "2") {
	$error .= ($hasEntry == "") ? $_SESSION['Leg_73']."<br />" : "";
} 

if (($step == "2") && ($error == "")) {
	//redirect zur n�chsten Seite
	header("Location: /step3.php");
	exit;
}

include($_SERVER['DOCUMENT_ROOT']. "/includes/x_get_value_list2.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Online Registration</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 <link href="/css/style.css" rel="stylesheet" type="text/css" title="KFT" />
<script type="text/javascript" src="includes/scripts.js"></script>
<link rel="stylesheet" type="text/css" href="includes/yui/container/assets/container.css">
<script type="text/javascript" src="includes/yui/fileupload/yahoo-dom-event.js"></script>
<script type="text/javascript" src="includes/yui/animation/animation-min.js"></script>
<script type="text/javascript" src="includes/yui/dragdrop/dragdrop-min.js"></script>
<script type="text/javascript" src="includes/yui/fileupload/connection.js"></script>
<script type="text/javascript" src="includes/yui/fileupload/container.js"></script>
<script type="text/javascript" src="includes/yui/fileupload/Ext.js"></script>
<script type="text/javascript" src="includes/yui/fileupload/DomQuery.js"></script>
<script type="text/javascript" src="includes/yui/fileupload/momche-imageupload.js"></script>
<!-- Dialog Functions -->
<script>
	YAHOO.util.Event.onDOMReady( 
		function()
		{
			var hClearLink = YAHOO.util.Dom.get( 'clearImage' );
			YAHOO.util.Event.addListener( hClearLink, 'click', function() { hImageUploader.clear( document.getElementById( 'image' ) ) } );				
		}		
	);
</script>
</head>
<body>

<script type="text/javascript" src="includes/safari_hack.js"></script>
<script type="text/javascript" language="javascript">
<? include($_SERVER['DOCUMENT_ROOT'].'/includes_js/step2.js.php'); ?>	
<? include($_SERVER['DOCUMENT_ROOT'].'/includes_js/inits.js.php'); ?>	
function init() {
	<? include($_SERVER['DOCUMENT_ROOT'].'/includes_js/callbacks.js.php'); ?>
<? include($_SERVER['DOCUMENT_ROOT'].'/includes_js/newPerson_dialog.js.php'); ?>	
<? include($_SERVER['DOCUMENT_ROOT'].'/includes_js/addPerson_dialog.js.php'); ?>	
}
<? include($_SERVER['DOCUMENT_ROOT'].'/includes_js/person_handler.js.php'); ?>
</script>
<div id="container">
  <div id="top">
    <? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
	<? if ($error != "") { ?>	
	<p style="border:1px solid #990000; background-color:#FFDCD6; padding:5px; width: 450px"><?=$error; ?></p>
	<? } ?>
  </div>
	<br clear="all" />
  <div id="leftSide">
<fieldset>
<legend><?=$_SESSION['Leg_8']?> 2 / 13: <?=$_SESSION['Leg_122']?></legend>
	<? echo ($_SESSION['Leg_102'] != "") ? '<p class="pagenote">'.$_SESSION['Leg_102'].'</p>' : ""; ?>
<form action="step2.php" method="post" name="form1">
<input type="hidden" value="2" name="step2">
	<label for="name"><?=$_SESSION['Leg_9']?></label>
	<div class="div_blankbox">

		<input type="button" class="liteoption" value="<?=$_SESSION['Leg_142']?>" onClick="javascript:newPerson('1');" />
		<input type="hidden" value="" name="personen_typ" id="personen_typ">
		&nbsp;<br />&nbsp;<br />
		<div id="person_list">
<?
$q = new FX("host8.kon5.net", "80", "FMPro7"); 
$q->SetDBData("Filmtage-Solothurn", "cgi_k_03__filme_personen", "999");
$q->SetDBUserPass ("Admin", "xs4kon5");
$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);
$q->AddDBParam('_kf__Stabbezeichnung_Id', "1");	
$DBData = $q->FMFind();  

$span = "";
foreach ($DBData['data'] as $key => $value) {
	$span .= "Regie: ".$value['h_01__personen::Vorname'][0].' '.$value['h_01__personen::Name'][0].' <img src="/images/delete.png" border="0" style="cursor:pointer" onClick="deletePerson(\'1\',\''.$value['_kp__record_id'][0].'\')" align="absmiddle"><br /><input type="hidden" name="hasEntry" value="1" />';
}
	echo $span;
		?>
		</div>
		
		
	</div>
	<br clear="all">
	<div class="button_div">
	<input name="submit" type="submit" id="submit" value="<?=$_SESSION['Leg_32']?>">
	</div>
</form>

<div class="clear"></div>
</fieldset>

  </div>
  <div class="clear"></div>
</div>

<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>

<div id="div_dlg_newperson">
	<div class="hd"><?=$_SESSION['Leg_154']?></div>
	<div class="bd">
		<span id="span_newperson_step1">
		 <form id="form_search_newperson" action="includes/ajax_search_person.php" method="post"> 
			<input type="hidden" id="newperson_type" name="newperson_type" value="0" />
			<div style="float:left; font-size:11px;">
				<?=$_SESSION['Leg_154']?><br />
				<?=$_SESSION['Leg_34']?><br />
				<input type="text" class="post" name="vorname" id="vorname" value="" size="18" style="border: 1px solid #666666; font-size:11px;" />
			</div>
			<div style="float:left; font-size:11px;">
				&nbsp;<br />
				<?=$_SESSION['Leg_35']?><br />
				<input type="text" class="post" name="nachname" id="nachname" value="" size="18" style="border: 1px solid #666666; font-size:11px;" />
				<input type="hidden" name="type" id="type" value="1" /> 
			</div>
			<div style="float:left; font-size:11px;">
				&nbsp;<br />
				&nbsp;<br />
				<input type="button" class="mainoption" value="<?=$_SESSION['Leg_100']?>" onClick="newperson_search();" />
			</div>
			 <br clear="all" /> 
			
		 </form><br />
		<span id="span_searchresult_newperson"></span>
		</span>
		<span id="span_newperson_step2"></span>
	</div>
</div>

<div id="div_dlg_addperson">
	<div class="hd">Person hinzuf�gen</div>
	<div class="bd">
		<span id="span_addperson">
		 <form id="form_add_newperson" action="includes/ajax_add_newperson.php" method="post" enctype="multipart/form-data"> 
			<input type="hidden" id="newperson_type" name="newperson_type" value="0" />
			<div style="float:left; width: 250px;">
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_34']?>*</div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="vorname2" id="p_vorname2" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_35']?>*</div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="name" id="p_name" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_36']?>*</div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="strasse1" id="p_strasse1" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_37']?></div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="strasse2" id="p_strasse2" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_38']?>*</div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="plz" id="p_plz" value="" size="6" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_39']?>*</div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="ort" id="p_ort" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_40']?>*</div>
				<div style="float:left; font-size:11px;">
    <select class="post" name="land" id="p_land" style="border: 1px solid #666666; font-size:11px;">
	<option value="">-</option>
	<?=$output_Land?>
	</select></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_41']?></div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="fon" id="p_fon" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_43']?></div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="fax" id="p_fax" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_44']?></div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="email" id="p_email" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_45']?></div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="www" id="p_www" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_46']?></div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="geburtsjahr" id="p_geburtsjahr" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
			</div>
			<div style="float:left; width: 250px;">
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_49']?><br>(English)</div>
				<div style="float:left; font-size:11px;"><textarea name="bio_en" rows="4" style="border: 1px solid #666666; font-size:11px;"></textarea></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_49']?><br>(Deutsch)</div>
				<div style="float:left; font-size:11px;"><textarea name="bio_de" rows="4" style="border: 1px solid #666666; font-size:11px;"></textarea></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_49']?><br>(Francais)</div>
				<div style="float:left; font-size:11px;"><textarea name="bio_fr" rows="4" style="border: 1px solid #666666; font-size:11px;"></textarea></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_49']?><br>(Italiano)</div>
				<div style="float:left; font-size:11px;"><textarea name="bio_it" rows="4" style="border: 1px solid #666666; font-size:11px;"></textarea></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_47']?></div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="geburtsort" id="p_geburtsort" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_48']?></div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="heimatort" id="p_heimatort" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
			</div>
			<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_50']?></div>
				<div style="float:left; font-size:11px;"><?=$_SESSION['Leg_56']?><br /><input type="text" class="post" name="jahr" id="jahr" value="" size="4" maxlength="4" style="border: 1px solid #666666; font-size:11px;" /></div>
				<div style="float:left; font-size:11px;"><?=$_SESSION['Leg_1']?><br /><input type="text" class="post" name="filmtitel" id="filmtitel" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /><img src="/images/add.png" align="absmiddle" border="0" onClick="javascript:addFilmographie();" title="<?=$_SESSION['Leg_78']?>"></div>
				<br clear="all" />				
				<div style="float:left; font-size:11px; width: 70px;">&nbsp;</div>
				<div style="float:left; font-size:11px;" id="filmographie"></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_51']?></div>
				<div style="float:left; font-size:11px;">
					<input type="hidden" name="image" id="image" value="add.png" class="imagePath target_TheImage"/>
					<div>
						<div id="TheImage" class="imageContainer" style="float:left;">&nbsp;</div>
						<div style="float:left;margin-left:10px;"><a href="#" id="clearImage" style="text-decoration:none">delete image <img src="/images/delete.png" border="0" title="<?=$_SESSION['Leg_88']?>"/></a></div>
					</div>
				</div>
				<br clear="all" />
			<input type="hidden" name="type" value="1" />
		 </form>
		<span id="span_searchresult_newperson"></span>
		</span>
		<span id="span_newperson_step2"></span>
	</div>
</div>

<!-- IMAGE UPLOAD DIALOG -->
<div id="uploadImageDialog">
	<div class="hd"><?=$_SESSION['Leg_51']?> Upload</div>
	<div class="bd">
		<form method="post" enctype="multipart/form-data" action="upload.php">
			<input type="file" name="personenbild" style="width: 250px"/>
		</form>
	</div>
</div>
<script language="javascript" type="text/javascript">
<!--

init();

//-->
</script>
</body>
</html>
