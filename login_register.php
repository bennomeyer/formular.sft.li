<?
session_start();
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
require_once ('includes/db.inc.php');


	if (isset($_GET['sprache'])) $_SESSION['sprache'] = $_GET['sprache'];

	if (!isset($_SESSION['sprache'])) {
		$sprache = (isset($_GET['sprache'])) ? $_GET['sprache'] : "de";
		$_SESSION['sprache'] = $sprache;
	} else {
		$sprache = $_SESSION['sprache'];
	}
	
	// Sprach-Legenden holen ___
	read_labels($sprache);
	
$sprache = (isset($_SESSION['sprache'])) ? $_SESSION['sprache'] : "de";
$s = (isset($_POST['s'])) ? $_POST['s'] : "1";
$vorname = (isset($_POST['vorname'])) ? $_POST['vorname'] : "";
$name = (isset($_POST['name'])) ? $_POST['name'] : "";
$tel = (isset($_POST['tel'])) ? $_POST['tel'] : "";
$email = (isset($_POST['email'])) ? $_POST['email'] : "";
$pwd = (isset($_POST['pwd'])) ? $_POST['pwd'] : "";
$user_exists = FALSE;
$foundrec = "";
$error = "";



if ($s == "2") {
	$error = (($vorname == "") || ($name == "") || ($tel == "") || ($email == "") || ($pwd == ""))? TRUE : FALSE;
	// check if user already exits in DB
	$find_user =& $fm->newFindCommand('cgi_Anmeldeuser'); 
	$find_user->addFindCriterion('Mail', "==\"".$email."\""); 
	$result_user = $find_user->execute(); 
	if (!FileMaker::isError($result_user)) {
		$foundrec = $result_user->getFoundSetCount();
		$user_exists = TRUE;
		$error = TRUE;
		$records = $result_user->getRecords(); 
		$foundrec = $result_user->getFoundSetCount();
		$record = $records[0];
		$vorname = $record->getField('Vorname');
		$name = $record->getField('Name');
		$email = $record->getField('Mail');
		$pwd = $record->getField('Passwort');
	}
	if (!$error) {
		$data = array(
			'Mail'			=> $email,
			'Passwort'		=> $pwd,
			'Vorname'		=> $vorname,
			'Name'			=> $name,
			'Tel'			=> $tel
		);
		$newRequest =& $fm->newAddCommand('cgi_Anmeldeuser', $data);
		$result = $newRequest->execute();
		if (!FileMaker::isError($result)) {
			$records = $result->getRecords(); 
			$record = $records[0];
			$_SESSION['login_ok'] = TRUE;
			$_SESSION['email'] = $email;
			$_SESSION['user_id'] = $record->getField('__kp__id');
			$_SESSION['sprache'] = $sprache;
			require($_SERVER['DOCUMENT_ROOT'] . "/classes/phpmailer/class.phpmailer.php");
			$mail = new PHPMailer();
			$mail->From     = "info@kon5.net";
			$mail->FromName = "Solothurn";
			$mail->Subject	= "Account-data Solothurn";
			$body = $_SESSION['Leg_231']." $vorname $name

".$_SESSION['Leg_232']."

".$_SESSION['Leg_222'].": $email
".$_SESSION['Leg_223'].": $pwd

".$_SESSION['Leg_233'];
			$mail->SetLanguage("en", $_SERVER['DOCUMENT_ROOT']."/classes/phpmailer/language/"); 
			$mail->Body    = utf8_decode($body);
			$mail->AddAddress($email);
			if(!$mail->Send()) echo "Ihre Anmeldung konnte nicht bearbeitet werden.<br>";
			$mail->ClearAddresses();
			$mail->ClearAttachments();
			$mail->SmtpClose(); 
			header("Location: films_overview.php");
			exit;
		} else {
			$error = TRUE;
		}
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Online Registration</title>
<link href="/css/style2008.css" rel="stylesheet" type="text/css" title="KFT" />
</head>
<body>
<div id="container">
  <div id="top">
    <? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
  </div>
	<br clear="all" />
<div id="navi">
	<? if ($_SESSION['sprache'] == "de") { ?>
	<a href="http://www.solothurnerfilmtage.ch/filmanmeldung" target="_top"><?=$_SESSION['Leg_285']?></a>
	<? } ?>
	<? if ($_SESSION['sprache'] == "fr") { ?>
	<a href="http://www.journeesdesoleure.ch/inscription" target="_top"><?=$_SESSION['Leg_285']?></a>
	<? } ?>
	<? if ($_SESSION['sprache'] == "en") { ?>
	<a href="http://www.solothurnerfilmtage.ch/filmentry" target="_top"><?=$_SESSION['Leg_285']?></a>
	<? } ?>
	<? if ($_SESSION['sprache'] == "it") { ?>
	<a href="http://www.journeesdesoleure.ch/inscription" target="_top"><?=$_SESSION['Leg_285']?></a>
	<? } ?>
</div>
  <div id="leftSide">
<fieldset>
<legend><?=$_SESSION['Leg_227']?></legend>
<p><?=$_SESSION['Leg_228']?></p>
<? if ($error) { ?>	
<p style="border:1px solid #990000; background-color:#FFDCD6; padding:5px; width: 510px">
<?
if ($user_exists) {
	echo $_SESSION['Leg_239'].'<br /><br /><a href="login.php" class="link_button">'.$_SESSION['Leg_237'].'</a>';
	// send login-data 2 user
	require($_SERVER['DOCUMENT_ROOT'] . "/classes/phpmailer/class.phpmailer.php");
	$mail = new PHPMailer();
	$mail->From     = "info@solothurnerfilmtage.ch";
	$mail->FromName = "Solothurner Filmtage";
	$mail->Subject	= "Account-data Solothurn";
	$body = $_SESSION['Leg_231']." $vorname $name

".$_SESSION['Leg_232']."

".$_SESSION['Leg_222'].": $email
".$_SESSION['Leg_223'].": $pwd

".$_SESSION['Leg_233'];
	$mail->SetLanguage("en", $_SERVER['DOCUMENT_ROOT']."/classes/phpmailer/language/"); 
	$mail->Body    = utf8_decode($body);
	//$mail->AddAddress($email);
	$mail->AddAddress($email);
	if(!$mail->Send()) echo "Ihre Anmeldung konnte nicht bearbeitet werden.<br>";
	$mail->ClearAddresses();
	$mail->ClearAttachments();
	$mail->SmtpClose(); 
} else {
	echo $_SESSION['Leg_286'];
}
?>
</p>
<? } ?>

<? if (($error != "") || ($s == "1")) { ?>
	<form action="login_register.php" method="post">
		<input type="hidden" name="s" value="2" />
		<input type="hidden" name="sprache" value="<?=$sprache?>" />
		<label><?=$_SESSION['Leg_34']?></label>
		<div class="div_blankbox"><input type="text" name="vorname" style="width: 110px" class="textbox" /></div><br clear="all" />
		<label><?=$_SESSION['Leg_35']?></label>
		<div class="div_blankbox"><input type="text" name="name" style="width: 110px" class="textbox" /></div><br clear="all" />
		<label><?=$_SESSION['Leg_41']?></label>
		<div class="div_blankbox"><input type="text" name="tel" style="width: 110px" class="textbox" /></div><br clear="all" />
		<label><?=$_SESSION['Leg_229']?></label>
		<div class="div_blankbox"><input type="text" name="email" style="width: 110px" class="textbox" /></div><br clear="all" />
		<label><?=$_SESSION['Leg_223']?></label>
		<div class="div_blankbox"><input type="password" name="pwd" style="width: 110px" class="textbox" /></div><br clear="all" />
		<label>&nbsp;</label>
		<div class="div_blankbox" style="background:none">
		<input type="submit" value="<?=$_SESSION['Leg_230']?>"  class="link_button"/><br /><br />
		</div>		
	</form>
<? } ?>

<div class="clear"></div>
</fieldset>
  </div>
  <div class="clear"></div>
</div>
</body>
</html>
