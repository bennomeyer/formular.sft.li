<?
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
session_start();
$s = (isset($_POST['s']))? $_POST['s'] : "";
$sprache_form = (isset($_POST['sprache_form']))? $_POST['sprache_form'] : "";

//print_r($_POST);
if ($s != "2") {
	$sprache = (isset($_GET['sprache'])) ? $_GET['sprache'] : "de";
} else {
	$sprache = (isset($_SESSION['sprache'])) ? $_SESSION['sprache'] : $sprache_form;
}
$_SESSION['sprache'] = $sprache;
//echo isset($_SESSION['sprache']).'--'.$_SESSION['sprache'];
$u = (isset($_POST['u'])) ? $_POST['u'] : "";
$p = (isset($_POST['p'])) ? $_POST['p'] : "";
$foundrec = "";
$error = "";

require_once ('includes/db.inc.php');

// Sprach-Legenden holen ___
read_labels($sprache);

if ($s == "2") {
	$find_user =& $fm->newFindCommand('cgi_Anmeldeuser'); 
	$find_user->addFindCriterion('Mail', "==\"".$u."\""); 
	$find_user->addFindCriterion('Passwort', "==\"".$p."\""); 
	$result_user = $find_user->execute(); 
	if (!FileMaker::isError($result_user)) {
		$foundrec = $result_user->getFoundSetCount();
	}
	if ($foundrec == "1") {
		$records = $result_user->getRecords(); 
		$record = $records[0];
		$_SESSION['email'] = $record->getField('Mail');
		$_SESSION['user_id'] = $record->getField('__kp__id');
		$_SESSION['login_ok'] = TRUE;
		header("Location: films_overview.php");
		exit;
	} else {
		$error = TRUE;
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Online Registration</title>
<link href="/css/style2008.css" rel="stylesheet" type="text/css" title="KFT" />
</head>
<body>
<div id="container">
  <div id="top">
    <? include('includes/ly_header.inc.php'); ?>
  </div>
	<br clear="all" />
<div id="navi">
	<? if ($_SESSION['sprache'] == "de") { ?>
	<a href="http://www.solothurnerfilmtage.ch/filmanmeldung" target="_top"><?=$_SESSION['Leg_285']?></a>
	<? } ?>
	<? if ($_SESSION['sprache'] == "fr") { ?>
	<a href="http://www.journeesdesoleure.ch/inscription" target="_top"><?=$_SESSION['Leg_285']?></a>
	<? } ?>
	<? if ($_SESSION['sprache'] == "en") { ?>
	<a href="http://www.solothurnerfilmtage.ch/filmentry" target="_top"><?=$_SESSION['Leg_285']?></a>
	<? } ?>
	<? if ($_SESSION['sprache'] == "it") { ?>
	<a href="http://www.journeesdesoleure.ch/inscription" target="_top"><?=$_SESSION['Leg_285']?></a>
	<? } ?>
</div>

<div id="leftSide">
<fieldset>
<legend><?=$_SESSION['Leg_220']?></legend>
<p><?=$_SESSION['Leg_221']?></p>

<form action="<?=$_SERVER['PHP_SELF']?>" method="post" name="form1">
	<input type="hidden" name="s" value="2" />
	<input type="hidden" name="sprache_form" value="<?=$sprache?>" />
	<label><?=$_SESSION['Leg_222']?></label>
	<div class="div_blankbox"><input type="text" name="u" style="width: 110px" class="textbox_flex" /></div> <br clear="all" />
	<label><?=$_SESSION['Leg_223']?></label>
	<div class="div_blankbox"><input type="password" name="p" style="width: 110px" class="textbox_flex" />&nbsp;&nbsp;&nbsp;<a href="login_pwd_resend.php"><?=$_SESSION['Leg_224']?></a></div><br clear="all" />
	<label>&nbsp;</label>
	<div class="div_blankbox" style="background:none">
	<input type="submit" value="login"  class="link_button"/><br />
	</div>		

<br clear="all" />

<? if ($error == "") { ?>	
<p><?=$_SESSION['Leg_225']?> <a href="login_register.php?sprache=<?=$sprache?>" class="link_button"><?=$_SESSION['Leg_226']?></a></p>
<? } ?>
<? if ($error != "") { ?>	
<p style="border:1px solid #990000; background-color:#FFDCD6; padding:5px; width: 400px"><?=$_SESSION['Leg_238']?>
<br /><br />
<a href="login_register.php?sprache=<?=$sprache?>" class="link_button"><?=$_SESSION['Leg_226']?></a></p>
<? } ?>
</form>
<div class="clear"></div>
</fieldset>
  </div>
  <div class="clear"></div>
</div>
</body>
</html>
