<?
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
session_start();

if (!isset($_SESSION['Leg_1'])) {
	
	// redirect zur�ck zur Startseite wg. abgelaufener Session
	header("Location: /index.php?error=Session%20abgelaufen");
	exit;
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/classes/FX/FX.php"); 

$step = (isset($_POST['step_11'])) ? $_POST['step_11'] : "";
$Premiere = "";
$FS_Datum = (!empty($_POST['FS_Datum'])) ? $_POST['FS_Datum'] : array();
$FS_Land = (!empty($_POST['FS_Land'])) ? $_POST['FS_Land'] : array();
$FS_Txt = (!empty($_POST['FS_Txt'])) ? $_POST['FS_Txt'] : array();
$VV_Land = (!empty($_POST['VV_Land'])) ? $_POST['VV_Land'] : array();
$VV_Txt = (!empty($_POST['VV_Txt'])) ? $_POST['VV_Txt'] : array();

$error = "";
if ($step == "2") {
	//if ((empty($_POST['Sprache'])) || (empty($_POST['Filmformat'])) || (empty($_POST['Bildformat'])) || (empty($_POST['Tonformat'])) ) $error .= $_SESSION['Leg_92'];
} 



if (($error == "") && ($step == "2")) {
		
	
	if (isset($FS_Datum[0])) {
		$i = 0;
		foreach ($FS_Datum as $fs_datum_zahl) { 
			$fs_land_code = explode("|", $FS_Land[$i]);
			
			// Festivaleintrag
			$q = new FX('host8.kon5.net', '80', 'FMPro7');
			$q->SetDBData("Filmtage-Solothurn", "cgi_h_37__fernsehen", "1"); 
			$q->SetDBUserPass ("Admin", "xs4kon5");
			$q->AddDBParam('Datum', $FS_Datum[$i]);
			$q->AddDBParam('_kf__ISO_Land_Id', $fs_land_code[0]);	
			$q->AddDBParam('Station', $FS_Txt[$i]);	
			$DBData = $q->FMNew();
			foreach ($DBData['data'] as $key => $value) {
				$fs_id = $value['_kp__id'][0];
			}
			
			// Kreuztabelleneintrag
			$q = new FX('host8.kon5.net', '80', 'FMPro7');
			$q->SetDBData("Filmtage-Solothurn", "cgi_k_36__filme_fernsehen", "1"); 
			$q->SetDBUserPass ("Admin", "xs4kon5");
			$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);	
			$q->AddDBParam('_kf__Fernsehen_Id', $fs_id);	
			$q->AddDBParam('_kf__ISO_Land_Id', $fs_land_code[0]);	
			$DBData = $q->FMNew();
			$i++;		
		}
	}	
	if (isset($VV_Land[0])) {
		$i = 0;

		foreach ($VV_Land as $vv_land_zahl) { 
			$vv_land_code = explode("|", $VV_Land[$i]);
			
			// Verkeufseintrag
			$q = new FX('host8.kon5.net', '80', 'FMPro7');
			$q->SetDBData("Filmtage-Solothurn", "cgi_h_35__verkauf", "1"); 
			$q->SetDBUserPass ("Admin", "xs4kon5");
			$q->AddDBParam('_kf__ISO_Land_Id', $vv_land_code[0]);	
			$q->AddDBParam('Verleiher', $VV_Txt[$i]);
			$DBData = $q->FMNew();
			foreach ($DBData['data'] as $key => $value) {
				$vv_id = $value['_kp__id'][0];
			}

			// Kreuztabelleneintrag
			$q = new FX('host8.kon5.net', '80', 'FMPro7');
			$q->SetDBData("Filmtage-Solothurn", "cgi_k_34__filme_verkauf", "1"); 
			$q->SetDBUserPass ("Admin", "xs4kon5");
			$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);	
			$q->AddDBParam('_kf__Verkauf_Id', $vv_id);		
			$DBData = $q->FMNew();
			$i++;		
		}
	}	
	// redirect zur n�chsten Seite
	header("Location: /step12.php");
	exit;
	//echo 'done';
}

include($_SERVER['DOCUMENT_ROOT']. "/includes/x_get_value_list10.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Online Registration</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 <link href="/css/style.css" rel="stylesheet" type="text/css" title="KFT" />
 
<script type="text/javascript" src="includes/scripts.js"></script>
<link rel="stylesheet" type="text/css" href="includes/yui/container/assets/container.css">
<script type="text/javascript" src="includes/yui/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript" src="includes/yui/animation/animation-min.js"></script>
<script type="text/javascript" src="includes/yui/dragdrop/dragdrop-min.js"></script>
<script type="text/javascript" src="includes/yui/connection/connection.js"></script>
<script type="text/javascript" src="includes/yui/container/container.js"></script>
<script language="javascript">

// ###### Fersehsender #######

var fs_datum = new Array;
var fs_land = new Array;
var fs_txt = new Array;

function addFS() {
	if 	(
		(document.getElementById('fs_datum').value != "") &&
		(document.getElementById('fs_land_select').value != "") &&
		(document.getElementById('fs_txt').value != "")
		) {
		
		fs_datum.push(document.getElementById('fs_datum').value);
		document.getElementById('fs_datum').value = "";
		fs_land.push(document.getElementById('fs_land_select').value);
		document.getElementById('fs_land_select').value = "";
		fs_txt.push(document.getElementById('fs_txt').value);
		document.getElementById('fs_txt').value = "";
		showFS();		
	} else {
		miniDialog = new YAHOO.widget.SimpleDialog("mini_dlg", { visible:false, width: "25em", effect:[
{effect:YAHOO.widget.ContainerEffect.SLIDE,duration:0.25}, {effect:YAHOO.widget.ContainerEffect.FADE,duration:0.25}
], fixedcenter : true, modal:true, draggable:false });
		miniDialog.setHeader("<?=$_SESSION['Leg_91']?>");
		miniDialog.setBody("<?=$_SESSION['Leg_95']?>");
		miniDialog.cfg.queueProperty("icon",YAHOO.widget.SimpleDialog.ICON_WARN);
		miniDialog.cfg.queueProperty("buttons", [ 
			{ text: "<?=$_SESSION['Leg_93']?>", handler:handlerHide, isDefault:true  }
		]);
		miniDialog.render(document.body);
		miniDialog.show();
	}
}
function delFS(i) {
	fs_datum.splice(i,1);
	fs_land.splice(i,1);
	fs_txt.splice(i,1);
	showFS();	
}
function showFS() {
	var output = "";
	for (var i = 0; i < fs_datum.length; ++i) {
		var land = fs_land[i].split("|");
 		output += "<input type=\"hidden\" name=\"FS_Datum[]\" value=\"" + fs_datum[i] + "\">\n";
 		output += "<input type=\"hidden\" name=\"FS_Land[]\" value=\"" + fs_land[i] + "\">\n";
		output += "<input type=\"hidden\" name=\"FS_Txt[]\" value=\"" + fs_txt[i] + "\">\n";		
 		output += "<hr style=\"width: 330px; height: 1px\" noshade=\"noshade\" color=\"#FFFFFF\">\n";
		output += fs_datum[i] + ", " + land[1] + ", " + fs_txt[i] +"<br />";
 		output += "<img src=\"images/delete.png\" onClick=\"delFS(" + i + ")\" align=\"absmiddle\"> <a href=\"javascript:void(0);\" onClick=\"delFS(" + i + ")\"><?=$_SESSION['Leg_88']?></a><br />\n";
	}
	document.getElementById('FS').innerHTML = "<p>"+ output +"</p>";
	document.getElementById('FS').style.display = "block";
}


// ###### Verleih Verkauf #######

var vv_land = new Array;
var vv_txt = new Array;

function addVV() {
	if 	(
		(document.getElementById('vv_land_select').value != "") &&
		(document.getElementById('vv_txt').value != "")
		) {
		
		vv_land.push(document.getElementById('vv_land_select').value);
		document.getElementById('vv_land_select').value = "";
		vv_txt.push(document.getElementById('vv_txt').value);
		document.getElementById('vv_txt').value = "";
		showVV();		
	} else {
		miniDialog = new YAHOO.widget.SimpleDialog("mini_dlg", { visible:false, width: "25em", effect:[
{effect:YAHOO.widget.ContainerEffect.SLIDE,duration:0.25}, {effect:YAHOO.widget.ContainerEffect.FADE,duration:0.25}
], fixedcenter : true, modal:true, draggable:false });
		miniDialog.setHeader("<?=$_SESSION['Leg_91']?>");
		miniDialog.setBody("<?=$_SESSION['Leg_95']?>");
		miniDialog.cfg.queueProperty("icon",YAHOO.widget.SimpleDialog.ICON_WARN);
		miniDialog.cfg.queueProperty("buttons", [ 
			{ text: "<?=$_SESSION['Leg_93']?>", handler:handlerHide, isDefault:true  }
		]);
		miniDialog.render(document.body);
		miniDialog.show();
	}
}
function delVV(i) {
	vv_land.splice(i,1);
	vv_txt.splice(i,1);
	showVV();	
}
function showVV() {
	var output = "";
	for (var i = 0; i < vv_land.length; ++i) {
		var land2 = vv_land[i].split("|");
 		output += "<input type=\"hidden\" name=\"VV_Land[]\" value=\"" + vv_land[i] + "\">\n";
		output += "<input type=\"hidden\" name=\"VV_Txt[]\" value=\"" + vv_txt[i] + "\">\n";		
 		output += "<hr style=\"width: 330px; height: 1px\" noshade=\"noshade\" color=\"#FFFFFF\">\n";
		output += land2[1] + ": " +  vv_txt[i] +"<br />";
 		output += "<img src=\"images/delete.png\" onClick=\"delVV(" + i + ")\" align=\"absmiddle\"> <a href=\"javascript:void(0);\" onClick=\"delVV(" + i + ")\"><?=$_SESSION['Leg_88']?></a><br />\n";
	}
	document.getElementById('VV').innerHTML = "<p>"+ output +"</p>";
	document.getElementById('VV').style.display = "block";
}


</script> 
</head>
<body>


<div id="container">
  <div id="top">
    <? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
	<? if (($error != "") && ($step == "2")) { ?>	
	<p style="border:1px solid #990000; background-color:#FFDCD6; padding:5px; width: 450px"><?=$error; ?></p>
	<? } ?>
  </div>
	<br clear="all" />
  <div id="leftSide">
<fieldset>
<legend><?=$_SESSION['Leg_8']?> 11 / 13: <?=$_SESSION['Leg_131']?></legend>
	<? echo ($_SESSION['Leg_111'] != "") ? '<p class="pagenote">'.$_SESSION['Leg_111'].'</p>' : ""; ?>
<form action="<?=$_SERVER['PHP_SELF']?>" method="post" name="form1">
<input type="hidden" value="2" name="step_11">

	
	<label for="name"><?=$_SESSION['Leg_24']?></label>
	<div class="div_blankbox">
		<p><?=$_SESSION['Leg_25']?></p>
		<div style="float:left">
			<?=$_SESSION['Leg_61']?><br />
			<input type="text" name="fs_datum" id="fs_datum" class="textbox_short" style="width:80px !important; width:70px;" /><br />
		</div>
		<div style="float:left">
			<?=$_SESSION['Leg_60']?><br />
			<input type="text" name="fs_txt" id="fs_txt" class="textbox" style="width:200px !important; width:210px;" /><br />
		</div>
		<br clear="all" />
		<div style="float:left; margin-bottom:8px;"><?=$_SESSION['Leg_40']?><br />
			<select name="fs_land_select" id="fs_land_select" class="textbox">
			<option value="">-</option>
			<?=$output_Land?>
			</select>
		</div>
		<br clear="all" />
		<img src="images/add.png" alt="+" width="16" height="16" align="absmiddle" onClick="addFS()"> <a href="#" onClick="addFS()"><?=$_SESSION['Leg_78']?></a><br />
		<div id="FS"></div>
	</div>
	<br clear="all" />
	<label for="name"><?=$_SESSION['Leg_26']?></label>
	<div class="div_blankbox">
		<p><?=$_SESSION['Leg_27']?></p>
		<div><?=$_SESSION['Leg_59']?><br />
			<input type="text" name="vv_txt" id="vv_txt" class="textbox" /><br />
		</div>
		<br clear="all" />
		<div style="float:left; margin-bottom:8px;"><?=$_SESSION['Leg_40']?><br />
			<select name="vv_land_select" id="vv_land_select" class="textbox">
			<option value="">-</option>
			<?=$output_Land?>
			</select>
		</div>
		<br clear="all" />
		<img src="images/add.png" alt="+" width="16" height="16" align="absmiddle" onClick="addVV()"> <a href="#" onClick="addVV()"><?=$_SESSION['Leg_78']?></a><br />
		<div id="VV"></div>
	</div>
	<br clear="all" />
	<div class="button_div">
	<input name="submit" type="submit" id="submit" value="<?=$_SESSION['Leg_32']?>">
	</div>
</form>
<div class="clear"></div>
</fieldset>

  </div>
  <div class="clear"></div>
</div>


<? if (($step == "2") && isset($Produktionsland[0])) {
echo '<script language="javascript">'."\n";
echo 'laender.push("'.implode("\",\"", $Produktionsland).'");'."\n";
echo 'showLaender();'."\n";
echo '</script>'."\n";
}
?>
<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>
</body>
</html>
