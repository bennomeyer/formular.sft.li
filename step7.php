<?
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
session_start();

if (!isset($_SESSION['Leg_1'])) {
	
	// redirect zur�ck zur Startseite wg. abgelaufener Session
	header("Location: /index.php?error=Session%20abgelaufen");
	exit;
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/classes/FX/FX.php"); 

$step = (isset($_POST['step_7'])) ? $_POST['step_7'] : "";
$Synop_de = (isset($_POST['Synop_de'])) ? $_POST['Synop_de'] : "";
$Synop_en = (isset($_POST['Synop_en'])) ? $_POST['Synop_en'] : "";
$Synop_fr = (isset($_POST['Synop_fr'])) ? $_POST['Synop_fr'] : "";
$Synop_it = (isset($_POST['Synop_it'])) ? $_POST['Synop_it'] : "";

$error = "";
if (($Synop_de == "") && ($Synop_en == "") && ($Synop_fr == "") && ($Synop_it == "")) {
	$error .= $_SESSION['Leg_138'];
}

if (($error == "") && ($step == "2")) {
	//Synopsis wird neu eingetragen
	if ($Synop_de != "") {
		$q = new FX('host8.kon5.net', '80', 'FMPro7');
		$q->SetDBData("Filmtage-Solothurn", "cgi_h_26__synopsis", "1"); 
		$q->SetDBUserPass ("Admin", "xs4kon5");
		$q->AddDBParam('Synopsistext', utf8_encode($Synop_de));
		$DBData = $q->FMNew();		
		foreach ($DBData['data'] as $key => $value) {
			$synop_de_id = $value['_kp__id'][0];
		}
		$q = new FX('host8.kon5.net', '80', 'FMPro7');
		$q->SetDBData("Filmtage-Solothurn", "cgi_k_24__filme_synopsis", "1"); 
		$q->SetDBUserPass ("Admin", "xs4kon5");
		$q->AddDBParam('_kf__4sprachen_id', 'de');
		$q->AddDBParam('_kf__Synopsis_Id', $synop_de_id);
		$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);
		$DBData = $q->FMNew();		
	}
	if ($Synop_en != "") {
		$q = new FX('host8.kon5.net', '80', 'FMPro7');
		$q->SetDBData("Filmtage-Solothurn", "cgi_h_26__synopsis", "1"); 
		$q->SetDBUserPass ("Admin", "xs4kon5");
		$q->AddDBParam('Synopsistext', utf8_encode($Synop_en));
		$DBData = $q->FMNew();		
		foreach ($DBData['data'] as $key => $value) {
			$synop_en_id = $value['_kp__id'][0];
		}
		$q = new FX('host8.kon5.net', '80', 'FMPro7');
		$q->SetDBData("Filmtage-Solothurn", "cgi_k_24__filme_synopsis", "1"); 
		$q->SetDBUserPass ("Admin", "xs4kon5");
		$q->AddDBParam('_kf__4sprachen_id', 'en');
		$q->AddDBParam('_kf__Synopsis_Id', $synop_en_id);
		$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);
		$DBData = $q->FMNew();		
	}
	if ($Synop_fr != "") {
		$q = new FX('host8.kon5.net', '80', 'FMPro7');
		$q->SetDBData("Filmtage-Solothurn", "cgi_h_26__synopsis", "1"); 
		$q->SetDBUserPass ("Admin", "xs4kon5");
		$q->AddDBParam('Synopsistext', utf8_encode($Synop_fr));
		$DBData = $q->FMNew();	
		foreach ($DBData['data'] as $key => $value) {
			$synop_fr_id = $value['_kp__id'][0];
		}
		$q = new FX('host8.kon5.net', '80', 'FMPro7');
		$q->SetDBData("Filmtage-Solothurn", "cgi_k_24__filme_synopsis", "1"); 
		$q->SetDBUserPass ("Admin", "xs4kon5");
		$q->AddDBParam('_kf__4sprachen_id', 'fr');
		$q->AddDBParam('_kf__Synopsis_Id', $synop_fr_id);
		$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);
		$DBData = $q->FMNew();		
	}
	if ($Synop_it != "") {
		$q = new FX('host8.kon5.net', '80', 'FMPro7');
		$q->SetDBData("Filmtage-Solothurn", "cgi_h_26__synopsis", "1"); 
		$q->SetDBUserPass ("Admin", "xs4kon5");
		$q->AddDBParam('Synopsistext', utf8_encode($Synop_it));
		$DBData = $q->FMNew();	
		foreach ($DBData['data'] as $key => $value) {
			$synop_it_id = $value['_kp__id'][0];
		}
		$q = new FX('host8.kon5.net', '80', 'FMPro7');
		$q->SetDBData("Filmtage-Solothurn", "cgi_k_24__filme_synopsis", "1"); 
		$q->SetDBUserPass ("Admin", "xs4kon5");
		$q->AddDBParam('_kf__4sprachen_id', 'it');
		$q->AddDBParam('_kf__Synopsis_Id', $synop_it_id);
		$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);
		$DBData = $q->FMNew();		
	}
	
	// redirect zur n�chsten Seite
	header("Location: /step8.php");
	exit;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Online Registration</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 <link href="/css/style.css" rel="stylesheet" type="text/css" title="KFT" />
</head>
<body>


<div id="container">
  <div id="top">
    <? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
	<? if (($step == "2") && ($error != "")) { ?>	
	<p style="border:1px solid #990000; background-color:#FFDCD6; padding:5px; width: 450px"><?=$error; ?></p>
	<? } ?>
  </div>
	<br clear="all" />
  <div id="leftSide">
<fieldset>
<legend><?=$_SESSION['Leg_8']?> 7 / 13: <?=$_SESSION['Leg_127']?></legend>
	<? echo ($_SESSION['Leg_107'] != "") ? '<p class="pagenote">'.$_SESSION['Leg_107'].'</p>' : ""; ?>
<form action="<?=$_SERVER['PHP_SELF']?>" method="post" name="form1">
<input type="hidden" value="2" name="step_7">
	<!-- p><?=$_SESSION['Leg_73']?></p -->
<?
$q = new FX("host8.kon5.net", "80", "FMPro7"); 
$q->SetDBData("Filmtage-Solothurn", "w_25__4sprachen", "999");
$q->SetDBUserPass ("Admin", "xs4kon5");
$q->AddDBParam('_kp__Sprache_ISO_id', ">0");
$DBData = $q->FMFind();  
foreach ($DBData['data'] as $key => $value) { ?>
	<label for="name"><?=$_SESSION['Leg_15']?><br /><?=$value['Sprache_'.$_SESSION['sprache']][0]?></label>
	<div class="div_blankbox"><textarea name="Synop_<?=$value['_kp__Sprache_ISO_id'][0]?>" rows="10" class="textbox"></textarea>
	</div>
	<br clear="all">
<?	
}
?>	
	
	<div class="button_div">
	<input name="submit" type="submit" id="submit" value="<?=$_SESSION['Leg_32']?>">
	</div>
</form>
<div class="clear"></div>
</fieldset>

  </div>
  <div class="clear"></div>
</div>


<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>

</body>
</html>
