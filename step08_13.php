<?
session_start();
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
if ((!$_SESSION['login_ok']) || (($_SESSION['film_id'] == "") && ($_SESSION['m'] == "u"))) {
	header("Location: login.php");
	exit;
}

require_once ('includes/db.inc.php');
$q = FX_open_layout( "cgi_h_02__filme", "1");

$step = (isset($_POST['step_13'])) ? $_POST['step_13'] : "";
$direction = (isset($_POST['direction'])) ? $_POST['direction'] : "";
$target = (isset($_POST['target'])) ? $_POST['target'] : "";
$still1 = (isset($_POST['still1'])) ? $_POST['still1'] : "";
$still2 = (isset($_POST['still2'])) ? $_POST['still2'] : "";
$still3 = (isset($_POST['still3'])) ? $_POST['still3'] : "";
$url_film = (isset($_POST['url_film'])) ? $_POST['url_film'] : "";
$url_trailer = (isset($_POST['url_trailer'])) ? $_POST['url_trailer'] : "";


// UPDATE init - alte Daten aus DB ziehen
//__________________________________________________

if (($_SESSION['m'] == "u") && ($step != "2")) {
	// Filmdaten holen
	$find =& $fm->newFindCommand('cgi_h_02__filme');
	$find->addFindCriterion('_kp__id', $_SESSION['film_id']);
	$find->addFindCriterion('_kf__Anmeldeuser_Id', $_SESSION['user_id']);
	$result = $find->execute();
	if (!FileMaker::isError($result)) {
		$records = $result->getRecords();
		$record = $records[0];
		$url_film = $record->getField('URL_Film');
		$url_trailer = $record->getField('URL_Trailer');
	}
}


// ERROR-Handling
//__________________________________________________

$error = "";
$debug = "";
if ($step == "2") {
	//if ((empty($_POST['Sprache'])) || (empty($_POST['Filmformat'])) || (empty($_POST['Bildformat'])) || (empty($_POST['Tonformat'])) ) $error .= $_SESSION['Leg_92'];
}

if (($step == "2") && ($error != "")) {
	// Update Seite2Flag mit 0
	$q->AddDBParam('-recid', $_SESSION['record_id']);
	$q->AddDBParam('zz_Anmeldung_Seite13_Flag', "0");
	$DBData = $q->FMEdit();
} elseif (($step == "2") && ($error == "")) {
	// Update Seite2Flag mit 1
	$q->AddDBParam('-recid', $_SESSION['record_id']);
	$q->AddDBParam('zz_Anmeldung_Seite13_Flag', "1");
	$DBData = $q->FMEdit();
}


// DB-Actions für Neueintragung & Update
//__________________________________________________

if (($error == "") && ($step == "2")) {

	$debug = var_export($_FILES,true);
	if (!empty($_FILES["still1"]['name'])) {
		$ext_tmp = explode(".",$_FILES['still1']['name']);
		$ext = $ext_tmp[count($ext_tmp)-1];
		$destination = $_SERVER['DOCUMENT_ROOT']."/bilder/a_original/".$_SESSION['film_id'].'.'.$ext;
		// Bild kopieren
		$debug .= "Bild1: ".$_FILES['still1']['tmp_name'].', Size: '.$_FILES['still1']['size'].', Destination: '.$destination."\n";
		if ($_FILES['still1']['size'] < 20000000) {
			$debug .= "Size OK\n";
			if(move_uploaded_file($_FILES["still1"]["tmp_name"], $destination)) {
				$debug .= "Move OK\n";
				$bild1 = true;
				$BildA = $_SESSION['film_id'].'a.'.$ext;
				$command = "/usr/local/bin/convert ".$_SERVER['DOCUMENT_ROOT']."/bilder/a_original/".$_SESSION['film_id'].'.'.$ext." -thumbnail '100x100>' ".$_SERVER['DOCUMENT_ROOT']."/bilder/a_small/".$_SESSION['film_id'].".jpg";
				exec($command);
				$command = "/usr/local/bin/convert ".$_SERVER['DOCUMENT_ROOT']."/bilder/a_original/".$_SESSION['film_id'].'.'.$ext." -thumbnail '300x300>' ".$_SERVER['DOCUMENT_ROOT']."/bilder/a_medium/".$_SESSION['film_id'].".jpg";
				exec($command);
				$command = "/usr/local/bin/convert ".$_SERVER['DOCUMENT_ROOT']."/bilder/a_original/".$_SESSION['film_id'].'.'.$ext." -thumbnail '600x600>' ".$_SERVER['DOCUMENT_ROOT']."/bilder/a_large/".$_SESSION['film_id'].".jpg";
				exec($command);
				if ('jpg' != strtolower($ext)) {
					# nur JPG speichern
					$command = "/usr/local/bin/convert ".$_SERVER['DOCUMENT_ROOT']."/bilder/a_original/".$_SESSION['film_id'].'.'.$ext." ".$_SERVER['DOCUMENT_ROOT']."/bilder/a_original/".$_SESSION['film_id'].".jpg";
					exec($command);
					unlink($destination);
				}

			} else {
				$error.= "Bildupload Still 1 schlug fehl.<br>";
			}
				
		} else {
			$error.= "Bildupload Still 1 Dateigroessee groesser als 20 MB.<br>";
		}
	}
	if (!empty($_FILES["still2"]['name'])) {
		$ext_tmp = explode(".",$_FILES['still2']['name']);
		$ext = $ext_tmp[count($ext_tmp)-1];
		$destination = $_SERVER['DOCUMENT_ROOT']."/bilder/b_original/".$_SESSION['film_id'].'.'.$ext;
		// Bild kopieren
		if ($_FILES['still2']['size'] < 20000000) {
				
			if(move_uploaded_file($_FILES["still2"]["tmp_name"], $destination)) {
				$bild2 = true;
				$BildB = $_SESSION['film_id'].'b.'.$ext;
				$command = "/usr/local/bin/convert ".$_SERVER['DOCUMENT_ROOT']."/bilder/b_original/".$_SESSION['film_id'].'.'.$ext." -thumbnail '100x100>' ".$_SERVER['DOCUMENT_ROOT']."/bilder/b_small/".$_SESSION['film_id'].".jpg";
				exec($command);
				$command = "/usr/local/bin/convert ".$_SERVER['DOCUMENT_ROOT']."/bilder/b_original/".$_SESSION['film_id'].'.'.$ext." -thumbnail '300x300>' ".$_SERVER['DOCUMENT_ROOT']."/bilder/b_medium/".$_SESSION['film_id'].".jpg";
				exec($command);
				$command = "/usr/local/bin/convert ".$_SERVER['DOCUMENT_ROOT']."/bilder/b_original/".$_SESSION['film_id'].'.'.$ext." -thumbnail '600x600>' ".$_SERVER['DOCUMENT_ROOT']."/bilder/b_large/".$_SESSION['film_id'].".jpg";
				exec($command);
				if ('jpg' != strtolower($ext)) {
					# nur JPG speichern
					$command = "/usr/local/bin/convert ".$_SERVER['DOCUMENT_ROOT']."/bilder/b_original/".$_SESSION['film_id'].'.'.$ext." ".$_SERVER['DOCUMENT_ROOT']."/bilder/b_original/".$_SESSION['film_id'].".jpg";
					exec($command);
					unlink($destination);
				}
			} else {
				$error.= "Bildupload Still 2 schlug fehl.<br>";
			}
				
		} else {
			$error.= "Bildupload Still 2 Dateigroessee groesser als 20 MB.<br>";
		}
	}
	if (!empty($_FILES["still3"]['name'])) {
		$ext_tmp = explode(".",$_FILES['still3']['name']);
		$ext = $ext_tmp[count($ext_tmp)-1];
		$destination = $_SERVER['DOCUMENT_ROOT']."/bilder/c_original/".$_SESSION['film_id'].'.'.$ext;
		// Bild kopieren
		if ($_FILES['still3']['size'] < 20000000) {
				
			if(move_uploaded_file($_FILES["still3"]["tmp_name"], $destination)) {
				$bild3 = true;
				$BildC = $_SESSION['film_id'].'c.'.$ext;
				$command = "/usr/local/bin/convert ".$_SERVER['DOCUMENT_ROOT']."/bilder/c_original/".$_SESSION['film_id'].".".$ext." -thumbnail '100x100>' ".$_SERVER['DOCUMENT_ROOT']."/bilder/c_small/".$_SESSION['film_id'].".jpg";
				exec($command);
				$command = "/usr/local/bin/convert ".$_SERVER['DOCUMENT_ROOT']."/bilder/c_original/".$_SESSION['film_id'].".".$ext." -thumbnail '300x300>' ".$_SERVER['DOCUMENT_ROOT']."/bilder/c_medium/".$_SESSION['film_id'].".jpg";
				exec($command);
				$command = "/usr/local/bin/convert ".$_SERVER['DOCUMENT_ROOT']."/bilder/c_original/".$_SESSION['film_id'].".".$ext." -thumbnail '600x600>' ".$_SERVER['DOCUMENT_ROOT']."/bilder/c_large/".$_SESSION['film_id'].".jpg";
				exec($command);
				if ('jpg' != strtolower($ext)) {
					# nur JPG speichern
					$command = "/usr/local/bin/convert ".$_SERVER['DOCUMENT_ROOT']."/bilder/c_original/".$_SESSION['film_id'].'.'.$ext." ".$_SERVER['DOCUMENT_ROOT']."/bilder/c_original/".$_SESSION['film_id'].".jpg";
					exec($command);
					unlink($destination);
				}
			} else {
				$error.= "Bildupload Still 3 schlug fehl.<br>";
			}
				
		} else {
			$error.= "Bildupload Still 3 Dateigroessee groesser als 20 MB.<br>";
		}
	}

	// Update Datenbank mit Bildinfos und WWW-Adressen

	$q = FX_open_layout( "cgi_h_02__filme", "1");
	$q->AddDBParam('-recid', $_SESSION['record_id']);
	if ($bild1) $q->AddDBParam('Bild_a', "1");
	if ($bild2) $q->AddDBParam('Bild_b', "1");
	if ($bild3) $q->AddDBParam('Bild_c', "1");
	$q->AddDBParam('URL_Film', $url_film);
	$q->AddDBParam('URL_Trailer', $url_trailer);
	$DBData = $q->FMEdit();
}

if (($direction == "next") && ($step == "2") && ($error == "")) {
	if ($target != "") {
		header("Location: /$target");
	} else {
		//redirect zur nächsten Seite
		header("Location: /step08_14.php");
	}
	exit;
}
if (($direction == "back") && ($step == "2")) {
	if ($target != "") {
		header("Location: /$target");
	} else {
		//redirect zur vorherigen Seite
		header("Location: /step08_12.php");
	}
	exit;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- <?= $debug ?> -->
<title>Online Registration</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="/css/style2008.css" rel="stylesheet" type="text/css"
	title="KFT" />

<script type="text/javascript" src="includes/scripts.js"></script>
<script type="text/javascript" language="javascript">
<!--
	var ol_width = 300;
	var ol_fgcolor='#d9e2ec';
	var ol_bgcolor='#006699';
	
function goBack() {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = "back";
	document.form1.submit();
}
function goNext() {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = "next";
	document.form1.submit();
}
function jumpto(target,direction) {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = direction;
	document.getElementById('target').value = target;
	document.form1.submit();
}
//-->
</script>
<script language="javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>
</head>
<body onload="MM_preloadImages('images/loader.gif')">


	<div id="container">
		<div id="top">
		<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
		</div>
		<br clear="all" />
		<div id="navi">
		<? $seite = "13"; include($_SERVER['DOCUMENT_ROOT'].'/includes/navigation.inc.php'); ?>
			<div id="loader" style="display: none">
				<img src="/images/loader.gif"
					style="margin-top: 8px; margin-left: 45px" width="32" height="32" />
			</div>
		</div>
		<div id="leftSide">
			<fieldset>
				<p class="legend">
				<?=$_SESSION['Leg_8']?>
					13 / 14:
					<?=$_SESSION['Leg_132']?>
				</p>
				<? if (($error != "") && ($step == "2")) { ?>
				<p
					style="border: 1px solid #990000; background-color: #FFDCD6; padding: 5px; width: 545px">
					<?=$error; ?>
				</p>
				<? } ?>
				<? echo ($_SESSION['Leg_112'] != "") ? '<p class="pagenote">'.$_SESSION['Leg_112'].'</p>' : ""; ?>
				<form action="<?=$_SERVER['PHP_SELF']?>" method="post" name="form1"
					enctype="multipart/form-data">
					<input type="hidden" value="2" name="step_13" /> <input
						type="hidden" name="direction" id="direction" value="" /> <input
						type="hidden" name="target" id="target" value="" /> <label
						for="name"><?=$_SESSION['Leg_28']?> </label>
					<div class="div_blankbox">
						<p>
						<?=$_SESSION['Leg_29']?>
						</p>
						<div style="float: left">
						<?=$_SESSION['Leg_28']?>
							1: &nbsp;&nbsp;
						</div>
						<div style="float: left">
							<input type="file" name="still1" id="still1" class="textbox" /><br />
							<? if (file_exists($_SERVER['DOCUMENT_ROOT']."/bilder/a_small/".$_SESSION['film_id'].".jpg")) echo '<img src="/bilder/a_small/'.$_SESSION['film_id'].'.jpg">'; ?>
							<br /> <br />
						</div>
						<br clear="all" />
						<div style="float: left">
						<?=$_SESSION['Leg_28']?>
							2: &nbsp;&nbsp;
						</div>
						<div style="float: left">
							<input type="file" name="still2" id="still2" class="textbox" /><br />
							<? if (file_exists($_SERVER['DOCUMENT_ROOT']."/bilder/b_small/".$_SESSION['film_id'].".jpg")) echo '<img src="/bilder/b_small/'.$_SESSION['film_id'].'.jpg">'; ?>
							<br /> <br />
						</div>
						<br clear="all" />
						<div style="float: left">
						<?=$_SESSION['Leg_28']?>
							3: &nbsp;&nbsp;
						</div>
						<div style="float: left">
							<input type="file" name="still3" id="still3" class="textbox" /><br />
							<? if (file_exists($_SERVER['DOCUMENT_ROOT']."/bilder/c_small/".$_SESSION['film_id'].".jpg")) echo '<img src="/bilder/c_small/'.$_SESSION['film_id'].'.jpg">'; ?>
							<br /> <br />
						</div>
						<br clear="all" /> <br clear="all" />
						<div style="float: left; width: 70px;"><?= $_SESSION['Leg_273']?>: &nbsp;&nbsp;</div>
						<div style="float: left">
							<input type="text" name="url_film" id="url_film" class="textbox"
								value="<?=$url_film?>" /><br />
						</div>
						<br clear="all" />
						<div style="float: left; width: 70px;"><?= $_SESSION['Leg_274']?>: &nbsp;&nbsp;</div>
						<div style="float: left">
							<input type="text" name="url_trailer" id="url_trailer"
								class="textbox" value="<?=$url_trailer?>" /><br />
						</div>
						<br clear="all" />
					</div>
					<br clear="all" />
					<div class="prevBtn">
						<input type="button" id="back" value="<?=$_SESSION['Leg_31']?>"
							onclick="goBack('')" />
					</div>
					<div class="nxtBtn">
						<input type="button" id="next" value="<?=$_SESSION['Leg_32']?>"
							onclick="goNext('')" />
					</div>
					<br clear="all" />
				</form>
				<div class="clear"></div>
			</fieldset>

		</div>
		<br clear="all" />
		<div class="clear"></div>
	</div>


	<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>
</body>
</html>
