// Firmensuche
//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__


function newFirma(type){
	newFirmaDlg.show();
}

function newfirma_search(){
	document.getElementById('span_searchresult_newfirma').innerHTML = "<blink>...</blink>";
	var oForm = document.getElementById('form_search_newfirma');
	YAHOO.util.Connect.setForm(oForm);
	YAHOO.util.Connect.asyncRequest('POST', '/includes/ajax_search_newfirma.php', { success: newfirma_searchresult, failure: failure_ajax });
}

var newfirma_searchresult = function(obj){
	document.getElementById('span_searchresult_newfirma').innerHTML = obj.responseText;
};

function newfirma_save(id,firma,plz,ort,type){
	YAHOO.util.Connect.asyncRequest('GET', '/includes/ajax_save_firma.php?firma_id='+id+'&firma_name='+firma+'&firma_plz='+plz+'&firma_ort='+ort+'&firma_type='+type, { success: newfirma_saved });
}




function newfirma_add(){
	var error = "";
	var email_bad = false;
	if (document.getElementById('email').value != "") {
		var x = document.getElementById('email').value;
		var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		email_bad = true;
		if (filter.test(x)) email_bad = false;
	}
	if ((document.getElementById('firma').value == "") ||
		(document.getElementById('strasse1').value == "") ||
		(document.getElementById('plz').value == "") ||
		(document.getElementById('ort').value == "") ||
		(document.getElementById('land').value == "") ||
		(email_bad)
		) {
		var error_txt = "<?=$_SESSION['Leg_86']?>";
		if (email_bad) { error_txt += "<br /><?=$_SESSION['Leg_87']?>"; }
		miniDialog.setHeader("<?=$_SESSION['Leg_85']?>");
		miniDialog.setBody(error_txt);
		miniDialog.render(document.body);
		miniDialog.show();
		error = "1";
	}
	if (error == "") {
		var oForm = document.getElementById('form_add_newfirma');
		YAHOO.util.Connect.setForm(oForm);
		//YAHOO.util.Connect.asyncRequest('POST', '/includes/ajax_add_newperson.php', callback);
		YAHOO.util.Connect.asyncRequest('POST', '/includes/ajax_add_newfirma.php', { success: newfirma_addresult, failure: failure_ajax });
	}
}


var newfirma_saved = function(obj){
	newFirmaDlg.hide();
	document.getElementById('span_searchresult_newfirma').innerHTML = "";
	document.getElementById('firma').value = "";
	document.getElementById('ort').value = "";
	try {
		eval(obj.responseText);
	}
	catch(err){
		infoDialog("Hinweis","Es gab ein Problem bei der Kommunikation mit dem Server, bitte versuchen Sie es zu einem sp&auml;teren Zeitpunkt erneut.<br>"+err.description);
	}
};

var delFirmaSuccess = function (obj){
	miniDialog.hide();
	try{
		eval(obj.responseText);	
	}
	catch(err){
		infoDialog("Hinweis","Es gab ein Problem bei der Kommunikation mit dem Server, bitte versuchen Sie es zu einem sp&auml;teren Zeitpunkt erneut.<br>"+err.description);
	}
};
// Abfrage ob ein Verweis wirklich gel�scht werden soll.
function deleteFirma(type,id){
	delFirmaType = type;
	delFirmaId = id;
	
	miniDialog.setHeader("<?=$_SESSION['Leg_80']?>");
	miniDialog.setBody("<?=$_SESSION['Leg_84']?>");
	miniDialog.cfg.queueProperty("icon",YAHOO.widget.SimpleDialog.ICON_WARN);
	miniDialog.cfg.queueProperty("buttons", [ 
		{ text: "<?=$_SESSION['Leg_82']?>", handler:delFirmaConfirm },
		{ text: "<?=$_SESSION['Leg_83']?>", handler:handlerHide, isDefault:true  }
	]);
	miniDialog.render(document.body);
	miniDialog.show();
}

// Verweisentfernung durchfhren
var delFirmaConfirm = function (obj){
	//loading(true);
	YAHOO.util.Connect.asyncRequest('GET', '/includes/ajax_delete_firma.php?firma_type='+delFirmaType+'&firma_record_id='+delFirmaId, { success: delFirmaSuccess, failure: failure_ajax });
};





// Neue Firma der DB hinzuf�gen
//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

function addFirmaStart(){
	newFirmaDlg.hide();
	//document.getElementById('personen_typ').value = type;
	addFirmaDlg.show();
	document.getElementById('firma').focus();
}


// var callback = { upload: newperson_addresult }
var newfirma_addresult = function(obj){
	//loading(false);
	addFirmaDlg.hide();
	try {
		eval(obj.responseText);
	}
	catch(err){
		infoDialog("Hinweis","Es gab ein Problem bei der Kommunikation mit dem Server, bitte versuchen Sie es zu einem sp&auml;teren Zeitpunkt erneut.<br>"+err.description);
	}
};


