	addFirmaDlg = new YAHOO.widget.Dialog("div_dlg_addfirma", { modal:true, visible:false, iframe:true, width:"370px", x:100, y:70,  constraintoviewport:true, draggable:true });
	
	// START safari-fix of close on return
	addFirmaDlg.hideOnSubmit = false;	                
	var listeners = [new YAHOO.util.KeyListener(document, 
       {keys : 27}, {fn:handleCancel, scope:addFirmaDlg, correctScope:true}),
       new YAHOO.util.KeyListener(document, 
       {keys : [13,10]}, {fn:newfirma_add, scope:addFirmaDlg, correctScope:true})];
    addFirmaDlg.cfg.queueProperty("keylisteners", listeners);
	// END safari-fix of close on return
					
	addFirmaDlg.cfg.queueProperty("buttons", [ { text:"<?=$_SESSION['Leg_114']?>", handler:newfirma_add },{ text:"<?=$_SESSION['Leg_99']?>", handler:handleCancel } ]);
	addFirmaDlg.cfg.setProperty('postmethod','async')
	addFirmaDlg.render();

