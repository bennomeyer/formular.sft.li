	newFirmaDlg = new YAHOO.widget.Dialog("div_dlg_newfirma", { modal:true, visible:false, iframe:true, width:"370px", x:100, y:70,  constraintoviewport:true, draggable:true });
	
	// START safari-fix of close on return
	newFirmaDlg.hideOnSubmit = false;	                
	var listeners = [new YAHOO.util.KeyListener(document, 
       {keys : 27}, {fn:handleCancel, scope:newFirmaDlg, correctScope:true}),
       new YAHOO.util.KeyListener(document, 
       {keys : [13,10]}, {fn:newfirma_search, scope:newFirmaDlg, correctScope:true})];
    newFirmaDlg.cfg.queueProperty("keylisteners", listeners);
	// END safari-fix of close on return
					
	newFirmaDlg.cfg.queueProperty("buttons", [ { text:"<?=$_SESSION['Leg_99']?>", handler:handleCancel } ]);
	newFirmaDlg.cfg.setProperty('postmethod','async')
	newFirmaDlg.render();