
// Neue Stab-Person der DB hinzufügen
//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

function addStabStart(type){
	newPersonDlg.hide();
	document.getElementById('newperson_type').value = type;
	document.getElementById('new_type').value = type;
	addStabDlg.show();
}

function newstab_add(){
	var error = "";
	if ((document.getElementById('vorname2').value == "") ||
		(document.getElementById('name').value == "")
		) {
		miniDialog.setHeader("<?=$_SESSION['Leg_85']?>");
		miniDialog.setBody("<?=$_SESSION['Leg_86']?>");
		miniDialog.render(document.body);
		miniDialog.show();
		error = "1";
	}
	if (error == "") {
		var oForm = document.getElementById('form_add_stab');
		YAHOO.util.Connect.setForm(oForm);
		//YAHOO.util.Connect.asyncRequest('POST', '/includes/ajax_add_newperson.php', callback);
		YAHOO.util.Connect.asyncRequest('POST', '/includes/ajax_add_newperson.php', { success: newstab_addresult, failure: failure_ajax });
	}
}

// var callback = { upload: newperson_addresult }
var newstab_addresult = function(obj){
	//loading(false);
	addStabDlg.hide();
	try {
		eval(obj.responseText);
	}
	catch(err){
		infoDialog("Hinweis","Es gab ein Problem bei der Kommunikation mit dem Server, bitte versuchen Sie es zu einem sp&auml;teren Zeitpunkt erneut.<br>"+err.description);
	}
};