miniDialog = new YAHOO.widget.SimpleDialog("mini_dlg", { visible:false, width: "25em", effect:[
	{effect:YAHOO.widget.ContainerEffect.SLIDE,duration:0.25}, {effect:YAHOO.widget.ContainerEffect.FADE,duration:0.25}
	], fixedcenter : true, modal:true, draggable:false });

miniDialog.setHeader("Attention!");
miniDialog.setBody("Do you really want to do this?");

miniDialog.cfg.queueProperty("icon",YAHOO.widget.SimpleDialog.ICON_INFO);
miniDialog.cfg.queueProperty("buttons", [ 
	{ text:"OK", handler:handlerHide, isDefault:true }
]);

miniDialog.render(document.body);

function infoDialog(titel,text){
	miniDialog.setHeader(titel);
	miniDialog.setBody(text);
	miniDialog.cfg.queueProperty("icon",YAHOO.widget.SimpleDialog.ICON_INFO);
	miniDialog.cfg.queueProperty("buttons", [ 
		{ text:"OK", handler:handlerHide, isDefault:true }
	]);
	miniDialog.render(document.body);
	miniDialog.show();
}

function helpDialog(titel,text){
	miniDialog.setHeader(titel);
	miniDialog.setBody(text);
	miniDialog.cfg.queueProperty("icon",YAHOO.widget.SimpleDialog.ICON_HELP);
	miniDialog.cfg.queueProperty("buttons", [ 
		{ text:"OK", handler:handlerHide, isDefault:true }
	]);
	miniDialog.render(document.body);
	miniDialog.show();
}

	
