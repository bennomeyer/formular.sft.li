	
	addStabDlg = new YAHOO.widget.Dialog("div_dlg_addstab", { modal:true, visible:false, iframe:true, width:"370px", fixedcenter : true, constraintoviewport:true, draggable:true });
	
	// START safari-fix of close on return
	addStabDlg.hideOnSubmit = false;	                
	var listeners = [new YAHOO.util.KeyListener(document, 
       {keys : 27}, {fn:handleCancel, scope:addStabDlg, correctScope:true}),
       new YAHOO.util.KeyListener(document, 
       {keys : [13,10]}, {fn:newstab_add, scope:addStabDlg, correctScope:true})];
    addStabDlg.cfg.queueProperty("keylisteners", listeners);
	// END safari-fix of close on return
					
	addStabDlg.cfg.queueProperty("buttons", [ { text:"<?=$_SESSION['Leg_114']?>", handler:newstab_add },{ text:"<?=$_SESSION['Leg_99']?>", handler:handleCancel } ]);
	addStabDlg.cfg.setProperty('postmethod','async')
	addStabDlg.render();
