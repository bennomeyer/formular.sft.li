/* JavaScript for Step 9
 *	(c) 2013 Martin Meier, KON5
 * @param navi_passive (globale Variable, die die Navi-Spalte während der Seitenwechsels enthält)
 */

/* Common for all steps */
function jumpto(target,direction) {
	document.getElementById('navi').innerHTML = navi_passive;
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = direction;
	document.getElementById('target').value = target;
	document.form1.submit();
}

function goNext() { jumpto(null,'next'); }
function goBack() { jumpto(null,'back'); }


function setOptionList(sel,list,filter){
	// filter list by format_id
	var my_list = list.filter(function(el){
		return (el.format_id == filter);
	});
	var optionsAsString="<option value=''>-</option>";
	for(var i=0; i< my_list.length; i++){
		optionsAsString += '<option value="'+my_list[i].id+'">'+my_list[i].label+'</option>';
	}
	$(sel).find('option').remove().end().append(optionsAsString);
}

function display_ut(){
	// abhängig von ut-typ details anzeigen
	var utt = $('input[name=Untertitel_Typ]:checked').val();
	switch (utt) {
	case "11": // open caption
		$('#uts').show();
		$('#ut_sprachen').hide();
		break;
	case "12": // closed caption
		$('#uts').hide();
		$('#ut_sprachen').show();
		break;
	default:
		$('#uts').hide();
		$('#ut_sprachen').hide();
	}
}

function initial_hide(){
	$('.sync_lang').hide();
	$('#sync_lang').hide();
	$('.audio').hide();
	$('#uts').hide();
	$('#ut_sprachen').hide();
	$('#uts_dcp').hide();
	$('#uts_other').hide();

}

/*
 * Land-Auswahl-Listen (Ton und cc)
 */
var ton_s = new Array;
var cc_s  = new Array;

// disp ist cc oder ton
// select heist cc_select oder ton_select
// display heisst cc_list oder ton_list
// inputs heissen cc_sprachen oder ton_sprachen
function addSprache(liste,disp){
	var select=$('#'+disp+'_select');
	var val=select.val();
	if (val!=""){
		liste.push(val);
		select.val('');
		showSprachen(liste,disp);
	}
}
function delSprache(liste,i,disp){
	liste.splice(i,1);
	showSprachen(liste,disp);
}
function showSprachen(liste,disp){
	var out='';
	for (var i=0; i<liste.length; i++){
		var parts=liste[i].split('|');
		out +='<div class="lang_val"><input type="hidden" name="'+disp+'_sprachen[]" value="'+parts[0]+'" />'+parts[1];
		out += '<div class="list_del"><img src="images/delete.png" onclick="del_'+disp+'('+i+')" /></div></div>';
	}
	$('#'+disp+'_list').html(out);
}

function del_cc(i){delSprache(cc_s,i,'cc');}
function del_ton(i){delSprache(ton_s,i,'ton');}

function save_copy(){
	var values=$('#copy_form').serialize();
	$('#errors').hide();
	$('#btn_save').hide();
	$('#copy_loader').show();
	$.post("includes/ajax_add_film_copy.php",values,function(data){
		if (data['status']=='ok'){
			$('#saved_copies').append(data['new_copy']);
			$('#copy_loader').hide();
			$('#btn_save').show();
			$('#copy_form')[0].reset();
			cc_s=new Array;
			ton_s=new Array;
			showSprachen(cc_s,'cc');
			showSprachen(ton_s,'ton');
			initial_hide();
		} else {
			$('#errors').show().html(data['msg']);
			$('#copy_loader').hide();
			$('#btn_save').show();
		}
	},"json").fail(function(){
		$('#errors').show().html('Es gab ein Problem bei der Kommunikation mit dem Server, bitte versuchen Sie es zu einem sp&auml;teren Zeitpunkt erneut.');
		$('#copy_loader').hide();
		$('#btn_save').show();
	});
}

function del_copy(){
	var that=$(this);
	that.find('img').attr('src','images/loader.gif');
	var cid = { cid: that.data('cid')};
	$.post('includes/ajax_del_film_copy.php',cid,function(data){
		that.closest('.div_blankbox').remove();
	});
}
/**
 * Hierarchie select 
 */
jQuery(document).ready(function(){
	initial_hide();
	$('#Synchronfassung_Flag').change(function(){
		if (this.checked) {
			$('.sync_lang').show();
		} else {
			$('.sync_lang').hide();
		}
	});
	$('#Filmformat').change(function(){
		var that=$(this);
		var val=that.val();
		setOptionList('#Bildformat',bildformate,val);
		setOptionList('#Tonformat',tonformate,val);
		if (8==val) { // DCP
			$('#sync_lang').hide();
			$('.audio').show();
			display_ut();
			$('#uts_other').hide();
			$('#uts_dcp').show();
		} else if (''==val) {
			initial_hide();
		} else { // not DCP
			$('#sync_lang').show();
			$('.audio').hide();
			$('#uts').show();
			// $('#ut_sprachen').show();
			$('#uts_dcp').hide();
			$('#uts_other').show();
		}
	});
	$('.radios input').change(function(){
		display_ut();
	});
	
	$('#btn_add_ton').click(function(){
		addSprache(ton_s,'ton');
	});
	$('#btn_add_ut').click(function(){
		addSprache(cc_s,'cc');
	});
	$('#btn_save').click(save_copy);
	$(document).on('click','.del_copy',del_copy);
});
