newPersonDlg = new YAHOO.widget.Dialog("div_dlg_newperson", { modal:true, visible:false, iframe:true, width:"370px", x:100, y:70, constraintoviewport:true, draggable:true });
	
	// START safari-fix of close on return
	newPersonDlg.hideOnSubmit = false;	                
	var listeners = [new YAHOO.util.KeyListener(document, 
       {keys : 27}, {fn:handleCancel, scope:newPersonDlg, correctScope:true}),
       new YAHOO.util.KeyListener(document, 
       {keys : [13,10]}, {fn:newperson_search, scope:newPersonDlg, correctScope:true})];
    newPersonDlg.cfg.queueProperty("keylisteners", listeners);
	// END safari-fix of close on return
					
	newPersonDlg.cfg.queueProperty("buttons", [ { text:"<?=$_SESSION['Leg_212']?>", handler:handleCancel} ]);
	
	newPersonDlg.cfg.setProperty('postmethod','async')
	newPersonDlg.render();
	