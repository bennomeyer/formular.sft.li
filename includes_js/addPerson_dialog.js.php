	addPersonDlg = new YAHOO.widget.Dialog("div_dlg_addperson", { modal:true, visible:false, iframe:true, width:"550px",x:100,  y:70,  constraintoviewport:true, draggable:true });
	
	// START safari-fix of close on return
	addPersonDlg.hideOnSubmit = false;	                
	var listeners = [new YAHOO.util.KeyListener(document, 
       {keys : 27}, {fn:handleCancel, scope:addPersonDlg, correctScope:true}),
       new YAHOO.util.KeyListener(document, 
       {keys : [13,10]}, {fn:newperson_add, scope:addPersonDlg, correctScope:true})];
    addPersonDlg.cfg.queueProperty("keylisteners", listeners);
	// END safari-fix of close on return
					
	addPersonDlg.cfg.queueProperty("buttons", [ { text:"<?=$_SESSION['Leg_114']?>", handler:newperson_add },{ text:"<?=$_SESSION['Leg_212']?>", handler:handleCancel } ]);
	addPersonDlg.cfg.setProperty('postmethod','async')
	addPersonDlg.render();


// Email update


	updateEmailDlg = new YAHOO.widget.Dialog("addEmailAddress", { modal:true, visible:false, iframe:true, width:"550px",x:100,  y:70,  constraintoviewport:true, draggable:true });
	
	// START safari-fix of close on return
	updateEmailDlg.hideOnSubmit = false;	                
	var listeners = [new YAHOO.util.KeyListener(document, 
       {keys : 27}, {fn:handleCancel, scope:updateEmailDlg, correctScope:true}),
       new YAHOO.util.KeyListener(document, 
       {keys : [13,10]}, {fn:update_email, scope:updateEmailDlg, correctScope:true})];
    updateEmailDlg.cfg.queueProperty("keylisteners", listeners);
	// END safari-fix of close on return
					
	updateEmailDlg.cfg.queueProperty("buttons", [ { text:"<?=$_SESSION['Leg_114']?>", handler:update_email },{ text:"<?=$_SESSION['Leg_212']?>", handler:handleCancel } ]);
	updateEmailDlg.cfg.setProperty('postmethod','async')
	updateEmailDlg.render();
	
