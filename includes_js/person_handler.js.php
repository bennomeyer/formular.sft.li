// Personen dem Film hinzuf�gen und l�schen
//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__



function newPerson(type){
	document.getElementById('personen_typ').value = type;
	document.getElementById('type').value = type;
	newPersonDlg.show();
}

// Wann: success von Suche nach "Neuem Verweis"
var newperson_searchresult = function(obj){
	document.getElementById('span_searchresult_newperson').innerHTML = obj.responseText;
};

function newperson_search(){
	document.getElementById('span_searchresult_newperson').innerHTML = "<blink>...</blink>";
	document.getElementById('p_vorname2').value = "";
	document.getElementById('p_name').value = "";
	document.getElementById('p_strasse1').value = "";
	document.getElementById('p_strasse2').value = "";
	document.getElementById('p_plz').value = "";
	document.getElementById('p_ort').value = "";
	document.getElementById('p_land').value = "";
	document.getElementById('p_fon').value = "";
	document.getElementById('p_fax').value = "";
	document.getElementById('p_email').value = "";
	document.getElementById('p_www').value = "";
	document.getElementById('p_bio').value = "";
	document.getElementById('p_geburtsjahr').value = "";
	document.getElementById('p_geburtsort').value = "";
	document.getElementById('p_heimatort').value = "";
	document.getElementById('temp_jahr').value = "";
	document.getElementById('temp_filmtitel').value = "";
	var oForm = document.getElementById('form_search_newperson');
	YAHOO.util.Connect.setForm(oForm);
	YAHOO.util.Connect.asyncRequest('POST', '/includes/ajax_search_newperson.php', { success: newperson_searchresult, failure: failure_ajax });
}

function newperson_getpage(start){
	document.getElementById('span_searchresult_newperson').innerHTML = "<blink>...</blink>";
	YAHOO.util.Connect.asyncRequest('GET', '/includes/ajax_get_nlsr.php?start='+start, { success: newperson_searchresult, failure: failure_ajax });
}

var newperson_saved = function(obj){
	newPersonDlg.hide();
	document.getElementById('span_searchresult_newperson').innerHTML = "";
	document.getElementById('vorname').value = "";
	document.getElementById('nachname').value = "";
	try {
		eval(obj.responseText);
	}
	catch(err){
		infoDialog("Hinweis","Es gab ein Problem bei der Kommunikation mit dem Server, bitte versuchen Sie es zu einem sp&auml;teren Zeitpunkt erneut.<br>"+err.description);
	}
	new_update_email(id)
};


function newperson_save(id,vorname,name,type,needemail){
	YAHOO.util.Connect.asyncRequest('GET', '/includes/ajax_save_person.php?person_id='+id+'&person_vorname='+vorname+'&person_name='+name+'&person_type='+type+'&person_needemail='+needemail, { success: newperson_saved });
}

// Abfrage ob ein Verweis wirklich gel�scht werden soll.
function deletePerson(type,id){
	delPersonType = type;
	delPersonId = id;
	
	miniDialog.setHeader("<?=$_SESSION['Leg_80']?>");
	miniDialog.setBody("<?=$_SESSION['Leg_81']?>");
	miniDialog.cfg.queueProperty("icon",YAHOO.widget.SimpleDialog.ICON_WARN);
	miniDialog.cfg.queueProperty("buttons", [ 
		{ text: "<?=$_SESSION['Leg_82']?>", handler:delPersonConfirm },
		{ text: "<?=$_SESSION['Leg_83']?>", handler:handlerHide, isDefault:true  }
	]);
	miniDialog.render(document.body);
	miniDialog.show();
}

var delPersonSuccess = function (obj){
	miniDialog.hide();
	try{
		eval(obj.responseText);	
	}
	catch(err){
		infoDialog("Hinweis","Es gab ein Problem bei der Kommunikation mit dem Server, bitte versuchen Sie es zu einem sp&auml;teren Zeitpunkt erneut.<br>"+err.description);
	}
};


// Verweisentfernung durchfhren
var delPersonConfirm = function (obj){
	//loading(true);
	YAHOO.util.Connect.asyncRequest('GET', '/includes/ajax_delete_person.php?person_type='+delPersonType+"&person_record_id="+delPersonId, { success: delPersonSuccess, failure: failure_ajax });
};




// Neue Person der DB hinzuf�gen
//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

function addPersonStart(){
	newPersonDlg.hide();
	//document.getElementById('personen_typ').value = type;
	addPersonDlg.show();
}


// var callback = { upload: newperson_addresult }
var newperson_addresult = function(obj){
	//loading(false);
	addPersonDlg.hide();
	try {
		eval(obj.responseText);
	}
	catch(err){
		infoDialog("Hinweis","Es gab ein Problem bei der Kommunikation mit dem Server, bitte versuchen Sie es zu einem sp&auml;teren Zeitpunkt erneut.<br>"+err.description);
	}
};
function newperson_add(){
	var error = "";
	var email_bad = false;
	if (document.getElementById('p_email').value != "") {
		var x = document.getElementById('p_email').value;
		var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		email_bad = true;
		if (filter.test(x)) email_bad = false;
	}
	
	if ((document.getElementById('p_vorname2').value == "") ||
		(document.getElementById('p_name').value == "") ||
		(document.getElementById('p_strasse1').value == "") ||
		(document.getElementById('p_plz').value == "") ||
		(document.getElementById('p_ort').value == "") ||
		(document.getElementById('p_land').value == "") ||
		(document.getElementById('p_tel_mob').value == "") ||
		(document.getElementById('p_sex').value == "") ||
		(document.getElementById('p_korrsprache').value == "") ||
		(document.getElementById('p_email').value == "") ||
		(email_bad)
		) {
		var error_txt = "<?=$_SESSION['Leg_86']?>";
		if (email_bad) { error_txt += "<br /><?=$_SESSION['Leg_87']?>"; }
		miniDialog.setHeader("<?=$_SESSION['Leg_85']?>");
		miniDialog.setBody(error_txt);
		miniDialog.render(document.body);
		miniDialog.show();
		error = "1";
	}
	if (error == "") {
		var oForm = document.getElementById('form_add_newperson');
		YAHOO.util.Connect.setForm(oForm);
		//YAHOO.util.Connect.asyncRequest('POST', '/includes/ajax_add_newperson.php', callback);
		YAHOO.util.Connect.asyncRequest('POST', '/includes/ajax_add_newperson.php', { success: newperson_addresult, failure: failure_ajax });
	}
}

// E-Mail Adresse des Regisseurs updaten
//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

function new_update_email(id){
	document.getElementById('update_id').value = id;
	updateEmailDlg.show();
}

function update_email() {
	var oForm = document.getElementById('update_email_form');
	YAHOO.util.Connect.setForm(oForm);
	YAHOO.util.Connect.asyncRequest('POST', '/includes/ajax_update_email_regie.php', { success: update_email_final, failure: failure_ajax });
}

var update_email_final = function(obj){
	document.getElementById('update_id').value = "";
	updateEmailDlg.hide();
	try {
		eval(obj.responseText);
	}
	catch(err){
		infoDialog("Hinweis","Es gab ein Problem bei der Kommunikation mit dem Server, bitte versuchen Sie es zu einem sp&auml;teren Zeitpunkt erneut.<br>"+err.description);
	}
};