<?
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
session_start();

if (!isset($_SESSION['Leg_1'])) {
	
	// redirect zur�ck zur Startseite wg. abgelaufener Session
	header("Location: /index.php?error=Session%20abgelaufen");
	exit;
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/classes/FX/FX.php"); 

$step = (isset($_POST['step_10'])) ? $_POST['step_10'] : "";
$Premiere = (isset($_POST['Premiere'])) ? $_POST['Premiere'] : "";

$F_Jahr = (!empty($_POST['F_Jahr'])) ? $_POST['F_Jahr'] : array();
$F_Land = (!empty($_POST['F_Land'])) ? $_POST['F_Land'] : array();
$F_Txt = (!empty($_POST['F_Txt'])) ? $_POST['F_Txt'] : array();
$P_Jahr = (!empty($_POST['P_Jahr'])) ? $_POST['P_Jahr'] : array();
$P_Ort = (!empty($_POST['P_Ort'])) ? $_POST['P_Ort'] : array();
$P_Land = (!empty($_POST['P_Land'])) ? $_POST['P_Land'] : array();
$P_Txt = (!empty($_POST['P_Txt'])) ? $_POST['P_Txt'] : array();

$error = "";
if ($step == "2") {
	//if ((empty($_POST['Sprache'])) || (empty($_POST['Filmformat'])) || (empty($_POST['Bildformat'])) || (empty($_POST['Tonformat'])) ) $error .= $_SESSION['Leg_92'];
} 



if (($error == "") && ($step == "2")) {
		
	
	if (isset($F_Jahr[0])) {
		$i = 0;
		foreach ($F_Jahr as $F_Jahr_zahl) { 
			$f_land_code = explode("|", $F_Land[$i]);
			
			// Festivaleintrag
			$q = new FX('host8.kon5.net', '80', 'FMPro7');
			$q->SetDBData("Filmtage-Solothurn", "cgi_h_31__festivals", "1"); 
			$q->SetDBUserPass ("Admin", "xs4kon5");
			$q->AddDBParam('Jahr', $F_Jahr[$i]);
			$q->AddDBParam('_kf__ISO_Land_Id', $f_land_code[0]);	
			$q->AddDBParam('Festival', $F_Txt[$i]);	
			$DBData = $q->FMNew();
			foreach ($DBData['data'] as $key => $value) {
				$festival_id = $value['_kp__id'][0];
			}
			
			// Kreuztabelleneintrag
			$q = new FX('host8.kon5.net', '80', 'FMPro7');
			$q->SetDBData("Filmtage-Solothurn", "cgi_k_30__filme_festivals", "1"); 
			$q->SetDBUserPass ("Admin", "xs4kon5");
			$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);	
			$q->AddDBParam('_kf__Festival_Id', $festival_id);	
			$q->AddDBParam('_kf__ISO_Land_Id', $f_land_code[0]);	
			$DBData = $q->FMNew();
			$i++;		
		}
	}	
	if (isset($P_Jahr[0])) {
		$i = 0;
		foreach ($P_Jahr as $P_Jahr_zahl) { 
			$p_land_code = explode("|", $P_Land[$i]);
			
			// Festivaleintrag
			$q = new FX('host8.kon5.net', '80', 'FMPro7');
			$q->SetDBData("Filmtage-Solothurn", "cgi_h_33__preise", "1"); 
			$q->SetDBUserPass ("Admin", "xs4kon5");
			$q->AddDBParam('Jahr', $P_Jahr[$i]);
			$q->AddDBParam('_kf__ISO_Land_Id', $p_land_code[0]);	
			$q->AddDBParam('Preis', $P_Txt[$i]);	
			$q->AddDBParam('Ort', $P_Ort[$i]);	
			$DBData = $q->FMNew();
			foreach ($DBData['data'] as $key => $value) {
				$preis_id = $value['_kp__id'][0];
			}
			
			// Kreuztabelleneintrag
			$q = new FX('host8.kon5.net', '80', 'FMPro7');
			$q->SetDBData("Filmtage-Solothurn", "cgi_k_32__filme_preise", "1"); 
			$q->SetDBUserPass ("Admin", "xs4kon5");
			$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);	
			$q->AddDBParam('_kf__Preis_Id', $preis_id);	
			$q->AddDBParam('_kf__ISO_Land_Id', $p_land_code[0]);	
			$DBData = $q->FMNew();
			$i++;		
		}
		
	}	
	if ($Premiere != "") {
		$q = new FX('host8.kon5.net', '80', 'FMPro7');
		$q->SetDBData("Filmtage-Solothurn", "cgi_h_02__filme", "1"); 
		$q->SetDBUserPass ("Admin", "xs4kon5");
		$q->AddDBParam('-recid', $_SESSION['record_id']); 
		$q->AddDBParam('_kf__Premierentyp', $Premiere);
		$DBData = $q->FMEdit(); 		
	}
	// redirect zur n�chsten Seite
	header("Location: /step11.php");
	exit;
}

include($_SERVER['DOCUMENT_ROOT']. "/includes/x_get_value_list10.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Online Registration</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 <link href="/css/style.css" rel="stylesheet" type="text/css" title="KFT" />
 
<script type="text/javascript" src="includes/scripts.js"></script>
<script type="text/javascript" language="javascript">
<!--
	var ol_width = 300;
	var ol_fgcolor='#d9e2ec';
	var ol_bgcolor='#006699';
//-->
</script>
<link rel="stylesheet" type="text/css" href="includes/yui/container/assets/container.css">
<script type="text/javascript" src="includes/yui/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript" src="includes/yui/animation/animation-min.js"></script>
<script type="text/javascript" src="includes/yui/dragdrop/dragdrop-min.js"></script>
<script type="text/javascript" src="includes/yui/connection/connection.js"></script>
<script type="text/javascript" src="includes/yui/container/container.js"></script>
<script language="javascript">

// ###### Festivals #######

var f_jahr = new Array;
var f_land = new Array;
var f_txt = new Array;

function addFestival() {
	if 	(
		(document.getElementById('f_jahr_select').value != "") &&
		(document.getElementById('f_land_select').value != "") &&
		(document.getElementById('f_txt').value != "")
		) {
		
		f_jahr.push(document.getElementById('f_jahr_select').value);
		document.getElementById('f_jahr_select').value = "";
		f_land.push(document.getElementById('f_land_select').value);
		document.getElementById('f_land_select').value = "";
		f_txt.push(document.getElementById('f_txt').value);
		document.getElementById('f_txt').value = "";
		showFestival();		
	} else {
		miniDialog = new YAHOO.widget.SimpleDialog("mini_dlg", { visible:false, width: "25em", effect:[
{effect:YAHOO.widget.ContainerEffect.SLIDE,duration:0.25}, {effect:YAHOO.widget.ContainerEffect.FADE,duration:0.25}
], fixedcenter : true, modal:true, draggable:false });
		miniDialog.setHeader("<?=$_SESSION['Leg_91']?>");
		miniDialog.setBody("<?=$_SESSION['Leg_95']?>");
		miniDialog.cfg.queueProperty("icon",YAHOO.widget.SimpleDialog.ICON_WARN);
		miniDialog.cfg.queueProperty("buttons", [ 
			{ text: "<?=$_SESSION['Leg_93']?>", handler:handlerHide, isDefault:true  }
		]);
		miniDialog.render(document.body);
		miniDialog.show();
	}
}
function delFestival(i) {
	f_jahr.splice(i,1);
	f_land.splice(i,1);
	f_txt.splice(i,1);
	showFestival();	
}
function showFestival() {
	var output = "";
	for (var i = 0; i < f_jahr.length; ++i) {
		var land = f_land[i].split("|");
 		output += "<input type=\"hidden\" name=\"F_Jahr[]\" value=\"" + f_jahr[i] + "\">\n";
 		output += "<input type=\"hidden\" name=\"F_Land[]\" value=\"" + f_land[i] + "\">\n";
		output += "<input type=\"hidden\" name=\"F_Txt[]\" value=\"" + f_txt[i] + "\">\n";		
 		output += "<hr style=\"width: 330px; height: 1px\" noshade=\"noshade\" color=\"#FFFFFF\">\n";
		output += f_jahr[i] + " | " + land[1] + "<br />";
 		output += "<?=$_SESSION['Leg_55']?>: " + f_txt[i] + "<br />\n";
 		output += "<img src=\"images/delete.png\" onClick=\"delFestival(" + i + ")\" align=\"absmiddle\"> <a href=\"javascript:void(0);\" onClick=\"delFestival(" + i + ")\"><?=$_SESSION['Leg_88']?></a><br />\n";
	}
	document.getElementById('Festivals').innerHTML = "<p>"+ output +"</p>";
	document.getElementById('Festivals').style.display = "block";
}


// ###### Preise #######

var p_jahr = new Array;
var p_land = new Array;
var p_ort = new Array;
var p_txt = new Array;

function addPreis() {
	if 	(
		(document.getElementById('p_jahr_select').value != "") &&
		(document.getElementById('p_land_select').value != "") &&
		(document.getElementById('p_ort').value != "") &&
		(document.getElementById('p_txt').value != "")
		) {
		
		p_jahr.push(document.getElementById('p_jahr_select').value);
		document.getElementById('p_jahr_select').value = "";
		p_land.push(document.getElementById('p_land_select').value);
		document.getElementById('p_land_select').value = "";
		p_ort.push(document.getElementById('p_ort').value);
		document.getElementById('p_ort').value = "";
		p_txt.push(document.getElementById('p_txt').value);
		document.getElementById('p_txt').value = "";
		showPreis();		
	} else {
		miniDialog = new YAHOO.widget.SimpleDialog("mini_dlg", { visible:false, width: "25em", effect:[
{effect:YAHOO.widget.ContainerEffect.SLIDE,duration:0.25}, {effect:YAHOO.widget.ContainerEffect.FADE,duration:0.25}
], fixedcenter : true, modal:true, draggable:false });
		miniDialog.setHeader("<?=$_SESSION['Leg_91']?>");
		miniDialog.setBody("<?=$_SESSION['Leg_95']?>");
		miniDialog.cfg.queueProperty("icon",YAHOO.widget.SimpleDialog.ICON_WARN);
		miniDialog.cfg.queueProperty("buttons", [ 
			{ text: "<?=$_SESSION['Leg_93']?>", handler:handlerHide, isDefault:true  }
		]);
		miniDialog.render(document.body);
		miniDialog.show();
	}
}
function delPreis(i) {
	p_jahr.splice(i,1);
	p_land.splice(i,1);
	p_txt.splice(i,1);
	p_ort.splice(i,1);
	showPreis();	
}
function showPreis() {
	var output = "";
	for (var i = 0; i < p_jahr.length; ++i) {
		var land2 = p_land[i].split("|");
 		output += "<input type=\"hidden\" name=\"P_Jahr[]\" value=\"" + p_jahr[i] + "\">\n";
 		output += "<input type=\"hidden\" name=\"P_Land[]\" value=\"" + p_land[i] + "\">\n";
 		output += "<input type=\"hidden\" name=\"P_Ort[]\" value=\"" + p_ort[i] + "\">\n";
		output += "<input type=\"hidden\" name=\"P_Txt[]\" value=\"" + p_txt[i] + "\">\n";		
 		output += "<hr style=\"width: 330px; height: 1px\" noshade=\"noshade\" color=\"#FFFFFF\">\n";
		output += p_jahr[i] + " | " + p_ort[i] + ", " + land2[1] + "<br />";
 		output += "<?=$_SESSION['Leg_57']?>: " + p_txt[i] + "<br />\n";
 		output += "<img src=\"images/delete.png\" onClick=\"delPreis(" + i + ")\" align=\"absmiddle\"> <a href=\"javascript:void(0);\" onClick=\"delPreis(" + i + ")\"><?=$_SESSION['Leg_88']?></a><br />\n";
	}
	document.getElementById('Preise').innerHTML = "<p>"+ output +"</p>";
	document.getElementById('Preise').style.display = "block";
}


</script> 
</head>
<body>


<div id="container">
  <div id="top">
    <? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
	<? if (($error != "") && ($step == "2")) { ?>	
	<p style="border:1px solid #990000; background-color:#FFDCD6; padding:5px; width: 450px"><?=$error; ?></p>
	<? } ?>
  </div>
	<br clear="all" />
  <div id="leftSide">
<fieldset>
<legend><?=$_SESSION['Leg_8']?> 10 / 13: <?=$_SESSION['Leg_130']?></legend>
	<? echo ($_SESSION['Leg_110'] != "") ? '<p class="pagenote">'.$_SESSION['Leg_110'].'</p>' : ""; ?>
<form action="<?=$_SERVER['PHP_SELF']?>" method="post" name="form1">
<input type="hidden" value="2" name="step_10">

	
	<label for="name"><?=$_SESSION['Leg_159']?></label>
	<div class="div_blankbox">
		<p><?=$_SESSION['Leg_115']?></p>
		<div style="float:left">
			<select class="post" name="Premiere" id="Premiere" style="border: 1px solid #666666; font-size:11px;">
			<option value="">-</option>
			<?=$output_Premiere?>
			</select>
		</div>
	</div>
	<br clear="all" />
	<label for="name"><?=$_SESSION['Leg_20']?></label>
	<div class="div_blankbox">
		<p><?=$_SESSION['Leg_21']?></p>
		<div style="float:left"><?=$_SESSION['Leg_56']?><br />
			<select class="post" name="f_jahr_select" id="f_jahr_select" style="border: 1px solid #666666; font-size:11px;">
			<option value="">-</option>
			<?=$output_Jahr?>
			</select>
		</div>
		<div style="float:left"><?=$_SESSION['Leg_40']?><br />
			<select class="post" name="f_land_select" id="f_land_select" style="border: 1px solid #666666; font-size:11px;">
			<option value="">-</option>
			<?=$output_Land?>
			</select>
		</div>
		<br clear="all" />
		<?=$_SESSION['Leg_55']?><br />
		<input type="text" name="f_txt" id="f_txt" class="textbox" /><br />
		<img src="images/add.png" alt="+" width="16" height="16" align="absmiddle" onClick="addFestival()"> <a href="#" onClick="addFestival()"><?=$_SESSION['Leg_78']?></a><br />
		<div id="Festivals"></div>
	</div>
	<br clear="all" />
	<label for="name"><?=$_SESSION['Leg_22']?></label>
	<div class="div_blankbox">
		<p><?=$_SESSION['Leg_23']?></p>
		<div style="float:left"><?=$_SESSION['Leg_56']?><br />
			<select class="post" name="p_jahr_select" id="p_jahr_select" style="border: 1px solid #666666; font-size:11px;">
			<option value="">-</option>
			<?=$output_Jahr?>
			</select>
		</div>
		<div style="float:left"><?=$_SESSION['Leg_40']?><br />
			<select class="post" name="p_land_select" id="p_land_select" style="border: 1px solid #666666; font-size:11px;">
			<option value="">-</option>
			<?=$output_Land?>
			</select>
		</div>
		<br clear="all" />
		<?=$_SESSION['Leg_57']?><br />
		<input type="text" name="p_txt" id="p_txt" class="textbox" /><br />
		<?=$_SESSION['Leg_39']?><br />
		<input type="text" name="p_txt" id="p_ort" class="textbox" /><br />
		<img src="images/add.png" alt="+" width="16" height="16" align="absmiddle" onClick="addPreis()"> <a href="#" onClick="addPreis()"><?=$_SESSION['Leg_78']?></a><br />
		<div id="Preise"></div>
	</div>
	<br clear="all" />
	<div class="button_div">
	<input name="submit" type="submit" id="submit" value="<?=$_SESSION['Leg_32']?>">
	</div>
</form>
<div class="clear"></div>
</fieldset>

  </div>
  <div class="clear"></div>
</div>


<? if (($step == "2") && isset($Produktionsland[0])) {
echo '<script language="javascript">'."\n";
echo 'laender.push("'.implode("\",\"", $Produktionsland).'");'."\n";
echo 'showLaender();'."\n";
echo '</script>'."\n";
}
?>
<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>
</body>
</html>
