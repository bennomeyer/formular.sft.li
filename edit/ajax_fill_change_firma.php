<?
session_start();
// Bust cache in the head
header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
// always modified
header ("Cache-Control: no-cache, must-revalidate");  // HTTP/1.1
header ("Pragma: no-cache");                          // HTTP/1.0

$id = (isset($_REQUEST['id']))? $_REQUEST['id'] : "";
$callback = "";

require_once (__DIR__.'/../includes/db.inc.php');

$find =& $fm->newFindCommand('cgi_k_Angestellte'); 
$find->addFindCriterion('__kp__id', $id); 
$result = $find->execute(); 
if (FileMaker::isError($result)) {
		echo 'Leider ist ein Fehler aufgetreten. Bitte kontaktieren Sie den WebMaster.';
		exit;
	}
$records = $result->getRecords(); 
$foundrec = $result->getFoundSetCount();
$record = $records[0];

$callback = "document.getElementById('dialog4_abteilung').value = '".$record->getField('Abteilung')."'; ";
$callback .= "document.getElementById('dialog4_funktion').value = '".$record->getField('Funktion')."'; ";
$callback .= "document.getElementById('dialog4_tel').value = '".$record->getField('Direktwahl_Tel')."'; ";
$callback .= "document.getElementById('dialog4_fax').value = '".$record->getField('Direktwahl_Fax')."'; ";
$callback .= "document.getElementById('dialog4_mobil').value = '".$record->getField('Firmenhandy')."'; ";
$callback .= "document.getElementById('dialog4_mail').value = '".$record->getField('Mail')."'; ";
$callback .= "document.getElementById('dialog4_newsletter').value = '".$record->getField('NewsletterEmpfaenger')."'; ";
if ($record->getField('NewsletterEmpfaenger') == "1") {
	$callback .= "document.getElementById('dialog4_nl_checkbox').checked = true; ";
} else {
	$callback .= "document.getElementById('dialog4_nl_checkbox').checked = false; ";
}

die($callback);

?>