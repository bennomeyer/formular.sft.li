<?
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
/**
 * History:
 * 2015-10-08: Erweiterung um Mitarbeiter-Zeiten (#6155)
 * 2015-10-28: Erweiterung um Bemerkungen (#6264)
 * 2016-10-19: Erweiterung um Funktion, Notizen und Handy (#7886)
 * 2016-11-30: Layout, fixed values (#8168)
 */

session_start();

function ifset($name,$default="") {
	return isset($_REQUEST[$name])? $_REQUEST[$name] : $default;
}

function Label($id) {
	return (isset($_SESSION['Leg_'.$id])?$_SESSION['Leg_'.$id]:'');
}

// Neu: explizite Arbeitsbereiche teilweise mit Verbands-Angabe
// #7886: optional Notiz
$arbeitsbereiche = array(
	array('name'=>'Medien',        'verband'=>true,  'notiz'=>false, 'label'=>'301'),
	array('name'=>'Regie',         'verband'=>true,  'notiz'=>false, 'label'=>'302'),
	array('name'=>'Drehbuch',      'verband'=>true,  'notiz'=>true, 'label'=>'350', 'notiz_label'=>true),
	array('name'=>'Cast',          'verband'=>true,  'notiz'=>true, 'label'=>'303', 'notiz_label'=>true),
	array('name'=>'Crew',          'verband'=>true,  'notiz'=>true, 'label'=>'304', 'notiz_label'=>true),
	array('name'=>'Produktion',    'verband'=>true,  'notiz'=>false, 'label'=>'305'),
	//array('name'=>'Postproduktion','verband'=>true,  'notiz'=>false, 'label'=>'306'),
	array('name'=>'Verleih',       'verband'=>true,  'notiz'=>false, 'label'=>'307'),
	array('name'=>'Kino',          'verband'=>true,  'notiz'=>false, 'label'=>'309'),
	array('name'=>'Festival',      'verband'=>false, 'notiz'=>false, 'label'=>'308'),
	array('name'=>'Filmschule',    'verband'=>false, 'notiz'=>true, 'label'=>'310'),
	array('name'=>'Institution',   'verband'=>false, 'notiz'=>true,  'label'=>'311', 'notiz_label'=>false)
);
$akk_cat = array();
$akk_verband = array();
$akk_notiz = array();

// #6155: Mitarbeiter-Felder
$ma_field_names = array('Geburtsdatum','Mitarbeiter_AHV_Nr','Mitarbeiter_BerufKenntnisse',
'n__Mitarbeiter2::Zeiten_Tag1',
'n__Mitarbeiter2::Zeiten_Tag2',
'n__Mitarbeiter2::Zeiten_Tag3',
'n__Mitarbeiter2::Zeiten_Tag4',
'n__Mitarbeiter2::Zeiten_Tag5',
'n__Mitarbeiter2::Zeiten_Tag6',
'n__Mitarbeiter2::Zeiten_Tag7',
'n__Mitarbeiter2::Zeiten_Tag8',
'n__Mitarbeiter2::Einsatzbereiche',
'n__Mitarbeiter2::Locations',
'n__Mitarbeiter::Bemerkungen',
'Mitarbeiter_Kein_Interesse_Mehr',
);
$ma_simple_fields = array('Geburtsdatum','Mitarbeiter_AHV_Nr','Mitarbeiter_BerufKenntnisse','n__Mitarbeiter::Bemerkungen','Mitarbeiter_Kein_Interesse_Mehr');
// make friendly names
$ma_fields = array();
foreach ($ma_field_names as $field) {
	$ma_fields[$field] = strtolower(strtr($field,": ",'__'));
}

// Parameter übernehmen
$step = ifset('step');
$version = ifset('version');
$sprache = ifset('sprache', "en");
$vorname = ifset('vorname');
$name = ifset('name');
$adrtyp = ifset('adrtyp');
$firma = ifset('firma');
$funktion = ifset('funktion');
$strasse_1 = ifset('strasse_1');
$strasse_2 = ifset('strasse_2');
$plz = ifset('plz');
$ort = ifset('ort');
$land_1 = ifset('land_1');
$tel = ifset('tel');
$mobil = ifset('mobil');
$fax = ifset('fax');
$mail = ifset('mail');
$sex = ifset('sex');
$nl = ifset('nl');
$akk_arr = ifset('akk_arr');
$akk_dep = ifset('akk_dep');
$akk_cat_else_flag = ifset('akk_cat_else_flag','0');
$akk_cat_else = ifset('akk_cat_else');
$akk_conmail = ifset('akk_conmail');
$akk_conhandy = ifset('akk_conhandy');

$error = "";
// die neuen Arbeitsbereichs-Parameter
foreach ($arbeitsbereiche as $bereich) {
	$cat_name = 'akk_'.strtolower($bereich['name']);
	$verb_name = $cat_name.'_verband';
	$akk_cat[$bereich['name']] = ifset($cat_name,'0'); // nicht angeklickt wird nicht übergeben, bedeutet false (0)
	if ($bereich['verband']) {
		$akk_verband[$bereich['name']] = ifset($verb_name);		
	}
	if ($bereich['notiz']) {
		$notiz_name = $cat_name.'_notiz';
		$akk_notiz[$bereich['name']] = ifset($notiz_name);
	}
}

// Mitarbeiter-Parameter
foreach ($ma_fields as $field => $param) {
	if (in_array($field, $ma_simple_fields)) {
		$$param = ifset($param);
	} else {
		// "Array"
		$$param = implode("\n", array_filter(ifset($param,array())));
	}
}
// Ende der Mitarbeiter-Parameter

require_once (__DIR__.'/../includes/db.inc.php');

if ('mit' == $version || 'mod' == $version) {
	// Prüfe Formularsperren
	$request = $fm->newFindAnyCommand('cgi_Formularsperrungen');
	$result = $request->execute();
	$records = $result->getRecords();
	$record = $records[0];
	if ('mit'==$version) {
		$locked = $record->getField('Formularsperrung_Mitarbeiter');
	} else {
		$locked = $record->getField('Formularsperrung_Moderatoren');
	}
	if (!!$locked) {
		include 'not_available.html';
		die();
	}
}

// ------------------------
// Hole Feldbeschriftungen
// ------------------------
read_labels($sprache);


if ($step == "2") {
	
	$nl = ifset('nl');
	// Check 4 Errors
	$error .= (($vorname == "") || ($name == "") || ($adrtyp == "") || ($funktion == "") || ($strasse_1 == "") || ($plz == "") || ($ort == "") || ($land_1 == "") || ($mail == "") || ($sprache == "") || ($sex == ""))? $_SESSION['Leg_199']."<br />" : "";
	$error .= (($adrtyp == "p") && ($firma != ""))? $_SESSION['Leg_195'] : "";
	
	if ($error == "") {
		
	
		$data = array(
			'Vorname'	=> $vorname,
			'Name'		=> $name,
			'_kf__AdresstypFlag'	=> $adrtyp,
			'Firma'		=> $firma,
			'Funktion'  => $funktion,
			'Strasse_1'	=> $strasse_1,
			'Strasse_2'	=> $strasse_2,
			'PLZ'		=> $plz,
			'Ort'		=> $ort,
			'_kf__land_ISO_code'	=> $land_1,
			'Tel'		=> $tel,
			'Mobil'     => $mobil,
			'Fax'		=> $fax,
			'Mail'		=> $mail,
			'GeschlechtFlag'			=> $sex,
			'_kf__Korrespondenzsprache'	=> $sprache,
			'NewsletterEmpfaenger'	=> $nl,
			'Akkreditierung_Formular_AndereText'=> $akk_cat_else,
			'Akkreditierung_Formular_AndereFlag'=> $akk_cat_else_flag,
			'Akkreditierung_Formular_Anreise'	=> $akk_arr,
			'Akkreditierung_Formular_Abreise'	=> $akk_dep,
			'Akkreditierung_Formular_ConsentMail' => $akk_conmail,
			'Akkreditierung_Formular_ConsentHandy' => $akk_conhandy,
		);	
		// restliche Akkreditierungs-Daten übernehmen
		foreach ($arbeitsbereiche as $bereich) {
			$bname = $bereich['name'];
		  	$dbf_name = 'Akkreditierung_Formular_'.$bname;
		  	$dbf_verb_name = $dbf_name.'_Verband';
		  	$dbf_notiz_name = $dbf_name.'_Notiz';
		  	$data[$dbf_name] = $akk_cat[$bname];
			if ($bereich['verband']) {
				$data[$dbf_verb_name] = $akk_verband[$bname];
			}		
			if ($bereich['notiz']) {
				$data[$dbf_notiz_name] = $akk_notiz[$bname];
			}		
		}
	// Mitarbeiter-Values (#6155)
    if ('mit'==$version) {
		foreach ($ma_fields as $field => $param) {
			$data[$field] =  $$param;
		}    	
    }
		
		$newRequest =& $fm->newAddCommand('cgi_n__Newsletteranmeldung_Akkreditierung', $data);
		$result = $newRequest->execute();
		
		//Auf Fehler prüfen
		if (FileMaker::isError($result)) {
			echo "<p>Fehler: " . $result->getMessage() . "</p><p>Bitte nochmals abschicken</p>";
		} else {
			$id = $result->getLastRecord()->getField('_kp__id',0); # the ID of the newly created record
			if (!empty($_FILES['bild']['name'])) {
				# save akk portrait
				$pfad='people/';
				$ext_tmp = explode(".",$_FILES['bild']['name']);
				$ext = $ext_tmp[count($ext_tmp)-1];
				$id = "akk_".$id;
				$destination = $_SERVER['DOCUMENT_ROOT']."/bilder/".$pfad."original/".$id.'.'.$ext;
				if ($_FILES['bild']['size'] < 10000000) {
					if(move_uploaded_file($_FILES["bild"]["tmp_name"], $destination)) {
						$command = "/usr/local/bin/convert ".$destination." -thumbnail '100x100>' ".$_SERVER['DOCUMENT_ROOT']."/bilder/".$pfad."small/".$id.".jpg";
						exec($command);
						$command = "/usr/local/bin/convert ".$destination." -thumbnail '300x300>' ".$_SERVER['DOCUMENT_ROOT']."/bilder/".$pfad."medium/".$id.".jpg";
						exec($command);
						$command = "/usr/local/bin/convert ".$destination." -thumbnail '600x600>' ".$_SERVER['DOCUMENT_ROOT']."/bilder/".$pfad."large/".$id.".jpg";
						exec($command);
						if ('jpg' != strtolower($ext)) {
							# nur JPG speichern
							$command = "/usr/local/bin/convert ".$destination." ".$_SERVER['DOCUMENT_ROOT']."/bilder/".$pfad."original/".$id.".jpg";
							exec($command);
							unlink($destination);
						}			
					}	
				}
			}
			header("Location: danke.php?sprache=$sprache&version=$version");
			exit;
		}
	}
	
	// 6155: Multi-Value-Fields zu Arrays machen:
	foreach ($ma_fields as $field => $param) {
		if (!in_array($field, $ma_simple_fields)) {
			// explode to array
			$$param = explode("\n", $$param);
		}
	}
}

include($_SERVER['DOCUMENT_ROOT'].'/includes/x_get_value_list_country.inc.php');
include($_SERVER['DOCUMENT_ROOT'].'/includes/x_get_value_list_adrtyp.inc.php');

// 6155: Wertelisten für Zeiten, Einsatzbereiche und Locations
$layout = $fm->getLayout('cgi_n__Newsletteranmeldung_Akkreditierung');
$zeiten= $layout->getValueListTwoFields('Mitarbeiter_Uhrzeiten');
$einsatz= $layout->getValueListTwoFields('Mitarbeiter_Einsatzbereiche');
$locations= $layout->getValueListTwoFields('Mitarbeiter_Locations');

/**
 * 
 * Hilfsfunktionen zum Rendern der Zeiten (als checkboxes in td)
 * @param String $param NAME des parameters (also z.B. n__mitarbeiter__zeiten_tag1)
 * @return String mit Liste von td
 */
function render_zeiten($param) {
	global $zeiten, $$param;
	$output ="";
	foreach ($zeiten as $label => $value) {
		$checked = checked_attr(in_array($value, $$param));
		$output .= "<td><input type='checkbox' name='${param}[]' value='$value' $checked /></td>";
	}
	return $output;
}
// 
/**
 * 
 * Hilfsfunktion zum Rendern der anderen Checkbox-Listen
 * @param String $param NAME des parameters (also z.B. n__mitarbeiter2__zeiten_tag1)
 * @param Array $list Werteliste
 * @return String mit Labels und Checkboxen
 */
function render_fm_value_list($param,$list){
	global $$param;
	$output="";
	foreach ($list as $label => $value) {
		$checked = checked_attr(in_array($value, $$param));
		$output .= "<label><input type='checkbox' name='${param}[]' value='$value' $checked /> $label</label>";
	}	
	return $output;
}

/**
 * Rendern eines Text-Inputs mit Label
 * @param String $param NAME des parameters
 * @param $leg NR der Legende
 */
function render_text_input($param,$leg,$required=false) {
	global $$param;
	$legi = Label($leg) . ($required ? '*':'');
	$value = $$param;
	return "<label for='$param'>$legi</label>
		<div class='div_texbox'><input name='$param' type='text' class='textbox' id='$param' value='$value' /></div>
		<br clear='all' />
	";
}

function checked_attr($chk){
	return $chk ? 'checked="checked"' : '';
}

?>
<!DOCTYPE html>
<html>
<head>
<title><? echo ($version == "akk")? $_SESSION['Leg_294'] : $_SESSION['Leg_194']; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="style.css" rel="stylesheet" type="text/css" title="KFT" />
<link href="../css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="../includes_js/iframeResizer.contentWindow.min.js"></script>
<script type="text/javascript" src="../includes_js/datepicker.min.js"></script>
<script type="text/javascript" language="javascript" charset="utf-8">
<!--
//6155: Switch times by "immer"
$(function(){
	$('.mitarbeiter input:checkbox[value="immer"]').click(function(){
		var that = $(this);
		var chk = that.prop('checked');
		// falls checked, alle hinteren auch checken
		that.closest('tr').find('input[value!="immer"]').prop("checked",chk);
		});	
	$('.mitarbeiter td input:checkbox[value!="immer"]').change(function(){
		// die normalen Zeiten: immer löschen, wenn nicht gesetzt
		var that = $(this);
		var chk = that.prop('checked');
		// falls checked, alle hinteren auch checken
		if (!chk){
			that.closest('tr').find('input:checkbox[value="immer"]').prop("checked",false);
			}
		});	
	$('#akk_arr').Zebra_DatePicker({
		format: 'd/m/Y',
		pair: $('#akk_dep')
		});
	$('#akk_dep').Zebra_DatePicker({
		format: 'd/m/Y',
		direction: true
		});
});
//-->

</script>

</head>
<body>

<div id="container">
  <fieldset>
  <legend><?
switch ($version) {
	case 'akk':	echo  Label(294); break;
	case 'mit':	echo  Label(329); break;
	default: echo Label(194);	break;
}
?></legend>
	<? if ($error != "") { ?>	
	<p style="border:1px solid #990000; background-color:#FFDCD6; padding:5px; width: 450px"><?=$error; ?></p>
	<? } ?>
  <p><?= 'mit'==$version ? Label(342) : Label(197) ?></p>
  <br clear="all" />
  <form action="<?=$_SERVER['PHP_SELF']?>" method="post" name="form1" enctype="multipart/form-data">
	<input type="hidden" value="2" name="step" />
	<input type="hidden" value="<?=$version?>" name="version" />
	
	<div>
	<?= render_text_input("vorname", 34, true) ?>
	<?= render_text_input("name", 35, true) ?>
		
		<label for="sex"><?=Label(177) ?>*</label>
		<div class="div_texbox"><input type="radio" name="sex" value="m" <?= checked_attr($sex == "m") ?> />  m <input type="radio" name="sex" value="f" <?= checked_attr($sex == "f") ?> />  f</div>
		<br clear="all" />
	</div>
	<br clear="all" />
	
	<label for="sprache"><?=Label(79) ?>*</label>
	<div class="div_textbox"> 
    <select name="sprache">
	<option value="de" <? echo ($sprache == "de")? 'selected' : ""; ?>>DE</option>
	<option value="en" <? echo ($sprache == "en")? 'selected' : ""; ?>>EN</option>
	<option value="fr" <? echo ($sprache == "fr")? 'selected' : ""; ?>>FR</option>
	<option value="it" <? echo ($sprache == "it")? 'selected' : ""; ?>>IT</option>
	</select>
	</div>
	<br clear="all" />
	
	<div>
		<label for="adrtyp"><?=Label(193) ?>*</label>
		<div class="div_texbox"><?=$output_Adrtyp?></div>
		<br clear="all" />
		
	<?= render_text_input("firma", 53) ?>
	<?= render_text_input("funktion", 189, true) ?>
	<?= render_text_input("strasse_1", 36, true) ?>
	<?= render_text_input("strasse_2", 37) ?>
	<?= render_text_input("plz", 38, true) ?>
	<?= render_text_input("ort", 39, true) ?>
		
		<label for="land_1"><?=$_SESSION['Leg_40']?>*</label>
		<div class="div_texbox"><select name="land_1" id="land_1">
		<option value="">-</option>
		<?=$output_Land?>
		</select></div>
		<br clear="all" />
	</div>
	
	<div>
	<?= render_text_input("tel", 41) ?>
	<?= render_text_input("fax", 43) ?>
	<?= render_text_input("mobil", 176) ?>
	<?= render_text_input("mail", 44, true) ?>
	</div>
	<br clear="all" />	
	<p><input type="checkbox" name="nl" value="1" <?= checked_attr($nl == "1") ?> />  <?= Label(178) ?></p>

<?
/*****************************
AKK-Formular 
*****************************/


if ($version == "akk") { 	
?>	
	
	<h3><?=$_SESSION['Leg_287']?></h3>
	<p><?= Label(288) ?></p>
	<table border="0">
	<colgroup>
	  <col width="197" />
	  <col width="243" />
	</colgroup>
	<?php 
	$layout = $fm->getLayout('cgi_n__Newsletteranmeldung_Akkreditierung');
	$row=0;
	foreach ($arbeitsbereiche as $bereich) {
		$bname = $bereich['name'];
		$cat_name = 'akk_'.strtolower($bname);
		$with_syndicate = $bereich['verband'];
		$style = $with_syndicate?'class="'.($row++ % 2?'even':'odd').'"':'';
	  	// Checkbox anzeigen
	  	$selector = checked_attr($akk_cat[$bname]>0);
	  	echo '<tr '.$style.'><td><input type="checkbox" id="'.$cat_name.'" name="'.$cat_name.'" value="1"'.$selector.' /> '.Label($bereich['label']).'</td><td>';
	  	// optional Verband anzeigen
	  	if ($bereich['verband']) {
			$verb_name = $cat_name.'_verband';
	  		// Werteliste holen
			$vl = $layout->getValueList('Verband_'.$bname);
			// und als Drop-Down anzeigen
	  		echo Label('313').' <select style="width: 217px;" name="'.$verb_name.'" id="'.$verb_name.'"><option value=""></option>';
			foreach ($vl as $value) {
				$selektor = ($akk_verband[$bname]==$value)?' selecte="selected"':'';
				echo '<option value="'.$value.'"'.$selektor.' >'.$value.'</option>';
			}
			echo "</select>";
	  	}
	  	if ($bereich['notiz']) {
	  		$notiz_name = $cat_name.'_notiz';
	  		if ($bereich['verband']) echo '<br />';
	  		if ($bereich['notiz_label']) echo  Label(368);
	  		echo "<input type='text' class='textbox' name='$notiz_name' id='$notiz_name' value='".$akk_notiz[$bname]."'  />";
	  	}
	  	if (!($bereich['verband'] || $bereich['notiz'])) echo '&nbsp;';
	  	echo "</td></tr>\n";
	  }
	?>
	  <tr><td><input type="checkbox" id="akk_cat_else_flag" name="akk_cat_else_flag" value="1"<?= checked_attr($akk_cat_else_flag) ?> /> <?= Label('312') ?></td>
	  <td><input type="text" class="textbox" id="akk_cat_else" name="akk_cat_else" value="<?= $akk_cat_else ?>" /></td></tr>
	  <tr>
	      <td>&nbsp;</td>
	      <td>&nbsp;</td>
	  </tr>
	  <tr>
            <td><h3><?=$_SESSION['Leg_369']?></h3></td>
            <td>&nbsp;</td>
      </tr>
        <tr>
            <td colspan="2"><p><?=$_SESSION['Leg_359']?></p></td>
        </tr>
    </table>
	<?= render_text_input('akk_arr', 291);	?>
	<?= render_text_input('akk_dep', 292);	?>
	<label for='bild'><?=$_SESSION['Leg_132']?></label>
	<div class='div_texbox'><input type="file" name="bild" style="font-family:Verdana, Arial, Helvetica, sans-serif;font-size: 11px;border:1px solid #999999;" /></div>
	<br clear='all' />
	
	<br clear="all" />
	<div style="margin-left: 28px; text-indent: -24px;"><input type="checkbox" name="akk_conmail" value="1" <?= checked_attr($akk_conmail) ?>  /> <?= Label('314') ?></div>
	<div style="margin-left: 28px; text-indent: -24px;"><input type="checkbox" name="akk_conhandy" value="1" <?= checked_attr($akk_conhandy) ?>  /> <?= Label('315') ?></div>
	
		<br clear="all" />
<?
}
if ("mit"==$version) {
/*****************************
Mitarbeiter-Formular 
*****************************/

?>
<br  clear='all'/>
<div class="mitarbeiter">
	<?= render_text_input('geburtsdatum',343) ?>
	<?= render_text_input('mitarbeiter_ahv_nr',344) ?>
	<?= render_text_input('mitarbeiter_berufkenntnisse',345) ?>
	<p><?= Label(330) ?></p>
	<table class="zeiten">
	<thead>
	<tr>
	<th class="label"></th>
	<?php 
	foreach ($zeiten as $label => $value) {
		?>
		<td><?= $label ?></td>
		<?php 
	}
	?>
	</tr>
	</thead>
	<tbody>
		<tr><td class="label"><?= Label(331) ?></td><?= render_zeiten('n__mitarbeiter2__zeiten_tag1') ?></tr>
		<tr><td class="label"><?= Label(332) ?></td><?= render_zeiten('n__mitarbeiter2__zeiten_tag2') ?></tr>
		<tr><td class="label"><?= Label(333) ?></td><?= render_zeiten('n__mitarbeiter2__zeiten_tag3') ?></tr>
		<tr><td class="label"><?= Label(334) ?></td><?= render_zeiten('n__mitarbeiter2__zeiten_tag4') ?></tr>
		<tr><td class="label"><?= Label(335) ?></td><?= render_zeiten('n__mitarbeiter2__zeiten_tag5') ?></tr>
		<tr><td class="label"><?= Label(336) ?></td><?= render_zeiten('n__mitarbeiter2__zeiten_tag6') ?></tr>
		<tr><td class="label"><?= Label(337) ?></td><?= render_zeiten('n__mitarbeiter2__zeiten_tag7') ?></tr>
		<tr><td class="label"><?= Label(338) ?></td><?= render_zeiten('n__mitarbeiter2__zeiten_tag8') ?></tr>
	</tbody>
	</table>
	<br clear="all" />
	<label for=""><?= Label(339) ?></label>
		<div class="div_texbox"><?= render_fm_value_list('n__mitarbeiter2__einsatzbereiche', $einsatz) ?></div>
	<br clear="all" />
	<br clear="all" />
	<label for=""><?= Label(340) ?></label>
		<div class="div_texbox"><?= render_fm_value_list('n__mitarbeiter2__locations', $locations) ?></div>
	<br clear="all" />
	<br clear="all" />
	<label for="n__mitarbeiter__bemerkungen"><?= Label(347)?></label>
	<div class="div_texbox"><textarea rows=3 name="n__mitarbeiter__bemerkungen" class="textbox"><?= $n__mitarbeiter__bemerkungen ?></textarea></div>
	<br clear="all" />
	<br clear="all" />
	<br clear="all" />
	<label for="mitarbeiter_kein_interesse_mehr"><?= Label(346) ?></label>
	<div class="div_texbox">
	<input type="hidden" name="mitarbeiter_kein_interesse_mehr" value="0" />
	<input type="checkbox" name="mitarbeiter_kein_interesse_mehr" value="1" <?= checked_attr($mitarbeiter_kein_interesse_mehr) ?> />
	</div>
	<br clear="all" />
	<p><?= nl2br(Label(341)) ?></p>
</div>

<?php 
}
?>
	<br clear="all" />
	
	<input type="submit" value="<?=$_SESSION['Leg_114']?>"  />
  </form>
  </fieldset>
</div>

<? include('../includes/ly_footer.inc.php'); ?>

</body>
</html>
