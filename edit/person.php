<?
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
session_start();
if (isset($_GET['q'])) {
	$q = (isset($_REQUEST['q'])) ? $_REQUEST['q'] : "";
	$_SESSION['q'] = $q;
}
if (!isset($_SESSION['q'])) {
	echo 'ERROR: Wrong parameter';
	exit();
}
$step = (isset($_POST['step'])) ? $_POST['step'] : "";
$sprache = (isset($_POST['sprache'])) ? $_POST['sprache'] : "";
$vorname = (isset($_POST['vorname'])) ? $_POST['vorname'] : "";
$strasse_1 = (isset($_POST['strasse_1'])) ? $_POST['strasse_1'] : "";
$strasse_2 = (isset($_POST['strasse_2'])) ? $_POST['strasse_2'] : "";
$plz = (isset($_POST['plz'])) ? $_POST['plz'] : "";
$ort = (isset($_POST['ort'])) ? $_POST['ort'] : "";
$land_1 = (isset($_POST['land_1'])) ? $_POST['land_1'] : "";
$tel_p = (isset($_POST['tel_p'])) ? $_POST['tel_p'] : "";
$fax = (isset($_POST['fax'])) ? $_POST['fax'] : "";
$tel_m = (isset($_POST['tel_m'])) ? $_POST['tel_m'] : "";
$mail_1 = (isset($_POST['mail_1'])) ? $_POST['mail_1'] : "";
$newsletter = (isset($_POST['newsletter'])) ? $_POST['newsletter'] : "";
$biographie_formular = (isset($_POST['biographie_formular'])) ? $_POST['biographie_formular'] : "";
$error = "";
$portal = "";

require_once (__DIR__.'/../includes/db.inc.php');

// ------------------------
// Aktualisiere. Personendaten
// ------------------------
if ($step == "2") {
	$rec = $fm->getRecordById('cgi_Adressaenderung_Personen', $_SESSION['record_id']); 
	$rec->setField('_kf__Korrespondenzsprache', $sprache); 
	$rec->setField('Vorname', $vorname); 
	$rec->setField('Strasse_1', $strasse_1); 
	$rec->setField('Strasse_2', $strasse_2); 
	$rec->setField('PLZ', $plz); 
	$rec->setField('Ort', $ort); 
	$rec->setField('_kf__Land_ISO_Code', $land_1); 
	$rec->setField('Tel_P', $tel_p); 
	$rec->setField('Fax', $fax); 
	$rec->setField('Tel_Mob', $tel_m); 
	$rec->setField('Mail_1', $mail_1); 
	$rec->setField('NewsletterEmpfaenger', $newsletter); 
	$rec->setField('Biographie_Formular', $biographie_formular); 	
	
	$result = $rec->commit(); 
	if (FileMaker::isError($result)) {
		echo 'false';
		die('Database error. Please contact Webmaster');
	} 
}


// ------------------------
// Indiv. Personendaten
// ------------------------

$find =& $fm->newFindCommand('cgi_Adressaenderung_Personen'); 
$find->addFindCriterion('web_ID', $_SESSION['q']); 
$result = $find->execute(); 
if (FileMaker::isError($result)) {
		echo 'Leider ist ein Fehler aufgetreten. Bitte kontaktieren Sie den WebMaster.';
		exit;
	}
$records = $result->getRecords(); 
$foundrec = $result->getFoundSetCount();
$record = $records[0];
$_SESSION['record_id'] = $record->getField('_record__id');
$personen_id = $record->getField('_kp__id');
$_SESSION['personen_id'] = $personen_id;
$sprache = $record->getField('_kf__Korrespondenzsprache');
$vorname = $record->getField('Vorname');
$name = $record->getField('Name');
$strasse_1 = $record->getField('Strasse_1');
$strasse_2 = $record->getField('Strasse_2');
$plz = $record->getField('PLZ');
$ort = $record->getField('Ort');
$land_1 = $record->getField('_kf__Land_ISO_Code');
$tel_p = $record->getField('Tel_P');
$fax = $record->getField('Fax');
$tel_m = $record->getField('Tel_Mob');
$mail_1 = $record->getField('Mail_1');
$newsletter = $record->getField('NewsletterEmpfaenger');
$biographie_formular = $record->getField('Biographie_Formular');

	if ($sprache == "") $sprache = "en";
	
	// ------------------------
	// Hole Feldbeschriftungen
	// ------------------------
	read_labels($sprache);

	
$relatedSet = $record->getRelatedSet('zz_Angestellte 2'); 
foreach ($relatedSet as $relatedRow) {
	$portal .= '<div id="geschaeftsadresse">'."\n";
	$portal .= '<strong>'.$relatedRow->getField('h_44__firmen 2::zz_Synthese_Adresszeile').'</strong>'."<br><br>\n";
	
	$portal .= '<div style="width:240px; float:left;">'."\n";
	$portal .= '<div style="width:100px; float:left;">'.$_SESSION['Leg_188'].':</div><div style="width:130px; float:left;">'.$relatedRow->getField('zz_Angestellte 2::Abteilung')."</div><br clear=\"all\" />\n";
	$portal .= '<div style="width:100px; float:left;">'.$_SESSION['Leg_189'].':</div><div style="width:130px; float:left;">'.$relatedRow->getField('zz_Angestellte 2::Funktion')."</div><br clear=\"all\" /><br clear=\"all\" />\n";
	$checkbox_output = ($record->getField('zz_Angestellte 2::NewsletterEmpfaenger') == "1")? $_SESSION['Leg_82'] : "";
	$portal .= '<div style="width:100px; float:left;">Newsletter:</div><div style="width:130px; float:left;">'.$checkbox_output."</div><br clear=\"all\" /><br clear=\"all\" />\n";
	//$portal .= '<div style="width:220px; float:left;">'.$_SESSION['Leg_178'].':</div><div style="width:130px; float:left;">'.$relatedRow->getField('zz_Angestellte 2::NewsletterEmpfaenger')."</div><br clear=\"all\" />\n";
	$portal .= '</div><div style="width:240px; float:left;">'."\n";
	$portal .= '<div style="width:100px; float:left;">'.$_SESSION['Leg_190'].':</div><div style="width:130px; float:left;">'.$relatedRow->getField('zz_Angestellte 2::Direktwahl_Tel')."</div><br clear=\"all\" />\n";
	$portal .= '<div style="width:100px; float:left;">'.$_SESSION['Leg_191'].':</div><div style="width:130px; float:left;">'.$relatedRow->getField('zz_Angestellte 2::Direktwahl_Fax')."</div><br clear=\"all\" />\n";
	$portal .= '<div style="width:100px; float:left;">'.$_SESSION['Leg_192'].':</div><div style="width:130px; float:left;">'.$relatedRow->getField('zz_Angestellte 2::Firmenhandy')."</div><br clear=\"all\" />\n";
	$portal .= '<div style="width:100px; float:left;">'.$_SESSION['Leg_44'].':</div><div style="width:130px; float:left;">'.$relatedRow->getField('zz_Angestellte 2::Mail')."</div><br clear=\"all\" />\n";
	$portal .= '</div><br clear="all" />'."\n";
	$portal .= 	'<p><img src="/images/edit.png" align="absmiddle" border="0" onClick="javascript:open_dialog4(\''.$relatedRow->getField('zz_Angestellte 2::__kp__id').'\');" title="'.$_SESSION['Leg_198'].'"> '.$_SESSION['Leg_198'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="/images/delete.png" align="absmiddle" border="0" onClick="javascript:deleteGeschaeftsadresse(\''.$relatedRow->getField('zz_Angestellte 2::__kp__id').'\');" title="'.$_SESSION['Leg_182'].'"> '.$_SESSION['Leg_182'].'</p>';
	$portal .= '</div>';
}	

include($_SERVER['DOCUMENT_ROOT'].'/includes/x_get_value_list_country.inc.php');
include($_SERVER['DOCUMENT_ROOT'].'/includes/x_get_value_list_namensaenderung.inc.php');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Online Registration</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="style.css" rel="stylesheet" type="text/css" title="KFT" />

<link rel="stylesheet" type="text/css" href="/includes/yui/build/container/assets/container.css?_yuiversion=2.3.1" />
<link rel="stylesheet" type="text/css" href="/includes/yui/build/fonts/fonts-min.css?_yuiversion=2.3.1" />
<link rel="stylesheet" type="text/css" href="/includes/yui/build/button/assets/skins/sam/button.css?_yuiversion=2.3.1" />
<link rel="stylesheet" type="text/css" href="/includes/yui/build/container/assets/skins/sam/container.css?_yuiversion=2.3.1" />
<script type="text/javascript" src="/includes/safari_hack.js"></script>
<script type="text/javascript" src="/includes/yui/build/utilities/utilities.js?_yuiversion=2.3.1"></script>
<script type="text/javascript" src="/includes/yui/build/button/button-beta.js?_yuiversion=2.3.1"></script>
<script type="text/javascript" src="/includes/yui/build/yahoo-dom-event/yahoo-dom-event.js?_yuiversion=2.3.1"></script>
<script type="text/javascript" src="/includes/yui/build/animation/animation-min.js?_yuiversion=2.3.1"></script>
<script type="text/javascript" src="/includes/yui/build/dragdrop/dragdrop-min.js?_yuiversion=2.3.1"></script>
<script type="text/javascript" src="/includes/yui/build/connection/connection.js?_yuiversion=2.3.1"></script>
<script type="text/javascript" src="/includes/yui/build/container/container.js?_yuiversion=2.3.1"></script>
<script type="text/javascript" language="javascript" charset="utf-8">
<!--

miniDialog = new YAHOO.widget.SimpleDialog("mini_dlg", { visible:false, width: "25em", effect:[
	{effect:YAHOO.widget.ContainerEffect.SLIDE,duration:0.25}, {effect:YAHOO.widget.ContainerEffect.FADE,duration:0.25}
	], fixedcenter : true, modal:true, draggable:false });

miniDialog.setHeader("Attention!");
miniDialog.setBody("Do you really want to do this?");

miniDialog.cfg.queueProperty("icon",YAHOO.widget.SimpleDialog.ICON_INFO);
miniDialog.cfg.queueProperty("buttons", [ 
	{ text:"OK", handler:handlerHide, isDefault:true }
]);

miniDialog.render(document.body);

var handlerHide = function(){
	this.hide();
};
var handleCancel = function(){
	this.cancel();
};
var failure_ajax = function(obj){
alert("Hinweis<br>Es gab ein Problem bei der Kommunikation mit dem Server, bitte versuchen Sie es zu einem sp&auml;teren Zeitpunkt erneut.");
};


function open_dialog4(id) {
	document.getElementById('dialog4_id').value = id;
	YAHOO.util.Connect.asyncRequest('POST', 'ajax_fill_angestellter.php?id='+id, { success: fill_angestellter_final, failure: failure_ajax });
	Dialog4.show();
}

var fill_angestellter_final = function(obj){
	try {
		eval(obj.responseText);
	}
	catch(err){
		alert("Hinweis<br>Es gab ein Problem bei der Kommunikation mit dem Server, bitte versuchen Sie es zu einem sp&auml;teren Zeitpunkt erneut.");
	}
};


function init() {
	<? include('js_neuer_name.inc.php'); ?>
	<? include('js_neue_firma_dialoge.inc.php'); ?>
	<? include('js_angestellter_edit.inc.php'); ?>		
}

<? include('js_neue_firma_logik.inc.php'); ?>


var delFilmoSuccess = function (obj){
	miniDialog.hide();
	try{
		eval(obj.responseText);	
	}
	catch(err){
		alert("Hinweis<br>Es gab ein Problem bei der Kommunikation mit dem Server, bitte versuchen Sie es zu einem sp&auml;teren Zeitpunkt erneut.");
	}
};

function deleteFimographie(id){
	delId = id;
	
	miniDialog.setHeader("<?=$_SESSION['Leg_80']?>");
	miniDialog.setBody("");
	miniDialog.cfg.queueProperty("icon",YAHOO.widget.SimpleDialog.ICON_WARN);
	miniDialog.cfg.queueProperty("buttons", [ 
		{ text: "<?=$_SESSION['Leg_82']?>", handler:delFilmoConfirm },
		{ text: "<?=$_SESSION['Leg_83']?>", handler:handlerHide, isDefault:true  }
	]);
	miniDialog.render(document.body);
	miniDialog.show();
}

var delFilmoConfirm = function (obj){
	YAHOO.util.Connect.asyncRequest('GET', 'ajax_delete_filmo.php?id='+delId, { success: delFilmoSuccess, failure: failure_ajax });
};



var addFilmo = function (obj){
	YAHOO.util.Connect.asyncRequest('GET', 'ajax_add_filmo.php?filmo_titel='+document.getElementById('filmo_titel').value+'&filmo_jahr='+document.getElementById('filmo_jahr').value, { success: addFilmoSuccess, failure: failure_ajax });
};
var addFilmoSuccess = function (obj){
	document.getElementById('filmo_titel').value = "";
	document.getElementById('filmo_jahr').value = "";
	try{
		eval(obj.responseText);	
	}
	catch(err){
		alert("Hinweis<br>Es gab ein Problem bei der Kommunikation mit dem Server, bitte versuchen Sie es zu einem sp&auml;teren Zeitpunkt erneut.");
	}
};

YAHOO.util.Event.onDOMReady(init);
//-->

</script>
</head>
<body class=" yui-skin-sam">


<div id="container">
  <div id="top">
    <? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
	<? if ($error != "") { ?>	
	<p style="border:1px solid #990000; background-color:#FFDCD6; padding:5px; width: 450px"><?=$error; ?></p>
	<? } ?>
  </div>
  <br clear="all" />
  <fieldset>
  <legend><?=$_SESSION['Leg_162']?></legend>
  <p><?=$_SESSION['Leg_163']?></p>
  <br clear="all" />
  <form action="<?=$_SERVER['PHP_SELF']?>" method="post" name="form1">
	<input type="hidden" value="2" name="step" />
	
	<div style="width:250px; float:left;">
		<label for="vorname"><?=$_SESSION['Leg_34']?></label>
		<div class="div_texbox"><input name="vorname" type="text" class="textbox" id="vorname" value="<?=$vorname?>" /></div>
		<br clear="all" />
		
		<label for="name"><?=$_SESSION['Leg_35']?></label>
		<div class="div_texbox"><?=$name?></div>
		<br clear="all" /><br />
		<input type="button" name="name_aendern" id="name_aendern" value="Name ändern" />
	</div>
	<div style="width:250px; float:left;">
		<img src="/bilder/<?=$personen_id?>z_small.jpg?random=<?=time()?>" align="right" />
	</div>
	<br clear="all" />
	
	<p><?=$_SESSION['Leg_79']?> 
    <select name="sprache">
	<option value="" <? echo ($sprache == "")? 'selected' : ""; ?>>-</option>
	<option value="de" <? echo ($sprache == "de")? 'selected' : ""; ?>>DE</option>
	<option value="en" <? echo ($sprache == "en")? 'selected' : ""; ?>>EN</option>
	<option value="fr" <? echo ($sprache == "fr")? 'selected' : ""; ?>>FR</option>
	<option value="it" <? echo ($sprache == "it")? 'selected' : ""; ?>>IT</option>
	</select></p>
	
	<div style="width:250px; float:left;">
		<label for="strasse_1"><?=$_SESSION['Leg_36']?></label>
		<div class="div_texbox"><input name="strasse_1" type="text" class="textbox" id="strasse_1" value="<?=$strasse_1?>" /></div>
		<br clear="all" />
		
		<label for="strasse_2"><?=$_SESSION['Leg_37']?></label>
		<div class="div_texbox"><input name="strasse_2" type="text" class="textbox" id="strasse_2" value="<?=$strasse_2?>" /></div>
		<br clear="all" />
		
		<label for="plz"><?=$_SESSION['Leg_38']?></label>
		<div class="div_texbox"><input name="plz" type="text" class="textbox" id="plz" value="<?=$plz?>" /></div>
		<br clear="all" />
		
		<label for="ort"><?=$_SESSION['Leg_39']?></label>
		<div class="div_texbox"><input name="ort" type="text" class="textbox" id="ort" value="<?=$ort?>" /></div>
		<br clear="all" />
		
		<label for="land_1"><?=$_SESSION['Leg_40']?></label>
		<div class="div_texbox"><select name="land_1" id="land_1">
		<option value="">-</option>
		<?=$output_Land?>
		</select></div>
		<br clear="all" />
	</div>
	
	<div style="width:250px; float:left;">
		<label for="tel_p"><?=$_SESSION['Leg_41']?></label>
		<div class="div_texbox"><input name="tel_p" type="text" class="textbox" id="tel_p" value="<?=$tel_p?>" /></div>
		<br clear="all" />
		
		<label for="fax"><?=$_SESSION['Leg_43']?></label>
		<div class="div_texbox"><input name="fax" type="text" class="textbox" id="fax" value="<?=$fax?>" /></div>
		<br clear="all" />
		
		<label for="tel_m"><?=$_SESSION['Leg_176']?></label>
		<div class="div_texbox"><input name="tel_m" type="text" class="textbox" id="tel_m" value="<?=$tel_m?>" /></div>
		<br clear="all" />
		
		<label for="mail_1"><?=$_SESSION['Leg_44']?></label>
		<div class="div_texbox"><input name="mail_1" type="text" class="textbox" id="mail_1" value="<?=$mail_1?>" /></div>
		<br clear="all" />
	</div>
	<br clear="all" />	
	<p><?=$_SESSION['Leg_178']?> 
	<input type="radio" name="newsletter" value="1" <? echo ($newsletter == "1")? 'checked' : ''; ?> /> <?=$_SESSION['Leg_82']?> 	<input type="radio" name="newsletter" value="" <? echo ($newsletter == "")? 'checked' : ''; ?> /> <?=$_SESSION['Leg_83']?><br /></p>
	
	<h3><?=$_SESSION['Leg_179']?></h3>
	<p><?=$_SESSION['Leg_180']?></p>
	<p><img src="/images/add.png" align="absmiddle" border="0" onclick="javascript:newFirma();" title="<?=$_SESSION['Leg_181']?>" /> <?=$_SESSION['Leg_181']?></p>
	<div id="geschaeftsadressen"><?=$portal?></div>
		
	<h3><?=$_SESSION['Leg_49']?></h3>
	<p><?=$_SESSION['Leg_184']?><br /><br />
	<textarea name="biographie_formular" style="width:500px; border: 1px solid #999999; font-family:Arial, Helvetica, sans-serif; font-size:11px" rows="8"><?=$biographie_formular?></textarea>
	</p>
	
	<h3><?=$_SESSION['Leg_50']?></h3>
	<p><?=$_SESSION['Leg_185']?></p>
	<br />
	
	<div style="width:180px; float:left;">
		<p><?=$_SESSION['Leg_76']?><br />
		<input type="text" class="textbox" id="filmo_titel" name="filmo_titel" /></p>
	</div>
	<div style="width:70px; float:left;">
		<p><?=$_SESSION['Leg_56']?><br />
		<input type="text" class="textbox_short" id="filmo_jahr" name="filmo_jahr" style="width: 60px;" /></p>
	</div>
	<div style="width:80px; float:left;">
		<p>&nbsp;<br /><img src="/images/add.png" align="absmiddle" border="0" onclick="javascript:addFilmo();" title="<?=$_SESSION['Leg_78']?>" /> <?=$_SESSION['Leg_78']?></p>
	</div>
	<br clear="all" />
	<div id="filmographie">
	<?
	$find =& $fm->newFindCommand('cgi_k_Filmographien'); 
	$find->addFindCriterion('_kf__Person_Id', $_SESSION['personen_id']); 
	$find->addFindCriterion('OnlineDeleteFlag', "="); 
	$find->addSortRule('Jahr', 1, FILEMAKER_SORT_DESCEND); 
	$result = $find->execute(); 
	if (FileMaker::isError($result)) {
			echo 'Leider ist ein Fehler aufgetreten. Bitte kontaktieren Sie den WebMaster.';
			exit;
		}
	$records = $result->getRecords(); 
	foreach ($records as $record) {
	?>
		<div style="width:180px; float:left;"><?=$record->getField('Titel')?></div>
		<div style="width:70px; float:left;"><?=$record->getField('Jahr')?></div>
		<div style="width:80px; float:left;"><img src="/images/delete.png" align="absmiddle" border="0" onclick="javascript:deleteFimographie('<?=$record->getField('_kp__id')?>');" title="<?=$_SESSION['Leg_88']?>" /> <?=$_SESSION['Leg_88']?></div>
		<br clear="all" />
	<?
	}
	?>
	</div>
	<br />
	<input type="submit" value="<?=$_SESSION['Leg_114']?>"  style="background-color:#FFFFCC; border:1px solid #666666; font-size:16px;"/>
  </form>
  <div class="clear"></div>
  </fieldset>
</div>




<? /*

#########################################
#                                       #
#     Dialoge zur Namensänderung        #
#                                       #
#########################################

*/ ?>

<div id="div_name_aendern">
	<div class="hd"><?=$_SESSION['Leg_169']?></div>
	<div class="bd">
		<p><?=$_SESSION['Leg_165']?></p>
		<input type="button" id="change" class="mainoption" value="<?=$_SESSION['Leg_166']?>" />
		<br />&nbsp;<br />
		<p><?=$_SESSION['Leg_167']?></p>
		<input type="button" id="new" class="mainoption" value="<?=$_SESSION['Leg_168']?>" onclick="window.location.href='newsletter.php?sprache=<?=$sprache?>'" />
		<br />
	</div>
</div>

<div id="div_name_aendern2">
	<div class="hd"><?=$_SESSION['Leg_169']?></div>
	<div class="bd">
		<form id="new_name" action="javascript:void(0)" method="post">
		<p><?=$_SESSION['Leg_35']?> <input type="text" id="neuer_name" class="textbox" name="neuer_name" /></p>
		<p><?=$_SESSION['Leg_170']?> <select name="grund" id="grund" class="textbox">
		<option value="">-</option>
		<?=$output_Grund?>
		</select></p>
		<br />
			</form>
	</div>
</div>

<div id="div_name_aendern3">
	<div class="hd"><?=$_SESSION['Leg_169']?></div>
	<div class="bd">
		<p><?=$_SESSION['Leg_172']?></p>
	</div>
</div>


<? /*

#########################################
#                                       #
#  Dialoge zur Geschäftsadressauswahl   #
#                                       #
#########################################

*/ ?>

<div id="div_dlg_newfirma">
	<div class="hd">Firma suchen</div>
	<div class="bd">
		<div id="span_newfirma_step1">
		 <form id="form_search_newfirma" action="javascript:void(0)" method="post"> 
			<div style="float:left; font-size:11px;">
				Suche nach<br />
				Firma<br />
				<input type="text" class="post" name="s_firma" id="s_firma" value="" size="18" style="border: 1px solid #666666; font-size:11px;" />
			</div>
			<div style="float:left; font-size:11px;">
				&nbsp;<br />
				Ort<br />
				<input type="text" class="post" name="s_ort" id="s_ort" value="" size="18" style="border: 1px solid #666666; font-size:11px;" />
				<input type="hidden" name="type" value="1" />
			</div>
			<div style="float:left; font-size:11px;">
				&nbsp;<br />
				&nbsp;<br />
				<input type="button" class="mainoption" value="<?=$_SESSION['Leg_100']?>" onclick="newfirma_search()" />
			</div>
			 <br clear="all" /> 
			
		 </form><br />
		<span id="span_searchresult_newfirma"></span>
		</div>
		<span id="span_newfirma_step2"></span>
	</div>
</div>

<div id="div_dlg_addfirma">
	<div class="hd">Firma hinzufügen</div>
	<div class="bd">
		<div id="span_addfirma">
		 <form id="form_add_newfirma" action="javascript:void(0)" method="post" enctype="multipart/form-data"> 
			<div style="float:left; width: 250px;">
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_53']?>*</div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="dlg_firma_firma" id="dlg_firma_firma" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_36']?>*</div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="dlg_firma_strasse1" id="dlg_firma_strasse1" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_37']?></div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="dlg_firma_strasse2" id="dlg_firma_strasse2" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_38']?>*</div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="dlg_firma_plz" id="dlg_firma_plz" value="" size="6" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_39']?>*</div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="dlg_firma_ort" id="dlg_firma_ort" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_40']?>*</div>
				<div style="float:left; font-size:11px;">
    <select class="post" name="dlg_firma_land" id="dlg_firma_land" style="border: 1px solid #666666; font-size:11px;">
	<option value="">-</option>
	<?=$output_Land?>
	</select></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_41']?></div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="dlg_firma_fon" id="dlg_firma_fon" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_43']?></div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="dlg_firma_fax" id="dlg_firma_fax" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_44']?></div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="dlg_firma_email" id="dlg_firma_email" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
				<div style="float:left; font-size:11px; width: 70px;"><?=$_SESSION['Leg_45']?></div>
				<div style="float:left; font-size:11px;"><input type="text" class="post" name="dlg_firma_www" id="dlg_firma_www" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
				<br clear="all" />
			</div>
			<br clear="all" />
			<input type="hidden" name="type" value="1" />
		 </form><br />
		<span id="span_searchresult_newfirma"></span>
		</div>
		<span id="span_newfirma_step2"></span>
	</div>
</div>


<? /*

#########################################
#                                       #
#  Dialoge zur Geschäftsadressänderung  #
#                                       #
#########################################

*/ ?>

<div id="div_angestellter_aendern1">
	<div class="hd"><?=$_SESSION['Leg_179']?></div>
	<div class="bd">
		 <form id="edit_angestellter" action="javascript:void(0)" method="post" enctype="multipart/form-data"> 
		 <input type="hidden" name="dialog4_id" id ="dialog4_id" value="" />
		<div style="width:100px; float:left;"><?=$_SESSION['Leg_188']?>:</div>
		<div style="width:130px; float:left;"><input type="text" class="post" name="dialog4_abteiluung" id="dialog4_abteilung" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
		<br clear="all" />
		<div style="width:100px; float:left;"><?=$_SESSION['Leg_189']?>:</div>
		<div style="width:130px; float:left;"><input type="text" class="post" name="dialog4_funktion" id="dialog4_funktion" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
		<br clear="all" />
		<div style="width:100px; float:left;"><?=$_SESSION['Leg_190']?>:</div>
		<div style="width:130px; float:left;"><input type="text" class="post" name="dialog4_tel" id="dialog4_tel" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
		<br clear="all" />
		<div style="width:100px; float:left;"><?=$_SESSION['Leg_191']?>:</div>
		<div style="width:130px; float:left;"><input type="text" class="post" name="dialog4_fax" id="dialog4_fax" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
		<br clear="all" />
		<div style="width:100px; float:left;"><?=$_SESSION['Leg_192']?>:</div>
		<div style="width:130px; float:left;"><input type="text" class="post" name="dialog4_mobil" id="dialog4_mobil" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
		<br clear="all" />
		<div style="width:100px; float:left;"><?=$_SESSION['Leg_44']?>:</div>
		<div style="width:130px; float:left;"><input type="text" class="post" name="dialog4_mail" id="dialog4_mail" value="" size="20" style="border: 1px solid #666666; font-size:11px;" /></div>
		<br clear="all" /> 
		 <input type="hidden" name="dialog4_newsletter" id="dialog4_newsletter" value="" />
		<input type="checkbox" name="dialog4_nl_checkbox" id="dialog4_nl_checkbox" value="1" /> <?=$_SESSION['Leg_178']?> 
		 </form>

	</div>
</div>


<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>

</body>
</html>
