newFirmaDlg = new YAHOO.widget.Dialog("div_dlg_newfirma", { modal:true, visible:false, iframe:true, width:"370px", fixedcenter : true, constraintoviewport:true, draggable:true });
	
	// START safari-fix of close on return
	newFirmaDlg.hideOnSubmit = false;	                
	var listeners = [new YAHOO.util.KeyListener(document, 
       {keys : 27}, {fn:handleCancel, scope:newFirmaDlg, correctScope:true}),
       new YAHOO.util.KeyListener(document, 
       {keys : [13,10]}, {fn:newfirma_search, scope:newFirmaDlg, correctScope:true})];
    newFirmaDlg.cfg.queueProperty("keylisteners", listeners);
	// END safari-fix of close on return
					
	newFirmaDlg.cfg.queueProperty("buttons", [ { text:"<?=$_SESSION['Leg_99']?>", handler:handleCancel } ]);
	newFirmaDlg.cfg.setProperty('postmethod','async')
	newFirmaDlg.render();
	
	
	
addFirmaDlg = new YAHOO.widget.Dialog("div_dlg_addfirma", { modal:true, visible:false, iframe:true, width:"370px", fixedcenter : true,constraintoviewport:true, draggable:true });
	
	// START safari-fix of close on return
	addFirmaDlg.hideOnSubmit = false;	                
	var listeners = [new YAHOO.util.KeyListener(document, 
       {keys : 27}, {fn:handleCancel, scope:addFirmaDlg, correctScope:true}),
       new YAHOO.util.KeyListener(document, 
       {keys : [13,10]}, {fn:newfirma_add, scope:addFirmaDlg, correctScope:true})];
    addFirmaDlg.cfg.queueProperty("keylisteners", listeners);
	// END safari-fix of close on return
					
	addFirmaDlg.cfg.queueProperty("buttons", [ { text:"<?=$_SESSION['Leg_114']?>", handler:newfirma_add },{ text:"<?=$_SESSION['Leg_99']?>", handler:handleCancel } ]);
	addFirmaDlg.cfg.setProperty('postmethod','async')
	addFirmaDlg.render();
	
	
	
changeFirmaDlg = new YAHOO.widget.Dialog("div_dlg_changefirma", { modal:true, visible:false, iframe:true, width:"370px", fixedcenter : true,constraintoviewport:true, draggable:true });
	
	// START safari-fix of close on return
	changeFirmaDlg.hideOnSubmit = false;	                
	var listeners = [new YAHOO.util.KeyListener(document, 
       {keys : 27}, {fn:handleCancel, scope:changeFirmaDlg, correctScope:true}),
       new YAHOO.util.KeyListener(document, 
       {keys : [13,10]}, {fn:change_firma, scope:changeFirmaDlg, correctScope:true})];
    changeFirmaDlg.cfg.queueProperty("keylisteners", listeners);
	// END safari-fix of close on return
					
	changeFirmaDlg.cfg.queueProperty("buttons", [ { text:"<?=$_SESSION['Leg_213']?>", handler:change_firma },{ text:"<?=$_SESSION['Leg_99']?>", handler:handleCancel } ]);
	changeFirmaDlg.cfg.setProperty('postmethod','async')
	changeFirmaDlg.render();
	
	
	
changeFirmaDlgFinal = new YAHOO.widget.Dialog("div_dlg_changefirma_final", { modal:true, visible:false, iframe:true, width:"370px", fixedcenter : true,constraintoviewport:true, draggable:true });
	
	// START safari-fix of close on return
	changeFirmaDlgFinal.hideOnSubmit = false;	                
	var listeners = [new YAHOO.util.KeyListener(document, 
       {keys : 27}, {fn:handleCancel, scope:changeFirmaDlgFinal, correctScope:true}),
       new YAHOO.util.KeyListener(document, 
       {keys : [13,10]}, {fn:handleCancel, scope:changeFirmaDlgFinal, correctScope:true})];
    changeFirmaDlgFinal.cfg.queueProperty("keylisteners", listeners);
	// END safari-fix of close on return
					
	changeFirmaDlgFinal.cfg.queueProperty("buttons", [ { text:"<?=$_SESSION['Leg_93']?>", handler:handleCancel } ]);
	changeFirmaDlgFinal.cfg.setProperty('postmethod','async')
	changeFirmaDlgFinal.render();