<?
session_start();
// Bust cache in the head
header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
// always modified
header ("Cache-Control: no-cache, must-revalidate");  // HTTP/1.1
header ("Pragma: no-cache");                          // HTTP/1.0
header('Content-Type: text/html; charset=utf-8');

$id = (isset($_REQUEST['filmo_edit_rid']))? $_REQUEST['filmo_edit_rid'] : "";
$titel = (isset($_REQUEST['filmo_edit_titel']))? $_REQUEST['filmo_edit_titel'] : "";
$genre = (isset($_REQUEST['filmo_edit_genre']))? $_REQUEST['filmo_edit_genre'] : "";
$jahr = (isset($_REQUEST['filmo_edit_jahr']))? $_REQUEST['filmo_edit_jahr'] : "";
$coregie = (isset($_REQUEST['filmo_edit_coregie']))? $_REQUEST['filmo_edit_coregie'] : "";
$portal = "";


require_once (__DIR__.'/../includes/db.inc.php');

/* 
#################################
Record editieren
#################################
*/

$rec = $fm->getRecordById('cgi_k_Stabmitglieder', $id); 
$rec->setField('Filmtitel', $titel); 
$rec->setField('Jahr', $jahr); 
$rec->setField('Genre', $genre); 
$rec->setField('CoRegieFlag', $coregie); 
$result = $rec->commit(); 
if (FileMaker::isError($result)) {
	echo 'false';
	die();
} 
/* 
#################################
Liste der Filmos neu aufbauen
#################################
*/

$find =& $fm->newFindCommand('cgi_k_Stabmitglieder'); 
$find->addFindCriterion('_kf__Person_id', $_SESSION['personen_id']); 
$find->addFindCriterion('_kf__Stabbezeichnung_Id', "1"); 
$find->addFindCriterion('OnlineDeleteFlag', "="); 
	$find->addSortRule('Jahr', 1, FILEMAKER_SORT_DESCEND);
$result = $find->execute(); 
if (FileMaker::isError($result)) {
		echo 'Leider ist ein Fehler aufgetreten. Bitte kontaktieren Sie den WebMaster.';
		exit;
	}
$records = $result->getRecords(); 

foreach ($records as $record) {

	$portal .= '<div style="width:175px; float:left;">'.htmlspecialchars($record->getField('Filmtitel'),ENT_QUOTES).'</div>'."\\n";
	$portal .= '<div style="width:70px; float:left;">'.$record->getField('Jahr').'&nbsp;</div>'."\\n";
	$portal .= '<div style="width:140px; float:left;">'.$record->getField('zz_Stabmitglieder_Genretypen::Genre_'.$_SESSION['sprache']).'&nbsp;</div>'."\\n";
	$portal .= ($record->getField('CoRegieFlag') == "1")? '<div style="width:40px; float:left;"><img src="/images/accept.png" />'.'</div>'."\\n" : '<div style="width:40px; float:left;">&nbsp;'.'</div>'."\\n";
	$portal .= '<div style="width:60px; float:left;"><img src="/images/edit.png" align="absmiddle" border="0" onClick="javascript:editFimographie(\\\''.$record->getField('_kp__record_id').'\\\');" title="'.$_SESSION['Leg_217'].'">  <img src="/images/delete.png" align="absmiddle" border="0" onClick="javascript:deleteFimographie(\\\''.$record->getField('_kp__id').'\\\');" title="'.$_SESSION['Leg_88'].'"></div>'."\\n";
	
	//$portal .= '<div style="width:180px; float:left;"><img src="/images/delete.png" align="absmiddle" border="0" onClick="javascript:deleteFimographie(\\\''.$record->getField('_kp__id').'\\\');" title="'.$_SESSION['Leg_88'].'"> '.$_SESSION['Leg_88'].'</div>'."\\n";
	$portal .= '<br clear="all" />'."\\n";

}

$span = " document.getElementById('filmographie').innerHTML = '".$portal."';";
die($span);

?>