

// Firmensuche
//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__


function newFirma(type){
	newFirmaDlg.show();
}

function newfirma_search(){
	document.getElementById('span_searchresult_newfirma').innerHTML = "<blink>...</blink>";
	var oForm = document.getElementById('form_search_newfirma');
	YAHOO.util.Connect.setForm(oForm);
	YAHOO.util.Connect.asyncRequest('POST', '/includes/ajax_search_newfirma.php', { success: newfirma_searchresult, failure: failure_ajax });
}

var newfirma_searchresult = function(obj){
	document.getElementById('span_searchresult_newfirma').innerHTML = obj.responseText;
};

function newfirma_save(id,firma,plz,ort,type){
	YAHOO.util.Connect.asyncRequest('GET', 'ajax_add_new_firma.php?firma_id='+id, { success: newfirma_saved });
}

var newfirma_saved = function(obj){
	newFirmaDlg.hide();
	document.getElementById('span_searchresult_newfirma').innerHTML = "";
	//document.getElementById('firma').value = "";
	//document.getElementById('ort').value = "";
	try {
		eval(obj.responseText);
	}
	catch(err){
		alert("Hinweis<br>Es gab ein Problem bei der Kommunikation mit dem Server, bitte versuchen Sie es zu einem sp&auml;teren Zeitpunkt erneut.<br>");
	}
};





var delFirmaSuccess = function (obj){
	miniDialog.hide();
	try{
		eval(obj.responseText);	
	}
	catch(err){
		alert("Hinweis<br>Es gab ein Problem bei der Kommunikation mit dem Server, bitte versuchen Sie es zu einem sp&auml;teren Zeitpunkt erneut.");
	}
};
// Abfrage ob ein Verweis wirklich gel�scht werden soll.
function deleteGeschaeftsadresse(id){
	delId = id;
	
	miniDialog.setHeader("<?=$_SESSION['Leg_80']?>");
	miniDialog.setBody("<?=$_SESSION['Leg_84']?>");
	miniDialog.cfg.queueProperty("icon",YAHOO.widget.SimpleDialog.ICON_WARN);
	miniDialog.cfg.queueProperty("buttons", [ 
		{ text: "<?=$_SESSION['Leg_82']?>", handler:delFirmaConfirm },
		{ text: "<?=$_SESSION['Leg_83']?>", handler:handlerHide, isDefault:true  }
	]);
	miniDialog.render(document.body);
	miniDialog.show();
}

// Verweisentfernung durchfhren
var delFirmaConfirm = function (obj){
	//loading(true);
	YAHOO.util.Connect.asyncRequest('GET', 'ajax_delete_firma.php?id='+delId, { success: delFirmaSuccess, failure: failure_ajax });
};





// Neue Firma der DB hinzuf�gen
//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

function addFirmaStart(){
	newFirmaDlg.hide();
	//document.getElementById('personen_typ').value = type;
	addFirmaDlg.show();
}

function newfirma_add(){
	var error = "";
	var email_bad = false;
	if (document.getElementById('dlg_firma_email').value != "") {
		var x = document.getElementById('dlg_firma_email').value;
		var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		email_bad = true;
		if (filter.test(x)) email_bad = false;
	}
	if ((document.getElementById('dlg_firma_firma').value == "") ||
		(document.getElementById('dlg_firma_strasse1').value == "") ||
		(document.getElementById('dlg_firma_plz').value == "") ||
		(document.getElementById('dlg_firma_ort').value == "") ||
		(document.getElementById('dlg_firma_land').value == "") ||
		(email_bad)
		) {
		var error_txt = "<?=$_SESSION['Leg_86']?>";
		if (email_bad) { error_txt += "<br /><?=$_SESSION['Leg_87']?>"; }
		miniDialog.setHeader("<?=$_SESSION['Leg_85']?>");
		miniDialog.setBody(error_txt);
		miniDialog.cfg.queueProperty("icon",YAHOO.widget.SimpleDialog.ICON_INFO);
		miniDialog.cfg.queueProperty("buttons", [ { text:"OK", handler:handlerHide, isDefault:true } ]);
		miniDialog.render(document.body);
		miniDialog.show();
		error = "1";
	}
	if (error == "") {
		var oForm = document.getElementById('form_add_newfirma');
		YAHOO.util.Connect.setForm(oForm);
		//YAHOO.util.Connect.asyncRequest('POST', '/includes/ajax_add_newperson.php', callback);
		YAHOO.util.Connect.asyncRequest('POST', 'ajax_add_allnewfirma.php', { success: newfirma_addresult, failure: failure_ajax });
	}
}


// var callback = { upload: newperson_addresult }
var newfirma_addresult = function(obj){
	//loading(false);
	addFirmaDlg.hide();
	try {
		eval(obj.responseText);
	}
	catch(err){
		infoDialog("Hinweis","Es gab ein Problem bei der Kommunikation mit dem Server, bitte versuchen Sie es zu einem sp&auml;teren Zeitpunkt erneut.<br>"+err.description);
	}
};





// Firma der DB �ndern
//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

function change_firma(){
	var error = "";
	var email_bad = false;
	if (document.getElementById('dlg_firma_email_change').value != "") {
		var x = document.getElementById('dlg_firma_email_change').value;
		var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		email_bad = true;
		if (filter.test(x)) email_bad = false;
	}
	if ((document.getElementById('dlg_firma_firma_change').value == "") ||
		(document.getElementById('dlg_firma_strasse1_change').value == "") ||
		(document.getElementById('dlg_firma_plz_change').value == "") ||
		(document.getElementById('dlg_firma_ort_change').value == "") ||
		(document.getElementById('dlg_firma_land_change').value == "") ||
		(email_bad)
		) {
		var error_txt = "<?=$_SESSION['Leg_86']?>";
		if (email_bad) { error_txt += "<br /><?=$_SESSION['Leg_87']?>"; }
		miniDialog.setHeader("<?=$_SESSION['Leg_85']?>");
		miniDialog.setBody(error_txt);
		miniDialog.render(document.body);
		miniDialog.show();
		error = "1";
	}
	if (error == "") {
		var oForm = document.getElementById('form_change_firma');
		YAHOO.util.Connect.setForm(oForm);
		YAHOO.util.Connect.asyncRequest('POST', 'ajax_add_change_firma.php', { success: change_firma_result, failure: failure_ajax });
	}
}

var change_firma_result = function(obj){
	//loading(false);
	changeFirmaDlg.hide();
	changeFirmaDlgFinal.show();
	try {
		//eval(obj.responseText);
	}
	catch(err){
		infoDialog("Hinweis","Es gab ein Problem bei der Kommunikation mit dem Server, bitte versuchen Sie es zu einem sp&auml;teren Zeitpunkt erneut.<br>"+err.description);
	}
};
