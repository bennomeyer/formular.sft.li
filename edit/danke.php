<?
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
session_start();

$sprache = (isset($_REQUEST['sprache'])) ? $_REQUEST['sprache'] : "en";
$version = (isset($_REQUEST['version'])) ? $_REQUEST['version'] : "";

require_once (__DIR__.'/../includes/db.inc.php');

// ------------------------
// Hole Feldbeschriftungen
// ------------------------
read_labels($sprache);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><? echo ($version == "akk")? $_SESSION['Leg_294'] : $_SESSION['Leg_194']; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="style.css" rel="stylesheet" type="text/css" title="KFT" />
</head>
<body>

<div id="container">
  <div id="top">
    <? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
  </div>
  <br clear="all" />
  <fieldset>
  <legend><? echo ($version == "akk")? $_SESSION['Leg_294'] : $_SESSION['Leg_194']; ?></legend>

  <p><?=$_SESSION['Leg_196']?></p>
  <br clear="all" />
  <div class="clear"></div>
  </fieldset>
</div>

<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>

</body>
</html>
