	// ---------------------
	// Neuer Name Dialog 1
	// ---------------------
	Dialog1 = new YAHOO.widget.Dialog("div_name_aendern", 
		{	modal:true, 
			visible:false, 
			iframe:true, 
			width:"470px", 
			fixedcenter : true,
			constraintoviewport:true, 
			draggable:true,
			buttons : [ { text:"<?=$_SESSION['Leg_99']?>", handler:handleCancel } ]
		});
	
	// START safari-fix of close on return
	Dialog1.hideOnSubmit = false;	                
	var listeners = [new YAHOO.util.KeyListener(document, 
       {keys : 27}, {fn:handleCancel, scope:Dialog1, correctScope:true})];
    Dialog1.cfg.queueProperty("keylisteners", listeners);
	// END safari-fix of close on return
	
	Dialog1.cfg.setProperty('postmethod','async')
	Dialog1.render();
	YAHOO.util.Event.addListener("name_aendern", "click", Dialog1.show, Dialog1, true);
		
	// ---------------------
	// Neuer Name Dialog 2
	// ---------------------
	function name_aendern_submit(){
		var oForm = document.getElementById('new_name');
		YAHOO.util.Connect.setForm(oForm);
		sel = document.getElementById('grund');
		YAHOO.util.Connect.asyncRequest('POST', 'ajax_submit_new_name.php?v='+document.getElementById('neuer_vorname').value+'&n='+document.getElementById('neuer_name').value+'&g='+sel.options[sel.selectedIndex].value, { success: name_geaendert, failure: failure_ajax });
	}

	var name_geaendert = function(obj){
		if (obj.responseText == 'false') {
			alert('Error. Please try again later.');
		} else {
			Dialog2.hide();
			Dialog3.show();
		}
	};
	
	
	var showNewDialog = function() {
		Dialog1.hide();
		Dialog2.show();
	};
	Dialog2 = new YAHOO.widget.Dialog("div_name_aendern2", 
		{	modal:true, 
			visible:false, 
			iframe:true, 
			width:"470px", 
			fixedcenter : true,
			constraintoviewport:true, 
			draggable:true,
			buttons : [ { text:"<?=$_SESSION['Leg_171']?>", handler:name_aendern_submit } ]
		});
	
	// START safari-fix of close on return
	Dialog2.hideOnSubmit = false;	                
	var listeners = [new YAHOO.util.KeyListener(document, 
       {keys : 27}, {fn:handleCancel, scope:Dialog2, correctScope:true})];
    Dialog2.cfg.queueProperty("keylisteners", listeners);
	// END safari-fix of close on return
	
	Dialog2.cfg.setProperty('postmethod','async')
	Dialog2.render();
	YAHOO.util.Event.addListener("change", "click", showNewDialog, Dialog2, true);
	
	// ---------------------
	// Neuer Name Dialog 3
	// ---------------------
	Dialog3 = new YAHOO.widget.Dialog("div_name_aendern3", 
		{	modal:true, 
			visible:false, 
			iframe:true, 
			width:"470px", 
			fixedcenter : true,
			constraintoviewport:true, 
			draggable:true,
			buttons : [ { text:"<?=$_SESSION['Leg_99']?>", handler:handleCancel } ]
		});
	
	// START safari-fix of close on return
	Dialog3.hideOnSubmit = false;	                
	var listeners = [new YAHOO.util.KeyListener(document, 
       {keys : 27}, {fn:handleCancel, scope:Dialog3, correctScope:true})];
    Dialog3.cfg.queueProperty("keylisteners", listeners);
	// END safari-fix of close on return
	
	Dialog3.render();