<?php
session_start();
// Bust cache in the head
header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
// always modified
header ("Cache-Control: no-cache, must-revalidate");  // HTTP/1.1
header ("Pragma: no-cache");                          // HTTP/1.0
header('Content-Type: text/html; charset=utf-8');

$firma_id = (isset($_POST['dlg_firma_firma_change_id'])) ? $_POST['dlg_firma_firma_change_id'] : "";
$firma = (isset($_POST['dlg_firma_firma_change'])) ? $_POST['dlg_firma_firma_change'] : "";
$name = (isset($_POST['dlg_firma_name_change'])) ? $_POST['dlg_firma_name_change'] : "";
$strasse1 = (isset($_POST['dlg_firma_strasse1_change'])) ? $_POST['dlg_firma_strasse1_change'] : "";
$strasse2 = (isset($_POST['dlg_firma_strasse2_change'])) ? $_POST['dlg_firma_strasse2_change'] : "";
$plz = (isset($_POST['dlg_firma_plz_change'])) ? $_POST['dlg_firma_plz_change'] : "";
$ort = (isset($_POST['dlg_firma_ort_change'])) ? $_POST['dlg_firma_ort_change'] : "";
$land = (isset($_POST['dlg_firma_land_change'])) ? $_POST['dlg_firma_land_change'] : "";
$fon = (isset($_POST['dlg_firma_fon_change'])) ? $_POST['dlg_firma_fon_change'] : "";
$fax = (isset($_POST['dlg_firma_fax_change'])) ? $_POST['dlg_firma_fax_change'] : "";
$email = (isset($_POST['dlg_firma_email_change'])) ? $_POST['dlg_firma_email_change'] : "";
$www = (isset($_POST['dlg_firma_www_change'])) ? $_POST['dlg_firma_www_change'] : "";
$type = (isset($_POST['dlg_firma_type_change'])) ? $_POST['dlg_firma_type_change'] : "";
$portal = "";

$data = array(
	'_kf__Firmen_id'		=> $firma_id,
	'Firmenname'	=> $firma,
	'Strasse_1'		=> $strasse1,
	'Strasse_2'		=> $strasse2,
	'PLZ'			=> $plz,
	'Ort'			=> $ort,
	'_kf__Land'	=> $land,
	'Telefon'			=> $fon,
	'Fax'			=> $fax,
	'Mail'			=> $email,
	'Website'   	=> $www
);

require_once (__DIR__.'/../includes/db.inc.php');

$newRequest =& $fm->newAddCommand('cgi_Firmenaenderung', $data);
$result = $newRequest->execute();

//Auf Fehler prüfen
if (FileMaker::isError($result)) {
	
	echo "<p>Fehler: " . $result->getMessage() . "<p>";
	exit;
}
$records = $result->getRecords();

$span = "";
foreach ($records as $record) {
	$portal .= '<div id="geschaeftsadresse">'."\\n";
	$portal .= '<strong>'.$record->getField('Firma').'</strong>'."<br><br>\\n";
	$portal .= '<div style="width:240px; float:left;">'."\\n";
	$portal .= '<div style="width:100px; float:left;">'.$_SESSION['Leg_188'].':</div><div style="width:130px; float:left;">'.$record->getField('Abteilung')."</div><br clear=\"all\" />\\n";
	$portal .= '<div style="width:100px; float:left;">'.$_SESSION['Leg_189'].':</div><div style="width:130px; float:left;">'.$record->getField('Funktion')."</div><br clear=\"all\" /><br clear=\"all\" />\\n";
	$checkbox_output = ($record->getField('NewsletterEmpfaenger') == "1")? $_SESSION['Leg_82'] : "";
	$portal .= '<div style="width:100px; float:left;">Newsletter:</div><div style="width:130px; float:left;">'.$checkbox_output."</div><br clear=\"all\" /><br clear=\"all\" />\\n";
	//$portal .= '<div style="width:220px; float:left;">'.$_SESSION['Leg_178'].':</div><div style="width:130px; float:left;">'.$relatedRow->getField('zz_Angestellte 2::NewsletterEmpfaenger')."</div><br clear=\"all\" />\n";
	$portal .= '</div><div style="width:240px; float:left;">'."\\n";
	$portal .= '<div style="width:100px; float:left;">'.$_SESSION['Leg_190'].':</div><div style="width:130px; float:left;">'.$record->getField('Direktwahl_Tel')."</div><br clear=\"all\" />\\n";
	$portal .= '<div style="width:100px; float:left;">'.$_SESSION['Leg_191'].':</div><div style="width:130px; float:left;">'.$record->getField('Direktwahl_Fax')."</div><br clear=\"all\" />\\n";
	$portal .= '<div style="width:100px; float:left;">'.$_SESSION['Leg_192'].':</div><div style="width:130px; float:left;">'.$record->getField('Firmenhandy')."</div><br clear=\"all\" />\\n";
	$portal .= '<div style="width:100px; float:left;">'.$_SESSION['Leg_44'].':</div><div style="width:130px; float:left;">'.$record->getField('Mail')."</div><br clear=\"all\" />\\n";
	$portal .= '</div><br clear="all" />'."\\n";
	$portal .= 	'<p><img src="/images/edit.png" align="absmiddle" border="0" onClick="javascript:open_dialog4(\\\''.$record->getField('__kp__id').'\\\');" title="'.$_SESSION['Leg_198'].'"> '.$_SESSION['Leg_198'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="/images/delete.png" align="absmiddle" border="0" onClick="javascript:deleteGeschaeftsadresse(\\\''.$record->getField('__kp__id').'\\\');" title="'.$_SESSION['Leg_182'].'"> '.$_SESSION['Leg_182'].'</p>';
	$portal .= '</div>';

}

$span = " document.getElementById('geschaeftsadressen').innerHTML += '".$portal."';";
die($span);

?>