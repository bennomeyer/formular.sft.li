<?
session_start();
// Bust cache in the head
header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
// always modified
header ("Cache-Control: no-cache, must-revalidate");  // HTTP/1.1
header ("Pragma: no-cache");                          // HTTP/1.0


$v = (isset($_REQUEST['v']))? $_REQUEST['v'] : "";
$n = (isset($_REQUEST['n']))? $_REQUEST['n'] : "";
$g = (isset($_REQUEST['g']))? $_REQUEST['g'] : "";

require_once (__DIR__.'/../includes/db.inc.php');

$rec = $fm->getRecordById('cgi_Adressaenderung_Personen', $_SESSION['record_id']); 
$rec->setField('Namensaenderungsantrag_Vorname', $v); 
$rec->setField('Namensaenderungsantrag', $n); 
$rec->setField('_kf__Namensaenderungsgrund', $g); 
$result = $rec->commit(); 
if (FileMaker::isError($result)) {
	echo 'false';
} else {
	echo 'OK';
}
?>