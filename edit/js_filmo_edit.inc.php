// ---------------------
// Edit Filmo
// ---------------------
function filmo_aendern_submit(){
	var oForm = document.getElementById('filmo_aendern');
	YAHOO.util.Connect.setForm(oForm);
	if (document.getElementById('filmo_edit_coregie').checked == true) {
		coregieflag = 1;
	} else {
		coregieflag = '';
	}
	YAHOO.util.Connect.asyncRequest('POST', 'ajax_edit_change_filmo.php?id='+document.getElementById('filmo_edit_rid').value+'&filmo_edit_titel='+document.getElementById('filmo_edit_titel').value+'&filmo_edit_jahr='+document.getElementById('filmo_edit_jahr').value+'&filmo_edit_genre='+document.getElementById('filmo_edit_genre').value+'&filmo_edit_coregie='+coregieflag, { success: filmo_geaendert, failure: failure_ajax });
}

var filmo_geaendert = function(obj){
	FilmoChange.hide();
	try {
		eval(obj.responseText);
	}
	catch(err){
		alert("Hinweis<br>Es gab ein Problem bei der Kommunikation mit dem Server, bitte versuchen Sie es zu einem sp&auml;teren Zeitpunkt erneut.");
	}
};

FilmoChange = new YAHOO.widget.Dialog("div_filmo_aendern", 
	{	modal:true, 
		visible:false, 
		iframe:true, 
		width:"470px", 
		fixedcenter : true,
		constraintoviewport:true, 
		draggable:true,
		buttons : [ { text:"<?=$_SESSION['Leg_114']?>", handler:filmo_aendern_submit } ]
	});

// START safari-fix of close on return
FilmoChange.hideOnSubmit = false;	                
var listeners = [new YAHOO.util.KeyListener(document, 
   {keys : 27}, {fn:handleCancel, scope:Dialog4, correctScope:true})];
FilmoChange.cfg.queueProperty("keylisteners", listeners);
// END safari-fix of close on return

FilmoChange.cfg.setProperty('postmethod','async')
FilmoChange.render();
YAHOO.util.Event.addListener("change_filmo", "click", FilmoChange.show, FilmoChange, true);
//YAHOO.util.Event.addListener("firma_change", "click", changeFilmoDlg.show, changeFilmoDlg, true);