// ---------------------
	// Edit Gesch�ftsadresse1
	// ---------------------
	
	
	function angestellter_aendern_submit(){
		var oForm = document.getElementById('edit_angestellter');
		YAHOO.util.Connect.setForm(oForm);
		if (document.getElementById('dialog4_nl_checkbox').checked == true) {
			nl_status = "1";
		} else {
			nl_status = "";	
		}
		YAHOO.util.Connect.asyncRequest('POST', 'ajax_submit_edit_angestellter.php?id='+document.getElementById('dialog4_id').value+'&abteilung='+document.getElementById('dialog4_abteilung').value+'&funktion='+document.getElementById('dialog4_funktion').value+'&tel='+document.getElementById('dialog4_tel').value+'&fax='+document.getElementById('dialog4_fax').value+'&mobil='+document.getElementById('dialog4_mobil').value+'&mail='+document.getElementById('dialog4_mail').value+'&newsletter='+document.getElementById('dialog4_newsletter').value+'&nl_checkbox='+nl_status, { success: angestellter_geaendert, failure: failure_ajax });
	}

	var angestellter_geaendert = function(obj){
		Dialog4.hide();
		try {
			eval(obj.responseText);
		}
		catch(err){
			alert("Hinweis<br>Es gab ein Problem bei der Kommunikation mit dem Server, bitte versuchen Sie es zu einem sp&auml;teren Zeitpunkt erneut.");
		}
	};
	
	Dialog4 = new YAHOO.widget.Dialog("div_angestellter_aendern1", 
		{	modal:true, 
			visible:false, 
			iframe:true, 
			width:"470px", 
			fixedcenter : true,
			constraintoviewport:true, 
			draggable:true,
			buttons : [ { text:"<?=$_SESSION['Leg_114']?>", handler:angestellter_aendern_submit } ]
		});
	
	// START safari-fix of close on return
	Dialog4.hideOnSubmit = false;	                
	var listeners = [new YAHOO.util.KeyListener(document, 
       {keys : 27}, {fn:handleCancel, scope:Dialog4, correctScope:true})];
    Dialog4.cfg.queueProperty("keylisteners", listeners);
	// END safari-fix of close on return
	
	Dialog4.cfg.setProperty('postmethod','async')
	Dialog4.render();
	YAHOO.util.Event.addListener("change_angestellter", "click", Dialog4.show, Dialog4, true);
	YAHOO.util.Event.addListener("firma_change", "click", changeFirmaDlg.show, changeFirmaDlg, true);