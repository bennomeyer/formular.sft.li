<?php
session_start();
// Bust cache in the head
header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
// always modified
header ("Cache-Control: no-cache, must-revalidate");  // HTTP/1.1
header ("Pragma: no-cache");                          // HTTP/1.0
header('Content-Type: text/html; charset=utf-8');

$firma_id= (isset($_REQUEST['firma_id']))? $_REQUEST['firma_id'] : "";
$portal = "";

$data = array(
	'_kf__Firma_Id'         => $firma_id,
	'_kf__Person_Id'        => $_SESSION['personen_id']
);

require_once (__DIR__.'/../includes/db.inc.php');

$newRequest =& $fm->newAddCommand('cgi_k_Angestellte', $data);
$result = $newRequest->execute();

//Auf Fehler prüfen
if (FileMaker::isError($result)) {
	
	echo "<p>Fehler: " . $result->getMessage() . "<p>";
	exit;
}
$records = $result->getRecords();

$span = "";
foreach ($records as $record) {
	$portal .= '<div id="geschaeftsadresse">'."\\n";
	$portal .= '<strong>'.$record->getField('Firma').'</strong>'."<br><br>\\n";
	$portal .= '<div style="width:240px; float:left;">'."\\n";
	$portal .= '<div style="width:100px; float:left;">'.$_SESSION['Leg_188'].':</div><div style="width:130px; float:left;">'.$record->getField('Abteilung')."</div><br clear=\"all\" />\\n";
	$portal .= '<div style="width:100px; float:left;">'.$_SESSION['Leg_189'].':</div><div style="width:130px; float:left;">'.$record->getField('Funktion')."</div><br clear=\"all\" /><br clear=\"all\" />\\n";
	$checkbox_output = ($record->getField('NewsletterEmpfaenger') == "1")? $_SESSION['Leg_82'] : "";
	$portal .= '<div style="width:100px; float:left;">Newsletter:</div><div style="width:130px; float:left;">'.$checkbox_output."</div><br clear=\"all\" /><br clear=\"all\" />\\n";
	//$portal .= '<div style="width:220px; float:left;">'.$_SESSION['Leg_178'].':</div><div style="width:130px; float:left;">'.$relatedRow->getField('zz_Angestellte 2::NewsletterEmpfaenger')."</div><br clear=\"all\" />\n";
	$portal .= '</div><div style="width:240px; float:left;">'."\\n";
	$portal .= '<div style="width:100px; float:left;">'.$_SESSION['Leg_190'].':</div><div style="width:130px; float:left;">'.$record->getField('Direktwahl_Tel')."</div><br clear=\"all\" />\\n";
	$portal .= '<div style="width:100px; float:left;">'.$_SESSION['Leg_191'].':</div><div style="width:130px; float:left;">'.$record->getField('Direktwahl_Fax')."</div><br clear=\"all\" />\\n";
	$portal .= '<div style="width:100px; float:left;">'.$_SESSION['Leg_192'].':</div><div style="width:130px; float:left;">'.$record->getField('Firmenhandy')."</div><br clear=\"all\" />\\n";
	$portal .= '<div style="width:100px; float:left;">'.$_SESSION['Leg_44'].':</div><div style="width:130px; float:left;">'.$record->getField('Mail')."</div><br clear=\"all\" />\\n";
	$portal .= '</div><br clear="all" />'."\\n";
	$portal .= 	'<p><img src="/images/edit.png" align="absmiddle" border="0" onClick="javascript:open_dialog4(\\\''.$record->getField('__kp__id').'\\\');" title="'.$_SESSION['Leg_198'].'"> '.$_SESSION['Leg_198'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="/images/delete.png" align="absmiddle" border="0" onClick="javascript:deleteGeschaeftsadresse(\\\''.$record->getField('__kp__id').'\\\');" title="'.$_SESSION['Leg_182'].'"> '.$_SESSION['Leg_182'].'</p>';
	$portal .= '</div>';

}

$span = " document.getElementById('geschaeftsadressen').innerHTML += '".$portal."';";
$span = "open_dialog4('".$record->getField('__kp__id')."');";
die($span);

?>