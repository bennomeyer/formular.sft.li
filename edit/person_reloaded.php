<?
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
/**
 * History:
 * 2015-10-07: Erweiterung um Mitarbeiter-Zeiten (#6155)
 * 2015-10-28: Erweiterung um Bemerkungen (#6264)
 * 2015-12-03: Erweiterung um Moderdatoren (#6358)
 * 2016-10-19: Erweiterung um Funktion, Notizen und Handy (#7886)
 * 2016-11-07: Berufsfeld, neue Labels (#8026)
 * 2016-11-30: Layout, fixed values (#8168)
 */

session_start();
if (isset($_GET['q'])) {
	$q = (isset($_REQUEST['q'])) ? $_REQUEST['q'] : "";
	$_SESSION['q'] = $q;
}
if ((!isset($_SESSION['q'])) || ($_REQUEST['q'] == "") || (strstr($_REQUEST['q'],"*")) || (strlen($_REQUEST['q']) < 4) ) {
	echo 'ERROR: Wrong parameter';
	exit();
}
function ifset($name,$default="") {
	return isset($_POST[$name])? $_POST[$name] : $default;
}

function Label($id) {
	return (isset($_SESSION['Leg_'.$id])?$_SESSION['Leg_'.$id]:'');
}

// Neu: explizite Arbeitsbereiche teilweise mit Verbands-Angabe
// #7886: optional Notiz
$arbeitsbereiche = array(
	array('name'=>'Medien',        'verband'=>true,  'notiz'=>false, 'label'=>'301'),
	array('name'=>'Regie',         'verband'=>true,  'notiz'=>false, 'label'=>'302'),
	array('name'=>'Drehbuch',      'verband'=>true,  'notiz'=>true, 'label'=>'350', 'notiz_label'=>true),
	array('name'=>'Cast',          'verband'=>true,  'notiz'=>true, 'label'=>'303', 'notiz_label'=>true),
	array('name'=>'Crew',          'verband'=>true,  'notiz'=>true,  'label'=>'304', 'notiz_label'=>true),
	array('name'=>'Produktion',    'verband'=>true,  'notiz'=>false, 'label'=>'305'),
	//array('name'=>'Postproduktion','verband'=>true,  'notiz'=>false, 'label'=>'306'),
	array('name'=>'Verleih',       'verband'=>true,  'notiz'=>false, 'label'=>'307'),
	array('name'=>'Kino',          'verband'=>true,  'notiz'=>false, 'label'=>'309'),
	array('name'=>'Festival',      'verband'=>false, 'notiz'=>false, 'label'=>'308'),
	array('name'=>'Filmschule',    'verband'=>false, 'notiz'=>true, 'label'=>'310'),
	array('name'=>'Institution',   'verband'=>false, 'notiz'=>true,  'label'=>'311', 'notiz_label'=>false)
);
$akk_cat = array();
$akk_verband = array();
$akk_notiz = array();

// #6155: Mitarbeiter-Felder
$ma_field_names = array('Geburtsdatum','Mitarbeiter_AHV_Nr','Mitarbeiter_BerufKenntnisse',
'n__Mitarbeiter::Zeiten_Tag0',
'n__Mitarbeiter::Zeiten_Tag1',
'n__Mitarbeiter::Zeiten_Tag2',
'n__Mitarbeiter::Zeiten_Tag3',
'n__Mitarbeiter::Zeiten_Tag4',
'n__Mitarbeiter::Zeiten_Tag5',
'n__Mitarbeiter::Zeiten_Tag6',
'n__Mitarbeiter::Zeiten_Tag7',
'n__Mitarbeiter::Zeiten_Tag8',
'n__Mitarbeiter::Einsatzbereiche',
'n__Mitarbeiter::Locations',
'n__Mitarbeiter::Bemerkungen',
'Mitarbeiter_Kein_Interesse_Mehr',
);
$ma_simple_fields = array('Geburtsdatum','Mitarbeiter_AHV_Nr','Mitarbeiter_BerufKenntnisse','n__Mitarbeiter::Bemerkungen','Mitarbeiter_Kein_Interesse_Mehr');
// make friendly names
$ma_fields = array();
foreach ($ma_field_names as $field) {
	$ma_fields[$field] = strtolower(strtr($field,": ",'__'));
}

// #6358: Moderatoren-Felder
$mo_field_names = array(
'n__Moderatoren::Zeiten_Tag1',
'n__Moderatoren::Zeiten_Tag2',
'n__Moderatoren::Zeiten_Tag3',
'n__Moderatoren::Zeiten_Tag4',
'n__Moderatoren::Zeiten_Tag5',
'n__Moderatoren::Zeiten_Tag6',
'n__Moderatoren::Zeiten_Tag7',
'n__Moderatoren::Zeiten_Tag8',
// 'n__Moderatoren::Bemerkungen',
);
$mo_simple_fields = array('n__Moderatoren::Bemerkungen');
// make friendly names
$mo_fields = array();
foreach ($mo_field_names as $field) {
	$mo_fields[$field] = strtolower(strtr($field,": ",'__'));
}

// Parameter übernehmen
$version = (isset($_REQUEST['version'])) ? $_REQUEST['version'] : "";
$step = ifset('step');
$sprache = ifset('sprache');
$strasse_1 = ifset('strasse_1');
$strasse_2 = ifset('strasse_2');
$plz = ifset('plz');
$ort = ifset('ort');
$land_1 = ifset('land_1');
$tel_p = ifset('tel_p');
$fax = ifset('fax');
$tel_m = ifset('tel_m');
$mail_1 = ifset('mail_1');
$web_1 = ifset('web_1');
$newsletter = ifset('newsletter');
$biographie_formular_zusatz = ifset('biographie_formular_zusatz');
$geschlecht = ifset('geschlecht');
$bio_bearbeitet = ifset('bio_bearbeitet');
$akk_ok = ifset('akk_ok');
$akk_beruf = ifset('akk_beruf');
$akk_cat_else = ifset('akk_cat_else');
$akk_arr = ifset('akk_arr');
$akk_dep = ifset('akk_dep');
$akk_cat_else_flag = ifset('akk_cat_else_flag','0');
$akk_conmail = ifset('akk_conmail');
$akk_conhandy = ifset('akk_conhandy');
// die neuen Arbeitsbereichs-Parameter
foreach ($arbeitsbereiche as $bereich) {
	$cat_name = 'akk_'.strtolower($bereich['name']);
	$verb_name = $cat_name.'_verband';
	$akk_cat[$bereich['name']] = ifset($cat_name,'0'); // nicht angeklickt wird nicht übergeben, bedeutet false (0)
	if ($bereich['verband']) {
		$akk_verband[$bereich['name']] = ifset($verb_name);
	}
	if ($bereich['notiz']) {
		$notiz_name = $cat_name.'_notiz';
		$akk_notiz[$bereich['name']] = ifset($notiz_name);
	}
}
// Mitarbeiter-Parameter
foreach ($ma_fields as $field => $param) {
	if (in_array($field, $ma_simple_fields)) {
		$$param = ifset($param);
	} else {
		// "Array"
		$$param = implode("\n", array_filter(ifset($param,array())));
	}
}
// Moderatoren-Parameter
foreach ($mo_fields as $field => $param) {
	if (in_array($field, $mo_simple_fields)) {
		$$param = ifset($param);
	} else {
		// "Array"
		$$param = implode("\n", array_filter(ifset($param,array())));
	}
}


$error = "";
$portal = "";
$genre = "";

require_once ('../includes/db.inc.php');

if ('mit' == $version || 'mod' == $version || 'ext' == $version) {
	// Prüfe Formularsperren
	$request = $fm->newFindAnyCommand('cgi_Formularsperrungen');
	$result = $request->execute();
	$records = $result->getRecords();
	$record = $records[0];
	if ('mit'==$version) {
		$locked = $record->getField('Formularsperrung_Mitarbeiter');
	} elseif ('ext' == $version) {
		$locked = $record->getField('Formularsperrung_FilmoBio');
	} else {
		$locked = $record->getField('Formularsperrung_Moderatoren');
	}
	if (!!$locked) {
		include 'not_available.html';
		die();
	}
}

// ------------------------
// Aktualisiere. Personendaten
// ------------------------
if ($step == "2") {
	$rec = $fm->getRecordById('cgi_Adressaenderung_Personen', $_SESSION['record_id']);
	$rec->setField('_kf__Korrespondenzsprache', $sprache);
	$rec->setField('Geschlecht', $geschlecht);
	// if ('mit' != $version) {
	$rec->setField('Strasse_1', $strasse_1);
	$rec->setField('Strasse_2', $strasse_2);
	$rec->setField('PLZ', $plz);
	$rec->setField('Ort', $ort);
	$rec->setField('_kf__Land_ISO_Code', $land_1);
	$rec->setField('Tel_P', $tel_p);
	$rec->setField('Fax', $fax);
	$rec->setField('Tel_Mob', $tel_m);
	$rec->setField('Mail_1', $mail_1);
	$rec->setField('Website', $web_1);
	$rec->setField('NewsletterEmpfaenger', $newsletter);
	$rec->setField('Biographie_Formular_Zusatz', $biographie_formular_zusatz);
	$rec->setField('Biographie_Bearbeitet_Flag', $bio_bearbeitet);
	// }
	if ('akk'==$version) {
		$rec->setField('Akkreditierung_Formular_Flag', $akk_ok);
		$rec->setField('Akkreditierung_Beruf_Funktion', $akk_beruf);
		$rec->setField('_kf__Akkreditierungsantragskategorien', implode("\n\r",$akk_cat));
		$rec->setField('Akkreditierung_Formular_AndereText', $akk_cat_else);
		$rec->setField('Akkreditierung_Formular_AndereFlag', $akk_cat_else_flag);
		$rec->setField('Akkreditierung_Formular_Anreise', $akk_arr);
		$rec->setField('Akkreditierung_Formular_Abreise', $akk_dep);
		$rec->setField('Akkreditierung_Formular_ConsentMail',$akk_conmail);
		$rec->setField('Akkreditierung_Formular_ConsentHandy', $akk_conhandy);

		// Akkreditierungs-Felder
		foreach ($arbeitsbereiche as $bereich) {
			$bname = $bereich['name'];
			$dbf_name = 'Akkreditierung_Formular_'.$bname;
			$dbf_verb_name = $dbf_name.'_Verband';
			$dbf_notiz_name = $dbf_name.'_Notiz';
			$rec->setField($dbf_name,$akk_cat[$bname]);
			if ($bereich['verband']) {
				$rec->setField($dbf_verb_name,$akk_verband[$bname]);
			}
			if ($bereich['notiz']) {
				$rec->setField($dbf_notiz_name, $akk_notiz[$bname]);
			}		
		}
	}
	// Mitarbeiter-Values (#6155)
	if ('mit'==$version) {
		foreach ($ma_fields as $field => $param) {
			$rec->setField($field, $$param);
		}
	}

	// Moderatoren-Values (#6358)
	if ('mod'==$version) {
		foreach ($mo_fields as $field => $param) {
			$rec->setField($field, $$param);
		}
	}

	$result = $rec->commit();
	if (FileMaker::isError($result)) {
		echo 'false';
		die('Database error. Please contact Webmaster');
	}


	if (!empty($_FILES["userbild"]['name'])) {
		$ext_tmp = explode(".",$_FILES['userbild']['name']);
		$ext = $ext_tmp[count($ext_tmp)-1];
		$destination = $_SERVER['DOCUMENT_ROOT']."/bilder/people/original/".$_SESSION['personen_id'].'.'.$ext;
		// Bild kopieren
		if ($_FILES['userbild']['size'] < 10000000) {

			if(move_uploaded_file($_FILES["userbild"]["tmp_name"], $destination)) {
				$bild1 = true;
				$command = "/usr/local/bin/convert ".$destination." -thumbnail '100x100>' ".$_SERVER['DOCUMENT_ROOT']."/bilder/people/small/".$_SESSION['personen_id'].".jpg";
				exec($command);
				$command = "/usr/local/bin/convert ".$destination." -thumbnail '300x300>' ".$_SERVER['DOCUMENT_ROOT']."/bilder/people/medium/".$_SESSION['personen_id'].".jpg";
				exec($command);
				$command = "/usr/local/bin/convert ".$destination." -thumbnail '600x600>' ".$_SERVER['DOCUMENT_ROOT']."/bilder/people/large/".$_SESSION['personen_id'].".jpg";
				exec($command);
				if ('jpg' != strtolower($ext)) {
					# nur JPG speichern
					$command = "/usr/local/bin/convert ".$destination." ".$_SERVER['DOCUMENT_ROOT']."/bilder/people/original/".$id.".jpg";
					exec($command);
					unlink($destination);
				}

			} else {
				$error.= "Bildupload schlug fehl.<br>";
			}
				
		} else {
			$error.= "Bildupload Dateigroessee groesser als 10 MB.<br>";
		}
	}
}


// ------------------------
// Indiv. Personendaten
// ------------------------

$find =& $fm->newFindCommand('cgi_Adressaenderung_Personen');
$find->addFindCriterion('web_ID', "=\"".$_SESSION['q']."\"");
$result = $find->execute();
if (FileMaker::isError($result)) {
	echo 'Leider ist ein Fehler aufgetreten. Bitte kontaktieren Sie den WebMaster.';
	echo '<!-- ';
	// var_dump($result);
	echo '-->';
	exit;
}
$records = $result->getRecords();
$foundrec = $result->getFoundSetCount();
$record = $records[0];
$_SESSION['record_id'] = $record->getField('_record__id');
$personen_id = $record->getField('_kp__id');
$_SESSION['personen_id'] = $personen_id;
$sprache = $record->getField('_kf__Korrespondenzsprache');
$_SESSION['sprache'] = $sprache;
$vorname = $record->getField('Vorname');
$name = $record->getField('Name');
$strasse_1 = $record->getField('Strasse_1');
$strasse_2 = $record->getField('Strasse_2');
$plz = $record->getField('PLZ');
$ort = $record->getField('Ort');
$land_1 = $record->getField('_kf__Land_ISO_Code');
$tel_p = $record->getField('Tel_P');
$fax = $record->getField('Fax');
$tel_m = $record->getField('Tel_Mob');
$mail_1 = $record->getField('Mail_1');
$web_1 = $record->getField('Website');
$newsletter = $record->getField('NewsletterEmpfaenger');
$biographie_formular = $record->getField('Biographie_Formular');
$biographie_katalog_vorhanden = $record->getField('Biographie_Katalog_Vorhanden');
$biographie_katalog = $record->getField('Biographie_Katalog');
$biographie_formular_zusatz = $record->getField('Biographie_Formular_Zusatz');
$bio_bearbeitet = $record->getField('Biographie_Bearbeitet_Flag');
$geschlecht = $record->getField('Geschlecht');
$adressblock = $record->getField('h_44__firmen 2::zz_Synthese_Adresszeile_mit_Zeilenumbruch');
$akk_ok = $record->getField('Akkreditierung_Formular_Flag');
$akk_beruf = $record->getField('Akkreditierung_Beruf_Funktion');
$akk_cat_else = $record->getField('Akkreditierung_Formular_AndereText');
$akk_cat_else_flag = $record->getField('Akkreditierung_Formular_AndereFlag');
$akk_arr_temp = explode("/",$record->getField('Akkreditierung_Formular_Anreise'));
if (count($akk_arr_temp) == 3) $akk_arr = $akk_arr_temp[1].'/'.$akk_arr_temp[0].'/'.$akk_arr_temp[2];
$akk_dep_temp =  explode("/",$record->getField('Akkreditierung_Formular_Abreise'));
if (count($akk_dep_temp) == 3) $akk_dep = $akk_dep_temp[1].'/'.$akk_dep_temp[0].'/'.$akk_dep_temp[2];
// Akkreditierung
foreach ($arbeitsbereiche as $bereich) {
	$bname = $bereich['name'];
	$dbf_name = 'Akkreditierung_Formular_'.$bname;
	$dbf_verb_name = $dbf_name.'_Verband';
	$dbf_notiz_name = $dbf_name.'_Notiz';
	$akk_cat[$bname] = $record->getField($dbf_name);
	if ($bereich['verband']) {
		$akk_verband[$bname] = $record->getField($dbf_verb_name);
	}
	if ($bereich['notiz']) {
		$akk_notiz[$bname] = $record->getField($dbf_notiz_name);
	}
}
$akk_conmail = $record->getField('Akkreditierung_Formular_ConsentMail');
$akk_conhandy = $record->getField('Akkreditierung_Formular_ConsentHandy');

// Mitarbeiter #6155
foreach ($ma_fields as $field => $param) {
	$$param = $record->getField($field);
	if (!in_array($field, $ma_simple_fields)) {
		// explode to array
		$my_array= explode("\n", $$param);
		$$param = $my_array;
	}
}
// Moderatoren #61358
foreach ($mo_fields as $field => $param) {
	$$param = $record->getField($field);
	if (!in_array($field, $mo_simple_fields)) {
		// explode to array
		$my_array= explode("\n", $$param);
		$$param = $my_array;
	}
}
// 6155: Wertelisten für Zeiten, Einsatzbereiche und Locations
$layout = $fm->getLayout('cgi_Adressaenderung_Personen');
$zeiten= $layout->getValueListTwoFields('Mitarbeiter_Uhrzeiten');
$mod_zeiten= $layout->getValueListTwoFields('Moderatoren_Tageszeiten'); // #6358
$einsatz= $layout->getValueListTwoFields('Mitarbeiter_Einsatzbereiche');
$locations= $layout->getValueListTwoFields('Mitarbeiter_Locations');

/**
 *
 * Hilfsfunktionen zum Rendern der Mitarbeiter Zeiten (als checkboxes in td)
 * @param String $param NUMMER des parameters (also z.B. 1 für n__mitarbeiter__zeiten_tag1)
 * @return String mit Liste von td
 */
function render_mit_zeiten($param) {
	global $zeiten;
	return render_td_zeiten("n__mitarbeiter__zeiten_tag$param", $zeiten);
}
/**
 *
 * Hilfsfunktionen zum Rendern der Moderatoren-Zeiten (als checkboxes in td)
 * @param String $param NUMMER des parameters (also z.B. 1 für  n__moderatoren__zeiten_tag1)
 * @return String mit Liste von td
 */
function render_mod_zeiten($param) {
	global $mod_zeiten;
	return render_td_zeiten("n__moderatoren__zeiten_tag$param", $mod_zeiten);
}
/**
 *
 * Hilfsfunktionen zum Rendern der Zeiten (als checkboxes in td)
 * @param String $param NAME des parameters (also z.B. n__moderatoren__zeiten_tag1)
 * @return String mit Liste von td
 */
function render_td_zeiten($param,$list) {
	global $$param;
	$output ="";
	foreach ($list as $label => $value) {
		$checked = checked_attr(in_array($value, $$param));
		$output .= "<td><input type='checkbox' name='${param}[]' value='$value' $checked /></td>";
}
return $output;
}
//
/**
 *
 * Hilfsfunktion zum Rendern der anderen Checkbox-Listen
 * @param String $param NAME des parameters (also z.B. n__mitarbeiter__zeiten_tag1)
 * @param Array $list Werteliste
 * @return String mit Labels und Checkboxen
 */
function render_fm_value_list($param,$list){
	global $$param;
	$output="";
	foreach ($list as $label => $value) {
		$checked = checked_attr(in_array($value, $$param));
		$output .= "<label><input type='checkbox' name='${param}[]' value='$value' $checked /> $label</label>";
}
return $output;
}

/**
 * Rendern eines Text-Inputs mit Label
 * @param String $param NAME des parameters
 * @param $leg NR der Legende
 */
function render_text_input($param,$leg) {
	global $$param;
	$legi = Label($leg);
	$value = $$param;
	return "<label for='$param'>$legi</label>
		<div class='div_texbox'><input name='$param' type='text' class='textbox' id='$param' value='$value' /></div>
		<br clear='all' />
	";
}

function checked_attr($chk){
	return $chk ? 'checked="checked"' : '';
}


if ($sprache == "") $sprache = "en";

// ------------------------
// Hole Feldbeschriftungen
// ------------------------
read_labels($sprache);

$relatedSet = $record->getRelatedSet('zz_Angestellte 2');
//print_r($relatedSet);
$portal .= '<div id="geschaeftsadresse">'."\n";

$related_id = "";

if (!FileMaker::isError($relatedSet)) {
	foreach ($relatedSet as $relatedRow) {
		$related_id = $relatedRow->getField('zz_Angestellte 2::__kp__id');
		$portal .= '<span class="address">'.$relatedRow->getField('h_44__firmen 2::zz_Synthese_Adresszeile').'</span>'."<br><br>\n";

		$portal .= '<div style="width:240px; float:left;">'."\n";
		$portal .= '<div style="width:100px; float:left;">'.$_SESSION['Leg_188'].':</div><div style="width:130px; float:left;">'.$relatedRow->getField('zz_Angestellte 2::Abteilung')."</div><br clear=\"all\" />\n";
		$portal .= '<div style="width:100px; float:left;">'.$_SESSION['Leg_189'].':</div><div style="width:130px; float:left;">'.$relatedRow->getField('zz_Angestellte 2::Funktion')."</div><br clear=\"all\" /><br clear=\"all\" />\n";
		$checkbox_output = ($relatedRow->getField('zz_Angestellte 2::NewsletterEmpfaenger') == "1")? $_SESSION['Leg_82'] : "";
		$portal .= '<div style="width:100px; float:left;">Newsletter:</div><div style="width:130px; float:left;">'.$checkbox_output."</div><br clear=\"all\" /><br clear=\"all\" />\n";
		//$portal .= '<div style="width:220px; float:left;">'.$_SESSION['Leg_178'].':</div><div style="width:130px; float:left;">'.$relatedRow->getField('zz_Angestellte 2::NewsletterEmpfaenger')."</div><br clear=\"all\" />\n";
		$portal .= '</div><div style="width:240px; float:left;">'."\n";
		$portal .= '<div style="width:100px; float:left;">'.$_SESSION['Leg_190'].':</div><div style="width:130px; float:left;">'.$relatedRow->getField('zz_Angestellte 2::Direktwahl_Tel')."</div><br clear=\"all\" />\n";
		$portal .= '<div style="width:100px; float:left;">'.$_SESSION['Leg_191'].':</div><div style="width:130px; float:left;">'.$relatedRow->getField('zz_Angestellte 2::Direktwahl_Fax')."</div><br clear=\"all\" />\n";
		$portal .= '<div style="width:100px; float:left;">'.$_SESSION['Leg_192'].':</div><div style="width:130px; float:left;">'.$relatedRow->getField('zz_Angestellte 2::Firmenhandy')."</div><br clear=\"all\" />\n";
		$portal .= '<div style="width:100px; float:left;">'.$_SESSION['Leg_44'].':</div><div style="width:130px; float:left;">'.$relatedRow->getField('zz_Angestellte 2::Mail')."</div><br clear=\"all\" />\n";
		$portal .= '</div><br style="clear: both" />'."\n";
		$portal .= 	'<p><img src="/images/edit.png" align="absmiddle" border="0" onClick="javascript:open_dialog4(\''.$relatedRow->getField('zz_Angestellte 2::__kp__id').'\');" title="'.$_SESSION['Leg_198'].'"> '.$_SESSION['Leg_198'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="/images/delete.png" align="absmiddle" border="0" onClick="javascript:deleteGeschaeftsadresse(\''.$relatedRow->getField('zz_Angestellte 2::__kp__id').'\');" title="'.$_SESSION['Leg_182'].'"> '.$_SESSION['Leg_182'].'</p>';
	}
}
$portal .= '</div>';
include($_SERVER['DOCUMENT_ROOT'].'/includes/x_get_value_list_country.inc.php');
include($_SERVER['DOCUMENT_ROOT'].'/includes/x_get_value_list_namensaenderung.inc.php');
include($_SERVER['DOCUMENT_ROOT'].'/includes/x_get_value_list_filmogenre.inc.php');

?>
<!DOCTYPE html>
<html>
<head>
<title>Online Registration</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="style.css" rel="stylesheet" type="text/css" title="KFT" />

<link rel="stylesheet" type="text/css"
	href="/includes/yui/build/container/assets/container.css?_yuiversion=2.3.1" />
<link rel="stylesheet" type="text/css"
	href="/includes/yui/build/fonts/fonts-min.css?_yuiversion=2.3.1" />
<link rel="stylesheet" type="text/css"
	href="/includes/yui/build/button/assets/skins/sam/button.css?_yuiversion=2.3.1" />
<link rel="stylesheet" type="text/css"
	href="/includes/yui/build/container/assets/skins/sam/container.css?_yuiversion=2.3.1" />
<link href="../css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript"
	src="/includes/yui/build/utilities/utilities.js?_yuiversion=2.3.1"></script>
<script type="text/javascript"
	src="/includes/yui/build/button/button-beta.js?_yuiversion=2.3.1"></script>
<script type="text/javascript"
	src="/includes/yui/build/yahoo-dom-event/yahoo-dom-event.js?_yuiversion=2.3.1"></script>
<script type="text/javascript"
	src="/includes/yui/build/animation/animation-min.js?_yuiversion=2.3.1"></script>
<script type="text/javascript"
	src="/includes/yui/build/dragdrop/dragdrop-min.js?_yuiversion=2.3.1"></script>
<script type="text/javascript"
	src="/includes/yui/build/connection/connection.js?_yuiversion=2.3.1"></script>
<script type="text/javascript"
	src="/includes/yui/build/container/container.js?_yuiversion=2.3.1"></script>
<script type="text/javascript" src="/includes/safari_hack.js"></script>
<script type="text/javascript"
	src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="../includes_js/datepicker.min.js"></script>

<script type="text/javascript" language="javascript" charset="utf-8">
<!--

miniDialog = new YAHOO.widget.SimpleDialog("mini_dlg", { visible:false, width: "25em", effect:[
	{effect:YAHOO.widget.ContainerEffect.SLIDE,duration:0.25}, {effect:YAHOO.widget.ContainerEffect.FADE,duration:0.25}
	], fixedcenter : true, modal:true, draggable:false });

miniDialog.setHeader("Attention!");
miniDialog.setBody("Do you really want to do this?");

miniDialog.cfg.queueProperty("icon",YAHOO.widget.SimpleDialog.ICON_INFO);
miniDialog.cfg.queueProperty("buttons", [ 
	{ text:"OK", handler:handlerHide, isDefault:true }
]);

miniDialog.render(document.body);

var handlerHide = function(){
	this.hide();
};
var handleCancel = function(){
	this.cancel();
};
var failure_ajax = function(obj){
alert("Hinweis<br>Es gab ein Problem bei der Kommunikation mit dem Server, bitte versuchen Sie es zu einem sp&auml;teren Zeitpunkt erneut.");
};


function open_dialog4(id) {
	document.getElementById('dialog4_id').value = id;
	YAHOO.util.Connect.asyncRequest('POST', 'ajax_fill_angestellter.php?id='+id, { success: fill_angestellter_final, failure: failure_ajax });
	Dialog4.show();
}

function open_dialog_change_firma(id) {
	document.getElementById('dlg_firma_firma_change_id').value = id;
	//YAHOO.util.Connect.asyncRequest('POST', 'ajax_fill_change_firma.php?id='+id, { success: fill_change_firma_final, failure: failure_ajax });
	Dialog4.hide();
	changeFirmaDlg.show();
}
var fill_angestellter_final = function(obj){
	try {
		eval(obj.responseText);
	}
	catch(err){
		alert("Hinweis<br>Es gab ein Problem bei der Kommunikation mit dem Server, bitte versuchen Sie es zu einem sp&auml;teren Zeitpunkt erneut.");
	}
};


function init() {
	<? include('js_neuer_name.inc.php'); ?>
	<? include('js_neue_firma_dialoge.inc.php'); ?>
	<? include('js_angestellter_edit.inc.php'); ?>	
	<? include('js_filmo_edit.inc.php'); ?>		
}

<? include('js_neue_firma_logik.inc.php'); ?>


function editFimographie(id){
	document.getElementById("filmo_edit_rid").value = id;
	YAHOO.util.Connect.asyncRequest('POST', 'ajax_fill_filmo.php?id='+id, { success: fill_filmo_final, failure: failure_ajax });
	FilmoChange.show();

}
var fill_filmo_final = function(obj){
	try {
		eval(obj.responseText);

	}
	catch(err){
		alert("Hinweis<br>Es gab ein Problem bei der Kommunikation mit dem Server, bitte versuchen Sie es zu einem sp&auml;teren Zeitpunkt erneut.");
	}		
	for (var i=0; i<document.getElementById("filmo_edit_genre").options.length; i++) 
		{
			if (document.getElementById("filmo_edit_genre").options[i].value == document.getElementById("filmo_edit_genre_hidden").value) 
			{
				document.getElementById("filmo_edit_genre").options[i].selected = true;		
			} else {
				document.getElementById("filmo_edit_genre").options[i].selected = false;	
			}
		}
};


var delFilmoSuccess = function (obj){
	miniDialog.hide();
	try{
		eval(obj.responseText);	
	}
	catch(err){
		alert("Hinweis<br>Es gab ein Problem bei der Kommunikation mit dem Server, bitte versuchen Sie es zu einem sp&auml;teren Zeitpunkt erneut.");
	}
};

function deleteFimographie(id){
	delId = id;
	
	miniDialog.setHeader("<?=$_SESSION['Leg_80']?>");
	miniDialog.setBody("");
	miniDialog.cfg.queueProperty("icon",YAHOO.widget.SimpleDialog.ICON_WARN);
	miniDialog.cfg.queueProperty("buttons", [ 
		{ text: "<?=$_SESSION['Leg_82']?>", handler:delFilmoConfirm },
		{ text: "<?=$_SESSION['Leg_83']?>", handler:handlerHide, isDefault:true  }
	]);
	miniDialog.render(document.body);
	miniDialog.show();
}

var delFilmoConfirm = function (obj){
	YAHOO.util.Connect.asyncRequest('GET', 'ajax_delete_filmo.php?id='+delId, { success: delFilmoSuccess, failure: failure_ajax });
};



var addFilmo = function (obj){
	var coregie = '';
	var error = '';
	if (document.getElementById('filmo_titel').value == "") {
		document.getElementById("filmo_titel_wrap").className = "rot";  
		error = '1';
	} else {
		document.getElementById("filmo_titel_wrap").className = "schwarz"; 
	}
	if (document.getElementById('filmo_jahr').value == "") {
		document.getElementById("filmo_jahr_wrap").className = "rot";  
		error = '1';
	} else {
		document.getElementById("filmo_jahr_wrap").className = "schwarz"; 
	}
	if (document.getElementById('filmo_genre').value == "") {
		document.getElementById("filmo_genre_wrap").className = "rot"; 
		error = '1'; 
	} else {
		document.getElementById("filmo_genre_wrap").className = "schwarz"; 
	}
	if (error == '') {
		document.getElementById("filmo_error").innerHTML = ""; 
		document.getElementById('add_filmo_button').innerHTML = "<img src=\"/images/load_ani.gif\" align=\"absmiddle\" border=\"0\">";
		if (document.getElementById('filmo_coregie').checked) coregie = 1;
		YAHOO.util.Connect.asyncRequest('GET', 'ajax_add_filmo.php?filmo_titel='+document.getElementById('filmo_titel').value+'&filmo_jahr='+document.getElementById('filmo_jahr').value+'&filmo_genre='+document.getElementById('filmo_genre').value+'&filmo_coregie='+coregie, { success: addFilmoSuccess, failure: failure_ajax });
	} else {
		document.getElementById("filmo_error").innerHTML = "<span class=\"rot\"><?=$_SESSION['Leg_300']?><br><br></span>"; 
	}
	
};
var addFilmoSuccess = function (obj){
	document.getElementById('filmo_titel').value = "";
	document.getElementById("filmo_titel_wrap").className = "schwarz";  
	document.getElementById('filmo_jahr').value = "";
	document.getElementById("filmo_jahr_wrap").className = "schwarz";  
	document.getElementById('filmo_genre').value = "";
	document.getElementById("filmo_genre_wrap").className = "schwarz"; 
	document.getElementById('add_filmo_button').innerHTML = "<img src=\"/images/add.png\" align=\"absmiddle\" border=\"0\" onClick=\"javascript:addFilmo();\" title=\"<?=$_SESSION['Leg_78']?>\">";
	document.getElementById('filmo_coregie').checked = false;
	try{
		eval(obj.responseText);	
	}
	catch(err){
		alert("Hinweis<br>Es gab ein Problem bei der Kommunikation mit dem Server, bitte versuchen Sie es zu einem sp&auml;teren Zeitpunkt erneut.");
	}
};

YAHOO.util.Event.onDOMReady(init);

// 6155: Switch times by "immer"
$(function(){
	$('input:checkbox[value="immer"]').click(function(){
		var that = $(this);
		var chk = that.prop('checked');
		// falls checked, alle hinteren auch checken
		that.closest('tr').find('input[value!="immer"]').prop("checked",chk);
		});	
	$('td input:checkbox[value!="immer"]').change(function(){
		// die normalen Zeiten: immer löschen, wenn nicht gesetzt
		var that = $(this);
		var chk = that.prop('checked');
		// falls checked, alle hinteren auch checken
		if (!chk){
			that.closest('tr').find('input:checkbox[value="immer"]').prop("checked",false);
			}
		});	
	// #7886 DatePicker
	$('#akk_arr').Zebra_DatePicker({
		format: 'd/m/Y',
		pair: $('#akk_dep')
		});
	$('#akk_dep').Zebra_DatePicker({
		format: 'd/m/Y',
		direction: true
		});	
});
//-->

</script>
<style type="text/css">
</style>
</head>
<body class=" yui-skin-sam">

	<div id="container">
		<div id="top">
		<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
		<? if ($error != "") { ?>
			<p
				style="border: 1px solid #990000; background-color: #FFDCD6; padding: 5px; width: 538px">
				<?=$error; ?>
			</p>
			<? } ?>
			<? if (($error == "") && ($step == "2")) { ?>
			<p
				style="border: 1px solid #006600; background-color: #D8FFE4; padding: 5px; width: 538px">
				<?=$_SESSION['Leg_295']?>
			</p>
			&nbsp;<br />
			<? } ?>
		</div>
		<br clear="all" />
		<fieldset>
			<legend>
			<?
			switch ($version) {
				case 'akk':	echo  $_SESSION['Leg_294'] ; break;
				case 'mit':	echo  $_SESSION['Leg_329'] ; break;
				case 'mod': echo  $_SESSION['Leg_348'] ; break;
				default: echo $_SESSION['Leg_162'];	break;
			}
			?>
			</legend>
			<p>
			<? switch ($version) {
				case 'akk':	echo  $_SESSION['Leg_293'] ; break;
				case 'mit':	echo  $_SESSION['Leg_342'] ; break;
				case 'mod':	echo  $_SESSION['Leg_342'] ; break;
				default: echo $_SESSION['Leg_163'];	break;
			}
			?>
			</p>
			<br clear="all" />
			<form action="<?=$_SERVER['PHP_SELF']?>" method="post" name="form1"
				id="form1" enctype="multipart/form-data">
				<input type="hidden" value="2" name="step" /> <input type="hidden"
					value="<?php echo $version; ?>" name="version" /> <input
					type="hidden" value="<?php echo $_SESSION['q']; ?>" name="q" />

				<div>
					<label for="vorname"><?=$_SESSION['Leg_34']?> </label>
					<div class="div_texbox">
						<span style="font-family: SpartanHeavy;"><?=$vorname?> </span>
					</div>
					<br clear="all" /> <label for="name"><?=$_SESSION['Leg_35']?> </label>
					<div class="div_texbox">
						<span style="font-family: SpartanHeavy;"><?=$name?> </span>
					</div>
					<br clear="all" /> <br clear="all" /> <input type="button"
						name="name_aendern" id="name_aendern" value="<?= Label(164) ?>" /> <br
						clear="all" />
					<p>
					<?=$_SESSION['Leg_79']?>
						<select name="sprache">
							<option value="" <? echo ($sprache == "")? 'selected' : ""; ?>></option>
							<option value="de"
							<? echo ($sprache == "de")? 'selected' : ""; ?>>DE</option>
							<option value="en"
							<? echo ($sprache == "en")? 'selected' : ""; ?>>EN</option>
							<option value="fr"
							<? echo ($sprache == "fr")? 'selected' : ""; ?>>FR</option>
							<option value="it"
							<? echo ($sprache == "it")? 'selected' : ""; ?>>IT</option>
						</select> <br />
						<?=$_SESSION['Leg_177']?>
						: <input type="radio" name="geschlecht" value="f"
						<? if ($geschlecht == "f") echo 'checked'; ?> /> f
						&nbsp;&nbsp;&nbsp;&nbsp; <input type="radio" name="geschlecht"
							value="m" <? if ($geschlecht == "m") echo 'checked'; ?> /> m
					</p>

				</div>
				<br clear="all" />
				<?
				/*****************************
				 AKK-Formular
				 *****************************/


				if ($version == "akk") {
					?>

				<h3>
				<?=$_SESSION['Leg_287']?>
				</h3>
				<? $akk_ok = "1"; ?>
				<?= Label(288) ?>
				<input type="hidden" id="akk_ok" name="akk_ok" value="1" />
				<br />
				<table border="0" >
					<colgroup>
						<col width="197" />
						<col width="243" />
					</colgroup>
				  <tbody>
				    <tr>
				      <td class="h4"><?= Label(358) ?></td>
				      <td><input type="text" class="textbox" id="akk_beruf" name="akk_beruf" value="<?= $akk_beruf ?>" required="required" /></td>
			        </tr>
			        <tr><td colspan="2"><?= nl2br(Label(360)) ?></td></tr>
                    <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
					<?php
					$layout = $fm->getLayout('cgi_Adressaenderung_Personen');
					$row=0;
					foreach ($arbeitsbereiche as $bereich) {
						$bname = $bereich['name'];
						$cat_name = 'akk_'.strtolower($bname);
						$with_syndicate = $bereich['verband'];
						$style = $with_syndicate?'class="'.($row++ % 2?'even':'odd').'"':'';
						// Checkbox anzeigen
						$selector = ($akk_cat[$bname]>0) ? ' checked="checked"' : '';
						echo '<tr '.$style.'><td><input type="checkbox" id="'.$cat_name.'" name="'.$cat_name.'" value="1"'.$selector.' /> '.Label($bereich['label']).'</td><td>';
						// optional Verband anzeigen
						if ($with_syndicate) {
							// Werteliste holen
							$vl = $layout->getValueList('Verband_'.$bname);
							// und als Drop-Down anzeigen
							$verb_name = $cat_name.'_verband';
							echo Label('313').' <select style="width: 150px;" name="'.$verb_name.'" id="'.$verb_name.'"><option value=""></option>';
							foreach ($vl as $value) {
								$selektor = ($akk_verband[$bname]==$value)?' selected="selected"':'';
								echo '<option value="'.$value.'"'.$selektor.' >'.$value.'</option>';
							}
							echo "</select>";
						}
						if ($bereich['notiz']) {
							$notiz_name = $cat_name.'_notiz';
							if ($with_syndicate) echo '<br />';
							if ($bereich['notiz_label']) echo Label(361);
							echo " <input type='text' class='textbox' name='$notiz_name' id='$notiz_name' value='".$akk_notiz[$bname]."'  />";
						}
						if (!($with_syndicate || $bereich['notiz'])) echo '&nbsp;';
						echo "</td></tr>\n";
					}
					?>
					<tr>
						<td><input type="checkbox" id="akk_cat_else_flag"
							name="akk_cat_else_flag" value="1"
							<?= ($akk_cat_else_flag?' checked="checked"':'') ?> /> <?= Label('312') ?>
						</td>
						<td><input type="text" class="textbox" id="akk_cat_else"
							name="akk_cat_else" value="<?= $akk_cat_else ?>" /></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
                        <td><h3><?=$_SESSION['Leg_369']?></h3></td>
                        <td>&nbsp;</td>
                        <td><p><?=$_SESSION['Leg_359']?></p></td>
						<td>&nbsp;</td>
					</tr>                      
					<tr>
						<td><?=$_SESSION['Leg_291']?></td>
						<td><input type="text" class="textbox" id="akk_arr" name="akk_arr"
							value="<?=$akk_arr?>" /></td>
					</tr>
					<tr>
						<td><?=$_SESSION['Leg_292']?></td>
						<td><input type="text" class="textbox" id="akk_dep" name="akk_dep"
							value="<?=$akk_dep?>" /></td>
					</tr>
				  </tbody>
				</table>

				<br clear="all" />
				<div style="margin-left: 28px; text-indent: -24px;">
					<input type="checkbox" name="akk_conmail" value="1"
					<?= ($akk_conmail?'checked="checked"':'') ?> />
					<?= Label('314') ?>
				</div>
				<div style="margin-left: 28px; text-indent: -24px;">
					<input type="checkbox" name="akk_conhandy" value="1"
					<?= ($akk_conhandy?'checked="checked"':'') ?> />
					<?= Label('315') ?>
				</div>


				<br clear="all" />
				<?
				}
				if ('mit' == $version) {
					/*****************************
					 Mitarbeiter-Formular
					 *****************************/

					?>
				<br clear='all' />
				<div class="mitarbeiter">
				<?= render_text_input('geburtsdatum',343) ?>
				<?= render_text_input('mitarbeiter_ahv_nr',344) ?>
				<?= render_text_input('mitarbeiter_berufkenntnisse',345) ?>
					<p>
					<?= Label(330) ?>
					</p>
					<table class="zeiten">
						<thead>
							<tr>
								<th class="label"></th>
								<?php
								foreach ($zeiten as $label => $value) {
									?>
								<td><?= $label ?></td>
								<?php
								}
								?>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="label"><?= Label(370) ?></td>
								<?= render_mit_zeiten('0') ?>
							</tr>
							<tr>
								<td class="label"><?= Label(331) ?></td>
								<?= render_mit_zeiten('1') ?>
							</tr>
							<tr>
								<td class="label"><?= Label(332) ?></td>
								<?= render_mit_zeiten('2') ?>
							</tr>
							<tr>
								<td class="label"><?= Label(333) ?></td>
								<?= render_mit_zeiten('3') ?>
							</tr>
							<tr>
								<td class="label"><?= Label(334) ?></td>
								<?= render_mit_zeiten('4') ?>
							</tr>
							<tr>
								<td class="label"><?= Label(335) ?></td>
								<?= render_mit_zeiten('5') ?>
							</tr>
							<tr>
								<td class="label"><?= Label(336) ?></td>
								<?= render_mit_zeiten('6') ?>
							</tr>
							<tr>
								<td class="label"><?= Label(337) ?></td>
								<?= render_mit_zeiten('7') ?>
							</tr>
							<tr>
								<td class="label"><?= Label(338) ?></td>
								<?= render_mit_zeiten('8') ?>
							</tr>
						</tbody>
					</table>
					<br clear="all" /> <label for=""><?= Label(339) ?> </label>
					<div class="div_texbox">
					<?= render_fm_value_list('n__mitarbeiter__einsatzbereiche', $einsatz) ?>
					</div>
					<br clear="all" /> <br clear="all" /> <label for=""><?= Label(340) ?>
					</label>
					<div class="div_texbox">
					<?= render_fm_value_list('n__mitarbeiter__locations', $locations) ?>
					</div>
					<br clear="all" /> <br clear="all" /> <label
						for="n__mitarbeiter__bemerkungen"><?= Label(347)?> </label>
					<div class="div_texbox">
						<textarea rows=3 name="n__mitarbeiter__bemerkungen"
							class="textbox">
							<?= $n__mitarbeiter__bemerkungen ?>
						</textarea>
					</div>
					<br clear="all" /> <br clear="all" /> <br clear="all" /> <label
						for="mitarbeiter_kein_interesse_mehr"><?= Label(346) ?> </label>
					<div class="div_texbox">
						<input type="hidden" name="mitarbeiter_kein_interesse_mehr"
							value="0" /> <input type="checkbox"
							name="mitarbeiter_kein_interesse_mehr" value="1"
							<?= checked_attr($mitarbeiter_kein_interesse_mehr) ?> />
					</div>
					<br clear="all" />
					<p>
					<?= nl2br(Label(341)) ?>
					</p>
				</div>
				<?php
				}
				if ('mod' == $version) {
					/*****************************
					 Moderatoren-Formular
					 *****************************/

					?>
				<br clear='all' />
				<div class="mitarbeiter">
					<p>
					<?= Label(349) ?>
					</p>
					<table class="zeiten">
						<thead>
							<tr>
								<th class="label"></th>
								<?php
								foreach ($mod_zeiten as $label => $value) {
									?>
								<td><?= $label ?></td>
								<?php
								}
								?>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="label"><?= Label(331) ?></td>
								<?= render_mod_zeiten('1') ?>
							</tr>
							<tr>
								<td class="label"><?= Label(332) ?></td>
								<?= render_mod_zeiten('2') ?>
							</tr>
							<tr>
								<td class="label"><?= Label(333) ?></td>
								<?= render_mod_zeiten('3') ?>
							</tr>
							<tr>
								<td class="label"><?= Label(334) ?></td>
								<?= render_mod_zeiten('4') ?>
							</tr>
							<tr>
								<td class="label"><?= Label(335) ?></td>
								<?= render_mod_zeiten('5') ?>
							</tr>
							<tr>
								<td class="label"><?= Label(336) ?></td>
								<?= render_mod_zeiten('6') ?>
							</tr>
							<tr>
								<td class="label"><?= Label(337) ?></td>
								<?= render_mod_zeiten('7') ?>
							</tr>
							<tr>
								<td class="label"><?= Label(338) ?></td>
								<?= render_mod_zeiten('8') ?>
							</tr>
						</tbody>
					</table>
					<br clear="all" /> <label for="n__moderatoren__bemerkungen"><?= Label(347)?>
					</label>
					<div class="div_texbox">
						<textarea rows=3 name="n__moderatoren__bemerkungen"
							class="textbox">
							<?= $n__moderatoren__bemerkungen ?>
						</textarea>
					</div>
					<br clear="all" />

				</div>
				<?php
				}
				?>
				<br clear="all" />
				<h3>
				<?=$_SESSION['Leg_174']?>
				</h3>

				<div class="priv_addr">
				<?= render_text_input('strasse_1', 36) ?>
				<?= render_text_input('strasse_2', 37) ?>
				<?= render_text_input('plz', 38) ?>
				<?= render_text_input('ort', 39) ?>

					<label for="land_1"><?=$_SESSION['Leg_40']?> </label>
					<div class="div_texbox">
						<select name="land_1" id="land_1">
							<option value="">-</option>
							<?=$output_Land?>
						</select>
					</div>
					<br clear="all" />


					<?= render_text_input('tel_p', 41) ?>
					<?= render_text_input('fax', 43) ?>
					<?= render_text_input('tel_m', 176) ?>
					<?= render_text_input('mail_1', 44) ?>
					<?= render_text_input('web_1', 45) ?>
				</div>
				<div id="people_img">
					<h3>
					<?=$_SESSION['Leg_28']?>
						:
					</h3>
					<? if (file_exists($_SERVER['DOCUMENT_ROOT'].'/bilder/people/small/'.$personen_id.'.jpg')) { ?>
					<img
						src="/bilder/people/small/<?=$personen_id?>.jpg?random=<?=time()?>" />
						<? } ?>
					<br />&nbsp;<br /> <input type="file" name="userbild" id="userbild"
						size="10" /><br />&nbsp;<br />

				</div>

				<br clear="all" />
				<p>
					<input type="checkbox" name="newsletter" value="1"
					<? echo ($newsletter == "1")? 'checked' : ''; ?> />
					<?=$_SESSION['Leg_178']?>
					<br />
				</p>
				<br />
				<h3>
				<?=$_SESSION['Leg_179']?>
				</h3>
				<p>
				<?=$_SESSION['Leg_180']?>
				</p>
				<p>
					<img src="/images/add.png" align="absmiddle" border="0"
						onclick="javascript:newFirma();" title="<?=$_SESSION['Leg_181']?>" />
						<?=$_SESSION['Leg_181']?>
				</p>
				<div id="geschaeftsadressen">
				<?=$portal?>
				</div>
				<?

				/*****************************
				 EXT-Formular
				 *****************************/


				if ($version == "ext") {

					if ($biographie_katalog_vorhanden != "1") {
						?>

				<br clear="all" />&nbsp;<br />
				<h3>
				<?=$_SESSION['Leg_49']?>
				</h3>
				<p>
				<?=$_SESSION['Leg_184']?>
					<br /> <br />
					<textarea name="biographie_formular_zusatz"
						style="width: 500px; border: 1px solid #999999; font-family: Arial, Helvetica, sans-serif; font-size: 11px"
						rows="8">
						<?=$biographie_formular_zusatz?>
					</textarea>
				</p>
				<? } else { ?>

				<br clear="all" />&nbsp;<br />
				<h3>
				<?=$_SESSION['Leg_49']?>
				</h3>
				<p>
				<?=$_SESSION['Leg_215']?>
				</p>
				<p>
					<strong><?=$biographie_katalog?> </strong>
				</p>
				<p>
				<?=$_SESSION['Leg_216']?>
				</p>
				<p>
					<textarea name="biographie_formular_zusatz"
						style="width: 500px; border: 1px solid #999999; font-family: Arial, Helvetica, sans-serif; font-size: 11px"
						rows="8">
						<?=$biographie_formular_zusatz?>
					</textarea>
				</p>

				<? } ?>

				<br />
				<h3>
				<?=$_SESSION['Leg_50']?>
				</h3>
				<p>
				<?=$_SESSION['Leg_185']?>
				</p>
				<br />

				<div style="width: 175px; float: left;">
					<p>
						<span id="filmo_titel_wrap"><?=$_SESSION['Leg_76']?> </span><br />
						<input type="text" class="textbox" id="filmo_titel"
							name="filmo_titel" />
					</p>
				</div>
				<div style="width: 70px; float: left;">
					<p>
						&nbsp;<br /> <span id="filmo_jahr_wrap"><?=$_SESSION['Leg_56']?> </span><br />
						<input type="text" class="textbox_short" id="filmo_jahr"
							name="filmo_jahr" style="width: 60px;" />
					</p>
				</div>
				<div style="width: 155px; float: left;">
					<p>
						&nbsp;<br /> <span id="filmo_genre_wrap"><?=$_SESSION['Leg_2']?> </span><br />
						<select name="filmo_genre" id="filmo_genre">
							<option value="" selected="selected">-</option>
							<?=$output_Genre?>
						</select>
					</p>
				</div>
				<div style="width: 35px; float: left; text-align: center">
					<p>
					<?=str_replace("-","-<br />",$_SESSION['Leg_186'])?>
						<br /> <input type="checkbox" id="filmo_coregie"
							name="filmo_coregie" value="1" />
					</p>
				</div>
				<div style="width: 80px; float: left; text-align: center">
					<p>
						&nbsp;<br />
						<?=$_SESSION['Leg_78']?>
						<br /> <span id="add_filmo_button"><img src="/images/add.png"
							align="absmiddle" border="0" onclick="javascript:addFilmo();"
							title="<?=$_SESSION['Leg_78']?>" /> </span>
					</p>
				</div>
				<br clear="all" />
				<div id="filmo_error"></div>
				<div id="filmographie"
					style="border: 1px solid #CCCCCC; padding: 5px; background-color: #EEEEEE">
					<?
					$find =& $fm->newFindCommand('cgi_k_Stabmitglieder');
					$find->addFindCriterion('_kf__Person_Id', $_SESSION['personen_id']);
					$find->addFindCriterion('_kf__Stabbezeichnung_Id', "1");
					$find->addFindCriterion('OnlineDeleteFlag',"=");
					$find->addSortRule('Jahr', 1, FILEMAKER_SORT_DESCEND);
					$result = $find->execute();
					if (!FileMaker::isError($result)) {
						$records = $result->getRecords();
						foreach ($records as $record) {
							?>
					<div style="width: 175px; float: left;">
					<?=$record->getField('Filmtitel')?>
					</div>
					<div style="width: 70px; float: left;">
					<?=$record->getField('Jahr')?>
						&nbsp;
					</div>
					<div style="width: 155px; float: left;">
					<? echo $record->getField('zz_Stabmitglieder_Genretypen::Genre_'.$_SESSION['sprache']); ?>
						&nbsp;
					</div>
					<div style="width: 40px; float: left;">
					<? echo ($record->getField('CoRegieFlag') == "1")? "<img src=\"/images/accept.png\" />": "&nbsp;"; ?>
					</div>
					<div style="width: 60px; float: left;">
						<img src="/images/edit.png" align="absmiddle" border="0"
							onclick="javascript:editFimographie('<?=$record->getField('_kp__record_id')?>');"
							title="<?=$_SESSION['Leg_217']?>" /> <img
							src="/images/delete.png" border="0"
							onclick="javascript:deleteFimographie('<?=$record->getField('_kp__id')?>');"
							title="<?=$_SESSION['Leg_88']?>" />
					</div>
					<br clear="all" />
					<?
						}

					}
					?>
				</div>
				<br />&nbsp;<br /> <input type="checkbox" id="bio_bearbeitet"
					name="bio_bearbeitet" value="1"
					<? if ($bio_bearbeitet == "1") echo 'checked'; ?> />
					<?=$_SESSION['Leg_219']?>
				<br />&nbsp;<br />
				<? } ?>




				<br /> <input type="submit" value="<?=$_SESSION['Leg_114']?>" />
			</form>
			<div class="clear"></div>
		</fieldset>
	</div>

	<br style="clear: both" />



	<? /*

	#########################################
	#                                       #
	#     Dialoge zur Filmographie        #
	#                                       #
	#########################################

	*/ ?>

	<div id="div_filmo_aendern">
		<div class="hd">
		<?=$_SESSION['Leg_218']?>
		</div>
		<div class="bd">
			<form id="filmo_aendern" action="javascript:void(0)" method="post">
				<input type="hidden" name="filmo_edit_rid" id="filmo_edit_rid" /> <input
					type="hidden" name="filmo_edit_genre_hidden"
					id="filmo_edit_genre_hidden" />
				<p>
				<?=$_SESSION['Leg_1']?>
					<input type="text" id="filmo_edit_titel" class="textbox"
						name="filmo_edit_titel" />
				</p>
				<p>
				<?=$_SESSION['Leg_56']?>
					<input type="text" id="filmo_edit_jahr" class="textbox"
						name="filmo_edit_jahr" />
				</p>
				<p>
				<?=$_SESSION['Leg_2']?>
					<select name="filmo_edit_genre" id="filmo_edit_genre"
						class="textbox">
						<option value="">-</option>
						<?=$output_Genre?>
					</select>
				</p>
				<p>
				<?=$_SESSION['Leg_186']?>
					<input type="checkbox" id="filmo_edit_coregie"
						name="filmo_edit_coregie" value="1" />
				</p>
				<br />
			</form>
		</div>
	</div>

	<? /*

	#########################################
	#                                       #
	#     Dialoge zur Namensänderung        #
	#                                       #
	#########################################

	*/ ?>

	<div id="div_name_aendern">
		<div class="hd">
		<?=$_SESSION['Leg_169']?>
		</div>
		<div class="bd">
			<p>
			<?=$_SESSION['Leg_165']?>
			</p>
			<input type="button" id="change" class="mainoption"
				value="<?=$_SESSION['Leg_166']?>" /> <br />&nbsp;<br />
			<p>
			<?=$_SESSION['Leg_167']?>
			</p>
			<input type="button" id="new" class="mainoption"
				value="<?=$_SESSION['Leg_168']?>"
				onclick="window.location.href='newsletter.php?sprache=<?=$sprache?>&version=<?=$version?>'" />
			<br />
		</div>
	</div>

	<div id="div_name_aendern2">
		<div class="hd">
		<?=$_SESSION['Leg_169']?>
		</div>
		<div class="bd">
			<form id="new_name" action="javascript:void(0)" method="post">
				<p>
				<?=$_SESSION['Leg_34']?>
					<input type="text" id="neuer_vorname" class="textbox"
						name="neuer_vorname" />
				</p>
				<p>
				<?=$_SESSION['Leg_35']?>
					<input type="text" id="neuer_name" class="textbox"
						name="neuer_name" />
				</p>
				<p>
				<?=$_SESSION['Leg_170']?>
					<select name="grund" id="grund" class="textbox">
						<option value="">-</option>
						<?=$output_Grund?>
					</select>
				</p>
				<br />
			</form>
		</div>
	</div>

	<div id="div_name_aendern3">
		<div class="hd">
		<?=$_SESSION['Leg_169']?>
		</div>
		<div class="bd">
			<p>
			<?=$_SESSION['Leg_172']?>
			</p>
		</div>
	</div>


	<? /*

	#########################################
	#                                       #
	#  Dialoge zur Geschäftsadressauswahl   #
	#                                       #
	#########################################

	*/ ?>

	<div id="div_dlg_newfirma">
		<div class="hd">Firma suchen</div>
		<div class="bd">
			<div id="span_newfirma_step1">
				<form id="form_search_newfirma" action="javascript:void(0)"
					method="post">
					<div style="float: left; font-size: 11px;">
						Suche nach<br /> Firma<br /> <input type="text" class="post"
							name="s_firma" id="s_firma" value="" size="18"
							style="border: 1px solid #666666; font-size: 11px;" />
					</div>
					<div style="float: left; font-size: 11px;">
						&nbsp;<br /> Ort<br /> <input type="text" class="post"
							name="s_ort" id="s_ort" value="" size="18"
							style="border: 1px solid #666666; font-size: 11px;" /> <input
							type="hidden" name="type" value="1" />
					</div>
					<div style="float: left; font-size: 11px;">
						&nbsp;<br /> &nbsp;<br /> <input type="button" class="mainoption"
							value="<?=$_SESSION['Leg_100']?>" onclick="newfirma_search();" />
					</div>
					<br clear="all" />

				</form>
				<br /> <span id="span_searchresult_newfirma"></span>
			</div>
			<span id="span_newfirma_step2"></span>
		</div>
	</div>

	<div id="div_dlg_addfirma">
		<div class="hd">Firma hinzufügen</div>
		<div class="bd">
			<div id="span_addfirma">
				<form id="form_add_newfirma" action="javascript:void(0)"
					method="post" enctype="multipart/form-data">
					<div style="float: left; width: 250px;">
						<div style="float: left; font-size: 11px; width: 70px;">
						<?=$_SESSION['Leg_53']?>
							*
						</div>
						<div style="float: left; font-size: 11px;">
							<input type="text" class="post" name="dlg_firma_firma"
								id="dlg_firma_firma" value="" size="20"
								style="border: 1px solid #666666; font-size: 11px;" />
						</div>
						<br clear="all" />
						<div style="float: left; font-size: 11px; width: 70px;">
						<?=$_SESSION['Leg_36']?>
							*
						</div>
						<div style="float: left; font-size: 11px;">
							<input type="text" class="post" name="dlg_firma_strasse1"
								id="dlg_firma_strasse1" value="" size="20"
								style="border: 1px solid #666666; font-size: 11px;" />
						</div>
						<br clear="all" />
						<div style="float: left; font-size: 11px; width: 70px;">
						<?=$_SESSION['Leg_37']?>
						</div>
						<div style="float: left; font-size: 11px;">
							<input type="text" class="post" name="dlg_firma_strasse2"
								id="dlg_firma_strasse2" value="" size="20"
								style="border: 1px solid #666666; font-size: 11px;" />
						</div>
						<br clear="all" />
						<div style="float: left; font-size: 11px; width: 70px;">
						<?=$_SESSION['Leg_38']?>
							*
						</div>
						<div style="float: left; font-size: 11px;">
							<input type="text" class="post" name="dlg_firma_plz"
								id="dlg_firma_plz" value="" size="6"
								style="border: 1px solid #666666; font-size: 11px;" />
						</div>
						<br clear="all" />
						<div style="float: left; font-size: 11px; width: 70px;">
						<?=$_SESSION['Leg_39']?>
							*
						</div>
						<div style="float: left; font-size: 11px;">
							<input type="text" class="post" name="dlg_firma_ort"
								id="dlg_firma_ort" value="" size="20"
								style="border: 1px solid #666666; font-size: 11px;" />
						</div>
						<br clear="all" />
						<div style="float: left; font-size: 11px; width: 70px;">
						<?=$_SESSION['Leg_40']?>
							*
						</div>
						<div style="float: left; font-size: 11px;">
							<select class="post" name="dlg_firma_land" id="dlg_firma_land"
								style="border: 1px solid #666666; font-size: 11px;">
								<option value="">-</option>
								<?=$output_Land?>
							</select>
						</div>
						<br clear="all" />
						<div style="float: left; font-size: 11px; width: 70px;">
						<?=$_SESSION['Leg_41']?>
						</div>
						<div style="float: left; font-size: 11px;">
							<input type="text" class="post" name="dlg_firma_fon"
								id="dlg_firma_fon" value="" size="20"
								style="border: 1px solid #666666; font-size: 11px;" />
						</div>
						<br clear="all" />
						<div style="float: left; font-size: 11px; width: 70px;">
						<?=$_SESSION['Leg_43']?>
						</div>
						<div style="float: left; font-size: 11px;">
							<input type="text" class="post" name="dlg_firma_fax"
								id="dlg_firma_fax" value="" size="20"
								style="border: 1px solid #666666; font-size: 11px;" />
						</div>
						<br clear="all" />
						<div style="float: left; font-size: 11px; width: 70px;">
						<?=$_SESSION['Leg_44']?>
						</div>
						<div style="float: left; font-size: 11px;">
							<input type="text" class="post" name="dlg_firma_email"
								id="dlg_firma_email" value="" size="20"
								style="border: 1px solid #666666; font-size: 11px;" />
						</div>
						<br clear="all" />
						<div style="float: left; font-size: 11px; width: 70px;">
						<?=$_SESSION['Leg_45']?>
						</div>
						<div style="float: left; font-size: 11px;">
							<input type="text" class="post" name="dlg_firma_www"
								id="dlg_firma_www" value="" size="20"
								style="border: 1px solid #666666; font-size: 11px;" />
						</div>
						<br clear="all" />
					</div>
					<br clear="all" /> <input type="hidden" name="type" value="1" />
				</form>
				<br /> <span id="span_searchresult_newfirma"></span>
			</div>
			<span id="span_newfirma_step2"></span>
		</div>
	</div>


	<? /*

	#########################################
	#                                       #
	#  Dialoge zur Geschäftsadressänderung  #
	#                                       #
	#########################################

	*/ ?>

	<div id="div_angestellter_aendern1">
		<div class="hd">
		<?=$_SESSION['Leg_179']?>
		</div>
		<div class="bd">
			<div style="float: left" id="adressblock">...</div>
			<div style="float: right">
				&nbsp;<br />&nbsp;<br /> <input type="button" id="firma_change"
					class="mainoption" value="<?=$_SESSION['Leg_210']?>"
					onclick="javascript:open_dialog_change_firma('<?=$related_id?>');" />
			</div>
			<br clear="all" />
			<hr size="1" noshade="noshade" />
			&nbsp;<br />
			<form id="edit_angestellter" action="javascript:void(0)"
				method="post" enctype="multipart/form-data">
				<input type="hidden" name="dialog4_id" id="dialog4_id" value="" />
				<div style="width: 100px; float: left;">
				<?=$_SESSION['Leg_188']?>
					:
				</div>
				<div style="width: 130px; float: left;">
					<input type="text" class="post" name="dialog4_abteiluung"
						id="dialog4_abteilung" value="" size="20"
						style="border: 1px solid #666666; font-size: 11px;" />
				</div>
				<br clear="all" />
				<div style="width: 100px; float: left;">
				<?=$_SESSION['Leg_189']?>
					:
				</div>
				<div style="width: 130px; float: left;">
					<input type="text" class="post" name="dialog4_funktion"
						id="dialog4_funktion" value="" size="20"
						style="border: 1px solid #666666; font-size: 11px;" />
				</div>
				<br clear="all" />
				<div style="width: 100px; float: left;">
				<?=$_SESSION['Leg_190']?>
					:
				</div>
				<div style="width: 130px; float: left;">
					<input type="text" class="post" name="dialog4_tel" id="dialog4_tel"
						value="" size="20"
						style="border: 1px solid #666666; font-size: 11px;" />
				</div>
				<br clear="all" />
				<div style="width: 100px; float: left;">
				<?=$_SESSION['Leg_191']?>
					:
				</div>
				<div style="width: 130px; float: left;">
					<input type="text" class="post" name="dialog4_fax" id="dialog4_fax"
						value="" size="20"
						style="border: 1px solid #666666; font-size: 11px;" />
				</div>
				<br clear="all" />
				<div style="width: 100px; float: left;">
				<?=$_SESSION['Leg_192']?>
					:
				</div>
				<div style="width: 130px; float: left;">
					<input type="text" class="post" name="dialog4_mobil"
						id="dialog4_mobil" value="" size="20"
						style="border: 1px solid #666666; font-size: 11px;" />
				</div>
				<br clear="all" />
				<div style="width: 100px; float: left;">
				<?=$_SESSION['Leg_44']?>
					:
				</div>
				<div style="width: 130px; float: left;">
					<input type="text" class="post" name="dialog4_mail"
						id="dialog4_mail" value="" size="20"
						style="border: 1px solid #666666; font-size: 11px;" />
				</div>
				<br clear="all" /> <input type="hidden" name="dialog4_newsletter"
					id="dialog4_newsletter" value="" /> <input type="checkbox"
					name="dialog4_nl_checkbox" id="dialog4_nl_checkbox" value="1" />
					<?=$_SESSION['Leg_178']?>
			</form>

		</div>
	</div>



	<? /*

	#####################################################
	#                                                   #
	#  Dialoge zur Geschäftsadress - Adressänderung     #
	#                                                   #
	#####################################################

	*/ ?>

	<div id="div_dlg_changefirma">
		<div class="hd">
		<?=$_SESSION['Leg_210']?>
		</div>
		<div class="bd">
			<p>
			<?=$_SESSION['Leg_211']?>
			</p>
			<div id="span_addfirma">
				<form id="form_change_firma" action="javascript:void(0)"
					method="post" enctype="multipart/form-data">
					<input type="hidden" name="dlg_firma_firma_change_id"
						id="dlg_firma_firma_change_id" />
					<div style="float: left; width: 250px;">
						<div style="float: left; font-size: 11px; width: 70px;">
						<?=$_SESSION['Leg_53']?>
							*
						</div>
						<div style="float: left; font-size: 11px;">
							<input type="text" class="post" name="dlg_firma_firma_change"
								id="dlg_firma_firma_change" value="" size="20"
								style="border: 1px solid #666666; font-size: 11px;" />
						</div>
						<br clear="all" />
						<div style="float: left; font-size: 11px; width: 70px;">
							<?=$_SESSION['Leg_36']?>
							*
						</div>
						<div style="float: left; font-size: 11px;">
							<input type="text" class="post" name="dlg_firma_strasse1_change"
								id="dlg_firma_strasse1_change" value="" size="20"
								style="border: 1px solid #666666; font-size: 11px;" />
						</div>
						<br clear="all" />
						<div style="float: left; font-size: 11px; width: 70px;">
							<?=$_SESSION['Leg_37']?>
						</div>
						<div style="float: left; font-size: 11px;">
							<input type="text" class="post" name="dlg_firma_strasse2_change"
								id="dlg_firma_strasse2_change" value="" size="20"
								style="border: 1px solid #666666; font-size: 11px;" />
						</div>
						<br clear="all" />
						<div style="float: left; font-size: 11px; width: 70px;">
							<?=$_SESSION['Leg_38']?>
							*
						</div>
						<div style="float: left; font-size: 11px;">
							<input type="text" class="post" name="dlg_firma_plz_change"
								id="dlg_firma_plz_change" value="" size="6"
								style="border: 1px solid #666666; font-size: 11px;" />
						</div>
						<br clear="all" />
						<div style="float: left; font-size: 11px; width: 70px;">
							<?=$_SESSION['Leg_39']?>
							*
						</div>
						<div style="float: left; font-size: 11px;">
							<input type="text" class="post" name="dlg_firma_ort_change"
								id="dlg_firma_ort_change" value="" size="20"
								style="border: 1px solid #666666; font-size: 11px;" />
						</div>
						<br clear="all" />
						<div style="float: left; font-size: 11px; width: 70px;">
							<?=$_SESSION['Leg_40']?>
							*
						</div>
						<div style="float: left; font-size: 11px;">
							<select class="post" name="dlg_firma_land_change"
								id="dlg_firma_land_change"
								style="border: 1px solid #666666; font-size: 11px;">
								<option value="">-</option>
								<?=$output_Land?>
							</select>
						</div>
						<br clear="all" />
						<div style="float: left; font-size: 11px; width: 70px;">
							<?=$_SESSION['Leg_41']?>
						</div>
						<div style="float: left; font-size: 11px;">
							<input type="text" class="post" name="dlg_firma_fon_change"
								id="dlg_firma_fon_change" value="" size="20"
								style="border: 1px solid #666666; font-size: 11px;" />
						</div>
						<br clear="all" />
						<div style="float: left; font-size: 11px; width: 70px;">
							<?=$_SESSION['Leg_43']?>
						</div>
						<div style="float: left; font-size: 11px;">
							<input type="text" class="post" name="dlg_firma_fax_change"
								id="dlg_firma_fax_change" value="" size="20"
								style="border: 1px solid #666666; font-size: 11px;" />
						</div>
						<br clear="all" />
						<div style="float: left; font-size: 11px; width: 70px;">
							<?=$_SESSION['Leg_44']?>
						</div>
						<div style="float: left; font-size: 11px;">
							<input type="text" class="post" name="dlg_firma_email_change"
								id="dlg_firma_email_change" value="" size="20"
								style="border: 1px solid #666666; font-size: 11px;" />
						</div>
						<br clear="all" />
						<div style="float: left; font-size: 11px; width: 70px;">
							<?=$_SESSION['Leg_45']?>
						</div>
						<div style="float: left; font-size: 11px;">
							<input type="text" class="post" name="dlg_firma_www_change"
								id="dlg_firma_www_change" value="" size="20"
								style="border: 1px solid #666666; font-size: 11px;" />
						</div>
						<br clear="all" />
					</div>
					<br clear="all" /> <input type="hidden" name="type" value="1" />
				</form>
				<br /> <span id="span_searchresult_newfirma"></span>
			</div>
			<span id="span_newfirma_step2"></span>
		</div>
	</div>

	<div id="div_dlg_changefirma_final">
		<div class="hd">
			<?=$_SESSION['Leg_210']?>
		</div>
		<div class="bd">
			<p>
				<?=$_SESSION['Leg_214']?>
			</p>
		</div>
	</div>


	<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>

</body>
</html>
