<?php
session_start();
// Bust cache in the head
header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
// always modified
header ("Cache-Control: no-cache, must-revalidate");  // HTTP/1.1
header ("Pragma: no-cache");                          // HTTP/1.0
header('Content-Type: text/html; charset=utf-8');

$titel= (isset($_REQUEST['filmo_titel']))? $_REQUEST['filmo_titel'] : "";
$jahr= (isset($_REQUEST['filmo_jahr']))? $_REQUEST['filmo_jahr'] : "";
$genre= (isset($_REQUEST['filmo_genre']))? $_REQUEST['filmo_genre'] : "";
$coregie= (isset($_REQUEST['filmo_coregie']))? $_REQUEST['filmo_coregie'] : "";
$portal = "";

require_once (__DIR__.'/../includes/db.inc.php');

$data = array(
	'_kf__Person_Id'	=> $_SESSION['personen_id'],
	'Filmtitel'				=> $titel,
	'Jahr'				=> $jahr,
	'Genre'				=> $genre,
	'_kf__Stabbezeichnung_Id'	=> "1",
	'CoRegieFlag'	=> $coregie
);
//'per_JCRW_Stabmitglieder::_kf__Stabbezeichnung_Id'	=> "1"

$newRequest =& $fm->newAddCommand('cgi_k_Stabmitglieder', $data);
$result = $newRequest->execute();

//Auf Fehler prüfen
if (FileMaker::isError($result)) {
	
	echo "<p>Fehler: " . $result->getMessage() . "<p>";
	exit;
}
/* 
#################################
Liste der Filmos neu aufbauen
#################################
*/

$find =& $fm->newFindCommand('cgi_k_Stabmitglieder'); 
$find->addFindCriterion('_kf__Person_id', $_SESSION['personen_id']); 
$find->addFindCriterion('_kf__Stabbezeichnung_Id', "1"); 
$find->addFindCriterion('OnlineDeleteFlag',"="); 
$find->addSortRule('Jahr', 1, FILEMAKER_SORT_DESCEND);
$result = $find->execute(); 
if (FileMaker::isError($result)) {
		echo 'Leider ist ein Fehler aufgetreten. Bitte kontaktieren Sie den WebMaster.';
		exit;
	}
$records = $result->getRecords(); 

foreach ($records as $record) {

	$portal .= '<div style="width:175px; float:left;">'.str_replace("'", "&#039;", htmlspecialchars($record->getField('Filmtitel'))).'</div>'."\\n";
	$portal .= '<div style="width:70px; float:left;">'.$record->getField('Jahr').'&nbsp;</div>'."\\n";
	$portal .= '<div style="width:155px; float:left;">'.$record->getField('zz_Stabmitglieder_Genretypen::Genre_'.$_SESSION['sprache']).'&nbsp;</div>'."\\n";
	$portal .= ($record->getField('CoRegieFlag') == "1")? '<div style="width:40px; float:left;"><img src="/images/accept.png" />'.'</div>'."\\n" : '<div style="width:40px; float:left;">&nbsp;'.'</div>'."\\n";
	$portal .= '<div style="width:60px; float:left;"><img src="/images/edit.png" align="absmiddle" border="0" onClick="javascript:editFimographie(\\\''.$record->getField('_kp__record_id').'\\\');" title="'.$_SESSION['Leg_217'].'">  <img src="/images/delete.png" align="absmiddle" border="0" onClick="javascript:deleteFimographie(\\\''.$record->getField('_kp__id').'\\\');" title="'.$_SESSION['Leg_88'].'"></div>'."\\n";
	
	//$portal .= '<div style="width:180px; float:left;"><img src="/images/delete.png" align="absmiddle" border="0" onClick="javascript:deleteFimographie(\\\''.$record->getField('_kp__id').'\\\');" title="'.$_SESSION['Leg_88'].'"> '.$_SESSION['Leg_88'].'</div>'."\\n";
	$portal .= '<br clear="all" />'."\\n";

}

$span = " document.getElementById('filmographie').innerHTML = '".$portal."';";
die($span);

?>