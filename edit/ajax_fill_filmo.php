<?
session_start();
// Bust cache in the head
header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
// always modified
header ("Cache-Control: no-cache, must-revalidate");  // HTTP/1.1
header ("Pragma: no-cache");                          // HTTP/1.0

$id = (isset($_REQUEST['id']))? $_REQUEST['id'] : "";
$callback = "";

require_once (__DIR__.'/../includes/db.inc.php');

$find =& $fm->newFindCommand('cgi_k_Stabmitglieder'); 
$find->addFindCriterion('_kp__record_id', $id); 
$result = $find->execute(); 
if (FileMaker::isError($result)) {
		echo 'Leider ist ein Fehler aufgetreten. Bitte kontaktieren Sie den WebMaster.';
		exit;
	}
$records = $result->getRecords(); 
$foundrec = $result->getFoundSetCount();
$record = $records[0];

$callback .= "document.getElementById('filmo_edit_titel').value = '".addslashes($record->getField('Filmtitel'))."'; ";
$callback .= "document.getElementById('filmo_edit_jahr').value = '".$record->getField('Jahr')."'; ";
$callback .= "document.getElementById('filmo_edit_genre_hidden').value = '".$record->getField('Genre')."'; ";
if ($record->getField('CoRegieFlag') == "1") {
	$callback .= "document.getElementById('filmo_edit_coregie').checked = true; ";
} else {
	$callback .= "document.getElementById('filmo_edit_coregie').checked = false; ";
}

die($callback);

?>