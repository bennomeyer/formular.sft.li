<?
session_start();
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
if ((!$_SESSION['login_ok']) || (($_SESSION['film_id'] == "") && ($_SESSION['m'] == "u"))) {
	header("Location: login.php");
	exit;
}

require_once ('includes/db.inc.php');
$q = FX_open_layout( "cgi_h_02__filme", "1"); 


$step = (isset($_POST['step_8'])) ? $_POST['step_8'] : "";
$direction = (isset($_POST['direction'])) ? $_POST['direction'] : "";
$target = (isset($_POST['target'])) ? $_POST['target'] : "";
$Synop_de = (isset($_POST['Synop_de'])) ? $_POST['Synop_de'] : "";
$Synop_en = (isset($_POST['Synop_en'])) ? $_POST['Synop_en'] : "";
$Synop_fr = (isset($_POST['Synop_fr'])) ? $_POST['Synop_fr'] : "";
$Synop_it = (isset($_POST['Synop_it'])) ? $_POST['Synop_it'] : "";
$Logline_de = (isset($_POST['Logline_de'])) ? $_POST['Logline_de'] : "";
$Logline_fr = (isset($_POST['Logline_fr'])) ? $_POST['Logline_fr'] : "";


// UPDATE init - alte Daten aus DB ziehen
//__________________________________________________

if (($_SESSION['m'] == "u") && ($step != "2")) {
	
	// Filmdaten holen
	$find =& $fm->newFindCommand('cgi_h_02__filme'); 
	$find->addFindCriterion('_kp__id', $_SESSION['film_id']); 
	$find->addFindCriterion('_kf__Anmeldeuser_Id', $_SESSION['user_id']); 
	$result = $find->execute(); 
	if (!FileMaker::isError($result)) {
		$records = $result->getRecords(); 
		$record = $records[0];
		$_SESSION['record_id'] = $record->getField('_record__id');
		$Synop_de = $record->getField('Synopsis_Formular_De');
		$Synop_en = $record->getField('Synopsis_Formular_En');
		$Synop_fr = $record->getField('Synopsis_Formular_Fr');
		$Synop_it = $record->getField('Synopsis_Formular_It');
		$Logline_de = $record->getField('Logline_DE');
		$Logline_fr = $record->getField('Logline_FR');
		}
}

// ERROR-Handling
//__________________________________________________
$error = "";
if ($step == "2") {
	if ((($Synop_de == "") && ($Synop_en == "") && ($Synop_fr == "") && ($Synop_it == "")) || 
			(($Logline_de == "") && ($Logline_fr == ""))) {
		$error .= $_SESSION['Leg_138'];
	}
	if (($_SESSION['maxminutes'] == "59") && ((strlen(utf8_decode($Synop_de)) > 640) || (strlen(utf8_decode($Synop_en)) > 640) || (strlen(utf8_decode($Synop_fr)) > 640) || (strlen(utf8_decode($Synop_it)) > 640))) {
		$error .= $_SESSION['Leg_282']; //
	}
	if (($_SESSION['maxminutes'] == "300") && ((strlen(utf8_decode($Synop_de)) > 900) || (strlen(utf8_decode($Synop_en)) > 900) || (strlen(utf8_decode($Synop_fr)) > 900) || (strlen(utf8_decode($Synop_it)) > 900))) {
		$error .= $_SESSION['Leg_283'];
	}
	if ((strlen(utf8_decode($Logline_de)) > 140) || (strlen(utf8_decode($Logline_fr)) > 140) ) $error .= $_SESSION['Leg_363'];
	
	if ($error != "") {
		// Update Seite2Flag mit 0
		$q->AddDBParam('-recid', $_SESSION['record_id']); 
		$q->AddDBParam('zz_Anmeldung_Seite08_Flag', "0");
		$DBData = $q->FMEdit(); 	
	} elseif ($error == "") {
		// Update Seite2Flag mit 1
		$q->AddDBParam('-recid', $_SESSION['record_id']); 
		$q->AddDBParam('zz_Anmeldung_Seite08_Flag', "1");
		$DBData = $q->FMEdit(); 	
	}
}

// UPDATE final & Neueintrag - Daten aktualisieren
//__________________________________________________
if ((($error == "") && ($step == "2") && ($direction == "next")) || (($step == "2") && ($direction == "back"))) {

	$q = FX_open_layout( "cgi_h_02__filme", "1");
	$q->AddDBParam('-recid', $_SESSION['record_id']); 
	$q->AddDBParam('Synopsis_Formular_De', $Synop_de);
	$q->AddDBParam('Synopsis_Formular_En', $Synop_en);
	$q->AddDBParam('Synopsis_Formular_Fr', $Synop_fr);
	$q->AddDBParam('Synopsis_Formular_It', $Synop_it);
	$q->AddDBParam('Logline_DE', $Logline_de);
	$q->AddDBParam('Logline_FR', $Logline_fr);
	$DBData = $q->FMEdit();	
}


if (($direction == "next") && ($step == "2") && ($error == "")) {
	if ($target != "") {
		header("Location: /$target");
	} else {
		//redirect zur nächsten Seite
		header("Location: /step08_9.php");
	}
	exit;
}
if (($direction == "back") && ($step == "2")) {
	if ($target != "") {
		header("Location: /$target");
	} else {
		//redirect zur vorherigen Seite
		header("Location: /step08_7.php");
	}
	exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Online Registration</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <link href="/css/style2008.css" rel="stylesheet" type="text/css" title="KFT" />
<script language="javascript">
function goBack() {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = "back";
	document.form1.submit();
}
function goNext() {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = "next";
	document.form1.submit();
}
function jumpto(target,direction) {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = direction;
	document.getElementById('target').value = target;
	document.form1.submit();
}
</script>
<script language="javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>
</head>
<body onload="MM_preloadImages('images/loader.gif')">


<div id="container">
  <div id="top">
    <? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
  </div>
	<br clear="all" />
<div id="navi"><? $seite = "8"; include($_SERVER['DOCUMENT_ROOT'].'/includes/navigation.inc.php'); ?><div id="loader" style="display:none"><img src="/images/loader.gif" style="margin-top: 8px; margin-left:45px" width="32" height="32" /></div></div>
  <div id="leftSide">
<fieldset>
<p class="legend"><?=$_SESSION['Leg_8']?> 8 / 14: <?=$_SESSION['Leg_127']?></p>
	<? if (($step == "2") && ($error != "")) { ?>	
	<p style="border:1px solid #990000; background-color:#FFDCD6; padding:5px; width: 545px"><?=$error; ?></p>
	<? } ?>
	<? echo (($_SESSION['Leg_107'] != "") && ($_SESSION['maxminutes'] == "59")) ? '<p class="pagenote">'.$_SESSION['Leg_107'].'</p>' : ""; ?>
	<? echo (($_SESSION['Leg_281'] != "") && ($_SESSION['maxminutes'] == "300")) ? '<p class="pagenote">'.$_SESSION['Leg_281'].'</p>' : ""; ?>
<form action="<?=$_SERVER['PHP_SELF']?>" method="post" name="form1">
<input type="hidden" value="2" name="step_8" />
<input type="hidden" name="direction" id="direction" value="" />
<input type="hidden" name="target" id="target" value="" />
	<!-- p><?=$_SESSION['Leg_73']?></p -->
<?
$q = FX_open_layout( "w_25__4sprachen", "999");
$q->AddDBParam('_kp__Sprache_ISO_id', ">0");
$DBData = $q->FMFind();  
foreach ($DBData['data'] as $key => $value) {
	$locale = $value['_kp__Sprache_ISO_id'][0];
	$lang = $value['Sprache_'.$_SESSION['sprache']][0];
	?>
	<label for="name"><?=$_SESSION['Leg_15']?><br /><?=$lang ?></label>
	<div class="div_blankbox"><textarea name="Synop_<?=$locale ?>" rows="10" class="textbox"><? $var = 'Synop_'.$locale; echo htmlspecialchars($$var); ?></textarea>
	</div>
	<br clear="all" />
	<?	
	if (in_array($locale, array('de','fr'))) {
		?>
		<label for="Logline_<?=$locale?>"><?= $_SESSION['Leg_362'] ?><br /><?=$value['Sprache_'.$_SESSION['sprache']][0] ?></label>
		<div class="div_blankbox"><textarea name="Logline_<?=$locale?>" rows="3" class="textbox"><? $var = 'Logline_'.$locale; echo htmlspecialchars($$var); ?></textarea>
		</div>
		<br clear="all" />
		<?php 
	}
}
?>	
	
	<br clear="all" />
	<div class="prevBtn"><input type="button" id="back" value="<?=$_SESSION['Leg_31']?>" onclick="goBack('')" /></div>
	<div class="nxtBtn"><input type="button" id="next" value="<?=$_SESSION['Leg_32']?>" onclick="goNext('')" /></div>
	<br clear="all" />
</form>
<div class="clear"></div>
</fieldset>

  </div>
	<br clear="all" />
  <div class="clear"></div>
</div>


<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>

</body>
</html>
