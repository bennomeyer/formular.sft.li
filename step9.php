<?
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
session_start();

if (!isset($_SESSION['Leg_1'])) {
	
	// redirect zur�ck zur Startseite wg. abgelaufener Session
	header("Location: /index.php?error=Session%20abgelaufen");
	exit;
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/classes/FX/FX.php"); 

$step = (isset($_POST['step_9'])) ? $_POST['step_9'] : "";

$Budget = (isset($_POST['Budget'])) ? $_POST['Budget'] : "";
$Anteil = (isset($_POST['Anteil'])) ? $_POST['Anteil'] : "";
$Finanzkategorie = (!empty($_POST['Finanzkategorie'])) ? $_POST['Finanzkategorie'] : array();
$Betrag = (!empty($_POST['Betrag'])) ? $_POST['Betrag'] : array();

$error = "";
if ($step == "2") {
	//if ((empty($_POST['Sprache'])) || (empty($_POST['Filmformat'])) || (empty($_POST['Bildformat'])) || (empty($_POST['Tonformat'])) ) $error .= $_SESSION['Leg_92'];
} 



if (($error == "") && ($step == "2")) {
	
	//Update im Film-Eintrag
	$q = new FX('host8.kon5.net', '80', 'FMPro7');
	$q->SetDBData("Filmtage-Solothurn", "cgi_h_02__filme", "1"); 
	$q->SetDBUserPass ("Admin", "xs4kon5");
	$q->AddDBParam('-recid', $_SESSION['record_id']); 
	$q->AddDBParam('Budget', $Budget);
	$q->AddDBParam('CH_Finanzierungsanteil_Prozent', $Anteil);
	$DBData = $q->FMEdit(); 		
	
	
	//Eintrag der Kat/Betrag-Kreuz...
	if (isset($Finanzkategorie[0])) {
		$i = 0;
		foreach ($Finanzkategorie as $finanzkategorie_id) { 
			$finanzkategorie = explode("|", $Finanzkategorie[$i]);
	
			$q = new FX('host8.kon5.net', '80', 'FMPro7');
			$q->SetDBData("Filmtage-Solothurn", "cgi_k_27__filme_finanzierung", "1"); 
			$q->SetDBUserPass ("Admin", "xs4kon5");
			$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);	
			$q->AddDBParam('_kf__finanzierungskategorie_Id', $finanzkategorie[0]);	
			$q->AddDBParam('Betrag', $Betrag[$i]);	
			$DBData = $q->FMNew();
			$i++;		
		}
	}	
	// redirect zur n�chsten Seite
	header("Location: /step10.php");
	exit;
}

include($_SERVER['DOCUMENT_ROOT']. "/includes/x_get_value_list9.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Online Registration</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 <link href="/css/style.css" rel="stylesheet" type="text/css" title="KFT" />
 
<script type="text/javascript" src="includes/scripts.js"></script>
<script type="text/javascript" language="javascript">
<!--
	var ol_width = 300;
	var ol_fgcolor='#d9e2ec';
	var ol_bgcolor='#006699';
//-->
</script>
<link rel="stylesheet" type="text/css" href="includes/yui/container/assets/container.css">
<script type="text/javascript" src="includes/yui/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript" src="includes/yui/animation/animation-min.js"></script>
<script type="text/javascript" src="includes/yui/dragdrop/dragdrop-min.js"></script>
<script type="text/javascript" src="includes/yui/connection/connection.js"></script>
<script type="text/javascript" src="includes/yui/container/container.js"></script>
<script language="javascript">
var finanzkategorien = new Array;
var betraege = new Array;

function addEntry() {
	if 	(
		(document.getElementById('Finanzkategorie_select').value != "") &&
		(document.getElementById('Betrag_select').value != "")
		) {
		finanzkategorien.push(document.getElementById('Finanzkategorie_select').value);
		document.getElementById('Finanzkategorie_select').value = "";
		betraege.push(document.getElementById('Betrag_select').value);
		document.getElementById('Betrag_select').value = "";
		showEntries();		
	} else {
		miniDialog = new YAHOO.widget.SimpleDialog("mini_dlg", { visible:false, width: "25em", effect:[
{effect:YAHOO.widget.ContainerEffect.SLIDE,duration:0.25}, {effect:YAHOO.widget.ContainerEffect.FADE,duration:0.25}
], fixedcenter : true, modal:true, draggable:false });
		miniDialog.setHeader("<?=$_SESSION['Leg_91']?>");
		miniDialog.setBody("<?=$_SESSION['Leg_95']?>");
		miniDialog.cfg.queueProperty("icon",YAHOO.widget.SimpleDialog.ICON_WARN);
		miniDialog.cfg.queueProperty("buttons", [ 
			{ text: "<?=$_SESSION['Leg_93']?>", handler:handlerHide, isDefault:true  }
		]);
		miniDialog.render(document.body);
		miniDialog.show();
	}
}
function delEntry(i) {
	finanzkategorien.splice(i,1);
	betraege.splice(i,1);
	showEntries();	
}
function showEntries() {
	var output = "";
	for (var i = 0; i < finanzkategorien.length; ++i) {
		var finanzkategorie = finanzkategorien[i].split("|");
 		output += "<input type=\"hidden\" name=\"Finanzkategorie[]\" value=\"" + finanzkategorien[i] + "\"><?=$_SESSION['Leg_98']?>: " + finanzkategorie[1] + "<br />\n";
 		output += "<input type=\"hidden\" name=\"Betrag[]\" value=\"" + betraege[i] + "\"><?=$_SESSION['Leg_97']?>: " + betraege[i] + "<br />\n";
 		output += "<img src=\"images/delete.png\" onClick=\"delEntry(" + i + ")\" align=\"absmiddle\"> <a href=\"javascript:void(0);\" onClick=\"delEntry(" + i + ")\"><?=$_SESSION['Leg_88']?></a><br />\n";
 		output += "<hr style=\"width: 330px; height: 2px\" noshade=\"noshade\" color=\"#FFFFFF\"><br />\n";
	}
	document.getElementById('Finanzkategorien').innerHTML = "<p>"+ output +"</p>";
	document.getElementById('Finanzkategorien').style.display = "block";
}
function checkEntry() {
	if 	(
		(document.getElementById('Finanzkategorie_select').value == "") &&
		(document.getElementById('Betrag_select').value == "") 
		) { 
			return true; 
		} else {
			return false;
		}
	
}
</script> 
</head>
<body>


<div id="container">
  <div id="top">
    <? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
	<? if (($error != "") && ($step == "2")) { ?>	
	<p style="border:1px solid #990000; background-color:#FFDCD6; padding:5px; width: 450px"><?=$error; ?></p>
	<? } ?>
  </div>
	<br clear="all" />
  <div id="leftSide">
<fieldset>
<legend><?=$_SESSION['Leg_8']?> 9 / 13: <?=$_SESSION['Leg_129']?></legend>
	<? echo ($_SESSION['Leg_109'] != "") ? '<p class="pagenote">'.$_SESSION['Leg_109'].'</p>' : ""; ?>
<form action="<?=$_SERVER['PHP_SELF']?>" method="post" name="form1" onsubmit="return checkEntry()">
<input type="hidden" value="2" name="step_9">

	<!-- p><?=$_SESSION['Leg_74']?></p -->
	<label for="name"><?=$_SESSION['Leg_16']?></label>
	<div class="div_blankbox">
		<?=$_SESSION['Leg_17']?> <input type="text" name="Budget" id="Budget" class="textbox_medium" /><br />&nbsp;<br />
		<?=$_SESSION['Leg_18']?> <input type="text" name="Anteil" class="textbox_short" id="Anteil" />
	</div>
	<br clear="all" />
	<label for="Finanzkategorien"><?=$_SESSION['Leg_19']?></label>	
	<div class="div_blankbox">
		<div style="float:left; width:90px; padding-right: 10px;"><?=$_SESSION['Leg_96']?></div>
		<div style="float:left;">
			<select name="Finanzkategorie_select" id="Finanzkategorie_select">
			<option value="">-</option>
			<?=$output_Finanzkategorie?>
			</select>
		</div>
		<br clear="all" />
		<br clear="all" />
		<div style="float:left; width:90px; padding-right: 10px;"><?=$_SESSION['Leg_97']?></div>
		<div style="float:left;">
			<input type="text" name="Betrag_select" class="textbox_medium" id="Betrag_select" />
		</div>
		<br clear="all" />
		<br clear="all" />
	<img src="images/add.png" alt="<?=$_SESSION['Leg_78']?>" width="16" height="16" align="absmiddle" onClick="addEntry()" title="<?=$_SESSION['Leg_78']?>"> <a href="#" onClick="addEntry()"><?=$_SESSION['Leg_78']?></a><br />
		<br />
		<hr style="width: 330px; height: 2px" noshade="noshade" color="#FFFFFF">
		<div id="Finanzkategorien"></div>
	</div>
	<div class="button_div">
	<input name="submit" type="submit" id="submit" value="<?=$_SESSION['Leg_32']?>">
	</div>
</form>
<div class="clear"></div>
</fieldset>

  </div>
  <div class="clear"></div>
</div>


<? if (($step == "2") && isset($Produktionsland[0])) {
echo '<script language="javascript">'."\n";
echo 'laender.push("'.implode("\",\"", $Produktionsland).'");'."\n";
echo 'showLaender();'."\n";
echo '</script>'."\n";
}
?>
<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>
</body>
</html>
