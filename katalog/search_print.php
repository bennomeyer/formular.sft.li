<?
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
session_start();
//require_once($_SERVER['DOCUMENT_ROOT'] . "/classes/FX/FX.php"); 
//include($_SERVER['DOCUMENT_ROOT']. "/includes/suchmaske.inc.php");
$id = (isset($_REQUEST['id'])) ? $_REQUEST['id'] : "";


require_once (__DIR__.'/../includes/db.inc.php');

$find =& $fm->newFindCommand('cgi__Katalog_Einzel');  
$find->addFindCriterion('__kp__id', $id); 
$result = $find->execute(); 
//echo $foundrec;

if (FileMaker::isError($result)) {
	echo '<div align="center"><h1 style="font-family: Arial, Helvetica; font-size:14px;">Sorry, there there is an error - please try your search again.</h1></div>';
	exit;
}
$records = $result->getRecords(); 
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Katalog</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <link href="/css/style.css" rel="stylesheet" type="text/css" title="KFT" />
<script language="javascript">
function loader() {
	print();
	self.close();
}
</script>
</head>
<body onload="loader()">


<div id="container">
  <div id="top">
    <? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
  </div>
	<br clear="all" />
  <div id="leftSide">
<?

foreach ($records as $record) {
?>
<h1 style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; margin-top: 4px;"><?=$record->getField('zz_Onlinekatalog_Filmtitel')?></h1>
<h3 style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; margin-top: 4px;"><?=$record->getField('zz_Onlinekatalog_Filmtitel_Zusatz')?></h3>
<table width="500" cellpadding="2" cellspacing="0" border="0">
<tr>
<td width="186" valign="top"><p><span style="font-size:10px;"><?=nl2br($record->getField('zz_Onlinekatalog_Stabspalte'))?></span></p></td>
<td width="8" valign="top">&nbsp;</td>
<td width="304" valign="top"><p><img src="/bilder/<?=$record->getField('zz_Onlinekatalog_Filmstillnummer')?>_medium.jpg" /><br /><br /><?=nl2br($record->getField('zz_Onlinekatalog_Synopsenspalte'))?></p></td>
</tr>
</table>
<?
	}
?>
  
  

  </div>
  <div class="clear"></div>
</div>


<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>

</body>
</html>
