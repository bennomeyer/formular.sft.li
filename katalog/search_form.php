<?
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
session_start();

require_once (__DIR__.'/../includes/db.inc.php');

if (empty($_SESSION['Leg_1'])) {
	read_labels('de');
}


$layout = $fm->getLayout('cgi__Katalog_Suche');
$datumsliste = $layout->getValueList('FILME: Programmdaten');
$zeiten = $layout->getValueList('PROGRAMMBLOECKE: Zeiten');
$kinos = $layout->getValueList('ONLINEKATALOG: Kino');

if (!FileMaker::isError($datumsliste)) {
	$output_datum = "";
	foreach ($datumsliste as $datum) {		
		$datum_array = explode("/", $datum);
		$output_datum .= ($datum_array[2] == $_SESSION['jahr']) ? '<option value="'.$datum.'">'.$datum_array[1].'.'.$datum_array[0].'.'.$datum_array[2].'</option>'."\n" : "";
	}
}
if (!FileMaker::isError($zeiten)) {
	$output_zeit = "";
	foreach ($zeiten as $zeit) {		
		$zeit_array = explode(":", $zeit);
		$output_zeit .= '<option value="'.$zeit.'">'.$zeit_array[0].':'.$zeit_array[1].'</option>'."\n";
	}
}
if (!FileMaker::isError($kinos)) {
	$output_kino = "";
	foreach ($kinos as $kino) {		
		$output_kino .= '<option value="'.$kino.'">'.$kino.'</option>'."\n";
	}
}


$find_genre =& $fm->newFindCommand('w_10__genres'); 
$find_genre->addFindCriterion('Genre_'.$_SESSION['sprache'], ">0"); 
$result_genre = $find_genre->execute(); 
if (!FileMaker::isError($result_genre)) {
$records_genre = $result_genre->getRecords(); 
	$output_genre = "";
	foreach ($records_genre as $record_genre) {
		$output_genre .= '<option value="'.$record_genre->getField('_kp__id').'">'.$record_genre->getField('Genre_'.$_SESSION['sprache']).'</option>'."\n";
	}
}


$find_sektion =& $fm->newFindCommand('cgi_w__Sektion'); 
$find_sektion->addFindCriterion('Sektion_'.$_SESSION['sprache'], ">0"); 
$result_sektion = $find_sektion->execute(); 
if (!FileMaker::isError($result_sektion)) {
$records_sektion = $result_sektion->getRecords(); 
	$output_sektion = "";
	foreach ($records_sektion as $record_sektion) {
		$output_sektion .= '<option value="'.$record_sektion->getField('_kp__id').'">'.$record_sektion->getField('Sektion_'.$_SESSION['sprache']).'</option>'."\n";
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Katalog</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link href="/css/style_katalog.css" rel="stylesheet" type="text/css" title="KFT" />

</head>
<body>


<div id="container">
  <div id="top">
  </div>
	<br clear="all" />
  <div id="leftSide">
<fieldset>
<legend><?=$_SESSION['Leg_200']?></legend>
<form action="search_list.php" method="get" name="form1">
<input type="hidden" value="2" name="step">

	<label for="name"><?=$_SESSION['Leg_1']?></label>
    <div class="div_texbox">
    <input name="filmtitel" type="text" class="textbox" id="filmtitel" />
	</div>
	<br clear="all">

	<label for="name"><?=$_SESSION['Leg_9']?></label>
    <div class="div_texbox">
    <input name="regie" type="text" class="textbox" id="regie" />
	</div>
	<br clear="all">
	
	<label for="Genre"><?=$_SESSION['Leg_2']?></label>
	<div class="div_blankbox">
    <select name="genre" id="genre_select">
	<option value="">-</option>
	<?=$output_genre?>
	</select>
	</div>
	<br clear="all">
	
	<label for="Datum"><?=$_SESSION['Leg_201']?></label>
	<div class="div_blankbox">
    <select name="datum">
	<option value="">-</option>
	<?=$output_datum?>
	</select>
	</div>
	<br clear="all">
	
	<label for="Zeit"><?=$_SESSION['Leg_202']?></label>
	<div class="div_blankbox">
    <?=$_SESSION['Leg_203']?> <select name="zeit">
	<option value="">-</option>
	<?=$output_zeit?>
	</select>
	</div>
	<br clear="all">
	
	<label for="Kinos"><?=$_SESSION['Leg_204']?></label>
    <div class="div_texbox">
    <select name="kino">
	<option value="">-</option>
	<?=$output_kino?>
	</select>
	</div>
	<br clear="all">
	
	<label for="Sektion"><?=$_SESSION['Leg_205']?></label>
    <div class="div_texbox">
    <select name="sektion">
	<option value="">-</option>
	<?=$output_sektion?>
	</select>
	</div>
	<br clear="all">
	
	<div class="button_div" style="text-align:right; padding-right:0px; font-size:14px;">
	<input name="submit" type="submit" id="submit" value="<?=$_SESSION['Leg_100']?>">
	</div>
</form>
<div class="clear"></div>
</fieldset>

  </div>
  <div class="clear"></div>
</div>



</body>
</html>
