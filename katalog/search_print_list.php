<?
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
session_start();
//require_once($_SERVER['DOCUMENT_ROOT'] . "/classes/FX/FX.php"); 
//include($_SERVER['DOCUMENT_ROOT']. "/includes/suchmaske.inc.php");
$filmtitel = (!empty($_GET['filmtitel'])) ? $_GET['filmtitel'] : "";
$regie = (!empty($_GET['regie'])) ? $_GET['regie'] : "";
$genre = (!empty($_GET['genre'])) ? $_GET['genre'] : "";
$datum = (!empty($_GET['datum'])) ? $_GET['datum'] : "";
$zeit = (!empty($_GET['zeit'])) ? $_GET['zeit'] : "07:45:00";
$kino = (!empty($_GET['kino'])) ? $_GET['kino'] : "";
$sektion = (!empty($_GET['sektion'])) ? $_GET['sektion'] : "";
$foundrec = 0;

require_once (__DIR__.'/../includes/db.inc.php');

$find =& $fm->newFindCommand('cgi__Katalog_Suche');  
$find->addFindCriterion('zz_Onlinekatalog_Filmtitel', $filmtitel); 
$find->addFindCriterion('zz_Onlinekatalog_Regie', $regie); 
$find->addFindCriterion('zz_Onlinekatalog_Genre', $genre); 
$find->addFindCriterion('zz_Onlinekatalog_Datum', $datum); 
$find->addFindCriterion('zz_Zeit', '>'.$zeit); 
$find->addFindCriterion('zz_Onlinekatalog_Kino', $kino); 
$find->addFindCriterion('zz_OnlineKatalog_Sektion', $sektion); 
$find->addSortRule('zz_Onlinekatalog_Datum', 1, FILEMAKER_SORT_ASCEND); 
$find->addSortRule('zz_Zeit', 2, FILEMAKER_SORT_ASCEND); 
$result = $find->execute(); 
//echo $foundrec;

if (!FileMaker::isError($result)) {
	$records = $result->getRecords(); 
	$foundrec = $result->getFoundSetCount();
	$linkparameter = '&filmtitel='.$filmtitel.'&regie='.$regie.'&genre='.$genre.'&datum='.$datum.'&zeit='.$zeit.'&kino='.$kino.'&sektion='.$sektion;
} else {
echo $result->getMessage();
	echo '<div align="center"><h1>Sorry, there were no results - please try your search again.</h1></div>';
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Katalog</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <link href="/css/style.css" rel="stylesheet" type="text/css" title="KFT" />
<script language="javascript">
function loader() {
	print();
	self.close();
}
</script>
<style type="text/css">
a {
	color:#333333;
	text-decoration:none;
}
a:hover {
	text-decoration:underline;
}
</style>
</head>
<body onload="loader()">


<div id="container">
  <div id="top">
    <? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
  </div>
	<br clear="all" />
  <div id="leftSide">
<?

if ($foundrec >0) {	
	if (FileMaker::isError($result)) {
		echo "<body>Error: " . $result->getMessage(). "</body>";
		exit;
	}
	
	
foreach ($records as $record) {
?>
<table width="500" cellpadding="2" cellspacing="0" border="0" style="border-top: 1px solid #CCCCCC;">
<tr>
<td width="110" valign="top"><p><a href="search_detail.php?id=<?=$record->getField('__kp__id')?>"><img src="/bilder/<?=$record->getField('zz_Onlinekatalog_Filmstillnummer')?>_small.jpg" border="0" /></a></p></td>
<td width="190" valign="top"><p><span style="font-size:12px; font-weight:bold;"><a href="search_detail.php?id=<?=$record->getField('__kp__id')?>" style="text-decoration:none; color:#000000"><?=$record->getField('zz_Onlinekatalog_Filmtitel')?></a></span><br />
<span style="font-size:11px;"><?=$record->getField('zz_Onlinekatalog_Regie')?><br />
<?=$record->getField('zz_Onlinekatalog_Infozeile')?></span></p></td>
<td width="10">&nbsp;</td>
<td width="190" valign="top"><p><span style="font-size:11px; font-weight:bold;"><?=$record->getField('zz_OnlineKatalog_Hauptzeit')?></span><br />
<span style="font-size:9px;"><?=nl2br($record->getField('zz_Onlinekatalog_AndereZeiten'))?></span></p></td>
</tr>
</table>
<?
	}
}	
?>
  
  

  </div>
  <div class="clear"></div>
</div>


<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>

</body>
</html>
