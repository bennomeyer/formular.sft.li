<?
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
session_start();



if (empty($_SESSION['Leg_1'])) {
	require_once (__DIR__.'/../includes/db.inc.php');
	read_labels($sprache);	
}

//require_once($_SERVER['DOCUMENT_ROOT'] . "/classes/FX/FX.php"); 
//include($_SERVER['DOCUMENT_ROOT']. "/includes/suchmaske.inc.php");
$filmtitel = (!empty($_GET['filmtitel'])) ? $_GET['filmtitel'] : "";
$regie = (!empty($_GET['regie'])) ? $_GET['regie'] : "";
$genre = (!empty($_GET['genre'])) ? $_GET['genre'] : "";
$datum = (!empty($_GET['datum'])) ? $_GET['datum'] : "";
$zeit = (!empty($_GET['zeit'])) ? $_GET['zeit'] : "07:45:00";
$kino = (!empty($_GET['kino'])) ? $_GET['kino'] : "";
$sektion = (!empty($_GET['sektion'])) ? $_GET['sektion'] : "";
$step = (!empty($_GET['step'])) ? $_GET['step'] : "1";
$start = (!empty($_GET['start'])) ? $_GET['start'] : 0;
$skip = 5;
$foundrec = 0;
$error = "";


$find =& $fm->newCompoundFindCommand('cgi__Katalog_Suche'); 
$findreq1 =& $fm->newFindRequest('cgi__Katalog_Suche');  
$findreq2 =& $fm->newFindRequest('cgi__Katalog_Suche');

$findreq1->addFindCriterion('zz_Onlinekatalog_Filmtitel', $filmtitel); 
$findreq1->addFindCriterion('zz_Onlinekatalog_Regie', $regie); 
$findreq1->addFindCriterion('zz_Onlinekatalog_Genre', $genre); 
$findreq1->addFindCriterion('zz_Onlinekatalog_Datum', $datum); 
$findreq1->addFindCriterion('zz_Zeit', '>='.$zeit); 
$findreq1->addFindCriterion('zz_Onlinekatalog_Kino', $kino); 
$findreq1->addFindCriterion('zz_OnlineKatalog_Sektion', $sektion); 
$findreq2->addFindCriterion('zz_OnlineKatalog_Ausschluss', '1'); 
$findreq2->setOmit(true); 
$find->add(1,$findreq1); 
//$find->add(2,$findreq2); 

$find->addSortRule('zz_Onlinekatalog_Datum', 1, FILEMAKER_SORT_ASCEND); 
$find->addSortRule('zz_Zeit', 2, FILEMAKER_SORT_ASCEND);  
$find->addSortRule('zz_Onlinekatalog_Kino', 3, FILEMAKER_SORT_ASCEND);  
$find->addSortRule('Reihenfolge', 4, FILEMAKER_SORT_ASCEND); 
$find->setRange($start, $skip);
$result = $find->execute(); 
//echo $foundrec;

if (FileMaker::isError($result)) {
 echo $result->getMessage();
}
if (!FileMaker::isError($result)) {
	$records = $result->getRecords(); 
	$foundrec = $result->getFoundSetCount();
	$linkparameter = '&filmtitel='.$filmtitel.'&regie='.$regie.'&genre='.$genre.'&datum='.$datum.'&zeit='.$zeit.'&kino='.$kino.'&sektion='.$sektion.'&PHPSESSID='.session_id();
} else {
	//echo $result->getMessage();
	$error = '<p>&nbsp;</p><div align="center"><h1>'.$_SESSION['Leg_209'].'</h1></div>';
	$error .=  '<p>&nbsp;</p><div align="center"><a href="search_form.php?PHPSESSID='.session_id().'" style="color:#999999; text-decoration:none; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;">'.$_SESSION['Leg_207'].'</a></div>';
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Katalog</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link href="/css/style_katalog.css" rel="stylesheet" type="text/css" title="KFT" />
<style type="text/css">
a {
	color:#333333;
	text-decoration:none;
}
a:hover {
	text-decoration:underline;
}
</style>
</head>
<body>
<?
if ($error != "") {
 echo $error;
 echo '</body></html>';
 exit;
}
?>

<div id="container">
  <div id="top">
    <? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
  </div>
	<br clear="all" />
  <div id="leftSide">
  <div style="width:500px; color:#999999; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; text-align:center; margin-bottom: 8px;">
  <a href="javascript:void()" onclick="javascript:window.open('search_print_list.php?print=list<?=$linkparameter?>&PHPSESSID=<?=session_id()?>','Printversion','width=560,height=300,scrollbars=yes'); return false;" style="color:#999999; text-decoration:none"><?=$_SESSION['Leg_206']?></a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<a href="search_form.php?PHPSESSID=<?=session_id()?>" style="color:#999999; text-decoration:none"><?=$_SESSION['Leg_207']?></a>
  </div>
<?

if ($foundrec >0) {	
	if (FileMaker::isError($result)) {
		echo "<body>Error: " . $result->getMessage(). "</body>";
		exit;
	}
	
	
foreach ($records as $record) {
?>
<table width="500" cellpadding="2" cellspacing="0" border="0" style="border-top: 1px solid #CCCCCC;">
<tr>
<td width="110" valign="top"><p><a href="search_detail.php?id=<?=$record->getField('__kp__id')?>&PHPSESSID=<?=session_id()?>"><img src="/bilder/<?=$record->getField('zz_Onlinekatalog_Filmstillnummer')?>_small.jpg" border="0" /></a></p></td>
<td width="190" valign="top"><p><span style="font-size:12px; font-weight:bold;"><a href="search_detail.php?id=<?=$record->getField('__kp__id')?>&PHPSESSID=<?=session_id()?>" style="text-decoration:none; color:#000000"><?=$record->getField('zz_Onlinekatalog_Filmtitel')?></a></span><br />
<span style="font-size:11px;"><?=$record->getField('zz_Onlinekatalog_Regie')?><br />
<?=$record->getField('zz_Onlinekatalog_Infozeile')?></span></p></td>
<td width="10">&nbsp;</td>
<td width="190" valign="top"><p><span style="font-size:11px; font-weight:bold;"><?=$record->getField('zz_OnlineKatalog_Hauptzeit')?></span><br />
<span style="font-size:9px;"><?=nl2br($record->getField('zz_Onlinekatalog_AndereZeiten'))?></span></p></td>
</tr>
</table>
<?
	}
	echo '<p><br><br>';
	$view_start = $start+1;
	$view_end = $start+$skip;
	echo 'Found: '.$foundrec.' Films - Showing: '.$view_start.' to '.$view_end; 
	
	echo '<br><br>';
	
	$seite = floor(($start+$skip)/$skip);
	
	$anzahl_seiten = ceil($foundrec / $skip);

	if($start > 1) {
		$zurueck = $start - $skip;
		echo '<a href="'.$_SERVER['PHP_SELF'].'?start='.$zurueck.$linkparameter.'">< Back</a>&nbsp;';
	}
	$n = 0;
	echo 'Page ';
	for($i = 1; $i <= $anzahl_seiten; $i++) {
		echo ($seite == $i) ? ' <b>'.$i.'</b> ' : ' <a href="'.$_SERVER['PHP_SELF'].'?start='.$n.$linkparameter.'">'.$i.'</a> ';
		$n += $skip;
	}
	$weiter = $start + $skip;
	if($foundrec > $weiter) {
	echo '<a href="'.$_SERVER['PHP_SELF'].'?start='.$weiter.$linkparameter.'">Next ></a>';
	}
	echo '</p>';
}	
?>
  
  

  </div>
  <div class="clear"></div>
</div>


<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>

</body>
</html>
