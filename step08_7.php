<?
session_start();
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
if ((!$_SESSION['login_ok']) || (($_SESSION['film_id'] == "") && ($_SESSION['m'] == "u"))) {
	header("Location: login.php");
	exit;
}


require_once ('includes/db.inc.php');
$q = FX_open_layout( "cgi_h_02__filme", "1"); 

$step = (isset($_POST['step7'])) ? $_POST['step7'] : "";
$direction = (isset($_POST['direction'])) ? $_POST['direction'] : "";
$target = (isset($_POST['target'])) ? $_POST['target'] : "";
$Swissperform = $_POST['Swissperform'];
$CastingListeFileName = $_POST['CastingListeFileName'];

function getPostArray($name){
	global $$name;
	if (in_array($name, array_keys($_POST))) {
	$$name = ((empty($_POST[$name]) ? array() : $_POST[$name]));
	} else {
		$$name = array();
	}
}
// von 7 Bereichen jeweils die Array Ganz, Vorname und Name aus POST holen 
// die Variablen $BuGanz etc. werden gesetzt
foreach(array('Bu','Ka','Mo','To','Mu','Da','Ca') as $name) {
	getPostArray($name.'Ganz');
	getPostArray($name.'Vorname');
	getPostArray($name.'Name');
}


// UPDATE init - alte Daten aus DB ziehen
//__________________________________________________

if (($_SESSION['m'] == "u") && ($step != "2")) {
	
	// Swissperform aus film auslesen!
	$find =& $fm->newFindCommand('cgi_h_02__filme'); 
	$find->addFindCriterion('_kp__id', $_SESSION['film_id']); 
	$find->addFindCriterion('_kf__Anmeldeuser_Id', $_SESSION['user_id']); 
	$result = $find->execute(); 
	if (!FileMaker::isError($result)) {
		$records = $result->getRecords(); 
		$record = $records[0];
		$Swissperform = $record->getField('Swissperform');
		$CastingListeFileName = $record->getField('CastingListeFileName');
	}
	
	$find =& $fm->newFindCommand('cgi_k_Stabmitglieder'); 
	$find->addFindCriterion('_kf__Film_Id', $_SESSION['film_id']); 
	$result = $find->execute(); 
	if (!FileMaker::isError($result)) {
		$records = $result->getRecords(); 
		foreach ($records as $record) {
			// Stabdaten "Buch" holen
			if ($record->getField('_kf__Stabbezeichnung_Id') == "2") {		
				$BuGanz[] = htmlspecialchars($record->getField('Vorname').'||'.$record->getField('Name'));
				$BuVorname[] = htmlspecialchars($record->getField('Vorname'));
				$BuName[] = htmlspecialchars($record->getField('Name'));
			}
			// Stabdaten "Kamera" holen
			if ($record->getField('_kf__Stabbezeichnung_Id') == "3") {		
				$KaGanz[] = htmlspecialchars($record->getField('Vorname').'||'.$record->getField('Name'));
				$KaVorname[] = htmlspecialchars($record->getField('Vorname'));
				$KaName[] = htmlspecialchars($record->getField('Name'));
			}
			// Stabdaten "Montage" holen
			if ($record->getField('_kf__Stabbezeichnung_Id') == "4") {		
				$MoGanz[] = htmlspecialchars($record->getField('Vorname').'||'.$record->getField('Name'));
				$MoVorname[] = htmlspecialchars($record->getField('Vorname'));
				$MoName[] = htmlspecialchars($record->getField('Name'));
			}
			// Stabdaten "Ton" holen
			if ($record->getField('_kf__Stabbezeichnung_Id') == "5") {		
				$ToGanz[] = htmlspecialchars($record->getField('Vorname').'||'.$record->getField('Name'));
				$ToVorname[] = htmlspecialchars($record->getField('Vorname'));
				$ToName[] = htmlspecialchars($record->getField('Name'));
			}
			// Stabdaten "Musik" holen
			if ($record->getField('_kf__Stabbezeichnung_Id') == "6") {		
				$MuGanz[] = htmlspecialchars($record->getField('Vorname').'||'.$record->getField('Name'));
				$MuVorname[] = htmlspecialchars($record->getField('Vorname'));
				$MuName[] = htmlspecialchars($record->getField('Name'));
			}
			// Stabdaten "Darsteller" holen
			if ($record->getField('_kf__Stabbezeichnung_Id') == "7") {		
				$DaGanz[] = htmlspecialchars($record->getField('Vorname').'||'.$record->getField('Name'));
				$DaVorname[] = htmlspecialchars($record->getField('Vorname'));
				$DaName[] = htmlspecialchars($record->getField('Name'));
			}
			// Stabdaten "Casting" holen
			if ($record->getField('_kf__Stabbezeichnung_Id') == "9") {		
				$CaGanz[] = htmlspecialchars($record->getField('Vorname').'||'.$record->getField('Name'));
				$CaVorname[] = htmlspecialchars($record->getField('Vorname'));
				$CaName[] = htmlspecialchars($record->getField('Name'));
			}
		}	
	}
	
}


// ERROR-Handling
//__________________________________________________
$error = "";
if ($step == "2") {
	// save uploaded file
	if (!empty($_FILES['CastingList']) && ($_FILES['CastingList']['error'] == UPLOAD_ERR_OK)) {
		$destination = $_SERVER['DOCUMENT_ROOT']."/bilder/casting_lists/".$_SESSION['film_id']."_".basename($_FILES['CastingList']['name']);
		if (move_uploaded_file($_FILES['CastingList']['tmp_name'], $destination)) {
			// save filename
			$CastingListeFileName = basename($destination);
			$q->AddDBParam('CastingListeFileName', $CastingListeFileName);
		} else {
			$error .= "Error uploading file<br />\n";
			$Swissperform = 1;
		}
	}
 
	if ((!isset($BuVorname[0])) || (!isset($BuName[0])) || (!isset($KaVorname[0])) || (!isset($KaName[0])) || (!isset($MoVorname[0])) || (!isset($MoName[0])) || (!isset($ToVorname[0])) || (!isset($ToName[0])) ||
	// Falls Spielfilm müssen auch Darsteller gesetzt sein
		 ((1==$_SESSION['genre']) && (!isset($DaName[0]) || !isset($DaVorname[0]))) ||
		// Falls Swissperform muss auch das PDF vorhanden sein
		(($Swissperform > 0) && empty($CastingListeFileName)) 
		) {
		$error .= $_SESSION['Leg_106']."<br />";
	}
	

	if ($error != "") {
		// Update Seite2Flag mit 0
		$q->AddDBParam('-recid', $_SESSION['record_id']); 
		$q->AddDBParam('zz_Anmeldung_Seite07_Flag', "0");
		$DBData = $q->FMEdit(); 	
	} else {
		// Update Seite2Flag mit 1
		$q->AddDBParam('-recid', $_SESSION['record_id']); 
		$q->AddDBParam('zz_Anmeldung_Seite07_Flag', "1");
		$DBData = $q->FMEdit(); 	
	}
}

// UPDATE final - Daten aktualisieren
//__________________________________________________

if  ((($_SESSION['m'] == "u") && ($step == "2") && ($error == "")) ||
	(($_SESSION['m'] == "u") && ($step == "2") && ($direction == "back"))) {
	// Alle Infos aus Stabbezeichnungs-Tabelle löschen;
	if ($_SESSION['film_id'] != "") {
		$find =& $fm->newFindCommand('cgi_k_Stabmitglieder'); 
		$find->addFindCriterion('_kf__Film_Id', $_SESSION['film_id']); 
		$find->addFindCriterion('_kf__Stabbezeichnung_Id', '2...7'); 
		$result = $find->execute(); 
		if (!FileMaker::isError($result)) {
			$records = $result->getRecords(); 
			foreach ($records as $record) {
				$rec_ID = $record->getRecordId();
				$rec = $fm->getRecordById('cgi_k_Stabmitglieder', $rec_ID); 
				$rec->delete();
			}
		}
		// und nochmal für Casting (ohne 8 = Briefempfänger zu stören)
		$find =& $fm->newFindCommand('cgi_k_Stabmitglieder'); 
		$find->addFindCriterion('_kf__Film_Id', $_SESSION['film_id']); 
		$find->addFindCriterion('_kf__Stabbezeichnung_Id', '9'); 
		$result = $find->execute(); 
		if (!FileMaker::isError($result)) {
			$records = $result->getRecords(); 
			foreach ($records as $record) {
				$rec_ID = $record->getRecordId();
				$rec = $fm->getRecordById('cgi_k_Stabmitglieder', $rec_ID); 
				$rec->delete();
			}
		}
	}			
}



// DB-Actions für Neueintragung
//__________________________________________________
if ((($error == "") && ($step == "2") && ($direction == "next")) || (($step == "2") && ($direction == "back"))) {

	// Datenbank-Eintrag der einzelnen Stabmitglieder
		
	$q = FX_open_layout( "cgi_k_Stabmitglieder", "1"); 
	for ($i = 0 ; $i < count($BuName) ; $i++) {
		$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);
		$q->AddDBParam('_kf__Stabbezeichnung_Id', "2");	
		$q->AddDBParam('Vorname', htmlspecialchars_decode($BuVorname[$i]));	
		$q->AddDBParam('Name', htmlspecialchars_decode($BuName[$i]));
		$DBData = $q->FMNew(); 		
	}
	for ($i = 0 ; $i < count($KaName) ; $i++) {
		$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);
		$q->AddDBParam('_kf__Stabbezeichnung_Id', "3");	
		$q->AddDBParam('Vorname', htmlspecialchars_decode($KaVorname[$i]));	
		$q->AddDBParam('Name', htmlspecialchars_decode($KaName[$i]));
		$DBData = $q->FMNew(); 		
	}
	for ($i = 0 ; $i < count($MoName) ; $i++) {
		$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);
		$q->AddDBParam('_kf__Stabbezeichnung_Id', "4");	
		$q->AddDBParam('Vorname', htmlspecialchars_decode($MoVorname[$i]));	
		$q->AddDBParam('Name', htmlspecialchars_decode($MoName[$i]));
		$DBData = $q->FMNew(); 		
	}
	for ($i = 0 ; $i < count($ToName) ; $i++) {
		$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);
		$q->AddDBParam('_kf__Stabbezeichnung_Id', "5");	
		$q->AddDBParam('Vorname', htmlspecialchars_decode($ToVorname[$i]));	
		$q->AddDBParam('Name', htmlspecialchars_decode($ToName[$i]));
		$DBData = $q->FMNew(); 		
	}
	for ($i = 0 ; $i < count($MuName) ; $i++) {
		$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);
		$q->AddDBParam('_kf__Stabbezeichnung_Id', "6");	
		$q->AddDBParam('Vorname', htmlspecialchars_decode($MuVorname[$i]));	
		$q->AddDBParam('Name', htmlspecialchars_decode($MuName[$i]));
		$DBData = $q->FMNew(); 		
	}
	for ($i = 0 ; $i < count($DaName) ; $i++) {
		$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);
		$q->AddDBParam('_kf__Stabbezeichnung_Id', "7");	
		$q->AddDBParam('Vorname', htmlspecialchars_decode($DaVorname[$i]));	
		$q->AddDBParam('Name', htmlspecialchars_decode($DaName[$i]));
		$DBData = $q->FMNew(); 		
	}
	for ($i = 0 ; $i < count($CaName) ; $i++) {
		$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);
		$q->AddDBParam('_kf__Stabbezeichnung_Id', "9");	
		$q->AddDBParam('Vorname', htmlspecialchars_decode($CaVorname[$i]));	
		$q->AddDBParam('Name', htmlspecialchars_decode($CaName[$i]));
		$DBData = $q->FMNew(); 		
	}
}


if (($direction == "next") && ($step == "2") && ($error == "")) {
	if ($target != "") {
		header("Location: /$target");
	} else {
		//redirect zur nächsten Seite
		header("Location: /step08_8.php");
	}
	exit;
}
if (($direction == "back") && ($step == "2")) {
	if ($target != "") {
		header("Location: /$target");
	} else {
		//redirect zur vorherigen Seite
		header("Location: /step08_6.php");
	}
	exit;
}

include($_SERVER['DOCUMENT_ROOT']. "/includes/x_get_value_list2.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Online Registration</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <link href="/css/style2008.css" rel="stylesheet" type="text/css" title="KFT" />
<script type="text/javascript" src="includes/scripts.js"></script>
<link rel="stylesheet" type="text/css" href="includes/yui/container/assets/container.css" />
<script type="text/javascript" src="includes/yui/fileupload/yahoo-dom-event.js"></script>
<script type="text/javascript" src="includes/yui/animation/animation-min.js"></script>
<script type="text/javascript" src="includes/yui/dragdrop/dragdrop-min.js"></script>
<script type="text/javascript" src="includes/yui/fileupload/connection.js"></script>
<script type="text/javascript" src="includes/yui/fileupload/container.js"></script>
<script type="text/javascript" src="includes/yui/fileupload/Ext.js"></script>
<script type="text/javascript" src="includes/yui/fileupload/DomQuery.js"></script>
<script language="javascript">

function htmlspecialchars(str,typ) {
  if(typeof str=="undefined") str="";
  if(typeof typ!="number") typ=2;
  typ=Math.max(0,Math.min(3,parseInt(typ)));
  var from=new Array(/&/g,/</g,/>/g);
  var to=new Array("&amp;","&lt;","&gt;");
  if(typ==1 || typ==3) {from.push(/'/g); to.push("&#039;");}
  if(typ==2 || typ==3) {from.push(/"/g); to.push("&quot;");}
  for(var i in from) str=str.replace(from[i],to[i]);
  return str;
}
var bu = new Array;
function addBu() {
	if ((document.getElementById('BuVor').value != "") && (document.getElementById('BuNach').value != "")) {
		bu.push(document.getElementById('BuVor').value+'||' +document.getElementById('BuNach').value);
		document.getElementById('BuVor').value = "";
		document.getElementById('BuNach').value = "";
		showBu();		
	}
}
function delBu(i) { bu.splice(i,1);	showBu(); }
function showBu() {
	var output = "";
	for (var i = 0; i < bu.length; ++i) {
		var teil = bu[i].split("||");
 		output += "<div id=\"list_values\"><input type=\"hidden\" name=\"BuGanz[]\" value=\"" + htmlspecialchars(bu[i]) + "\"><input type=\"hidden\" name=\"BuVorname[]\" value=\"" + htmlspecialchars(teil[0]) + "\"><input type=\"hidden\" name=\"BuName[]\" value=\"" + htmlspecialchars(teil[1]) + "\">" + teil[0] + " " + teil[1] + "</div><div id=\"list_delicons\"><img src=\"images/delete.png\" onClick=\"delBu(" + i + ")\" align=\"absmiddle\" title=\"<?=$_SESSION['Leg_88']?>\"></div><br clear \"all\" />\n";
	}
	document.getElementById('Bu_div').innerHTML = output;
	document.getElementById('Bu_div').style.display = "block";
}


var ka = new Array;
function addKa() {
	if ((document.getElementById('KaVor').value != "") && (document.getElementById('KaNach').value != "")) {
		ka.push(document.getElementById('KaVor').value+'||' +document.getElementById('KaNach').value);
		document.getElementById('KaVor').value = "";
		document.getElementById('KaNach').value = "";
		showKa();		
	}
}
function delKa(i) { ka.splice(i,1);	showKa(); }
function showKa() {
	var output = "";
	for (var i = 0; i < ka.length; ++i) {
		var teil = ka[i].split("||");
 		output += "<div id=\"list_values\"><input type=\"hidden\" name=\"KaGanz[]\" value=\"" + htmlspecialchars(ka[i]) + "\"><input type=\"hidden\" name=\"KaVorname[]\" value=\"" + htmlspecialchars(teil[0]) + "\"><input type=\"hidden\" name=\"KaName[]\" value=\"" + htmlspecialchars(teil[1]) + "\">" + teil[0] + " " + teil[1] + "</div><div id=\"list_delicons\"><img src=\"images/delete.png\" onClick=\"delKa(" + i + ")\" align=\"absmiddle\" title=\"<?=$_SESSION['Leg_88']?>\"></div><br clear \"all\" />\n";
	}
	document.getElementById('Ka_div').innerHTML = output;
	document.getElementById('Ka_div').style.display = "block";
}


var mo = new Array;
function addMo() {
	if ((document.getElementById('MoVor').value != "") && (document.getElementById('MoNach').value != "")) {
		mo.push(document.getElementById('MoVor').value+'||' +document.getElementById('MoNach').value);
		document.getElementById('MoVor').value = "";
		document.getElementById('MoNach').value = "";
		showMo();		
	}
}
function delMo(i) { mo.splice(i,1);	showMo(); }
function showMo() {
	var output = "";
	for (var i = 0; i < mo.length; ++i) {
		var teil = mo[i].split("||");
 		output += "<div id=\"list_values\"><input type=\"hidden\" name=\"MoGanz[]\" value=\"" + htmlspecialchars(mo[i]) + "\"><input type=\"hidden\" name=\"MoVorname[]\" value=\"" + htmlspecialchars(teil[0]) + "\"><input type=\"hidden\" name=\"MoName[]\" value=\"" + htmlspecialchars(teil[1]) + "\">" + teil[0] + " " + teil[1] + "</div><div id=\"list_delicons\"><img src=\"images/delete.png\" onClick=\"delMo(" + i + ")\" align=\"absmiddle\" title=\"<?=$_SESSION['Leg_88']?>\"></div><br clear \"all\" />\n";
	}
	document.getElementById('Mo_div').innerHTML = output;
	document.getElementById('Mo_div').style.display = "block";
}


var to = new Array;
function addTo() {
	if ((document.getElementById('ToVor').value != "") && (document.getElementById('ToNach').value != "")) {
		to.push(document.getElementById('ToVor').value+'||' +document.getElementById('ToNach').value);
		document.getElementById('ToVor').value = "";
		document.getElementById('ToNach').value = "";
		showTo();		
	}
}
function delTo(i) { to.splice(i,1);	showTo(); }
function showTo() {
	var output = "";
	for (var i = 0; i < to.length; ++i) {
		var teil = to[i].split("||");
 		output += "<div id=\"list_values\"><input type=\"hidden\" name=\"ToGanz[]\" value=\"" + htmlspecialchars(to[i]) + "\"><input type=\"hidden\" name=\"ToVorname[]\" value=\"" + htmlspecialchars(teil[0]) + "\"><input type=\"hidden\" name=\"ToName[]\" value=\"" + htmlspecialchars(teil[1]) + "\">" + teil[0] + " " + teil[1] + "</div><div id=\"list_delicons\"><img src=\"images/delete.png\" onClick=\"delTo(" + i + ")\" align=\"absmiddle\" title=\"<?=$_SESSION['Leg_88']?>\"></div><br clear \"all\" />\n";
	}
	document.getElementById('To_div').innerHTML = output;
	document.getElementById('To_div').style.display = "block";
}

var mu = new Array;
function addMu() {
	if ((document.getElementById('MuVor').value != "") && (document.getElementById('MuNach').value != "")) {
		mu.push(document.getElementById('MuVor').value+'||' +document.getElementById('MuNach').value);
		document.getElementById('MuVor').value = "";
		document.getElementById('MuNach').value = "";
		showMu();		
	}
}
function delMu(i) { mu.splice(i,1);	showMu(); }
function showMu() {
	var output = "";
	for (var i = 0; i < mu.length; ++i) {
		var teil = mu[i].split("||");
 		output += "<div id=\"list_values\"><input type=\"hidden\" name=\"MuGanz[]\" value=\"" + htmlspecialchars(mu[i]) + "\"><input type=\"hidden\" name=\"MuVorname[]\" value=\"" + htmlspecialchars(teil[0]) + "\"><input type=\"hidden\" name=\"MuName[]\" value=\"" + htmlspecialchars(teil[1]) + "\">" + teil[0] + " " + teil[1] + "</div><div id=\"list_delicons\"><img src=\"images/delete.png\" onClick=\"delMu(" + i + ")\" align=\"absmiddle\" title=\"<?=$_SESSION['Leg_88']?>\"></div><br clear \"all\" />\n";
	}
	document.getElementById('Mu_div').innerHTML = output;
	document.getElementById('Mu_div').style.display = "block";
}


var da = new Array;
function addDa() {
	if ((document.getElementById('DaVor').value != "") && (document.getElementById('DaNach').value != "")) {
		da.push(document.getElementById('DaVor').value+'||' +document.getElementById('DaNach').value);
		document.getElementById('DaVor').value = "";
		document.getElementById('DaNach').value = "";
		showDa();		
	}
}
function delDa(i) { da.splice(i,1);	showDa(); }
function showDa() {
	var output = "";
	for (var i = 0; i < da.length; ++i) {
		var teil = da[i].split("||");
 		output += "<div id=\"list_values\"><input type=\"hidden\" name=\"DaGanz[]\" value=\"" + htmlspecialchars(da[i]) + "\"><input type=\"hidden\" name=\"DaVorname[]\" value=\"" + htmlspecialchars(teil[0]) + "\"><input type=\"hidden\" name=\"DaName[]\" value=\"" + htmlspecialchars(teil[1]) + "\">" + teil[0] + " " + teil[1] + "</div><div id=\"list_delicons\"><img src=\"images/delete.png\" onClick=\"delDa(" + i + ")\" align=\"absmiddle\" title=\"<?=$_SESSION['Leg_88']?>\"></div><br clear \"all\" />\n";
	}
	document.getElementById('Da_div').innerHTML = output;
	document.getElementById('Da_div').style.display = "block";
}


var ca = new Array;
function addCa() {
	if ((document.getElementById('CaVor').value != "") && (document.getElementById('CaNach').value != "")) {
		ca.push(document.getElementById('CaVor').value+'||' +document.getElementById('CaNach').value);
		document.getElementById('CaVor').value = "";
		document.getElementById('CaNach').value = "";
		showCa();		
	}
}
function delCa(i) { ca.splice(i,1);	showCa(); }
function showCa() {
	var output = "";
	for (var i = 0; i < ca.length; ++i) {
		var teil = ca[i].split("||");
 		output += "<div id=\"list_values\"><input type=\"hidden\" name=\"CaGanz[]\" value=\"" + htmlspecialchars(ca[i]) + "\"><input type=\"hidden\" name=\"CaVorname[]\" value=\"" + htmlspecialchars(teil[0]) + "\"><input type=\"hidden\" name=\"CaName[]\" value=\"" + htmlspecialchars(teil[1]) + "\">" + teil[0] + " " + teil[1] + "</div><div id=\"list_delicons\"><img src=\"images/delete.png\" onClick=\"delCa(" + i + ")\" align=\"absmiddle\" title=\"<?=$_SESSION['Leg_88']?>\"></div><br clear \"all\" />\n";
	}
	document.getElementById('Ca_div').innerHTML = output;
	document.getElementById('Ca_div').style.display = "block";
}
</script>	
<script language="javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>
</head>
<body onload="MM_preloadImages('images/loader.gif')">

<script type="text/javascript" src="includes/safari_hack.js"></script>
<script type="text/javascript" language="javascript">
<? include($_SERVER['DOCUMENT_ROOT'].'/includes_js/inits.js.php'); ?>	
function init() {
	<? include($_SERVER['DOCUMENT_ROOT'].'/includes_js/callbacks.js.php'); ?>
<? include($_SERVER['DOCUMENT_ROOT'].'/includes_js/newPerson_dialog.js.php'); ?>	
<? include($_SERVER['DOCUMENT_ROOT'].'/includes_js/addStab_dialog.js.php'); ?>	
}
<? include($_SERVER['DOCUMENT_ROOT'].'/includes_js/person_handler.js.php'); ?>	
<? include($_SERVER['DOCUMENT_ROOT'].'/includes_js/stab_handler.js.php'); ?>
function goBack() {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = "back";
	document.form1.submit();
}
function goNext() {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = "next";
	document.form1.submit();
}
function jumpto(target,direction) {
	document.getElementById('navi').innerHTML = '<?=$_SESSION['navi_passiv']?>';
	document.getElementById('loader').style.display = 'block';
	document.getElementById('direction').value = direction;
	document.getElementById('target').value = target;
	document.form1.submit();
}
</script>
<div id="container">
  <div id="top">
    <? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
  </div>
	<br clear="all" />
<div id="navi"><? $seite = "7"; include($_SERVER['DOCUMENT_ROOT'].'/includes/navigation.inc.php'); ?><div id="loader" style="display:none"><img src="/images/loader.gif" style="margin-top: 8px; margin-left:45px" width="32" height="32" /></div></div>
  <div id="leftSide">
<fieldset>
<p class="legend"><?=$_SESSION['Leg_8']?> 7 / 14: <?=$_SESSION['Leg_126']?></p>

	<? if ($error != "") { ?>	
	<p style="border:1px solid #990000; background-color:#FFDCD6; padding:5px; width: 545px"><?=$error; ?></p>
	<? } ?>
	
	<? echo ($_SESSION['Leg_106'] != "") ? '<p class="pagenote">'.$_SESSION['Leg_106'].'</p>' : ""; ?>
<form action="step08_7.php" method="post" name="form1" enctype="multipart/form-data" >
<input type="hidden" value="2" name="step7" />
<input type="hidden" name="direction" id="direction" value="" />
<input type="hidden" name="target" id="target" value="" />
	<label for="Bu"><?=$_SESSION['Leg_262']?>*</label>
	<div class="div_blankbox">
		<div id="select_div" style="float:left; width: 95px;">
			<?=$_SESSION['Leg_34']?><br />
			<input type="text" name="BuVor" id="BuVor" value="" style="width: 90px; font-size:11px" /> 
		</div>
		<div id="select_div" style="float:left; width: 115px;">
			<?=$_SESSION['Leg_35']?><br />
			<input type="text" name="BuNach" id="BuNach" value="" style="width: 90px; font-size:11px" />
			<img src="images/arrow.png" alt="<?=$_SESSION['Leg_78']?>" align="absmiddle" onclick="addBu()" title="<?=$_SESSION['Leg_78']?>" /><br />
		</div>
		<div id="Bu_div" class="list_div"> </div>
		<br clear="all" />
	</div>
	<br clear="all" />
	
	
	<label for="Ka"><?=$_SESSION['Leg_263']?>*</label>
	<div class="div_blankbox">
		<div id="select_div" style="float:left; width: 95px;">
			<?=$_SESSION['Leg_34']?><br />
			<input type="text" name="KaVor" id="KaVor" value="" style="width: 90px; font-size:11px" /> 
		</div>
		<div id="select_div" style="float:left; width: 115px;">
			<?=$_SESSION['Leg_35']?><br />
			<input type="text" name="KaNach" id="KaNach" value="" style="width: 90px; font-size:11px" />
			<img src="images/arrow.png" alt="<?=$_SESSION['Leg_78']?>" align="absmiddle" onclick="addKa()" title="<?=$_SESSION['Leg_78']?>" /><br />
		</div>
		<div id="Ka_div" class="list_div"> </div>
		<br clear="all" />
	</div>
	<br clear="all" />
	
	<label for="Mo"><?=$_SESSION['Leg_264']?>*</label>
	<div class="div_blankbox">
		<div id="select_div" style="float:left; width: 95px;">
			<?=$_SESSION['Leg_34']?><br />
			<input type="text" name="MoVor" id="MoVor" value="" style="width: 90px; font-size:11px" /> 
		</div>
		<div id="select_div" style="float:left; width: 115px;">
			<?=$_SESSION['Leg_35']?><br />
			<input type="text" name="MoNach" id="MoNach" value="" style="width: 90px; font-size:11px" />
			<img src="images/arrow.png" alt="<?=$_SESSION['Leg_78']?>" align="absmiddle" onclick="addMo()" title="<?=$_SESSION['Leg_78']?>" /><br />
		</div>
		<div id="Mo_div" class="list_div"> </div>
		<br clear="all" />
	</div>
	<br clear="all" />
	
	<label for="To"><?=$_SESSION['Leg_265']?>*</label>
	<div class="div_blankbox">
		<div id="select_div" style="float:left; width: 95px;">
			<?=$_SESSION['Leg_34']?><br />
			<input type="text" name="ToVor" id="ToVor" value="" style="width: 90px; font-size:11px" /> 
		</div>
		<div id="select_div" style="float:left; width: 115px;">
			<?=$_SESSION['Leg_35']?><br />
			<input type="text" name="ToNach" id="ToNach" value="" style="width: 90px; font-size:11px" />
			<img src="images/arrow.png" alt="<?=$_SESSION['Leg_78']?>" align="absmiddle" onclick="addTo()" title="<?=$_SESSION['Leg_78']?>" /><br />
		</div>
		<div id="To_div" class="list_div"> </div>
		<br clear="all" />
	</div>
	<br clear="all" />
	
	<label for="Mu"><?=$_SESSION['Leg_266']?></label>
	<div class="div_blankbox">
		<div id="select_div" style="float:left; width: 95px;">
			<?=$_SESSION['Leg_34']?><br />
			<input type="text" name="MuVor" id="MuVor" value="" style="width: 90px; font-size:11px" /> 
		</div>
		<div id="select_div" style="float:left; width: 115px;">
			<?=$_SESSION['Leg_35']?><br />
			<input type="text" name="MuNach" id="MuNach" value="" style="width: 90px; font-size:11px" />
			<img src="images/arrow.png" alt="<?=$_SESSION['Leg_78']?>" align="absmiddle" onclick="addMu()" title="<?=$_SESSION['Leg_78']?>" /><br />
		</div>
		<div id="Mu_div" class="list_div"> </div>
		<br clear="all" />
	</div>
	<br clear="all" />
	<?php if ($Swissperform > 0) {
	?>
	<p><?= $_SESSION['Leg_355'] ?></p>
	<?php } ?>
	
	<label for="Da"><?=$_SESSION['Leg_267']?></label>
	<div class="div_blankbox">
		<div id="select_div" style="float:left; width: 95px;">
			<?=$_SESSION['Leg_34']?><br />
			<input type="text" name="DaVor" id="DaVor" value="" style="width: 90px; font-size:11px" /> 
		</div>
		<div id="select_div" style="float:left; width: 115px;">
			<?=$_SESSION['Leg_35']?><br />
			<input type="text" name="DaNach" id="DaNach" value="" style="width: 90px; font-size:11px" />
			<img src="images/arrow.png" alt="<?=$_SESSION['Leg_78']?>" align="absmiddle" onclick="addDa()" title="<?=$_SESSION['Leg_78']?>" /><br />
		</div>
		<div id="Da_div" class="list_div"> </div>
		<br clear="all" />
	</div>
	
	<br clear="all" />
	<label for="Ca"><?=$_SESSION['Leg_357']?></label>
	<div class="div_blankbox">
		<div id="select_div" style="float:left; width: 95px;">
			<?=$_SESSION['Leg_34']?><br />
			<input type="text" name="CaVor" id="CaVor" value="" style="width: 90px; font-size:11px" /> 
		</div>
		<div id="select_div" style="float:left; width: 115px;">
			<?=$_SESSION['Leg_35']?><br />
			<input type="text" name="CaNach" id="CaNach" value="" style="width: 90px; font-size:11px" />
			<img src="images/arrow.png" alt="<?=$_SESSION['Leg_78']?>" align="absmiddle" onclick="addCa()" title="<?=$_SESSION['Leg_78']?>" /><br />
		</div>
		<div id="Ca_div" class="list_div"> </div>
		<br clear="all" />
	</div>
	<input type="hidden" name="Swissperform" value="<?= $Swissperform ?>" />
	<input type="hidden" name="CastingListeFileName" value="<?= $CastingListeFileName ?>" />
	
	<?php if ($Swissperform > 0) {
	?>
	
	<br clear="all" />
	<label for="CastingList"><?= $_SESSION['Leg_356'] ?></label>
	<div class="div_blankbox">
		<input name="CastingList" type="file" />
		<?= $CastingListeFileName ?>
	</div>
	<?php } ?>
	
	
	<br clear="all" />
	<div class="prevBtn"><input type="button" id="back" value="<?=$_SESSION['Leg_31']?>" onclick="goBack('')" /></div>
	<div class="nxtBtn"><input type="button" id="next" value="<?=$_SESSION['Leg_32']?>" onclick="goNext('')" /></div>
	<br clear="all" />
</form>

<div class="clear"></div>
</fieldset>

  </div>
	<br clear="all" />
  <div class="clear"></div>
</div>

<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>


<? 

if (($step == "2") || ($_SESSION['m'] == "u")) {
echo '<script language="javascript">'."\n";
if (isset($BuGanz[0])) {
echo 'bu.push("'.implode("\",\"", $BuGanz).'");'."\n";
echo 'showBu();'."\n";
}
if (isset($KaGanz[0])) {
echo 'ka.push("'.implode("\",\"", $KaGanz).'");'."\n";
echo 'showKa();'."\n";
}
if (isset($MoGanz[0])) {
echo 'mo.push("'.implode("\",\"", $MoGanz).'");'."\n";
echo 'showMo();'."\n";
}
if (isset($ToGanz[0])) {
echo 'to.push("'.implode("\",\"", $ToGanz).'");'."\n";
echo 'showTo();'."\n";
}
if (isset($MuGanz[0])) {
echo 'mu.push("'.implode("\",\"", $MuGanz).'");'."\n";
echo 'showMu();'."\n";
}
if (isset($DaGanz[0])) {
echo 'da.push("'.implode("\",\"", $DaGanz).'");'."\n";
echo 'showDa();'."\n";
}
if (isset($CaGanz[0])) {
echo 'ca.push("'.implode("\",\"", $CaGanz).'");'."\n";
echo 'showCa();'."\n";
}
echo '</script>'."\n";
}
?>
</body>
</html>
