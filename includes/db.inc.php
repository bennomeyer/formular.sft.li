<?php
/*
 * @author Martin Meier
 *
 * Zugang zur FileMaker.DB
 * verwendet wird FX und FileMaker.php
 * deshalb hier sowohl definition der globalen Variablen als auch Bereitstellung eines initialen $fm
 */

error_reporting(E_ERROR);

$fm_host='host2.kon5.net';
$fm_database='Filmtage-Solothurn';
$fm_user='Admin';
$fm_password='xs4kon5';

include_once ('FileMaker.php');
include_once (__DIR__.'/../classes/FileMaker.php'); // development
$fm = new FileMaker();
$fm->setProperty('hostspec', 'http://'.$fm_host);
$fm->setProperty('database', $fm_database);
$fm->setProperty('username', $fm_user);
$fm->setProperty('password', $fm_password);

require_once(__DIR__."/../classes/FX/FX.php");

function FX_open_layout($layout,$group_zize=50) {
	global $fm_host, $fm_database, $fm_user, $fm_password;

	$q = new FX($fm_host, "80", "FMPro12");
	$q->SetDBData($fm_database, $layout, $group_zize);
	$q->SetDBUserPass ($fm_user, $fm_password);
	$q->SetCharacterEncoding('UTF-8');
	return $q;
}

function read_labels($sprache){
	global $fm;
	$find_sprache =& $fm->newFindCommand('zz_99');
	$find_sprache->addFindCriterion('_kp__Sprache_ISO_code', $sprache);
	$result_sprache = $find_sprache->execute();
	if (FileMaker::isError($result_sprache)) {
		echo 'Leider ist ein Fehler aufgetreten. Bitte kontaktieren Sie den WebMaster.';
		exit;
	}
	$records_sprache = $result_sprache->getRecords();
	foreach ($records_sprache as $record_sprache) {
		for ($i = 1; $i <= 370; $i++) {
			$_SESSION['Leg_'.$i] = html_entity_decode($record_sprache->getField('Leg_'.$i));
		}
	}
}

?>