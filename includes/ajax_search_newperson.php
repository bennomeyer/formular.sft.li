<?php
session_start();


$vorname = (isset($_POST['vorname'])) ? $_POST['vorname'] : "";
$nachname = (isset($_POST['nachname'])) ? $_POST['nachname'] : "";
$type = (isset($_POST['type'])) ? $_POST['type'] : "";
$out = "";

// Bust cache in the head
header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
// always modified
header ("Cache-Control: no-cache, must-revalidate");  // HTTP/1.1
header ("Pragma: no-cache");                          // HTTP/1.0

if (strlen($nachname) < 3) die('Bitte geben Sie min. 3 Buchstaben des Nachnamens an.');


require_once (__DIR__.'/../includes/db.inc.php');

$q = FX_open_layout("cgi_h_01__personen_fuer_regie", "50");
$q->AddDBParam('Vorname', $vorname);
$q->AddDBParam('Name', $nachname);
$DBData = $q->FMFind();
if (get_class($DBData) == 'FX_Error') error_log(var_export($DBData,true));

$i = 2;
$found = 0;
$out.= "<div id=\"search_personen\"  style=\"height:250px;overflow:auto;\">\n
<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"2\">\n";
foreach ($DBData['data'] as $key => $value) {
	$picture = (file_exists($_SERVER['DOCUMENT_ROOT'].'/bilder/'.$value['_kp__id'][0].'z_small.jpg')) ? '<img src="/bilder/'.$value['_kp__id'][0].'z_small.jpg" hspace="5">' : "";
	$needEmail = ($value['Mail_1'][0] == "")? "1" : "";
	$out .="
  <tr bgcolor=\"".((($i%2) >0)? "#EEEEEE" : "#FFFFFF")."\">\n
    <td>".$value['Vorname'][0]." ".$value['Name'][0]."<br />".$value['Ort'][0]."</td>
    <td valign=\"middle\" align=\"center\" width=\"5%\"><input type=\"button\" value=\"".$_SESSION['Leg_272']."\" onClick=\"javascript:newperson_save('".$value['_kp__id'][0]."','".$value['Vorname'][0]."','".$value['Name'][0]."','".$type."','".$needEmail."')\"></td>
    <td width=\"20%\" align=\"center\">".$picture."</td>
  </tr>\n";
	$i++;
	$found = 1;
}

if ($found == 0) $out .= '<tr><td colspan="3">'.$_SESSION['Leg_134'].'</td></tr>';
if ($type == "1") $out .= '<tr><td colspan="3"><input type="button" onClick="javascript:addPersonStart();" value="'.$_SESSION['Leg_270'].'"></td></tr>';
elseif ($type == "10") $out .= '<tr><td colspan="3"><input type="button" onClick="javascript:addPersonStart();" value="'.$_SESSION['Leg_367'].'"></td></tr>';
else $out .= '<tr><td colspan="3"><input type="button" onClick="javascript:addStabStart(\''.$type.'\');" value="'.$_SESSION['Leg_270'].'"></td></tr>';
$out .="
</table>\n
</div>";
?>
<?=$out?>