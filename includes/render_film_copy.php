<?php
/**
 * render one film copy
 * @author Martin Meier
 * @copyright 2013, Martin Meier KON5
 *
 */

function label_tag($cont){
	return '<label>'.$cont.'</label>';
}

function label($id) { return label_tag($_SESSION["Leg_$id"].':'); }

function div_tag($cont,$attr='') {
	return '<div '.$attr.'>'.$cont.'</div>';
}

function get_film_copy_from_record($record){
	$values=array(
		'_kp__id'=> 'id',
		'_kf__Filmformat_Id'=>'Filmformat',
		'_kf__Bildformat_Id'=>'Bildformat',
		'_kf__Tonformat_Id'=>'Tonformat',
		'Synchronfassung_Flag'=>'Synchronfassung_Flag',
		'_kf__Synchronsprache_iso'=>'Synchronsprache_iso',
		'_kf__Untertiteltyp_Id'=>'Untertitel_Typ',
		'_kf__Untertitelsprachen_Id'=>'Untertitel');

	$copy=array();
	foreach ($values as $field=>$variable) {
		$copy[$variable]=$record->getField($field);
	}
	$ton_sprachen=array();
	$ton_set=$record->getRelatedSet('zz_Tonspursprachen');
	if(!FileMaker::isError($ton_set)) {
		foreach ($ton_set as $tss) {
			$ton_sprachen[]= $tss->getField('zz_Tonspursprachen::_kf__Sprache_iso');
		}
	}
	$copy['ton_sprachen']=$ton_sprachen;

	$cc_sprachen=array();
	$cc_set=$record->getRelatedSet('zz_ClosedcaptionSprachen');
	if (!FileMaker::isError($cc_set)) {
		foreach ($cc_set as $ccs) {
			$cc_sprachen[]= $ccs->getField('zz_ClosedcaptionSprachen::_kf__Sprache_iso');
		}
	}
	$copy['cc_sprachen']=$cc_sprachen;
	return $copy;
}

function render_film_copy($copy) {
	$res  ='<div class="div_blankbox"><div class="copy">';
	$res .= '<div class="del_copy" data-cid="'.$copy['id'].'"><img src="images/delete.png" alt="löschen" width="16" /></div>';
	$res .= div_tag(label(68).div_tag(get_value_label($copy['Filmformat'],'filmformate')));
	$res .= div_tag(label(69).div_tag(get_value_label($copy['Bildformat'],'bildformate')));
	$res .= div_tag(label(70).div_tag(get_value_label($copy['Tonformat'], 'tonformate')));
	if ($copy['Synchronfassung_Flag']>0) {
		$res .= div_tag(label(318).div_tag(get_iso_label($copy['Synchronsprache_iso'])));
	}
	if (count($copy['ton_sprachen'])>0) {
		$ton_langs = array();
		foreach ($copy['ton_sprachen'] as $lang) {
			$ton_langs[] = get_iso_label($lang);
		}
		$res .= div_tag(label(319).div_tag(join(', ',$ton_langs)));
	}
	if ($copy['Untertitel']>0) {
		$res .= div_tag(label(67).div_tag(get_value_label($copy['Untertitel'], 'untertitel')));
	}
	if (count($copy['cc_sprachen'])>0) {
		$cc_langs=array();
		foreach ($copy['cc_sprachen'] as $lang) {
			$cc_langs[] = get_iso_label($lang);
		}
		$res .= div_tag(label(320).join(', ',$cc_langs));
	}
	if ($copy['Untertitel']<=0 && count($copy['cc_sprachen'])<=0) {
		$res .= div_tag(label(67).div_tag('--'));
	}
	$res .= '</div></div>';
	return $res;
}