var cMultiSelectOptions

(function()
{
	cMultiSelect = function( sSelectId )
	{
		var self = this;
		this.sId = sSelectId;
		this.hElement = document.getElementById( this.sId );
		this.bCheckGroup = true;
		if( this.hElement )
		{
			this.sElementName = this.hElement.name

			var hMS = this
			hMS.initMultiSelect = function()
			{
				var hHolder = hMS.hElement.parentNode
				var hOptions = hMS.hElement.options
				var hList = document.createElement( 'ul' )
				hList.id = this.sId + '_LIST'
				hList.className = 'ms-multi-select-list'
				hList.setAttribute( 'collapsed', 'true' )
				var hListItem
				var hInput
				var hLabel

				//append at the top the list of selected items
				hListItem = document.createElement( 'li' )
				hListItem.id = this.sId + '_SHORTLIST'
				hListItem.className = 'ms-multi-select-shortlisted'
				hListItem.appendChild( document.createTextNode( 'selected options: ' ) );
				YAHOO.util.Event.addListener( hListItem, 'mousedown', hMS.onMouseDownToFocus );
				hList.appendChild( hListItem )
				

				/*open/close button*/
				hListItem = document.createElement( 'li' )
				hListItem.className = 'ms-multi-select-button'
				hListItem.id = this.sId + '_BUTTON_ITEM'
				
				var hButton = document.createElement( 'a' )
				hButton.href = 'javascript:;'
				hButton.className = 'ms-multi-select-button'
				hButton.id = this.sId + '_BUTTON'
				hButton.appendChild( document.createTextNode( cMultiSelectOptions.CS_PLEASE_SELECT_ITEMS ) )
				
				hButton.onclick = hMS.toggleSelectOptions
				YAHOO.util.Event.addListener( hButton, 'mousedown', hMS.onMouseDownToFocus );
				
				hListItem.appendChild( hButton )
				hList.appendChild( hListItem )
				/*open/close button*/

				//is it a check or radio group
				var bCheckGroup = true
				if( hMS.hElement.type == 'select-one' )
				{
					//so it's a radio group
					bCheckGroup = false
				}
				
				var sElement = 'input'
				
				for( var nI = 0; nI < hOptions.length; nI++ )
				{
					hListItem = document.createElement( 'li' )
					hListItem.className = 'ms-multi-select-item'
					hListItem.id = this.sId + '_LI_' + nI
					hLabel = document.createElement( 'label' )

					/*@cc_on@*/
					/*@if (@_jscript_version <= 5.6)
					if( hOptions[ nI ].selected )
					{
						sChecked = 'checked="checked"'
					}
					else
					{
						sChecked = ''
					}
					if(  bCheckGroup )
					{
						sElement = "<input type='checkbox' name='"+hMS.sElementName+"' value='"+hOptions[ nI ].value+"' "+sChecked+">";
					}
					else
					{
						sElement = "<input type='radio' name='"+hMS.sElementName+"' value='"+hOptions[ nI ].value+"' "+sChecked+">";
					}
					/*@end@*/

					hInput = document.createElement( sElement )
					if(  bCheckGroup )
					{
						hInput.type = 'checkbox'
					}
					else
					{
						hInput.type = 'radio'
					}
					hInput.name = hMS.sElementName
					hInput.value = hOptions[ nI ].value
					hInput.id = this.sId + '_' + nI
					hInput.checked = hOptions[ nI ].selected

					YAHOO.util.Event.addListener( hInput, 'click', hMS.onInputClick );
					YAHOO.util.Event.addListener( hLabel, 'click', hMS.onInputClick );

					hLabel.htmlFor = hInput.id
					hLabel.appendChild( document.createTextNode( hOptions[ nI ].text ) )
					hListItem.appendChild( hInput )
					hListItem.appendChild( hLabel )
					hList.appendChild( hListItem );

					this.bCheckGroup = bCheckGroup;
				}

				hHolder.appendChild( hList );
				hList.tabIndex = "-1";
				
				YAHOO.util.Event.addListener( hList, 'keyup', hMS.onListKeyUp )
				YAHOO.util.Event.addListener( hList, 'focus', hMS.onListFocus )
				YAHOO.util.Event.addListener( hList, 'blur', hMS.onListBlur )
				
				hMS.removeOriginalSelect();
				
				//now set the names of the inputs
				window.setTimeout( function() {hMS.createSelectedOptionsList()}, 10 );
				//hMS.createSelectedOptionsList();

				hList.focus();
			}
			
			hMS.createSelectedOptionsList = function()
			{
				var hList = document.getElementById( hMS.sId + '_LIST' )
				//var hOptions = document.getElementsByName( this.sElementName )
				var hOptions = hList.getElementsByTagName( 'input' );
				var hShortListItem = document.getElementById( this.sId + '_SHORTLIST' )
				var hOriginalSelect = document.getElementById( this.sId )

				var sSelectedOptions = ''

				for( var nI = 0; nI < hOptions.length; nI ++ )
				{
					if( hOptions[ nI ].checked )
					{
						//not removed but just display:none
						if( hOriginalSelect )
						{
							hOriginalSelect.options[ nI ].selected = 'selected'
						}
						if( sSelectedOptions.length > 0 )
						{
							sSelectedOptions += ', '
						}
						sSelectedOptions += hOptions[ nI ].parentNode.getElementsByTagName( 'label' )[ 0 ].innerHTML
					}
					else
					{
						//not removed but just display:none
						if( hOriginalSelect )
						{
							hOriginalSelect.options[ nI ].selected = ''
						}
					}
				}
				if( sSelectedOptions == '' )
				{
					sSelectedOptions = cMultiSelectOptions.CS_PLEASE_SELECT_MESSAGE
				}
				hShortListItem.innerHTML = sSelectedOptions;
			}
			
			hMS.toggleSelectOptions = function()
			{
				var hList = document.getElementById( hMS.sId + '_LIST' )
				var sCollapsed = hList.getAttribute( 'collapsed' )
				//var hOptions = document.getElementsByName( hMS.sElementName )
				var hOptions = hList.getElementsByTagName( 'input' );
				var hButton = document.getElementById( hMS.sId + '_BUTTON' )

				if( sCollapsed == 'true' )
				{
					//show options
					for( var nI = 0; nI < hOptions.length; nI ++ )
					{
						if( hOptions[ nI ].parentNode.tagName.toLowerCase() != 'li' )
						{
							continue
						}
						hOptions[ nI ].parentNode.style.display = 'block'
					}

					while( hButton.childNodes.length > 0 )
					{
						hButton.removeChild( hButton.childNodes[ 0 ] )
					}
					hButton.appendChild( document.createTextNode( cMultiSelectOptions.CS_PLEASE_HIDE_ITEMS ) )
					hList.setAttribute( 'collapsed', 'false' )
				}
				else
				{
					//hide options
					for( var nI = 0; nI < hOptions.length; nI ++ )
					{
						if( hOptions[ nI ].parentNode.tagName.toLowerCase() != 'li' )
						{
							continue
						}
						hOptions[ nI ].parentNode.style.display = 'none'
					}

					while( hButton.childNodes.length > 0 )
					{
						hButton.removeChild( hButton.childNodes[ 0 ] )
					}
					hButton.appendChild( document.createTextNode( cMultiSelectOptions.CS_PLEASE_SELECT_ITEMS ) )
					hButton.focus()
					hList.setAttribute( 'collapsed', 'true' )
				}
			}
			
			hMS.onListKeyUp = function( hEvent )
			{
				if( hEvent.keyCode == 72 )
				{
					hMS.toggleSelectOptions()
				}
			}

			hMS.onMouseDownToFocus = function( hEvent )
			{
				var hList = document.getElementById( self.sId + '_LIST' );
				window.setTimeout( function(){ hList.focus() }, 0 );
			}

			hMS.onListFocus = function( hEvent )
			{
				YAHOO.util.Dom.addClass( this, 'ms-multi-select-list-focused' );
			}

			hMS.onListBlur = function( hEvent )
			{
				YAHOO.util.Dom.removeClass( this, 'ms-multi-select-list-focused' );
			}

			hMS.onInputClick = function()
			{
				hMS.createSelectedOptionsList();
				var hList = document.getElementById( self.sId + '_LIST' );
				window.setTimeout( function(){ hList.focus() }, 0 );
			}
			
			hMS.removeOriginalSelect = function()
			{
				//hMS.hElement.parentNode.removeChild( hMS.hElement )
				hMS.hElement.name = hMS.hElement.name + '_old'
				hMS.hElement.style.display = 'none'
			}

			hMS.initMultiSelect();
		}
		
	}
	
	cMultiSelectOptions = {}
	cMultiSelectOptions.CS_PLEASE_SELECT_ITEMS = 'select items'
	cMultiSelectOptions.CS_PLEASE_HIDE_ITEMS = 'hide options'
	cMultiSelectOptions.CS_PLEASE_SELECT_MESSAGE = 'open the list and select options'
	cMultiSelectOptions.CS_ID_PREFIX = 'mo_multi_select_'
	cMultiSelectOptions.nIdCount  = 0
	
	cMultiSelectOptions.hOptionsList = {}
	
	cMultiSelectOptions.initOptionList = function( hOL )
	{
		if( !hOL.id )
		{
			hOL.id = cMultiSelectOptions.CS_ID_PREFIX + ( cMultiSelectOptions.nIdCount++ )
		}
		cMultiSelectOptions.hOptionsList[ hOL.id ] = new cMultiSelect( hOL.id ) 
	}

	cMultiSelectOptions.autoAttach = function()
	{
		var hSelects = Ext.DomQuery.select( 'select[class*=ms-multi-select]' );
		for( var nI = 0; nI < hSelects.length; nI++ )
		{
			cMultiSelectOptions.initOptionList( hSelects[ nI ] );
		}
	}
	YAHOO.util.Event.onDOMReady( cMultiSelectOptions.autoAttach );
})()