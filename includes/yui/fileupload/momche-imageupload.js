YAHOO.namespace( "MOMCHE.widget" );
YAHOO.MOMCHE.widget.Dialog = function(el, userConfig)
{
	if( arguments.length > 0 )
	{
		YAHOO.MOMCHE.widget.Dialog.superclass.constructor.call( this, el, userConfig );
	}
}
YAHOO.extend( YAHOO.MOMCHE.widget.Dialog, YAHOO.widget.Dialog );

YAHOO.MOMCHE.widget.Dialog.prototype.doSubmit = function()
{
	var pm = this.cfg.getProperty("postmethod");
	//I override and try to submit via iframe
	this.callback.upload = this.callback.success;
	if( pm == 'async' )
	{
		var method = this.form.getAttribute("method") || 'POST';
		method = method.toUpperCase();
		//add true
		YAHOO.util.Connect.setForm(this.form, true);
		var cObj = YAHOO.util.Connect.asyncRequest(method, this.form.getAttribute("action"), this.callback);
		this.asyncSubmitEvent.fire();
		
		addPersonDlg.show();
	}
	else
	{
		YAHOO.MOMCHE.widget.Dialog.superclass.doSubmit.call( this );
	}
}

var hImageUploader;

(function()
{
	var hDialog = null;
	var hActiveInput = null;
	
	//working directory. Global for all instances
	var sPath = '/bilder/';

	function uploadImage( hInput )
	{
		hActiveInput = hInput;
		addPersonDlg.hide();
		hDialog.show();
	}

	function getImageContainer( hInput )
	{
		var aClassNames = hInput.className.match( /target_([^\s]*)/i );
		var sId = aClassNames[1];
		return YAHOO.util.Dom.get( sId );
	}

	// Define various event handlers for Dialog
	var handleSubmit = function() 
	{
		this.submit();
	};

	var handleCancel = function() 
	{
		this.cancel();
	};

	var handleSuccess = function(o) 
	{
		var response = o.responseText;
		var hResult = eval( '(function(){ return ' + response + ' })();' );
		hActiveInput.value = hResult.fileName;
		updateContainer( hActiveInput )
	};

	var handleFailure = function(o) 
	{
		alert("Submission failed: " + o.status);
	};	

	function updateContainer( hInput )
	{
		var hImageContainer = getImageContainer( hInput );
		if( hImageContainer )
		{
			YAHOO.util.Dom.setStyle( hImageContainer, 'background-image', 'url(' + sPath+hInput.value + ')' );
		}
	}
	
	function clearInput( hInput )
	{
		hInput.value = '';
		var hImageContainer = getImageContainer( hInput );
		if( hImageContainer )
		{
			YAHOO.util.Dom.setStyle( hImageContainer, 'background-image', 'url(images/add.png)' );
		}
	}
	
	hImageUploader = function()
	{
		return {
			init : function( hInput )
			{
				var hImageContainer = getImageContainer( hInput );
				if( hImageContainer )
				{
					YAHOO.util.Event.addListener( hImageContainer, 'click', function(){ uploadImage( hInput ) }, this , true );
				}
				updateContainer( hInput );
			},
			autoAttach : function()
			{
				// Instantiate the Dialog
	
				hDialog = new YAHOO.MOMCHE.widget.Dialog( "uploadImageDialog", 
															{ width : "500px",
															  modal: true,
															  visible : false,
															  iframe:true,
															  constraintoviewport : true,
															  fixedcenter : true,
															  draggable:true,
															  zIndex:10000,
															  buttons : [ { text:"Submit", handler:handleSubmit, isDefault:true },
																		  { text:"Cancel", handler:handleCancel } ]
															 } );

				hDialog.callback = { success: handleSuccess,
									failure: handleFailure };
				hDialog.render();
			
				var hImageInputs = Ext.DomQuery.select( 'input[class*=imagePath]' );
				for( var nI = 0; nI < hImageInputs.length; nI++ )
				{
					hImageUploader.init( hImageInputs[ nI ] );
				}
			},
			clear : function( hInput )
			{
				clearInput( hInput );
			}
		}
	}();
	YAHOO.util.Event.onDOMReady( hImageUploader.autoAttach );
})();