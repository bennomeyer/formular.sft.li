<?php
session_start();
// Bust cache in the head
header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
// always modified
header ("Cache-Control: no-cache, must-revalidate");  // HTTP/1.1
header ("Pragma: no-cache");                          // HTTP/1.0


require_once (__DIR__.'/../includes/db.inc.php');

$record_id = (isset($_GET['firma_record_id'])) ? $_GET['firma_record_id'] : "";
$type = (isset($_GET['firma_type'])) ? $_GET['firma_type'] : "";

// Eintrag l�schen

$q = FX_open_layout("cgi_k_45__filme_firmen");
$q->AddDBParam('-recid', $record_id);
$q->FMDelete(true);



// Alle gespeicherten Firmen auslesen

$q = FX_open_layout("cgi_k_45__filme_firmen", "999");
$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);
$q->AddDBParam('_kf__Firmentyp_Id', $type);
$DBData = $q->FMFind();

$span = "";
foreach ($DBData['data'] as $key => $value) {
	$span .= $value['h_44__firmen::Firmenname'][0].' '.$value['h_44__firmen::PLZ'][0].' '.$value['h_44__firmen::Ort'][0].' <img src="/images/delete.png" border="0" style="cursor:pointer" onClick="deleteFirma(\\\''.$type.'\\\',\\\''.$value['_kp__record_id'][0].'\\\')" align="absmiddle"><br /><input type="hidden" name="hasEntry" value="1" />';
}
$span = " document.getElementById('firmen_list').innerHTML = '".$span."';";
die("infoDialog('Hinweis','Die Firma wurde gelöscht.');".$span);


?>