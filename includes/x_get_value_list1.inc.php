<?

if (!function_exists('read_layout_list')) require_once dirname(__FILE__).'/read_layout_list.php';

// Werteliste für Genre (als radios)
$rows=read_layout_list('genres');
$rows=search_value_list($rows, 'Prioritaet', "1");
$field='Genre_'.$_SESSION['sprache'];
$key="_kp__id";
$output_Genre='';
foreach ($rows as $row) {
	$selector= ($row[$key]==$Genre?' checked':'');
	$output_Genre .= '<input type="radio" name="Genre" value="'.$row[$key].'" '.$selector.' onchange="showSwissperform();"/>'.$row[$field]."<br />\n";
}

// Werteliste für Produktionsjahr
$rows=read_layout_list('jahre');
$years=array();
foreach ($rows as $row) {
	$years[$row['_kp__Jahrwert']]=$row['_kp__Jahrwert'];
}
arsort($years);
$output_Jahr=render_options($years,trim($Produktionsjahr));

// Werteliste für Produktionsland
$rows=read_layout_list('laender');
function order_countries(&$a,&$b) {
	if ($a['Prioritaet']==$b['Prioritaet']) {
		$col='Land_'.$_SESSION['sprache'];
		return strcmp($a[$col], $b[$col]);
	} else {
		return ($a['Prioritaet']-$b['Prioritaet']);
	}
}
usort($rows, 'order_countries');
$output_Land = render_grouped_options($rows, '_kp__ISO_Code', 'Land_'.$_SESSION['sprache'], 'Prioritaet');

// Werteliste für Originalsprache
$rows=read_layout_list('sprachen');
function order_sprachen(&$a,&$b) {
	if ($a['Prioritaet']==$b['Prioritaet']) {
		$col='Sprache_'.$_SESSION['sprache'];
		return strcmp($a[$col], $b[$col]);
	} else {
		return ($a['Prioritaet']-$b['Prioritaet']);
	}
}
usort($rows, 'order_sprachen');
$output_Sprache = render_grouped_options($rows, '_kp__ISO_Code', 'Sprache_'.$_SESSION['sprache'], 'Prioritaet');

// Werteliste für Farbe vs. SW
$rows = read_layout_list('farbe_sw');
$output_Farbe='';
foreach ($rows as $row) {
	$selector = $Farbe == $row['_kp__id'] ? ' checked ':'';
	$output_Farbe.='<input type="radio" name="Farbe" value="'.$row['_kp__id'].'"'.$selector.'>'.$row['Farbe_'.$_SESSION['sprache']]."<br />\n";
}
?>