
YAHOO.widget.Dialog.prototype.submit = function() {
	if (this.validate()) {
		this.beforeSubmitEvent.fire();
		this.doSubmit();
		this.submitEvent.fire();
		if(this.hideOnSubmit !== false){
		   this.hide();
		}
		return true;
	} else {
		return false;
	}
};
