<?php

session_start();
if ((!$_SESSION['login_ok']) || (($_SESSION['film_id'] == "") && ($_SESSION['m'] == "u"))) {
	header("Location: login.php");
	exit;
}
header('Content-Type: application/json');

require_once 'read_layout_list.php'; // includes FX.php
require_once 'render_film_copy.php';

// read_layout_list('filmformate');
// read_layout_list('bildformate');
// read_layout_list('tonformate');
// read_layout_list('untertitel');
// read_layout_list('untertitel_typen');

$Untertitel = nvl($_POST['Untertitel']); // ID aus Liste mit vorgegebenen Sprachkombinationen (open caption)
$Filmformat =  nvl($_POST['Filmformat']);
$Bildformat =  nvl($_POST['Bildformat']);
$Tonformat =  nvl($_POST['Tonformat']);
$Untertitel_typ = nvl($_POST['Untertitel_Typ']);
$Untertitel_Sprachen = nvl($_POST['cc_sprachen'],array()); // dynamische Liste, closed caption
$Ton_Sprachen = nvl($_POST['ton_sprachen'],array());
$Synchronfassung_Flag = nvl($_POST['Synchronfassung_Flag']);
$Synchronsprache_iso = nvl($_POST['Synchronsprache_iso']);

$error = '';

if ($Filmformat == 8) { // DCP
	// Felder, die bei DCP nicht zutreffen, löschen
	$Synchronfassung_Flag="";
	$Synchronsprache_iso="";
	// Untertitel ist Pflicht
	if (empty($Untertitel_typ)) {
		$error = $_SESSION['Leg_328'];
	}
}
if (!empty($Synchronsprache_iso)) {
	$parts=explode('|',$Synchronsprache_iso);
	$Synchronsprache_iso=reset($parts);
}


// Untertitel auf erlaubte Werte reduzieren
/*
 $utt = reset(search_value_list($_SESSION['untertitel_typen'], '__kp_id', $Untertitel_typ));

 $anz_sprachen = empty($utt) ? 0 : $utt['Anz_Sprachen'];
 switch ($anz_sprachen) {
 case 0:
 $Untertitel_Sprachen=array();
 $Untertitel="";
 break;
 case 1:
 $Untertitel_Sprachen=array();
 break;
 case 2:
 $Untertitel="";
 break;
 default:
 break;
 }
 */
if (''==$error) {
	// Datensatz anlegen
	require_once (__DIR__.'/../includes/db.inc.php');

	$values=array(
	'_kf__Film_Id'=>$_SESSION['film_id'],
	'_kf__Filmformat_Id'=>$Filmformat,
	'_kf__Bildformat_Id'=>$Bildformat,
	'_kf__Tonformat_Id'=>$Tonformat,
	'Synchronfassung_Flag'=>$Synchronfassung_Flag,
	'_kf__Synchronsprache_iso'=>$Synchronsprache_iso,
	'_kf__Untertiteltyp_Id'=>$Untertitel_typ,
	'_kf__Untertitelsprachen_Id'=>$Untertitel
	);
	$rec =& $fm->newAddCommand('cgi_k_20__filme_vorfuehrkopie',$values);
	$result = $rec->execute();

	if (!FileMaker::isError($result)) {
		// Ton und Untertitle-Sprachen anlegen
		$newRecord = reset($result->getRecords());
		foreach ($Ton_Sprachen as $iso) {
			$newLang = $newRecord->newRelatedRecord('zz_Tonspursprachen');
			$newLang->setField('zz_Tonspursprachen::_kf__Sprache_iso',$iso);
			$res = $newLang->commit();
		}
		foreach ($Untertitel_Sprachen as $iso) {
			$newLang = $newRecord->newRelatedRecord('zz_ClosedcaptionSprachen');
			$newLang->setField('zz_ClosedcaptionSprachen::_kf__Sprache_iso',$iso);
			$res = $newLang->commit();
		}

		// build an internal representation of the new record;
		$find = $fm->newFindCommand('cgi_k_20__filme_vorfuehrkopie');
		$find->addFindCriterion('_kp__id','=='.$newRecord->getField('_kp__id'));
		$result = $find->execute();
		$records = $result->getRecords();
		$record= reset($records);
		$copy = get_film_copy_from_record($record);
		// build a HTML representation
		$new_copy = render_film_copy($copy);
		echo json_encode(array('status'=>'ok','new_copy'=>$new_copy,'dbg'=>$_REQUEST,'val'=>$values, 'copy'=>$copy));
	} else {
		echo json_encode(array('status'=>'error','msg'=>$result->getMessage()));
	}
} else {
	echo json_encode(array('status'=>'error','msg'=>$error));
}

?>