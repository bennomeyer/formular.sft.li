<?php
session_start();
// Bust cache in the head
header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
// always modified
header ("Cache-Control: no-cache, must-revalidate");  // HTTP/1.1
header ("Pragma: no-cache");                          // HTTP/1.0

require_once (__DIR__.'/../includes/db.inc.php');

$id = (isset($_GET['firma_id'])) ? $_GET['firma_id'] : "";
$firma = (isset($_GET['firma_name'])) ? urldecode($_GET['firma_name']) : "";
$plz = (isset($_GET['firma_plz'])) ? $_GET['firma_plz'] : "";
$ort = (isset($_GET['firma_ort'])) ? $_GET['firma_ort'] : "";
$type = (isset($_GET['firma_type'])) ? $_GET['firma_type'] : "";

// Firma speichern

$q = FX_open_layout("cgi_k_45__filme_firmen", "1");
$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);
$q->AddDBParam('_kf__Firma_Id', $id);
$q->AddDBParam('_kf__Firmentyp_Id', $type);
$DBData = $q->FMNew();


// Alle gespeicherten Firmen auslesen

$q = FX_open_layout("cgi_k_45__filme_firmen", "999");
$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);
$q->AddDBParam('_kf__Firmentyp_Id', $type);
$DBData = $q->FMFind();

$span = "";
foreach ($DBData['data'] as $key => $value) {
	$span .= str_replace("'", "\'",$value['h_44__firmen::Firmenname'][0]).' '.$value['h_44__firmen::PLZ'][0].' '.$value['h_44__firmen::Ort'][0].' <img src="/images/delete.png" border="0" style="cursor:pointer" onClick="deleteFirma(\\\''.$type.'\\\',\\\''.$value['_kp__record_id'][0].'\\\')" align="absmiddle"><br /><input type="hidden" name="hasEntry" value="1" />';
}
$span = " document.getElementById('firmen_list').innerHTML = '".$span."';";
die($span);

?>