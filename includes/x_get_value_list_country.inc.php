<?

require_once (__DIR__.'/../includes/db.inc.php');

// Werteliste f�r Produktionsland

$q = FX_open_layout( "w_06__ISO_laender", "999");
$q->AddDBParam('Land_'.$sprache, ">0");
$q->AddSortParam ('Prioritaet', 'ascend', 1);
$q->AddSortParam ('Land_'.$sprache, 'ascend', 2);
$DBData = $q->FMFind();
$output_Land = "";
//print_r($DBData);
$prio = "1";
foreach ($DBData['data'] as $key => $value) {
	if ($prio != $value['Prioritaet'][0]) {
		$output_Land .= '<option value="">-----------------------</option>'."\n";
		$prio = $value['Prioritaet'][0];
	}
	$selector = (strtoupper($value['_kp__ISO_Code'][0]) == strtoupper($land_1))? ' selected' : '';
	$output_Land .= '<option value="'.$value['_kp__ISO_Code'][0].'"'.$selector.'>'.$value['Land_'.$sprache][0].'</option>'."\n";
}

?>