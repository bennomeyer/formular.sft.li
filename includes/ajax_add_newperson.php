<?php
session_start();

// Bust cache in the head
header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
// always modified
header ("Cache-Control: no-cache, must-revalidate");  // HTTP/1.1
header ("Pragma: no-cache");                          // HTTP/1.0

require_once (__DIR__.'/../includes/db.inc.php');


$vorname = (isset($_POST['vorname2'])) ? $_POST['vorname2'] : "";
$name = (isset($_POST['name'])) ? $_POST['name'] : "";
$strasse1 = (isset($_POST['strasse1'])) ? $_POST['strasse1'] : "";
$strasse2 = (isset($_POST['strasse2'])) ? $_POST['strasse2'] : "";
$plz = (isset($_POST['plz'])) ? $_POST['plz'] : "";
$ort = (isset($_POST['ort'])) ? $_POST['ort'] : "";
$land = (isset($_POST['land'])) ? $_POST['land'] : "";
$fon = (isset($_POST['fon'])) ? $_POST['fon'] : "";
$fax = (isset($_POST['fax'])) ? $_POST['fax'] : "";
$tel_mob = (isset($_POST['tel_mob'])) ? $_POST['tel_mob'] : "";
$email = (isset($_POST['email'])) ? $_POST['email'] : "";
$www = (isset($_POST['www'])) ? $_POST['www'] : "";
$geburtsjahr = (isset($_POST['geburtsjahr'])) ? $_POST['geburtsjahr'] : "";
$geburtsort = (isset($_POST['geburtsort'])) ? $_POST['geburtsort'] : "";
$heimatort = (isset($_POST['heimatort'])) ? $_POST['heimatort'] : "";
$bio = (isset($_POST['bio'])) ? $_POST['bio'] : "";
$korrsprache = (isset($_POST['korrsprache'])) ? $_POST['korrsprache'] : "";
$sex = (isset($_POST['sex'])) ? $_POST['sex'] : "";

if(in_array('filmtitel',array_keys($_POST))) {
	$filmtitel = ((count($_POST['filmtitel']))>0) ? $_POST['filmtitel'] : array();
} else {
	$filmtitel = array();
}
if(in_array('jahr',array_keys($_POST))) {
	$jahr = ((count($_POST['jahr']))>0) ? $_POST['jahr'] : array();
} else {
	$jahr = array();
}

$personenbild = (isset($_POST['image'])) ? $_POST['image'] : "";
$type = (isset($_POST['type'])) ? $_POST['type'] : "";
if ($type == "") {
	$type = (isset($_POST['new_type'])) ? $_POST['new_type'] : "";
}
$out = "";
// die Person eintragen
//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__


$q = FX_open_layout("cgi_h_01__personen_fuer_regie", "1");

$q->AddDBParam('Vorname', $vorname);
$q->AddDBParam('Name', $name);
$q->AddDBParam('Strasse_1', $strasse1);
$q->AddDBParam('Strasse_2', $strasse2);
$q->AddDBParam('PLZ', $plz);
$q->AddDBParam('Ort', $ort);
$q->AddDBParam('_kf__Land_ISO_Code', $land);
$q->AddDBParam('Tel_P', $fon);
$q->AddDBParam('Fax', $fax);
$q->AddDBParam('Tel_Mob', $tel_mob);
$q->AddDBParam('Mail_1', $email);
$q->AddDBParam('Website', $www);
$q->AddDBParam('Geburtsjahr', $geburtsjahr);
$q->AddDBParam('Geburtsort', $geburtsort);
$q->AddDBParam('Heimatort', $heimatort);
$q->AddDBParam('Biographie_Formular_Zusatz', $bio);
$q->AddDBParam('_kf__Korrespondenzsprache', $korrsprache);
$q->AddDBParam('Geschlecht', $sex);
$q->AddDBParam('Brief_P_oder_G_Wahl', 'P');
$DBData = $q->FMNew();

foreach ($DBData['data'] as $key => $value) {
	$personen_id = $value['_kp__id'][0];
}



// die Person in die Kreuztabelle eintragen
//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__


$q = FX_open_layout( "cgi_k_03__filme_personen");
$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);
$q->AddDBParam('_kf__Person_Id', $personen_id);
$q->AddDBParam('_kf__Stabbezeichnung_Id', $type);
$DBData = $q->FMNew();




// Filmographien der Person eintragen
//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

if (isset($jahr[0])) {
	$filmographie = array();
	for($i=0; $i < count($jahr); $i++) {

		$q = FX_open_layout("cgi_k_Stabmitglieder", "1");
		$q->AddDBParam('Jahr', $jahr[$i]);
		$q->AddDBParam('Filmtitel', $filmtitel[$i]);
		$q->AddDBParam('_kf__Person_Id', $personen_id);
		$q->AddDBParam('_kf__Stabbezeichnung_Id', "1");
		$DBData = $q->FMNew();
	}
}


// Bild verarbeiten bzw. upgeloadetes Bild umbenennen
//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

$foto_arr = explode("?", $personenbild);
$foto = $foto_arr[0];

if (file_exists($_SERVER['DOCUMENT_ROOT'].'/bilder/'.$foto)) {

	$bildname_arr = explode("z_small.jpg?", $personenbild);
	$temp_nr = $bildname_arr[0];

	$command = "mv ".$_SERVER['DOCUMENT_ROOT']."/bilder/".$temp_nr."z.jpg ".$_SERVER['DOCUMENT_ROOT']."/bilder/".$personen_id."z.jpg";
	exec($command);
	$command = "mv ".$_SERVER['DOCUMENT_ROOT']."/bilder/".$temp_nr."z_small.jpg ".$_SERVER['DOCUMENT_ROOT']."/bilder/".$personen_id."z_small.jpg";
	exec($command);


}

// Aktualisierte Personenliste ausgeben
//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__



$q = FX_open_layout( "cgi_k_03__filme_personen", "999");
$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);
if ($type == "1" || $type == "10") {
	$q->AddDBParam('_kf__Stabbezeichnung_Id', $type);
} else {
	$q->AddDBParam('_kf__Stabbezeichnung_Id', "2...9");
}
$DBData = $q->FMFind();

$span = "";
foreach ($DBData['data'] as $key => $value) {
	$span .= $value['zz_Stabbezeichnungen::Bezeichnung_'.$_SESSION['sprache']][0].': '.$value['h_01__personen::Vorname'][0].' '.$value['h_01__personen::Name'][0].' <img src="/images/delete.png" border="0" style="cursor:pointer" onClick="deletePerson(\\\''.$value['_kf__Stabbezeichnung_Id'][0].'\\\',\\\''.$value['_kp__record_id'][0].'\\\')" align="absmiddle"><br /><input type="hidden" name="hasEntry'.$value['_kf__Stabbezeichnung_Id'][0].'" value="1" />';
}
$span = " document.getElementById('person_list').innerHTML = '".$span."';";
die("infoDialog('Hinweis','Die Person wurde gespeichert.');".$span);
