<?php

session_start();
if ((!$_SESSION['login_ok']) || (($_SESSION['film_id'] == "") && ($_SESSION['m'] == "u"))) {
	header("Location: login.php");
	exit;
}
header('Content-Type: application/json');
$mtime = microtime();
$mtime = explode(" ",$mtime);
$mtime = $mtime[1] + $mtime[0];
$starttime = $mtime;
 
// Datensatz löschen
$id = $_REQUEST['cid'];

if ($id) {
	require_once (__DIR__.'/../includes/db.inc.php');

	$find =& $fm->newFindCommand('cgi_k_20__filme_vorfuehrkopie');
	$find->addFindCriterion('_kf__Film_Id', '=='.$_SESSION['film_id']);
	$find->addFindCriterion('_kp__id','=='.$id);
	$result = $find->execute();
	$mtime = microtime();
	$mtime = explode(" ",$mtime);
	$mtime = $mtime[1] + $mtime[0];
	$endtime = $mtime;
	$findtime = ($endtime - $starttime);
	if (!FileMaker::isError($result)){
		$records=$result->getRecords();
		$record = reset($records);
		$rec = $fm->getRecordById('cgi_k_20__filme_vorfuehrkopie',$record->getRecordId());
		$rec->delete();
	}
	$mtime = microtime();
	$mtime = explode(" ",$mtime);
	$mtime = $mtime[1] + $mtime[0];
	$endtime = $mtime;
	$totaltime = ($endtime - $starttime);

	echo json_encode(array('status'=>'ok','findtime'=>$findtime,'total'=>$totaltime));
} else {
	echo json_encode(array('status'=>'error','msg'=>'no id'));
}
