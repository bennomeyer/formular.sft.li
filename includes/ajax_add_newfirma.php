<?php
session_start();

// Bust cache in the head
header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
// always modified
header ("Cache-Control: no-cache, must-revalidate");  // HTTP/1.1
header ("Pragma: no-cache");                          // HTTP/1.0

require_once (__DIR__.'/../includes/db.inc.php');


$firma = (isset($_POST['firma'])) ? $_POST['firma'] : "";
$name = (isset($_POST['name'])) ? $_POST['name'] : "";
$strasse1 = (isset($_POST['strasse1'])) ? $_POST['strasse1'] : "";
$strasse2 = (isset($_POST['strasse2'])) ? $_POST['strasse2'] : "";
$plz = (isset($_POST['plz'])) ? $_POST['plz'] : "";
$ort = (isset($_POST['ort'])) ? $_POST['ort'] : "";
$land = (isset($_POST['land'])) ? $_POST['land'] : "";
$fon = (isset($_POST['fon'])) ? $_POST['fon'] : "";
$fax = (isset($_POST['fax'])) ? $_POST['fax'] : "";
$email = (isset($_POST['email'])) ? $_POST['email'] : "";
$www = (isset($_POST['www'])) ? $_POST['www'] : "";
$type = (isset($_POST['type'])) ? $_POST['type'] : "";

$out = "";


// die Firma eintragen
//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__


$q = FX_open_layout("cgi_h_44__firmen", "1");

$q->AddDBParam('Firmenname', $firma);
$q->AddDBParam('Strasse_1', $strasse1);
$q->AddDBParam('Strasse_2', $strasse2);
$q->AddDBParam('PLZ', $plz);
$q->AddDBParam('Ort', $ort);
$q->AddDBParam('_kf__Land_ISO_Code', $land);
$q->AddDBParam('Tel', $fon);
$q->AddDBParam('Fax', $fax);
$q->AddDBParam('Mail', $email);
$q->AddDBParam('Website', $www);
$DBData = $q->FMNew();

foreach ($DBData['data'] as $key => $value) {
	$firmen_id = $value['_kp__id'][0];
}



// die Firma in die Kreuztabelle eintragen
//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__


$q = FX_open_layout( "cgi_h_45__filme_firmen", "1");

$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);
$q->AddDBParam('_kf__Firma_Id', $firmen_id);
$q->AddDBParam('_kf__Firmentyp_Id', $type);
$DBData = $q->FMNew();



// Aktualisierte Firmenliste ausgeben
//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__



$q = FX_open_layout("cgi_h_44__firmen", "1");

$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);
$q->AddDBParam('_kf__Firmentyp_Id', $type);
$DBData = $q->FMFind();

$span = "";
foreach ($DBData['data'] as $key => $value) {
	$span .= $value['h_44__firmen::Firmenname'][0].' '.$value['h_44__firmen::PLZ'][0].' '.$value['h_44__firmen::Ort'][0].' <img src="/images/delete.png" border="0" style="cursor:pointer" onClick="deleteFirma(\\\'.$type.\\\',\\\''.$value['_kp__record_id'][0].'\\\')" align="absmiddle"><br /><input type="hidden" name="hasEntry" value="1" />';
}
$span = " document.getElementById('firmen_list').innerHTML = '".$span."';";
die("infoDialog('Hinweis','Die Firma wurde gespeichert.');".$span);