<?php
session_start();
// Bust cache in the head
header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
// always modified
header ("Cache-Control: no-cache, must-revalidate");  // HTTP/1.1
header ("Pragma: no-cache");                          // HTTP/1.0

require_once (__DIR__.'/../includes/db.inc.php');

$id = (isset($_GET['person_id'])) ? $_GET['person_id'] : "";
$vorname = (isset($_GET['person_vorname'])) ? $_GET['person_vorname'] : "";
$name = (isset($_GET['person_name'])) ? $_GET['person_name'] : "";
$type = (isset($_GET['person_type'])) ? $_GET['person_type'] : "";
$needemail = (isset($_GET['person_needemail'])) ? $_GET['person_needemail'] : "";

// Person speichern

$q = FX_open_layout("cgi_k_03__filme_personen", "1");
$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);
$q->AddDBParam('_kf__Person_Id', $id);
$q->AddDBParam('_kf__Stabbezeichnung_Id', $type);
$DBData = $q->FMNew();


// Alle gespeicherten Personen auslesen

$q = FX_open_layout("cgi_k_03__filme_personen", "999");
$q->AddDBParam('_kf__Film_Id', $_SESSION['film_id']);
if ($type == "1" || $type == "10") {
	$q->AddDBParam('_kf__Stabbezeichnung_Id', $type);
} else {
	$q->AddDBParam('_kf__Stabbezeichnung_Id', "2...9");
}
$DBData = $q->FMFind();

$span = "";
foreach ($DBData['data'] as $key => $value) {
	$span .= $value['zz_Stabbezeichnungen::Bezeichnung_'.$_SESSION['sprache']][0].': '.$value['h_01__personen::Vorname'][0].' '.$value['h_01__personen::Name'][0].' <img src="/images/delete.png" border="0" style="cursor:pointer" onClick="deletePerson(\\\''.$value['_kf__Stabbezeichnung_Id'][0].'\\\',\\\''.$value['_kp__record_id'][0].'\\\')" align="absmiddle"><br /><input type="hidden" name="hasEntry'.$value['_kf__Stabbezeichnung_Id'][0].'" value="1" />';
}
$span = "document.getElementById('person_list').innerHTML = '".$span."';";
if ($needemail == "1") $span .= "var id=".$id.";";
die($span);

?>