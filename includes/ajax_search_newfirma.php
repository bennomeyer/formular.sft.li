<?php
session_start();


$firma = (isset($_POST['s_firma'])) ? $_POST['s_firma'] : "";
$ort = (isset($_POST['s_ort'])) ? $_POST['s_ort'] : "";
$type = (isset($_POST['type'])) ? $_POST['type'] : "";
$out = "";

// Bust cache in the head
header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
// always modified
header ("Cache-Control: no-cache, must-revalidate");  // HTTP/1.1
header ("Pragma: no-cache");                          // HTTP/1.0

if (strlen($firma) < 3) die('Bitte geben Sie min. 3 Buchstaben der Firma an.');


require_once (__DIR__.'/../includes/db.inc.php');

$q = FX_open_layout("cgi_h_44__firmen", "50");
$q->AddDBParam('Firmenname', $firma, 'cn');
$q->AddDBParam('Ort', $ort, 'cn');
$DBData = $q->FMFind();

$i = 2;
$found = 0;
$out.= "<div id=\"search_firmen\"  style=\"height:250px;overflow:auto;\">\n
<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"2\">\n";
$out .= '<tr><td colspan="2"><input type="button" onClick="javascript:addFirmaStart();" value="'.$_SESSION['Leg_271'].'"></td></tr>';
foreach ($DBData['data'] as $key => $value) {
	$out .="
  <tr bgcolor=\"".((($i%2) >0)? "#EEEEEE" : "#FFFFFF")."\">\n
    <td>".$value['Firmenname'][0]."<br>".$value['Strasse_1'][0]."<br />".$value['PLZ'][0].' '.$value['Ort'][0]."</td>
    <td valign=\"middle\" align=\"center\" width=\"5%\"><input type=\"button\" value=\"".$_SESSION['Leg_272']."\" onClick=\"javascript:newfirma_save('".$value['_kp__id'][0]."','".urlencode($value['Firmenname'][0])."','".$value['PLZ'][0]."','".$value['Ort'][0]."','".$type."')\"></td>
  </tr>\n";
	$i++;
	$found = 1;
}
if ($found == 0) $out .= '<tr><td colspan="3">'.$_SESSION['Leg_134'].'</td></tr>';
if ($found == 1) $out .= '<tr><td colspan="2"><input type="button" onClick="javascript:addFirmaStart();" value="'.$_SESSION['Leg_271'].'"></td></tr>';
$out .="
</table>\n
</div>";
?>
<?=$out?>