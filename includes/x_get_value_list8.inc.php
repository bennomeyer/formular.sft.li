<?

/**
 * diverse Wertelisten als select option speichern
 */

if (!function_exists('read_layout_list')) require_once dirname(__FILE__).'/read_layout_list.php';

// Werteliste für Untertitel
$output_Untertitel = render_value_list('untertitel', $Untertitel);

// Werteliste für Filmformat
$rows = read_layout_list('filmformate');
$list=array();
$sprach_feld='Format_'.$_SESSION['sprache'];
// je nach Anwendung filtern
switch ($show_value_lists_4) {
	case 'sas':	$filter="show_in_SaS"; break;
	case 'forumch': $filter="show_in_ForumCH"; break;
	default: $filter=null; break;
}

foreach ($rows as $row) {
	if (empty($filter) || $row[$filter]==1){
		$list[$row['_kp__id']]=$row[$sprach_feld];
	}
}
$list_Filmformate = $list;
$output_Filmformat = render_options($list,$Filmformat);


// Werteliste für Bildformat
$output_Bildformat = render_value_list('bildformate', $Bildformat);

// Werteliste für Tonformat
$output_Tonformat = render_value_list('tonformate', $Tonformat);

// Werteliste für Sprachen
$rows=read_layout_list('sprachen');
function order_sprachen(&$a,&$b) {
	if ($a['Prioritaet']==$b['Prioritaet']) {
		$col='Sprache_'.$_SESSION['sprache'];
		return strcmp($a[$col], $b[$col]);
	} else {
		return ($a['Prioritaet']-$b['Prioritaet']);
	}
}
usort($rows, 'order_sprachen');
$output_Sprache = render_grouped_options($rows, '_kp__ISO_Code', 'Sprache_'.$_SESSION['sprache'], 'Prioritaet');
// und nun ohne Prio 3
function maxPrio(&$a) {
	return $a['Prioritaet']<3;
}
$rows = array_filter($rows,'maxPrio');
$output_Sprache_2 = render_grouped_options($rows, '_kp__ISO_Code', 'Sprache_'.$_SESSION['sprache'], 'Prioritaet');

?>