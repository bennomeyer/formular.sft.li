<?php
/**
 * @abstract Alle Werte eines Layouts laden
 *           hilfreich für das Laden von Wertelisten aus eigenen Layouts
 * @author Martin Meier
 * @copyright 2013, Martin Meier, KON5
 *
 */

require_once (__DIR__.'/../includes/db.inc.php');

// Wertelisten nach cache-name mit Layout, key-column und Feldname
$ValueLists = array(
  'sprachen'=>array('layout'=>'w_08__ISO_sprachen','prefix'=>'Sprache_', 'key'=>'_kp__ISO_Code'),
  'filmformate'=>array('layout'=>'w_17__filmformat','prefix'=>'Format_','key'=>'_kp__id'),
  'bildformate'=>array('layout'=>'w_18__bildformat','prefix'=>'Bildformat_','key'=>'_kp__id'),
  'tonformate'=>array('layout'=>'w_19__tonformat','prefix'=>'Tonformat_','key'=>'_kp__id'),
  'untertitel'=>array('layout'=>'w_16__untertitel','prefix'=>'Untertitelsprache_','key'=>'_kp__id'),
  'jahre'=>array('layout'=>'w_22__jahr','prefix'=>'','key'=>'_kp__Jahrwert'),
  'laender'=>array('layout'=>'w_06__ISO_laender','prefix'=>'Land_','key'=>'_kp__ISO_Code'),
  'farbe_sw'=>array('layout'=>'w_23__farbe_oder_sw','prefix'=>'Farbe_','key'=>'_kp__id'),
  'untertitel_typen'=>array('layout'=>'w__Untertiteltypen','prefix'=>'Untertiteltyp_','key'=>'__kp_id'),
  'genres'=>array('layout'=>'w_10__genres','prefix'=>'Genre_','key'=>'_kp__id'),

);

/**
 * read all fields of a value list table
 * @param string $cache_name -  Session cache name
 * @return array of arrays - Rows of value list
 */
function read_layout_list($cache_name) {
	global $ValueLists;
	if (!empty($cache_name) &&  !empty($_SESSION[$cache_name])) return $_SESSION[$cache_name];
	$layout = $ValueLists[$cache_name][layout];
	$q = FX_open_layout($layout, 'All');
	$data = $q->FMFindAll();
	// echo "Layout: $layout<br>\n";
	// var_dump($data);
	$rows=array();
	foreach ($data['data'] as $row) {
		$rowdata=array();
		foreach ($row as $field => $value) {
			$rowdata[$field]=reset($value);
		}
		$rows[]=$rowdata;
	}
	if (!empty($cache_name)) $_SESSION[$cache_name]=$rows;
	return $rows;
}

function nvl($value,$default="") {
	return (isset($value) ? $value : $default);
}

class ValueListFilter {
	private $column, $value;

	public function __construct($column,$value){
		$this->column=$column;
		$this->value=$value;
	}
	public function find($var) {
		$myVal=$var[$this->column];
		// echo "Suche ".$this->value.' habe '.$myVal."<br>\n";
		return ($var[$this->column]==$this->value);
	}
}

function search_value_list($list,$column,$value) {
	// echo "Suche $value in ".count($list)." Feldern<br>\n";
	$filtered = array_filter($list,array(new ValueListFilter($column,$value),'find'));
	return $filtered;
}

/**
 * Select Options from Array
 * @param array $values
 * @param string $selected -- key of selected element
 */
function render_options($values,$selected=null){
	$out="";
	foreach ($values as $key => $value) {
		$sel = ($key==$selected ?  ' selected' : '');
		$out .= '<option value="'.$key.'"'.$sel.'>'.$value.'</option>';
	}
	return $out;
}

/**
 * render a row of array with group separator
 * @param array $rows
 * @param string $key - name of key column
 * @param string $value - name of value column
 * @param string $group - name of grouping column
 * @param mixed $selected
 */
function render_grouped_options($rows,$key,$value,$group,$selected=null) {
	$current=reset($rows);
	$current=$current[$group];
	$out='';
	foreach ($rows as $row) {
		if ($current != $row[$group]) {
			$current=$row[$group];
			$out .= '<option value="">-----------------------</option>'."\n";
		}
		$sel = ($selected==$row[$key] ? ' selected':'');
		$out .= '<option value="'.$row[$key].'|'.$row[$value].'"'.$sel.'>'.$row[$value];
	}
	return $out;
}

/**
 * render a value list to a list of options
 * Value list must contain a field $field_prefix<sprache>
 * @param string $cache -  Name in $_SESSION
 * @param string $selected
 */
function render_value_list($cache,$selected) {
	global $ValueLists;
	$field_prefix = $ValueLists[$cache]['prefix'];
	$rows=read_layout_list($cache);
	return render_ordered_list($rows, $field_prefix, $selected);
}

/**
 * get an key-value list from a value list (with sprache)
 * Value list must contain a field $field_prefix<sprache>
 * @param array $rows - ordered rows, must contain field '_kp__id'
 * @param string $field_prefix - rows must contain fields $field_prefix<sprache_iso>
 */
function get_ordered_list($rows,$field_prefix) {
	$list=array();
	$sprach_feld=$field_prefix.$_SESSION['sprache'];
	foreach ($rows as $row) {
		$list[$row['_kp__id']]=$row[$sprach_feld];
	}
	// echo $field_prefix;
	// var_dump($list);
	return $list;
}

function get_format_list($cache) {
	global $ValueLists;
	$field_prefix = $ValueLists[$cache]['prefix'];
	$source=$_SESSION[$cache];
	$list=array();
	$lang_field=$field_prefix.$_SESSION['sprache'];
	if (!empty($source)) {
		foreach ($source as $row) {
			$list[]=array("id"=>$row['_kp__id'], 'format_id'=>$row['_kf_Filmformat_Id'], 'label'=>$row[$lang_field]);
			;
		}
	}
	return $list;
}

/**
 * render an ordered value list to a list of options
 * Value list must contain a field $field_prefix<sprache>
 * @param array $rows - ordered rows, must contain field '_kp__id'
 * @param string $field_prefix - rows must contain fields $field_prefix<sprache_iso>
 * @param string $selected
 */
function render_ordered_list($rows,$field_prefix,$selected) {
	return render_options(get_ordered_list($rows,$field_prefix),$selected);
}

/**
 * get label for a value of a value list
 * @param mixed $id
 * @param string $layout - FileMaker layout name
 * @param string $cache_name - Session cache name
 * @param string $field_prefix - prefix for language postfixed field name
 * @param string $column_name - ID column name (default '_kp__id')
 * @return string
 */
function get_value_label($id,$cache_name) {
	global $ValueLists;
	$field_prefix=$ValueLists[$cache_name]['prefix'];
	$column_name=$ValueLists[$cache_name]['key'];
	$rows = read_layout_list($cache_name);
	// var_dump($rows);
	$row = reset(search_value_list($rows, $column_name, $id));
	// var_dump($row);
	if ($row) {
		$lang_field = $field_prefix.$_SESSION['sprache'];
		// echo "Feld ".$lang_field."=".$row[$lang_field]."<br>\n";
		return $row[$lang_field];
	}
	else return null;
}

/**
 * get language label from iso code
 * @param string $iso_id
 * @return string
 */
function get_iso_label($iso_id) {
	return get_value_label($iso_id,'sprachen');
}
?>
