<?
require_once (__DIR__.'/../includes/db.inc.php');
// Werteliste für Land

$q = FX_open_layout("w_06__ISO_laender", "999");
$q->AddDBParam('Land_'.$_SESSION['sprache'], ">0");
$q->AddSortParam ('Prioritaet', 'ascend', 1);
$q->AddSortParam ('Land_'.$_SESSION['sprache'], 'ascend', 2);
$DBData = $q->FMFind();
$output_Land = "";
$prio = "1";
foreach ($DBData['data'] as $key => $value) {
	if ($prio != $value['Prioritaet'][0]) {
		$output_Land .= '<option value="">-----------------------</option>'."\n";
		$prio = $value['Prioritaet'][0];
	}
	$output_Land .= '<option value="'.$value['_kp__ISO_Code'][0].'">'.$value['Land_'.$_SESSION['sprache']][0].'</option>'."\n";
}
?>