<?
require_once (__DIR__.'/../includes/db.inc.php');
// Werteliste für Land

$q = FX_open_layout( "w_06__ISO_laender", "999");

$q->AddDBParam('Land_'.$_SESSION['sprache'], ">0");
$q->AddSortParam ('Prioritaet', 'ascend', 1);
$q->AddSortParam ('Land_'.$_SESSION['sprache'], 'ascend', 2);
$DBData = $q->FMFind();
$output_Land = "";
$prio = "1";
foreach ($DBData['data'] as $key => $value) {
	if ($prio != $value['Prioritaet'][0]) {
		$output_Land .= '<option value="">-----------------------</option>'."\n";
		$prio = $value['Prioritaet'][0];
	}
	$output_Land .= '<option value="'.$value['_kp__ISO_Code'][0].'|'.$value['Land_'.$_SESSION['sprache']][0].'">'.$value['Land_'.$_SESSION['sprache']][0].'</option>'."\n";
}

// Werteliste für Jahr

$q = FX_open_layout("w_22__jahr", "30");
$q->AddSortParam ('_kp__Jahrwert', 'descend');
$DBData = $q->FMFindAll();
$output_Jahr = "";
$selector = "";
foreach ($DBData['data'] as $key => $value) {
	//$selector = (trim($Produktionsjahr) == trim($value['_kp__Jahrwert'][0])) ? ' selected' : '';
	$output_Jahr .= '<option value="'.$value['_kp__Jahrwert'][0].'"'.$selector.'>'.$value['_kp__Jahrwert'][0].'</option>'."\n";
}

// Wertelisten für Premiere

$q = FX_open_layout( "w_46__premierentyp", "30");
$DBData = $q->FMFindAll();
$output_Premiere = "";
$selector = "";
foreach ($DBData['data'] as $key => $value) {
	$selector = (trim($Premiere) == trim($value['_kp__id'][0])) ? ' selected' : '';
	$output_Premiere .= '<option value="'.$value['_kp__id'][0].'"'.$selector.'>'.$value['Premierentyp_'.$_SESSION['sprache']][0].'</option>'."\n";
}

$q = FX_open_layout("w_22__jahr", "30");
$q->AddSortParam ('_kp__Jahrwert', 'descend');
$DBData = $q->FMFindAll();
$output_PremiereJahr = "";
$selector = "";
foreach ($DBData['data'] as $key => $value) {
	$selector = (trim($PremiereJahr) == trim($value['_kp__Jahrwert'][0])) ? ' selected' : '';
	$output_PremiereJahr .= '<option value="'.$value['_kp__Jahrwert'][0].'"'.$selector.'>'.$value['_kp__Jahrwert'][0].'</option>'."\n";
}

$q = FX_open_layout( "w_21__monat", "30");
$DBData = $q->FMFindAll();
$output_PremiereMonat = "";
$selector = "";
foreach ($DBData['data'] as $key => $value) {
	$selector = (trim($PremiereMonat) == trim($value['_kp__id'][0])) ? ' selected' : '';
	$output_PremiereMonat .= '<option value="'.$value['_kp__id'][0].'"'.$selector.'>'.$value['Monat_'.$_SESSION['sprache']][0].'</option>'."\n";
}


$q = FX_open_layout( "w_06__ISO_laender", "999");
$q->AddDBParam('Land_'.$_SESSION['sprache'], ">0");
$q->AddSortParam ('Prioritaet', 'ascend', 1);
$q->AddSortParam ('Land_'.$_SESSION['sprache'], 'ascend', 2);
$DBData = $q->FMFind();
$output_PremiereLand = "";
$selector = "";
$prio = "1";
foreach ($DBData['data'] as $key => $value) {
	$selector = (trim($PremiereLand) == trim($value['_kp__ISO_Code'][0])) ? ' selected' : '';
	if ($prio != $value['Prioritaet'][0]) {
		$output_PremiereLand .= '<option value=""'.$selector.'>-----------------------</option>'."\n";
		$prio = $value['Prioritaet'][0];
	}
	$output_PremiereLand .= '<option value="'.$value['_kp__ISO_Code'][0].'"'.$selector.'>'.$value['Land_'.$_SESSION['sprache']][0].'</option>'."\n";
}
?>