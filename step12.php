<?
header("P3P: CP=\"NON DSP CURa ADMa DEVa CUSa TAIa PSAa PSDa IVAa IVDa OUR NOR UNI COMNAV\"");
session_start();

if (!isset($_SESSION['Leg_1'])) {
	
	// redirect zur�ck zur Startseite wg. abgelaufener Session
	header("Location: /index.php?error=Session%20abgelaufen");
	exit;
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/classes/FX/FX.php"); 

$step = (isset($_POST['step_12'])) ? $_POST['step_12'] : "";
$still1 = (isset($_POST['still1'])) ? $_POST['still1'] : "";
$still2 = (isset($_POST['still2'])) ? $_POST['still2'] : "";
$still3 = (isset($_POST['still3'])) ? $_POST['still3'] : "";

$error = "";
if ($step == "2") {
	//if ((empty($_POST['Sprache'])) || (empty($_POST['Filmformat'])) || (empty($_POST['Bildformat'])) || (empty($_POST['Tonformat'])) ) $error .= $_SESSION['Leg_92'];
} 



if (($error == "") && ($step == "2")) {
		
	if (!empty($_FILES["still1"]['name'])) {
		$ext_tmp = explode(".",$_FILES['still1']['name']);
		$ext = $ext_tmp[count($ext_tmp)-1];
		$destination = $_SERVER['DOCUMENT_ROOT']."/bilder/".$_SESSION['film_id'].'a.'.$ext;
		// Bild kopieren
		if ($_FILES['still1']['size'] < 10000000) {
			
		if(move_uploaded_file($_FILES["still1"]["tmp_name"], $destination)) {
			/*
			$bild2do = true;
			$BildA = $film_id.'a.'.$ext;
			$command = "/usr/local/bin/convert ".$_SERVER['DOCUMENT_ROOT']."/bilder/".$film_id."a.".$ext." -thumbnail '100x100>' ".$_SERVER['DOCUMENT_ROOT']."/bild_uploads/".$film_id."a_mini.jpg";
			exec($command);
			$command = "/usr/local/bin/convert ".$_SERVER['DOCUMENT_ROOT']."/bilder/".$film_id."a.".$ext." -thumbnail '200x200>' ".$_SERVER['DOCUMENT_ROOT']."/bild_uploads/".$film_id."a_small.jpg";
			exec($command);
			$command = "/usr/local/bin/convert ".$_SERVER['DOCUMENT_ROOT']."/bilder/".$film_id."a.".$ext." -thumbnail '300x300>' ".$_SERVER['DOCUMENT_ROOT']."/bild_uploads/".$film_id."a_medium.jpg";
			exec($command);
			*/

		} else {
			$error.= "Bildupload Still 1 schlug fehl.<br>";
		}
			
		} else {
			$error.= "Bildupload Still 1 Dateigroessee groesser als 10 MB.<br>";
		}
	}
	if (!empty($_FILES["still2"]['name'])) {
		$ext_tmp = explode(".",$_FILES['still2']['name']);
		$ext = $ext_tmp[count($ext_tmp)-1];
		$destination = $_SERVER['DOCUMENT_ROOT']."/bilder/".$_SESSION['film_id'].'b.'.$ext;
		// Bild kopieren
		if ($_FILES['still2']['size'] < 10000000) {
			
		if(move_uploaded_file($_FILES["still2"]["tmp_name"], $destination)) {
			/*
			$bild2do = true;
			$BildB = $film_id.'b.'.$ext;
			$command = "/usr/local/bin/convert ".$_SERVER['DOCUMENT_ROOT']."/bilder/".$film_id."b.".$ext." -thumbnail '100x100>' ".$_SERVER['DOCUMENT_ROOT']."/bild_uploads/".$film_id."b_mini.jpg";
			exec($command);
			$command = "/usr/local/bin/convert ".$_SERVER['DOCUMENT_ROOT']."/bilder/".$film_id."b.".$ext." -thumbnail '200x200>' ".$_SERVER['DOCUMENT_ROOT']."/bild_uploads/".$film_id."b_small.jpg";
			exec($command);
			$command = "/usr/local/bin/convert ".$_SERVER['DOCUMENT_ROOT']."/bilder/".$film_id."b.".$ext." -thumbnail '300x300>' ".$_SERVER['DOCUMENT_ROOT']."/bild_uploads/".$film_id."b_medium.jpg";
			exec($command);
			*/
		} else {
			$error.= "Bildupload Still 2 schlug fehl.<br>";
		}
			
		} else {
			$error.= "Bildupload Still 2 Dateigroessee groesser als 1 MB.<br>";
		}
	}
	if (!empty($_FILES["still3"]['name'])) {
		$ext_tmp = explode(".",$_FILES['still3']['name']);
		$ext = $ext_tmp[count($ext_tmp)-1];
		$destination = $_SERVER['DOCUMENT_ROOT']."/bilder/".$_SESSION['film_id'].'c.'.$ext;
		// Bild kopieren
		if ($_FILES['still3']['size'] < 10000000) {
			
		if(move_uploaded_file($_FILES["still3"]["tmp_name"], $destination)) {
			/*
			$bild2do = true;
			$BildC = $film_id.'c.'.$ext;
			$command = "/usr/local/bin/convert ".$_SERVER['DOCUMENT_ROOT']."/bilder/".$film_id."c.".$ext." -thumbnail '100x100>' ".$_SERVER['DOCUMENT_ROOT']."/bild_uploads/".$film_id."c_mini.jpg";
			exec($command);
			$command = "/usr/local/bin/convert ".$_SERVER['DOCUMENT_ROOT']."/bilder/".$film_id."c.".$ext." -thumbnail '200x200>' ".$_SERVER['DOCUMENT_ROOT']."/bild_uploads/".$film_id."c_small.jpg";
			exec($command);
			$command = "/usr/local/bin/convert ".$_SERVER['DOCUMENT_ROOT']."/bilder/".$film_id."c.".$ext." -thumbnail '300x300>' ".$_SERVER['DOCUMENT_ROOT']."/bild_uploads/".$film_id."c_medium.jpg";
			exec($command);
			*/
		} else {
			$error.= "Bildupload Still 3 schlug fehl.<br>";
		}
			
		} else {
			$error.= "Bildupload Still 3 Dateigroessee groesser als 1 MB.<br>";
		}
	}	

	// redirect zur n�chsten Seite
	header("Location: /step13.php");
	exit;
	//echo 'done';
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Online Registration</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 <link href="/css/style.css" rel="stylesheet" type="text/css" title="KFT" />
 
<script type="text/javascript" src="includes/scripts.js"></script>
<script type="text/javascript" language="javascript">
<!--
	var ol_width = 300;
	var ol_fgcolor='#d9e2ec';
	var ol_bgcolor='#006699';
//-->
</script>
<link rel="stylesheet" type="text/css" href="includes/yui/container/assets/container.css">
<script type="text/javascript" src="includes/yui/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript" src="includes/yui/animation/animation-min.js"></script>
<script type="text/javascript" src="includes/yui/dragdrop/dragdrop-min.js"></script>
<script type="text/javascript" src="includes/yui/connection/connection.js"></script>
<script type="text/javascript" src="includes/yui/container/container.js"></script>
</head>
<body>


<div id="container">
  <div id="top">
    <? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_header.inc.php'); ?>
	<? if (($error != "") && ($step == "2")) { ?>	
	<p style="border:1px solid #990000; background-color:#FFDCD6; padding:5px; width: 450px"><?=$error; ?></p>
	<? } ?>
  </div>
	<br clear="all" />
  <div id="leftSide">
<fieldset>
<legend><?=$_SESSION['Leg_8']?> 12 / 13: <?=$_SESSION['Leg_132']?></legend>
	<? echo ($_SESSION['Leg_112'] != "") ? '<p class="pagenote">'.$_SESSION['Leg_112'].'</p>' : ""; ?>
<form action="<?=$_SERVER['PHP_SELF']?>" method="post" name="form1" enctype="multipart/form-data">
<input type="hidden" value="2" name="step_12">

	
	<label for="name"><?=$_SESSION['Leg_28']?></label>
	<div class="div_blankbox">
		<p><?=$_SESSION['Leg_29']?></p>
		<div style="float:left">
			<?=$_SESSION['Leg_28']?> 1: 
		</div>
		<div style="float:left">
			<input type="file" name="still1" id="still1" class="textbox" /><br />
		</div>
		<br clear="all" />
		<div style="float:left">
			<?=$_SESSION['Leg_28']?> 2: 
		</div>
		<div style="float:left">
			<input type="file" name="still2" id="still2" class="textbox" /><br />
		</div>
		<br clear="all" />
		<div style="float:left">
			<?=$_SESSION['Leg_28']?> 3: 
		</div>
		<div style="float:left">
			<input type="file" name="still3" id="still3" class="textbox" /><br />
		</div>
		<br clear="all" />
	</div>
	<br clear="all" />
	<div class="button_div">
	<input name="submit" type="submit" id="submit" value="<?=$_SESSION['Leg_32']?>">
	</div>
</form>
<div class="clear"></div>
</fieldset>

  </div>
  <div class="clear"></div>
</div>


<? include($_SERVER['DOCUMENT_ROOT'].'/includes/ly_footer.inc.php'); ?>
</body>
</html>
