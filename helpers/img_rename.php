<?php
function ifset($name,$default="") {
	return isset($_REQUEST[$name])? $_REQUEST[$name] : $default;
}

$old = ifset('old');
$new = ifset('new');
$typ = ifset('typ','people');
$pfad = "";
switch ($typ) {
case "a":
    $pfad = "a_";
    break;
case "b":
    $pfad = "b_";
    break;
case "c":
    $pfad = "c_";
    break;
default:
    $pfad = "people/";
    break;
}
if (empty($old) || empty($new)) {
	$error='no file given';
} else {

	$base = $_SERVER['DOCUMENT_ROOT']."/bilder/".$pfad;
	if (file_exists($base.'original/'.$old)) {
		rename($base.'original/'.$old, $base.'original/'.$new);
		rename($base.'small/'.$old, $base.'small/'.$new);
		rename($base.'medium/'.$old, $base.'medium/'.$new);
		rename($base.'large/'.$old, $base.'large/'.$new);
		$error='OK';
	} else {
		$error = 'file not found';
	}
}
?>
<?= $error ?>
