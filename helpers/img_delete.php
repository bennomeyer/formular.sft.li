<?php
function ifset($name,$default="") {
	return isset($_REQUEST[$name])? $_REQUEST[$name] : $default;
}

$id = ifset('id');
$typ = ifset('typ','people');
$pfad = "";
switch ($typ) {
case "a":
    $pfad = "a_";
    break;
case "b":
    $pfad = "b_";
    break;
case "c":
    $pfad = "c_";
    break;
default:
    $pfad = "people/";
    break;
}

if (empty($id) ) {
	$error='no file given';
} else {

	$base = $_SERVER['DOCUMENT_ROOT']."/bilder/".$pfad;
	if (file_exists($base.'original/'.$id)) {
		unlink($base.'original/'.$id);
		unlink($base.'small/'.$id);
		unlink($base.'medium/'.$id);
		unlink($base.'large/'.$id);
		$error='OK';
	} else {
		$error = 'file not found';
	}
}
?>
<?= $error ?>
