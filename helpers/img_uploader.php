<?
$id = (isset($_REQUEST['id'])) ? $_REQUEST['id'] : "";
$bildart = (isset($_REQUEST['bildart'])) ? $_REQUEST['bildart'] : "";
$step = (isset($_REQUEST['step'])) ? $_REQUEST['step'] : "";
$typ = (isset($_REQUEST['typ'])) ? $_REQUEST['typ'] : "";
$size = (isset($_REQUEST['size'])) ? $_REQUEST['size'] : "";
$pfad = "";
switch ($typ) {
case "a":
    $pfad = "a_";
    break;
case "b":
    $pfad = "b_";
    break;
case "c":
    $pfad = "c_";
    break;
case "people":
    $pfad = "people/";
    break;
}
$error = "";

if (!empty($_FILES["bild"]['name'])) {
	$ext_tmp = explode(".",$_FILES['bild']['name']);
	$ext = $ext_tmp[count($ext_tmp)-1];
	$destination = $_SERVER['DOCUMENT_ROOT']."/bilder/".$pfad."original/".$id.'.'.$ext;
	//$destination = $_SERVER['DOCUMENT_ROOT']."/bilder/".$id.$bildart.'.'.$ext;
	// Bild kopieren
	if ($_FILES['bild']['size'] < 10000000) {
		
	if(move_uploaded_file($_FILES["bild"]["tmp_name"], $destination)) {
	
		$command = "/usr/local/bin/convert ".$destination." -thumbnail '100x100>' ".$_SERVER['DOCUMENT_ROOT']."/bilder/".$pfad."small/".$id.".jpg";
		exec($command);
		$command = "/usr/local/bin/convert ".$destination." -thumbnail '300x300>' ".$_SERVER['DOCUMENT_ROOT']."/bilder/".$pfad."medium/".$id.".jpg";
		exec($command);
		$command = "/usr/local/bin/convert ".$destination." -thumbnail '600x600>' ".$_SERVER['DOCUMENT_ROOT']."/bilder/".$pfad."large/".$id.".jpg";
		exec($command);
		if ('jpg' != strtolower($ext)) {
			# nur JPG speichern
			$command = "/usr/local/bin/convert ".$destination." ".$_SERVER['DOCUMENT_ROOT']."/bilder/".$pfad."original/".$id.".jpg";
			exec($command);
			unlink($destination);
		}
		

	} else {
		$error.= "Bildupload schlug fehl.<br>";
	}
		
	} else {
		$error.= "Bildupload: Dateigroessee groesser als 10 MB.<br>";
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
body {
margin: 0px;
padding: 4px 0px 0px 0px;
}
p {
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Unbenanntes Dokument</title>
</head>

<body>
<? 
if ($id == "") {
?>
<span style="border:1px solid #990000; background-color:#FFDCD6; padding:5px; width: 250px">
<?=$error?>
</span>
<p style="border:1px solid #990000; background-color:#FFDCD6; padding:5px; width: 250px">Es wurde keine ID übergeben.</p>
<? } else { ?>
<div align="center">
<? if (($error != "") && ($step == "2")) { ?>
<p style="border:1px solid #990000; background-color:#FFDCD6; padding:5px; width: 250px">&nbsp;</p>
<? } ?>

  <? if (file_exists($_SERVER['DOCUMENT_ROOT']."/bilder/".$pfad."medium/".$id.".jpg")) { 
  		
		$orig_extension = "";
  		if (file_exists($_SERVER['DOCUMENT_ROOT']."/bilder/".$pfad."original/".$id.'.tif')) $orig_extension = "tif";
  		if (file_exists($_SERVER['DOCUMENT_ROOT']."/bilder/".$pfad."original/".$id.'.tiff')) $orig_extension = "tiff";
  		if (file_exists($_SERVER['DOCUMENT_ROOT']."/bilder/".$pfad."original/".$id.'.png')) $orig_extension = "png";
  		if (file_exists($_SERVER['DOCUMENT_ROOT']."/bilder/".$pfad."original/".$id.'.jpg')) $orig_extension = "jpg";
  		?>
  	
  		<a href="/bilder/<?=$pfad?>original/<?=$id?>.<?=$orig_extension?>" target="_blank"><img src="/bilder/<?=$pfad?>medium/<?=$id?>.jpg?random=<?=time()?>" border="0" /></a>
	<? } ?>
	<div style="background-color:#EEEEEE; padding: 4px 0px 2px 0px; margin: 4px 0px 0px 0px;">
  <form action="<?=$_SERVER['PHP_SELF']?>" method="post" enctype="multipart/form-data" name="form1" id="form1" style="margin-bottom:0px;">
  <input type="hidden" value="2" name="step" />
  <input type="hidden" value="<?=$id?>" name="id" />
  <input type="hidden" value="<?=$bildart?>" name="bildart" />
  <input type="hidden" value="<?=$typ?>" name="typ" />
    <input type="file" name="bild" style="font-family:Verdana, Arial, Helvetica, sans-serif;font-size: 11px;border:1px solid #999999; width:150px;" />
      <br />
      <input type="submit" name="Submit" value="hochladen" style="font-family:Verdana, Arial, Helvetica, sans-serif;font-size: 11px;border:1px solid #009900; background-color:#D1EED0; width:150px;" />
  </form></div>
</div>  
<? } ?>

</body>
</html>
